#!/Users/clidman/Science/Programs/anaconda3/envs/iraf27/bin/python
# Python script to process HAWK-I data

import HAWKI_library as HAWKI

def reduce(options):
    params=HAWKI.buildDictionary(options.config)
    
    if options.reduceDarks:
        HAWKI.reduceDarks(options,params)
        
    if options.reduceFlats:
        HAWKI.reduceFlats(options,params)
        
    if options.badPixelMasks:
        HAWKI.badPixelMasks(options,params)
        
    if options.detrend:
        HAWKI.detrend(options,params)
        
    if options.skySubtract:
        HAWKI.skySubtract(options,params)

    if options.merge:
        HAWKI.merge(options,params)

    if options.extract:
        HAWKI.extract(options,params)

    if options.swarp:
        HAWKI.swarp(options,params)

    if options.combine:
        HAWKI.combine(options,params)

    if options.mergeResults:
        HAWKI.mergeResults(options,params)
        
    if options.zeroPoints:
        HAWKI.ZP(options,params)
        
    return

from optparse import OptionParser

if __name__ == '__main__':

    parser=OptionParser()

    parser.add_option('-c','--config',dest='config',
                      default=None,help='Configuration file')

    parser.add_option('-l','--list',dest='list',
                      default=None,help='Not used yet')

    parser.add_option('-D','--reduceDarks',dest='reduceDarks',
                      default=False, action="store_true",
                      help='Find and reduce dark frames')

    parser.add_option('-F','--reduceFlats',dest='reduceFlats',
                      default=False, action="store_true",
                      help='Find and reduce twilight flat field frames')

    parser.add_option('-B','--badPixelMasks',dest='badPixelMasks',
                      default=False, action="store_true",
                      help='Create bad pixel masks')

    parser.add_option('-d','--detrend',dest='detrend',
                      default=False, action="store_true",
                      help='Dark subtract and flat field')
    
    parser.add_option('-S','--skySubtract',dest='skySubtract',
                      default=False, action="store_true",
                      help='subtrsact the sky')

    parser.add_option('-o','--OB',dest='OB',
                      default=None,help='process this OB')

    parser.add_option('-m','--merge',dest='merge',
                      default=False, action="store_true",
                      help='create masks, apply chip based scaling, and rebuild')

    parser.add_option('-e','--extract',dest='extract',
                      default=False, action="store_true",
                      help='Use SExtractor to produce the catalogue used by SWARP')

    parser.add_option('-s','--swarp',dest='swarp',
                      default=False, action="store_true",
                      help='Run swarp')

    parser.add_option('-C','--combine',dest='combine',
                      default=False, action="store_true",
                      help='combine')
    
    parser.add_option('-M','--mergeResults',dest='mergeResults',
                      default=False, action="store_true",
                      help='Merge the image and exposure map')

    parser.add_option('-z','--zeroPoints',dest='zeroPoints',
                      default=False, action="store_true",
                      help='Compute zeropoints')

    parser.add_option('-i','--imagedepth',dest='imagedepth',
                      default=False, action="store_true",
                      help='Measure the depth')

    (options,args)=parser.parse_args()

    reduce(options)
    
