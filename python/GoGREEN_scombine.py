import Constants
from optparse import OptionParser
import GoGREEN_utils as GoGREEN
import os
import glob
from astropy.io import fits,ascii
import numpy as np
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import sys
import getopt
import pyfits
import scipy
from pylab import savefig
import matplotlib.ticker as plticker
from scipy.interpolate import griddata
import scipy.interpolate
import scipy.ndimage
parser=OptionParser()
parser.add_option("-p", "--prefix", dest="prefix",default='cal_tell_oned_rfcs_')
parser.add_option("-o", "--output", dest="output",default='test.fits')
(options,args) = parser.parse_args()
prefix=options.prefix
altprefix=prefix.replace('rf','')
output=options.output
try:
    cname=str(args[0])
except:
    cname = raw_input('Cluster name? e.g. SXDF64')
try:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mdat = fits.getdata(masterfile)
except:
    print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    print "Stopping"
    stop()
Nmaskmax=6
maxwavelength=10200
constants=Constants.GoGREENPath()
output=constants.reduced+"Finalspec/v0.3/"+output
mdat_row=mdat[np.where(mdat['Cluster']==cname)]
masks=np.array([])
files=np.array([])
for masknum in np.arange(Nmaskmax):
    maskname=mdat_row['mask'+str(masknum+1)+'cut'][0]
    maskobs=mdat_row['mask'+str(masknum+1)+'observed'][0]
    if maskname is not '' and maskobs == 1: 
        reduced=1
        mdir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"
        filename=mdir+prefix+maskname+'.fits'
        print filename
        if not os.path.isfile(filename):
            filename=mdir+prefix+maskname+'.fits.gz'
            if not os.path.isfile(filename):
                filename=mdir+altprefix+maskname+'.fits'
                if not os.path.isfile(filename):
                    filename=mdir+altprefix+maskname+'.fits.gz'
                    if not os.path.isfile(filename):
                        reduced=0
        if reduced==1:
            files=np.append(files,filename)
            masks=np.append(masks,maskname)
if np.size(files) == 0:
    raise SystemExit('No observed masks')
print str(np.size(files))+" observed masks"
print masks
photfile=constants.reduced+cname+"/Catalogues/Masks/"+mdat_row['maskPhotCat'][0]
if os.path.isfile(photfile):
    photdat=fits.getdata(photfile)
    photfile_exists=True
else:
    photfile_exists=False
ids=np.array([])
priority=np.array([])
ra=np.array([])
dec=np.array([])
magmask=np.array([])
mag=np.array([])
xccd=np.array([])
yccd=np.array([])
onedspecsum={}
weightsum={}
dqsum={}
baddata={}
harray={}
mcnt={}
etime={}
firstmask=True
for filename,mask in zip(files,masks):
    mdir=constants.reduced+cname+"/Spectroscopy/"+mask+"/"
    twodhdu=fits.open(filename.replace('oned','twod').replace('cal_tell_',''))
# Better: can get Nfiles from Ncombine in 2D spectrum
#    Nfiles=np.max([np.size(glob.glob(mdir+'rfhtgs*sky*')),np.size(glob.glob(mdir+'htgs*sky*'))])
    Nfiles=twodhdu['SCI',1].header['Ncombine']
    hdu=fits.open(filename)
    specdimension=np.shape(np.shape(hdu['SCI',1]))[0]
    regrid=False
    if specdimension==2:
        if firstmask:
            (xsize,ysize)=np.shape(hdu['SCI',1])
            regrid=False
        else:
            (xdim,ydim)=np.shape(hdu['SCI',1])
            if ydim != ysize or xdim != xsize:
                regrid=True
            else:
                regrid=False
    header_primary=hdu[0].header
    if ((header_primary['NODAYOFF']<0) | (header_primary['DETECTOR']=='GMOS + e2v DD CCD42-90')):
        REVERSE_NODAB=-1
    else:
        REVERSE_NODAB=+1
    mdf=hdu['mdf'].data
    maskidlist=np.array(map(int,mdf['ID']))
    maskp=np.array(map(int,mdf['priority']))
    ids=np.append(ids,maskidlist)
    priority=np.append(priority,maskp)
    ra=np.append(ra,np.array(map(float,mdf['RA'])))
    dec=np.append(dec,np.array(map(float,mdf['DEC'])))
    magmask=np.append(magmask,np.array(map(float,mdf['MAG'])))
    refentry=np.where(np.array(map(int,mdf['priority']))>0)[0][0]
    yim=np.array(map(float,mdf['y_ccd']))
    ys1=np.array(map(float,mdf['secy1']))
    ys2=np.array(map(float,mdf['secy2']))
    yspec=0.5*(ys1+ys2)
    specoverimbin=yim[refentry]/yspec[refentry]
    rescale= int(specoverimbin+0.5)
    xccd=np.append(xccd,np.array(map(float,mdf['x_ccd']/rescale)))
    yccd=np.append(yccd,np.array(map(float,mdf['y_ccd']/rescale)))
    for id in maskidlist[np.where((maskp==1)| (maskp==3))]:
        ext=hdu['mdf'].data['EXTVER'][np.where(hdu['mdf'].data['ID']==id)][0]
        if np.size(ext)>1: ext=ext[0]
        header=hdu['sci',ext].header
        data=hdu['sci',ext].data
        var=hdu['var',ext].data
        dq=hdu['dq',ext].data
        if regrid:
            #print 'Need to regrid data'
            #x=np.arange(0,xdim)
            #y=np.arange(0,ydim)
            #print 'Current dimension: ',np.size(x),np.size(y)
            #newx=np.arange(0,xdim,np.float(xdim)/xsize)
            #newy=np.arange(0,ydim,np.float(ydim)/ysize)
            #print 'Desired dimension: ',np.size(newx),np.size(newy)
            #Xm,Ym=np.meshgrid(x,y)
            #Xmi,Ymi=np.meshgrid(newx,newy)
            # method='linear' or 'cubic' not working for some strange reason
            # This is not really what we want anyway as it does not conserve flux
            #data=griddata((Xm.flatten(),Ym.flatten()),data.flatten(),(Xmi,Ymi),method='nearest').reshape(xsize,ysize)
            #var=griddata((Xm.flatten(),Ym.flatten()),var.flatten(),(Xmi,Ymi),method='nearest').reshape(xsize,ysize)
            #dq=griddata((Xm.flatten(),Ym.flatten()),dq.flatten(),(Xmi,Ymi),method='nearest').reshape(xsize,ysize).astype(int)
            #print np.shape(data)
            data=GoGREEN.congrid(data,(xsize,ysize),method='linear',minusone=True)
            var=GoGREEN.congrid(var,(xsize,ysize),method='linear',minusone=True)
            dq=GoGREEN.congrid(dq,(xsize,ysize),method='nearest',minusone=True)
        if specdimension<2:
            baddata=(np.isnan(var)) | (np.isnan (data)) | (dq<1) | (var==0) 
        else:
            baddata=(np.isnan(var)) | (np.isnan (data)) | (dq==1) | (var==0)
            data=REVERSE_NODAB*data
        datamask=dq*0
        datamask[np.where(baddata)]=1
        data_masked=np.ma.masked_array(data,datamask)
        var_masked=np.ma.masked_array(var,datamask)
        dq_masked=np.ma.masked_array(dq,datamask)
        if id in onedspecsum.keys():
            #print id
            #print onedspecsum[id][2,1000:1010]
            onedspecsum[id]=np.ma.sum((onedspecsum[id],np.ma.divide(data_masked,var_masked)),axis=0)
            #print onedspecsum[id][2,1000:1010]
            weightsum[id]=np.ma.sum((weightsum[id],np.ma.divide(1.,var_masked)),axis=0)
            dqsum[id]=np.ma.sum((dqsum[id],dq_masked),axis=0)
            mcnt[id]=mcnt[id]+1
            etime[id]=etime[id]+Nfiles*header_primary['EXPOSURE']*2.
        else:
            onedspecsum[id]=np.ma.divide(data_masked,var_masked)
            weightsum[id]=np.ma.divide(1.,var_masked)
            dqsum[id]=dq_masked
            harray[id]=header
            mcnt[id]=1
            etime[id]=Nfiles*header_primary['EXPOSURE']*2.
        harray[id]['ORIG'+str(mcnt[id])]=mask+' '+str(ext)
    hdu.close()
    firstmask=False
select=np.where((priority==1) | (priority==3))
idlist,indices=np.unique(ids[select],return_index=True)
idlist=map(int,idlist)
ralist=ra[select][indices]
declist=dec[select][indices]
maglist=magmask[select][indices]
plist=priority[select][indices]
xccd=xccd[select][indices]
yccd=yccd[select][indices]
nullarray=idlist*0
header_primary['NSCIEXT']=len(idlist)
header_primary['NEXTEND']=3*len(idlist)
if specdimension <2:
    tbhdu=fits.BinTableHDU.from_columns(
        [fits.Column(name='ID', format='1J', unit='#',disp='I11',array=idlist),
         fits.Column(name='EXTVER', format='1J', unit='#',disp='I3',array=np.arange(ext)+1),
         fits.Column(name='RA', format='1E', unit='H',disp='G15.7',array=ralist),
         fits.Column(name='DEC', format='1E', unit='deg',disp='G15.7',array=declist),
         fits.Column(name='MAGMASK', format='1E', unit='#',disp='G15.7',array=maglist),
         fits.Column(name='MAG', format='1E', unit='#',disp='G15.7',array=maglist),
         fits.Column(name='priority', format='1J', unit='#',disp='I1',array=plist),
         fits.Column(name='SNR1', format='1E', unit='#',disp='G10.2',array=plist),
         fits.Column(name='SNR2', format='1E', unit='#',disp='G10.2',array=plist),
         fits.Column(name='Nmask', format='1J', unit='#',disp='I1',array=np.array([])),
         fits.Column(name='Etime', format='1E', unit='s',disp='G15.7',array=np.array([])),
         fits.Column(name='Mask1', format='A20', unit='c',disp='A20',array=nullarray),
         fits.Column(name='EXTVER1', format='1J', unit='#',disp='I2',array=nullarray),
         fits.Column(name='Mask2', format='A20', unit='c',disp='A20',array=nullarray),
         fits.Column(name='EXTVER2', format='1J', unit='#',disp='I2',array=nullarray),
         fits.Column(name='Mask3', format='A20', unit='c',disp='A20',array=nullarray),
         fits.Column(name='EXTVER3', format='1J', unit='#',disp='I2',array=nullarray),
         fits.Column(name='Mask4', format='A20', unit='c',disp='A20',array=nullarray),
         fits.Column(name='EXTVER4', format='1J', unit='#',disp='I2',array=nullarray),
         fits.Column(name='Mask5', format='A20', unit='c',disp='A20',array=nullarray),
         fits.Column(name='EXTVER5', format='1J', unit='#',disp='I2',array=nullarray),
         fits.Column(name='XCCD', format='1E', unit='#',disp='G15.7',array=xccd),
         fits.Column(name='YCCD', format='1E', unit='#',disp='G15.7',array=yccd)]
        )
    header_primary['NEXTEND']=header_primary['NEXTEND']+1
hduloutput=fits.HDUList()
hduloutput.append(fits.PrimaryHDU(header=header_primary))
cnt=0
for id in idlist:
    #print 'Writing ID=',id
    cnt=cnt+1
    harray[id]['EXTVER']=cnt
    harray[id]['MDFROW']=cnt
    harray[id]['OBJID']=id
    signal=np.ma.divide(onedspecsum[id],weightsum[id]).data
    variance=np.ma.divide(1.,weightsum[id]).data
    crval1=harray[id]['CRVAL1']
    crpix1=harray[id]['CRPIX1']
    cd1_1=harray[id]['CD1_1']
    maxpix=int((maxwavelength-crval1)/cd1_1+crpix1)
    if specdimension<2:
        tbhdu.data['Nmask'][np.where(tbhdu.data['ID']==id)]=mcnt[id]
        tbhdu.data['Etime'][np.where(tbhdu.data['ID']==id)]=etime[id]
        tbhdu.data['EXTVER'][np.where(tbhdu.data['ID']==id)]=cnt
        if photfile_exists:
            try:
                tbhdu.data['MAG'][np.where(tbhdu.data['ID']==id)]=photdat['ZMAG'][np.where(photdat['ID']==id)][0]
            except:
                # IDs in COSMOS-125 are wrong.  Need to fix this.
                # Done.  Fixed with fixIDs.py.
                print 'No mag for ',id,'in ',photfile
                tbhdu.data['MAG'][np.where(tbhdu.data['ID']==id)]=-99
            for j in np.arange(Nmaskmax)+1:   
                try:
                    tbhdu.data['Mask'+str(j)][np.where(tbhdu.data['ID']==id)]=harray[id]['ORIG'+str(j)].split()[0]
                    tbhdu.data['EXTVER'+str(j)][np.where(tbhdu.data['ID']==id)]=harray[id]['ORIG'+str(j)].split()[1]
                except:
                    pass
        snr_central=8500
        dwave=500
        wave=np.array([snr_central-dwave,snr_central+dwave])
        pix=map(int,(wave-crval1)/cd1_1+crpix1)
        snr1=np.average(signal[pix[0]:pix[1]]/np.sqrt(variance[pix[0]:pix[1]]))
        snr2=np.average(signal[pix[0]:pix[1]])/np.std(signal[pix[0]:pix[1]])
        tbhdu.data['SNR1'][np.where(tbhdu.data['ID']==id)]=snr1
        tbhdu.data['SNR2'][np.where(tbhdu.data['ID']==id)]=snr2
        hduloutput.append(fits.ImageHDU(data=signal[1:maxpix],header=harray[id],name='SCI'))
        hduloutput.append(fits.ImageHDU(data=variance[1:maxpix],header=harray[id],name='VAR'))
        hduloutput.append(fits.ImageHDU(data=dqsum[id].data[1:maxpix],header=harray[id],name='DQ'))
    else:
        hduloutput.append(fits.ImageHDU(data=signal[:,1:maxpix],header=harray[id],name='SCI'))
        hduloutput.append(fits.ImageHDU(data=variance[:,1:maxpix],header=harray[id],name='VAR'))
        hduloutput.append(fits.ImageHDU(data=dqsum[id].data[:,1:maxpix],header=harray[id],name='DQ'))

if specdimension<2:
    hduloutput.append(fits.TableHDU(data=tbhdu.data,name='MDF'))
    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    good=tbhdu.data['priority']==1
    ax.scatter(tbhdu.data['MAG'][good],tbhdu.data['SNR1'][good],s=100)
    good=tbhdu.data['priority']!=1
    ax.scatter(tbhdu.data['MAG'][good],tbhdu.data['SNR1'][good],s=100,edgecolors='r',facecolors='none')
    ax.axis([21.5,24.5,0.1,50])
    ax.set_yscale('log')
    ax.get_yaxis().set_major_formatter(plticker.ScalarFormatter())
    plt.text(23.5,30, cname)
    plt.xlabel("Z Magnitude")
    plt.ylabel("SNR per 3.9A pixel")
    savefig(output.replace('.fits','.png'))
hduloutput.writeto(output,clobber=True)

