from astropy.io import fits,ascii
import numpy as np
import os
import scipy
def getprof(x,offset,w,dx,dw,nodpix,width_pix,trim=0):
    arrProfs = np.zeros( (len(dx), len(dw),len(x)) )
    for j,off in enumerate(dx):
        for k,woff in enumerate(dw):
### ZZZ MLB: set ABprof to -ABprof since GMOS-N neg/pos seem to be reversed???
            res = ABprof(offset=offset+off,nodDis=nodpix,npix = len(x),width=width_pix+woff)
            # -- truncate outer edges of gaussians to prevent spurious peaks from xcor with slit edge junk
            res[0:trim]=0 ; res[-trim::]= 0 
            # --
            arrProfs[j,k,:] = res
            #print offset+off,width_pix+woff,np.nansum(x*res)
    #xcorval = np.sum(x*arrProfs/v,axis=2)
    xcorval = np.nansum(x*arrProfs,axis=2)
    idx = np.argmax(xcorval)
    (pk,pw)=np.unravel_index(idx,xcorval.shape)
    #amp=np.average(abs(x),weights=1./v)/np.average(abs(arrProfs[pk][pw]),weights=1./v)
    si=np.sign(arrProfs[pk][pw])
    amp=np.average(x[trim:-trim]*si[trim:-trim])/np.average(abs(arrProfs[pk][pw][trim:-trim]))
    return (pk,pw,amp*arrProfs[pk][pw])
def ABprof(npix=20,nodDis=9.0625,offset=5.5,width=4.,amp=12.):
    # create A+B profile from two gaussians
    x = np.arange(npix)
    gA = gaussian(x,amp,offset,width,0.)
    gB = gaussian(x,-amp,offset+nodDis,width,0.)
    return gA+gB
def gaussian(x, a, mean, sigma, y0):
    return (a*np.exp(-(x-mean)*(x-mean)/sigma/sigma) + y0)
def calc_residual_ratio(residual,sky,rp1=2,rp2=4,sp1=16,sp2=18):
    line_strength=np.mean(sky[sp1:sp2,:],axis=0)
    residual_strength=np.mean(residual[rp1:rp2,:],axis=0)-np.mean(residual[-rp2:rp1,:],axis=0)
    ratio=np.mean(residual[rp1:rp2,:],axis=0)/line_strength
    return line_strength,ratio
def getxpos(cluster,mask,datafile,slit,refwave):
    # Read data header to get DETSEC and WAVTRAN keywords
    directory=os.getenv('GoGREEN_data')+'Data/reduced/'+cluster+'/Spectroscopy/'+mask+'/'
    hdu=fits.open(directory+datafile)
    pixel=getxposfromhdu(hdu,slit,refwave,directory)
    hdu.close()
    return pixel
def getxposfromhdu(hdu,slit,refwave,directory):
    header=hdu['sci',slit].header
    detsec=header['DETSEC']
    wavtran=header['WAVTRAN']
    detbin=header['CCDSUM']
    xbin=float(detbin.split()[0])
    ybin=float(detbin.split()[1])
    # Parse DETSEC to get x1 and x2.
    [x1,x2,y1,y2]=map(int,detsec.replace('[','').replace(']','').replace(',',':').split(':'))
    # Read file given by WAVTRAN to get Chebyshev coefficients
    try:
        xmin,xmax,ymin,ymax,cheb_coeff=read_fitcoords(directory+'database/fc'+wavtran)
        # Solve: refwave=np.polynomial.chebyshev.chebval2d(x0,0,cheb_coeff) for x
        x0=scipy.optimize.brentq(func_wavetox,-1.,1.,args=(cheb_coeff,refwave))
        pixel=(x0*(xmax-xmin)+(xmax+xmin))/2.+x1/xbin
    except:
        pixel=np.nan
    return pixel
def func_wavetox(x,coeff,refwave):
    ans=refwave-np.polynomial.chebyshev.chebval2d(x,0,coeff)
    return ans
def read_fitcoords(fcfile):
    f=open(fcfile,'r')
    cnt=1
    coeffs=np.array([])
    for line in f:
        line=line.strip()
        if line.split()[0]=="#":
            cnt=1
            coeffs=np.array([])
        if cnt==7:
            if float(line) != 1:
                print 'Not chebyshev! Broken.'
                stop()
        if cnt==8:
            xorder=float(line)
        if cnt==9:
            yorder=float(line)
        if cnt==11:
            xmin=float(line)
        if cnt==12:
            xmax=float(line)
        if cnt==13:
            ymin=float(line)
        if cnt==14:
            ymax=float(line)
        if cnt>14:
            coeffs=np.append(coeffs,float(line))
        cnt=cnt+1
    coeffs=coeffs.reshape(xorder,yorder).transpose()
    return xmin,xmax,ymin,ymax,coeffs

def remove_continuum_withfit(data,hd,hdp):
    l=np.array([9000,9300,9500,9600])
    crval1=hd['CRVAL1']
    crpix1=hd['CRPIX1']
    cd1_1=hd['CD1_1']
    p=((l-crval1)/cd1_1+crpix1).astype(int)
    profile1=np.median(data[:,p[0]:p[1]],axis=1)
    profile2=np.median(data[:,p[2]:p[3]],axis=1)
    # Try fitting:
    npix=profile1.shape[0]
    slit_centre=((npix-1)/2.)
    pixscale=hdp['PIXSCALE']
    width_arcsec=0.7
    width_pix=width_arcsec/pixscale
    nodApix=hdp['NODAYOFF']*2/pixscale
    offset=slit_centre-nodApix/2.0
    dxmax=2
    dxstep=0.1
    dwmax=0.2
    dwstep=0.025
    slittrim=2
    dx = np.arange(-dxmax,dxmax,dxstep)
    dw = np.arange(-dwmax,dwmax,dwstep)/pixscale # in pixels
    (pk1,pw1,useProf1)=getprof(profile1,offset,width_pix,dx,dw,nodApix,width_pix,slittrim)
    (pk2,pw2,useProf2)=getprof(profile2,offset,width_pix,dx,dw,nodApix,width_pix,slittrim)
    xvec=np.array([0.5*(p[0]+p[1]),0.5*(p[2]+p[3])])
    useProf=np.vstack((useProf1,useProf2))
    deg=1
    #poly=np.reshape(np.polyfit(xvec,useProf,deg),np.append((deg+1),np.shape(useProf1)))
    poly=np.polyfit(xvec,useProf,deg)
    # OK there has to be a better way to do this but I don't know what it is at the moment.
    for i in np.arange(p[3],np.shape(data)[1]):
        data[:,i]=data[:,i]-np.polyval(poly,i)
    
    # OK let's try just subtracting this, no wavelength dependent scaling
    #continuum=np.tile(profile1.transpose(),(np.shape(data)[1],1)).transpose()
    #continuum=np.tile(useProf.transpose(),(np.shape(data)[1],1)).transpose()
    #fig=plt.figure()
    #plt.plot(profile1,c='r')
    #plt.plot(useProf,c='b')
    #fig.show()
    #stop=raw_input('Wait, wait, wait!')
    #data=data-continuum
    return data
def remove_continuum(hdudata,hducontinuum,scalewave=np.array([9000,9600])):    
    hd=hdudata[1].header
    crval1=hd['CRVAL1']
    crpix1=hd['CRPIX1']
    cd1_1=hd['CD1_1']
    scalepix=((scalewave-crval1)/cd1_1+crpix1).astype(int)
    imid=int(hdudata['sci'].data.shape[0]/2.)
    imidc_p=int(imid/2)
    imidc_n=int((hdudata['sci'].data.shape[0]-imid)/2)+imid
    continuum_in_residual=hdudata['sci'].data[imidc_p-1:imidc_p+1,scalepix[0]:scalepix[1]]-hdudata['sci'].data[imidc_n-1:imidc_n+1,scalepix[0]:scalepix[1]]
    continuum_in_continuum=hducontinuum[1].data[imidc_p-1:imidc_p+1:imid,scalepix[0]:scalepix[1]]-hducontinuum[1].data[imidc_n-1:imidc_n+1:imid,scalepix[0]:scalepix[1]]
    scale_ratio=np.average(continuum_in_residual)/np.average(continuum_in_continuum)
    data_continuum_removed=hdudata['sci'].data-scale_ratio*hducontinuum[1].data
    hdul=fits.HDUList()
    hdul.append(fits.PrimaryHDU(header=hdudata[0].header))
    hdul.append(fits.ImageHDU(data=data_continuum_removed,header=hdudata['SCI'].header,name='SCI'))
    hdul.append(fits.ImageHDU(data=hdudata['SKY'].data,header=hdudata['SKY'].header,name='SKY'))
    return hdul
def oldremove_continuum(hdu):
    data=np.copy(hdu[1].data)
    hd=hdu[1].header
    hdp=hdu[0].header
    data=remove_continuum_arr(data,hd,hdp)
    return data
def oldremove_continuum_arr(data,hd,hdp):
    l=np.array([9000,9300,9500,9600])
    crval1=hd['CRVAL1']
    crpix1=hd['CRPIX1']
    cd1_1=hd['CD1_1']
    p=((l-crval1)/cd1_1+crpix1).astype(int)
    profile1=np.median(data[:,p[0]:p[1]],axis=1)
    profile2=np.median(data[:,p[2]:p[3]],axis=1)
    # Try fitting:
    npix=profile1.shape[0]
    slit_centre=((npix-1)/2.)
    pixscale=hdp['PIXSCALE']
    width_arcsec=0.7
    width_pix=width_arcsec/pixscale
    nodApix=hdp['NODAYOFF']*2/pixscale
    offset=slit_centre-nodApix/2.0
    dxmax=2
    dxstep=0.1
    dwmax=0.2
    dwstep=0.025
    slittrim=2
    dx = np.arange(-dxmax,dxmax,dxstep)
    dw = np.arange(-dwmax,dwmax,dwstep)/pixscale # in pixels
    (pk1,pw1,useProf1)=getprof(profile1,offset,width_pix,dx,dw,nodApix,width_pix,slittrim)
    (pk2,pw2,useProf2)=getprof(profile2,offset,width_pix,dx,dw,nodApix,width_pix,slittrim)
    xvec=np.array([0.5*(p[0]+p[1]),0.5*(p[2]+p[3])])
    useProf=np.vstack((useProf1,useProf2))
    deg=1
    #poly=np.reshape(np.polyfit(xvec,useProf,deg),np.append((deg+1),np.shape(useProf1)))
    poly=np.polyfit(xvec,useProf,deg)
    # OK there has to be a better way to do this but I don't know what it is at the moment.
    for i in np.arange(p[3],np.shape(data)[1]):
        data[:,i]=data[:,i]-np.polyval(poly,i)
    
    # OK let's try just subtracting this, no wavelength dependent scaling
    #continuum=np.tile(profile1.transpose(),(np.shape(data)[1],1)).transpose()
    #continuum=np.tile(useProf.transpose(),(np.shape(data)[1],1)).transpose()
    #fig=plt.figure()
    #plt.plot(profile1,c='r')
    #plt.plot(useProf,c='b')
    #fig.show()
    #stop=raw_input('Wait, wait, wait!')
    #data=data-continuum
    return data
