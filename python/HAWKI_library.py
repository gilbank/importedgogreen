import os
from pyraf import iraf
import subprocess as sp
import sys
import astropy.io.fits as fits
from astropy.table import Table
import numpy

trimSection=['[5:2044,5:2044]','[5:2044,5:2044]','[5:2044,5:2044]','[5:2044,5:2044]']
statSection='[200:1800,200:1800]'
chipList=[1,2,4,3]

def removeFile(file):
    if os.path.isfile(file):
        iraf.imdel(images=file,verify='no')
    return

def setup(params,options):
    # Copy accross the setup files for SExtraxtor
    cmd='cp %s/* %s/%s/.' % (params['setUp'],params['reduced'],options.OB)
    try:
        retcode=sp.call(cmd,shell=True)
    except:
        pass

    return

def buildDictionary(file):
    file=open(file,'r')
    lines=file.readlines()
    file.close()
    parameters=[]

    for line in lines:
        if line[0]!='#':
            parameters.append(line.split())

    return dict(parameters)

def mkdir(dir):
    try:
        os.mkdir(dir)
    except:
        pass
    return

def  unlearn(tasks):
    for task in tasks:
        iraf.unlearn(task)
    return

def reduceDarks(options,params):
    # Unlearn IRAF tasks
    unlearn(['imcom','imcopy','imdel'])

    # Make a directory in which we process the data
    darkDir=params['reduced']+'darks'
    mkdir(darkDir)

    # Move to that directory
    os.chdir(darkDir)

    # Search for dark frames in the raw data directory

    for date in os.listdir(params['raw']):
        # Record the results in lists
        filename=[]
        dit=[]
        ndit=[]
        for fitsfile in os.listdir(params['raw']+date):
            f=params['raw']+date+'/'+fitsfile
            header=fits.getheader(f)
            if header['HIERARCH ESO DPR TYPE']=='DARK':
                filename.append(f)
                dit.append(header['HIERARCH ESO DET DIT'])
                ndit.append(header['HIERARCH ESO DET NDIT'])
        

        # Convert the lists into a Table
        darks=Table([filename,dit,ndit],names=('filename','dit','ndit'))

        # Look for unique combinations of DIT and NDIT
        unique=numpy.unique(darks['dit','ndit'])

        for dit,ndit in unique:
            selection=(darks['dit']==dit) & (darks['ndit']==ndit)
            for chip in range(0,4):
                print 'Creating dark frame for DIT=%-7.3f, NDIT=%d, and chip %d' % (dit,ndit,chip+1)
                # Create a list for the IRAF task imcombine
                fileList=[]
                for filename in darks[selection]['filename']:
                    fileList.append('%s[%d]' % (filename,chip+1))
                fileList=','.join(fileList)

                output='dark_%s_%dx%f_CHIP%d' % (date,ndit,dit,chipList[chip])
                iraf.imdel(output,verify='no')
                
                iraf.imcom(input=fileList,output=output,combine='average',reject='minmax',scale='none',zero='median',statsec='[200:1800,200:1800]',nlow='1',nhigh='1')

                # Trim
                iraf.imcopy(input=output+trimSection[chip],output=params['calib']+output)

    # Move back to the reduced directory
    os.chdir(params['reduced'])

    return


def reduceFlats(options,params):
    # Unlearn IRAF tasks
    unlearn(['imcom','imcopy','imdel','imstat','imarith'])

    # There needs to be at least 25 frames per group. Otherwise return

    # Some parameters (these could go in the parameter file)
    saturation = 18000
    minimumNumber = 25
    
    # Make a directory in which we process the data
    flatDir=params['reduced']+'flats'
    mkdir(flatDir)

    # Move to that directory
    os.chdir(flatDir)

    # Search for flat frames in the raw data directory

    for date in os.listdir(params['raw']):
        # Record the results in lists
        filename=[]
        filt=[]
        for fitsfile in os.listdir(params['raw']+date):
            f=params['raw']+date+'/'+fitsfile
            header=fits.getheader(f)
            if header['HIERARCH ESO DPR TYPE']=='FLAT' and header['HIERARCH ESO TPL ID']=='HAWKI_img_cal_SkyFlats':
                filename.append(f)
                filt.append(header['HIERARCH ESO INS FILT1 NAME'])

    
        # Convert the lists into a Table
        flats=Table([filename,filt],names=('filename','filter'))

        # Look for unique filters
        unique=numpy.unique(flats['filter'])

        for filt in unique:
            selection=(flats['filter']==filt)
            filenames=flats[selection]['filename']
            # We examine the counts in the first extention
            stats=[]
            for file in filenames:
                stats.append(float(iraf.imstat(images=file+'[1]',fields='midpt',format='no',Stdout=1)[0]))

            # Cull saturated files

            filenames2=[]
            stats2=[]
            for i,stat in enumerate(stats):
                if stat < saturation:
                    filenames2.append(filenames[i])
                    stats2.append(stat)

            print 'Statistics'
            print stats2

            if len(filenames2) < minimumNumber:
                print 'Not enough frames to create a flat'
                return

            for chip in range(0,4):
                print 'Creating flat frame for %swith filter %s, and chip %d' % (date,filt,chip+1)
                output='flat_%s_%s_CHIP%d' % (date,filt,chipList[chip])
                iraf.imdel(output,verify='no')

                # Split into two groups (high counts and low counts) - assumes monatonically increasing or decreasing counts
                # We need a way to find flats that are affected by clouds
                split = int(len(filenames2)/2.)

                tempfile1=open('temp1','w')
                tempfile2=open('temp2','w')
                for j,filename in enumerate(filenames2):
                    if j < split:
                        tempfile1.write('%s[%d]\n' % (filename,chip+1))
                    else:
                        tempfile2.write('%s[%d]\n' % (filename,chip+1))

                tempfile1.close()
                tempfile2.close()
                print 'There are '+ `len(filenames2)`+' files'
                print 'The median of the first and last frames ',stats2[0],stats2[len(stats2)-1]
                # Add this to the header

                iraf.imcom(input='@temp1',output='flat_high',combine='average',reject='minmax',scale='median',zero='none',statsec='[200:1800,200:1800]',nlow='1',nhigh='5')
                iraf.imcom(input='@temp2',output='flat_low',combine='average',reject='minmax',scale='median',zero='none',statsec='[200:1800,200:1800]',nlow='1',nhigh='5')

                iraf.imar(operand1='flat_high',op='-',operand2='flat_low',result='flat')
                iraf.hedit(images='flat',fields='FLATSTAT',value='%f,%f' % (stats2[0],stats2[-1]),add='yes',verify='no')
                # Add a warning if the ratio is too low. ToDo
                median=float(iraf.imstat(images='flat',fields='midpt',format='no',Stdout=1)[0])
                # Test for output filename. If it exists, append _1 to name. ToDo
                iraf.imar(operand1='flat',op='/',operand2=`median`,result=output)
                iraf.imdel(images='flat_low,flat_high,flat',verify='no')
                iraf.imcopy(input=output+trimSection[chip],output=params['calib']+output)
                
    # Move back to the reduced directory
    os.chdir(params['reduced'])

    return

def detrend(options,params):
    # Unlearn IRAF tasks
    unlearn(['imar','imhead','imcopy'])

    # Determine what kinds of calibration frames are available
    # Look for darks, we only look for the first chip (we assmume that the others are there)
    
    filename=[]
    dit=[]
    ndit=[]
    mjdobs=[]
    
    for calib in os.listdir(params['calib']):
        f=params['calib']+calib
        if '.fits' in f:
            header=fits.getheader(f)
            if header['HIERARCH ESO DPR TYPE']=='DARK' and header['HIERARCH ESO DET CHIP NO']==1:
                filename.append(f)
                dit.append(header['HIERARCH ESO DET DIT'])
                ndit.append(header['HIERARCH ESO DET NDIT'])
                mjdobs.append(header['MJD-OBS'])

    darks=Table([filename,dit,ndit,mjdobs],names=('filename','dit','ndit','mjdobs'))

    # Look for flats, we only look for the first chip (we assmume that the others are there)

    filename=[]
    filt=[]
    mjdobs=[]
    
    for calib in os.listdir(params['calib']):
        f=params['calib']+calib
        if '.fits' in f:
            header=fits.getheader(f)
            if header['HIERARCH ESO DPR TYPE']=='FLAT' and header['HIERARCH ESO DET CHIP NO']==1:
                filename.append(f)
                filt.append(header['HIERARCH ESO INS FILT1 NAME'])
                mjdobs.append(header['MJD-OBS'])


    # Convert the lists into a Table
    flats=Table([filename,filt,mjdobs],names=('filename','filter','mjdobs'))

    # Search for science frames in the raw data directory

    for date in os.listdir(params['raw']):
        # Record the results in lists
        filename=[]
        filt=[]
        origname=[]
        dit=[]
        ndit=[]
        obsid=[]
        mjdobs=[]
        for fitsfile in os.listdir(params['raw']+date):
            f=params['raw']+date+'/'+fitsfile
            header=fits.getheader(f)
            if header['HIERARCH ESO DPR TYPE']=='OBJECT' and header['HIERARCH ESO TPL ID']=='HAWKI_img_obs_AutoJitter':
                filename.append(f)
                filt.append(header['HIERARCH ESO INS FILT1 NAME'])
                origname.append(header['ORIGFILE'])
                obsid.append(header['HIERARCH ESO OBS ID'])
                mjdobs.append(header['MJD-OBS'])
                dit.append(header['HIERARCH ESO DET DIT'])
                ndit.append(header['HIERARCH ESO DET NDIT'])


        # Convert the lists into a Table
        sciFrames=Table([filename,obsid,filt,origname,dit,ndit,mjdobs],names=('filename','obsid','filter','origname','dit','ndit','mjdobs'))

        
        for obs in sciFrames:
            # We process by observing ID, date, and filter
            OBdir='OB_%d_%s_%s' % (obs['obsid'],date,obs['filter'])
            mkdir(OBdir)

            # Look for the nearest flat and science, skip if none found
            flat_chip1,dark_chip1=findCals(obs,flats,darks)
            if flat_chip1==None or dark_chip1==None:
                print 'Could not find calibrations files for %s ... Skipping' % (obs['filename'])
                continue

            for chip in range(0,4):
                input='%s[%d]' % (obs['filename'],chipList[chip])
                # We trim the name to avoid overly long names for xdimsum
                output='%s/%s_CHIP%d.fits' % (OBdir,obs['origname'].replace('.fits','').replace('IMG_OBS_AutoJitter',''),chip+1)

                # Delete the old images if required
                try:
                    iraf.imdel(images=output,verify='no')
                except:
                    pass
                    
                # Trim the image
                iraf.imcopy(input='%s[%d]' % (input,chipList[chip]) ,output=output)
                iraf.imcopy(input=output+trimSection[chip],output=output)

                # Subtract the dark
                dark=dark_chip1.replace('CHIP1.fits','CHIP%d.fits' % (chip+1))
                iraf.imar(operand1=output,op='-',operand2=dark,result=output)
                iraf.hedit(images=output,fields='DARK',value=os.path.split(dark)[1],add='yes',verify='no',show='no')

                # Divide by the flat
                flat=flat_chip1.replace('CHIP1.fits','CHIP%d.fits' % (chip+1))
                iraf.imar(operand1=output,op='/',operand2=flat,result=output)
                iraf.hedit(images=output,fields='FLAT',value=os.path.split(flat)[1],add='yes',verify='no',show='no')

    return

def findCals(obs,flats,darks):
    # Returns the root name of the appropriate flat and dark taken nearest in time
    # For flats, we need to match the filter
    flats=flats[flats['filter']==obs['filter']]
    try:
        argmin=numpy.argmin(numpy.abs(flats['mjdobs'] - obs['mjdobs']))
        flat=flats[argmin]['filename']
    except:
        flat=None


    # For darks, we need to match dit and ndit
    darks=darks[(darks['dit']==obs['dit']) & (darks['ndit']==obs['ndit'])]
    try:
        argmin=numpy.argmin(numpy.abs(darks['mjdobs'] - obs['mjdobs']))
        dark=darks[argmin]['filename']
    except:
        dark=None
    
    return flat,dark


def badPixelMasks(options,params):
    # Unlearn IRAF tasks
    unlearn(['imexpr'])

    # Create bad pixel masks
    # We use the first flat field in the calibration directory to create the mask
    for calib in os.listdir(params['calib']):
        print calib
        if 'flat' in calib:
            for chip in range(0,4):
                input='%s%s%d.fits' % (params['calib'],calib[0:-6],chipList[chip])
                output='%sbad_CHIP%d.pl' % (params['calib'],chipList[chip])
                print input,output
                iraf.imexpr(expr='(a < 0.25 || a > 1.5) ? 0 : 1', output=output, a=input)

            break # We've found a flat
        
    return

def skySubtract(options,params):
    # Unlearn IRAF tasks
    iraf.xdimsum()
    unlearn(['xfirstpass','xmaskpass','imcopy','imdel'])

    # Copy accross the bad pixel masks
    for i in range(0,4):
        input='%sbad_CHIP%d.pl' % (params['calib'],i+1)
        output='%s/bad_CHIP%d.pl' % (options.OB,i+1)
        iraf.imdel(images=output, verify='no')
        iraf.copy(input=input,output=output)

    # If a list has not been provided, create one
    # This may not work well if an OB was executed more than once during the same night

    nFrames=0
    if options.list==None:
        xf=[] # Contain file handles
        for i in range(0,4):
            xf.append(open('%s/xfirstpass_CHIP%d.list' % (options.OB,i+1),'w'))

        ref=['','','','']

        for f in numpy.sort(os.listdir(options.OB)):
            if 'HAWKI' in f and 'CHIP1' in f:
                nFrames+=1
                for i in range(0,4):
                    output='%s/%s%d' % (options.OB,f.replace('1.fits',''),i+1)
                    xf[i].write(output+'\n')        
                    if ref[i]=='':
                        ref[i]=output

        # Close the files
        for i in range(0,4):
            xf[i].close()
    else:
        print 'Option not yet available'
        exit()
    
    # Adjust the number of sky frames based on the number of exposures one has to work with
    nskymin=min(nFrames,8)-1
    nmean=min(nFrames,12)-1
    print "Mean number of frames and minimumum number of skies will be",nmean,"and",nskymin,",respectively."

    for i in range(0,4):
        inlist='@%s/xfirstpass_CHIP%d.list' % (options.OB,i+1)
        bpmask='%s/bad_CHIP%d.pl' % (options.OB,i+1)
        # Run xfirstpass without measuring the offsets
        iraf.xfirstpass(inlist=inlist,referen=ref[i],output=ref[i]+'_fp',expmap='.exp',statsec=statSection,nmean=`nmean`,nskymin=`nskymin`,nreject='2',bpmask=bpmask,badpixu='yes',mkshift='no',chkshif='no',xnregis='no')

        # Measure the offsets. We might replace this code with something automatic
        iraf.xfirstpass(inlist=inlist,referen=ref[i],output=ref[i]+'_fp',expmap='.exp',xslm='no',maskfix='no',xzap='no',badpixu='no',mkshift='yes',chkshif='no',xnreg='yes',shiftli=ref[i]+'.offset')

        # Run xmaskpass

        iraf.xmaskpass(input=ref[i]+'_fp',inexpmap=ref[i]+'_fp.exp',seccorn=ref[i]+'_fp.corners',output=ref[i]+'_mp',outexpma='.exp',statsec=statSection,chkmask='no',kpchking='no',nmean=`nmean`,nskymin=`nskymin`,nreject='1',bpmask=bpmask, badpixu='no',shiftli=ref[i]+'.offset',mag='1',xnregis='yes')



    return


def merge(options,params):
    # Unlearn IRAF tasks
    unlearn(['imcopy','hedit','imexpr'])

    # We have not created flag maps at this point in time, as they may not be necessary
    
    # Get the list of exposures
    images=numpy.genfromtxt('%s/xfirstpass_CHIP1.list' % (options.OB),dtype=[('filename','a100')])
        
    # ---------- Apply scale factor ------------
    # Do not apply the scale factor if it has alredy been applied.
    filt=fits.getval(images['filename'][0]+'.fits','HIERARCH ESO INS FILT1 NAME')
    
    if filt=='Ks':
        scalephot=[1.00,1.170,0.993,1.253] # 2012
    elif filt=='J':
        scalephot=[1.00,1.133,0.956,1.190] # 2012


    for image in images['filename']:
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            input='%s.sub.fits' % (image.replace('CHIP1',chip))
            try:
                fits.getval(input,'SCALEFAC')
                print 'Scale factor has already been applied'
            except:
                iraf.imar(operand1=input, op='*', operand2=scalephot[i], result=input)
                iraf.hedit(input, fields='SCALEFAC', value=scalephot[i], add='yes', verify='no')

    # ---------- Create bad pixel masks
    for image in images['filename']:
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            crm='%s.sub.crm.pl' % (image.replace('CHIP1',chip))
            bpm='%s.sub.bpm.fits' % (image.replace('CHIP1',chip))
            bad='%s/bad_%s.pl' % (options.OB,chip)
            iraf.imexpr(expr='(a==0 && b==1) ? 1 : 0',output=bpm,outtype='short',a=crm,b=bad)

    # ---------- merge into a single FITS files ------------
    for image in images['filename']:
        # First, the image
        outputImage='%sred.fits' % (image.replace('CHIP1',''))
        hduList=fits.HDUList(fits.PrimaryHDU())
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            input='%s.sub.fits' % (image.replace('CHIP1',chip))
            data,header=fits.getdata(input,header=True)
            hduList.append(fits.ImageHDU(data=data,header=header))
        hduList.writeto(outputImage,clobber=True,output_verify='ignore')

        # Then the mask
        outputMask='%sred.bpm.fits' % (image.replace('CHIP1',''))
        hduList=fits.HDUList(fits.PrimaryHDU())
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            input='%s.sub.bpm.fits' % (image.replace('CHIP1',chip))
            data,header=fits.getdata(input,header=True)
            hduList.append(fits.ImageHDU(data=data,header=header))
        hduList.writeto(outputMask,clobber=True,output_verify='ignore')

    
    return

def makeCat(image,weight,config,params=None,test=False):

    if test:
        catName=image.replace('.fits','_test.cat')
        flag='-CATALOG_TYPE ASCII_HEAD'
    else:
        catName=image.replace('.fits','.cat')
        flag=''

    if params!=None:
        for key in params.keys():
            flag=flag+' -'+key+' '+params[key]

    print flag

    if weight==None:
        cmd='sex -c '+config+' '+flag+' -CATALOG_NAME '+catName+' ' + image
    else:
        cmd='sex -c '+config+' '+flag+' -CATALOG_NAME '+catName+' -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE '+ weight + ' ' + image

    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "SExtractor was terminated by signal", -retcode
        else:
            print >>sys.stderr, "SExtractor returned", retcode
    except OSError, e:
        print >>sys.stderr, "SExtractor failed:", e

    return catName
        

def extract(options, params):
    # Copy accross configuration files
    setup(params,options)

    # Get the list of exposures
    images=numpy.genfromtxt('%s/xfirstpass_CHIP1.list' % (options.OB),dtype=[('filename','a100')])
    
    # Run SExtractor
    for image in images['filename']:
        input=image.replace('_CHIP1','_red')+'.fits'
        weight=input.replace('.fits','.bpm.fits')
        parameters={'PARAMETERS_NAME':'%s/scamp.sex.param' % (options.OB),
                    'FILTER_NAME':'%s/gauss_3.0_7x7.conv' % (options.OB)}
        makeCat(input, weight, '%s/scamp.sex' % (options.OB), parameters)
 
    return


def swarp(options, params):
    # Copy accross configuration files
    setup(params,options)

    # Change the working directory
    os.chdir(options.OB)
    
    cmd='swarp -c swarp.config *red.fits'
    
    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "SWARP was terminated by signal", -retcode
        else:
            print >>sys.stderr, "SWARP returned", retcode
    except OSError, e:
        print >>sys.stderr, "SWARP failed:", e

    return

def combine(options, params):
    # Combine the swarped data

    # Unlearn IRAF tasks
    unlearn(['imcom','imcopy','imdel','hedit','imexpr'])

    # Get the list of exposures
    images=numpy.genfromtxt('%s/xfirstpass_CHIP1.list' % (options.OB),dtype=[('filename','a100')])

    # Combine the frames with iraf imcombine
    rootname='%s/%s' % (options.OB,options.OB)

    # Define the list
    chipNo={'CHIP1':'0001','CHIP2':'0002','CHIP3':'0003','CHIP4':'0004'}
    file=open(rootname+'.list4imcom','w')
    for image in images['filename']:
        for chip in ['CHIP1','CHIP2','CHIP3','CHIP4']:
            stub=image.replace('_CHIP1','_red')+'.'+chipNo[chip]
            file.write(stub+'.resamp.fits\n')
            # Create the bad pixel maps and update BPM keyword
            iraf.imexpr(expr="a > 0 ? 1 : 0", output=stub+".resamp.weight.pl", a=stub+".resamp.weight.fits")
            iraf.hedit(images=stub+".resamp.fits",fields='BPM',value=stub+".resamp.weight.pl",add='yes',verify='no',update='yes')
    file.close()

    # Combine the data
    version='_v'+params['version']
    output=rootname+version
    removeFile(output+'.fits')
    removeFile(output+'_exp.pl')
    removeFile(output+'_exp.fits')
    iraf.imcom(input='@'+rootname+'.list4imcom',output=output,expmasks=output+'_exp',combine='average',reject='minmax',offsets='wcs',masktyp='badvalue',maskval='0',scale='none',zero='median',statsec=statSection,nlow='2',nhigh='2')

    iraf.imdel(images=output+'_exp.fits',verify='no')
    iraf.imcopy(input=output+'_exp.pl',output=output+'_exp.fits')


    return

def mergeResults(options, params):
    image='%s/%s_v%s.fits' % (options.OB,options.OB,params['version'])
    expMap='%s/%s_v%s_exp.fits' % (options.OB,options.OB,params['version'])
    output='%s/%s_v%s_ipe.fits' % (options.OB,options.OB,params['version'])
    
    hduList=fits.HDUList(fits.PrimaryHDU())

    # Add a number of parameters to the header
    hduList[0].header['VERSION']=params['version'].strip()
    hduList[0].header['REDUCED']=params['reducedby'].strip()
    
    # Add the filter from the first file in the sequence
    images=numpy.genfromtxt('%s/xfirstpass_CHIP1.list' % (options.OB),dtype=[('filename','a100')])
    header=fits.getheader('%s.fits' % (images['filename'][0]))
    hduList[0].header['FILTER']=header['HIERARCH ESO INS FILT1 NAME'].strip()                  
    
    for frame in [image,expMap]:
        data,header=fits.getdata(frame,header=True)
        hduList.append(fits.ImageHDU(data=data,header=header))

    hduList.writeto(output,clobber=True,output_verify='ignore')
    
    return

def parseCDC(input,output,filter):
    lines=input.split('\n')
    file=open(output,'w')
    ra=[]
    dec=[]
    mag=[]
    magerr=[]
    flag=[]
    magIndex={'J':3,'J1':3,'Ks':5}
    flagIndex={'J':0,'J1':0,'Ks':2}
    file.write('ID\tRA\tDEC\tmag\tmag_err\tQfl\n')
    for line in lines:
        if len(line) > 0:
            if line[0]!='#':
                entries=line.split('|')
                TwoMASSflag=entries[6].split()[0][flagIndex[filter]:flagIndex[filter]+1]
                if TwoMASSflag in ['A'] and float(entries[magIndex[filter]].split()[1]) < 0.1:
                    ra.append(float(entries[0].split()[0]))
                    dec.append(float(entries[0].split()[1]))
                    mag.append(float(entries[magIndex[filter]].split()[0]))
                    if entries[5].split()[1]=='---':
                        magerr.append(-9.99)
                    else:
                        magerr.append(float(entries[magIndex[filter]].split()[1]))
                    flag.append(TwoMASSflag)
                    file.write('%s\t%9.5f\t%9.5f\t%s\t%s\t%s\n' % (entries[2],ra[-1],dec[-1],mag[-1],magerr[-1],flag[-1]))

    file.close()
    stars=numpy.zeros(len(ra),dtype=[('id','int'),('ra','float'),('dec','float'),('mag','float'),('magerr','float'),('flag','a3')])
    stars['ra']=ra
    stars['dec']=dec
    stars['mag']=mag
    stars['magerr']=magerr
    stars['flag']=flag
    stars['id']=numpy.arange(1,len(ra)+1)

    return stars

                    
def ZP(options, params):
    inp='%s/%s_v%s_ipe.fits' % (options.OB,options.OB,params['version'])

    # Get the coordinates from the FITS header
    filt=fits.getval(inp,'FILTER',0).strip()
    CRVAL=numpy.matrix([[fits.getval(inp,'CRVAL1',1)],[fits.getval(inp,'CRVAL2',1)]])
    CRPIX=numpy.matrix([[fits.getval(inp,'CRPIX1',1)],[fits.getval(inp,'CRPIX2',1)]])
    CD=numpy.matrix([[fits.getval(inp,'CD1_1',1),0.0],[0.0,fits.getval(inp,'CD2_2',1)]])

    # Find stars in the 2MASS catalogue
    cmd='find2mass -r %s -c %9.5f %9.5f' % (params['radius'],CRVAL[0,0],CRVAL[1,0])
    p1=sp.Popen(cmd, shell=True, stdout=sp.PIPE)
    stars=parseCDC(p1.communicate(0)[0],inp.replace('.fits','.2MASS'),filt)

    
    # Determine the x,y position of the 2MASS stars
    # The coordinate transformation in the header is
    # c-c_0 = M * (x-x_0)
    
    tvmarkFile=inp.replace('.fits','.txt')
    file=open(tvmarkFile,'w')
    xcenterRef=[]
    ycenterRef=[]
    for star in stars:
        coord=numpy.matrix([[star['ra']],[star['dec']]])
        proj=numpy.matrix([[numpy.cos(coord[1,0]*numpy.pi/180.),0.0],[0.0,1.0]])
        x=numpy.linalg.inv(CD)*proj*(coord-CRVAL)+CRPIX
        file.write('%7.2f %7.2f %s\n' % (x[0,0],x[1,0],star['id']))
        xcenterRef.append(x[0,0])
        ycenterRef.append(x[1,0])


    file.close()
    unlearn(['display','tvmark'])
    
    iraf.display(image=inp+'[1]',frame=1,zr='no',zs='no',z1=-100,z2=300)
    iraf.tvmark(frame='1',coords=tvmarkFile,label='yes',color=205,mark='circle',nxoffse=40,radii=20,pointsize=2,txsize=3)
    
    # If we do not trust the coordinates, then we ask the user to select stars in the image
    trust=True
    iraf.noao()
    iraf.digi()
    iraf.apphot()
    unlearn(['phot','txdump'])
    iraf.fitskypars.salgori='median'
    iraf.fitskypars.annulus='4'
    iraf.fitskypars.dannulu='6'
    iraf.datapars.scale='0.16'
    iraf.photpars.apertur='4'
    iraf.photpars.zmag='0'
    #    if not trust:

    mags=inp.replace('.fits','0.mag.1')
    iraf.phot(image=inp.replace('.fits','[1]'),output=mags,verify='no')
    #    else:
    #        iraf.phot(image=inp.replace('.fits','[1]'),coords=tvmarkFile,interact='no')
        
    mags=inp.replace('.fits','0.mag.1')
    xcenterIn=iraf.txdump(textfile=mags,fields='XCENTER',expr='yes',Stdout=1)
    ycenterIn=iraf.txdump(textfile=mags,fields='YCENTER',expr='yes',Stdout=1)
    mag=iraf.txdump(textfile=mags,fields='MAG',expr='yes',Stdout=1)
    # The magnitude error is not very precise
    magerr=iraf.txdump(textfile=mags,fields='MERR',expr='yes',Stdout=1)

    iraf.delete(files=mags,verify='no')

    # ZP
    ZP=[]
    ZP_err=[]
    sum1=0
    sum2=0
    phot=open(inp.replace('.fits','.phot'),'w')
    phot.write('ID\tRA\tDEC\tmag_INST\terr_mag_INST\tmag_2MASS\terr_mag_2MASS\tQfl\tZP\terr_ZP\n')
    # Only A class stars are considered
    use=[]
    for i in range(len(xcenterIn)):
         if stars[i]['flag'] in ['A'] and stars[i]['magerr'] > 0 :
             reply=raw_input('Using star # %d?' % (stars[i]['id']))
             if reply=='y':
                 use.append(i)

    for i in use:
        ZP.append(stars[i]['mag']-float(mag[i]))
        ZP_err.append(numpy.sqrt(stars[i]['magerr']**2.+float(magerr[i])**2.))
        print 'Using star #',stars[i]['id']
        phot.write('%s\t%9.5f\t%9.5f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%s\t%6.3f\t%6.3f\n' %(stars[i]['id'],stars[i]['ra'],stars[i]['dec'],float(mag[i]),float(magerr[i]),stars[i]['mag'],stars[i]['magerr'],stars[i]['flag'],ZP[-1],ZP_err[-1]))
        sum1+=ZP[-1]/ZP_err[-1]**2.
        sum2+=1.0/ZP_err[-1]**2.

    phot.close()

    # The error bars are probably understimated
    if len(ZP) > 0:
        iraf.hedit(images=inp+'[0]',fields='ZP',value=sum1/sum2,add='yes',verify='no')
        iraf.hedit(images=inp+'[0]',fields='err_ZP',value=numpy.sqrt(1.0/sum2),add='yes',verify='no')
        iraf.hedit(images=inp+'[0]',fields='scatt_ZP',value=numpy.std(ZP)/numpy.sqrt(len(ZP)),add='yes',verify='no')
    else:
        iraf.hedit(images=inp+'[0]',fields='ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=inp+'[0]',fields='err_ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=inp+'[0]',fields='scatt_ZP',value=-9.99,add='yes',verify='no')
    iraf.hedit(images=inp+'[0]',fields='n_ZP',value=len(ZP),add='yes',verify='no')

    
    # Add some keywords

    iraf.hedit(images=inp+'[0]',fields='MAGSYS',value='Vega',add='yes',verify='no')
  
    return
