import numpy as np
from astropy.io import fits,ascii
from astropy import wcs as pywcs
import os,string
from sys import exit
import scipy.interpolate
import scipy.ndimage

# query GOGREEN.fits and return name of latest phot fits table
def getPhotFilename(name):


    #photfileExpr = "'%s/Data/reduced/%s/Catalogues/Masks/%s_phot.fits'%(os.getenv('GoGREEN_data'),name,name)"

    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
#    masterfile = eval(cfg.masterfileExpr) # needs to be set before cfg is declared
    mdat = fits.getdata(masterfile)
    #print mdat['cluster'],name
    #print np.where(mdat['cluster']==name)[0]
    try:
        ok=np.where(mdat['cluster']==name)[0]
    except:
        print '%s not found in %s'%(name,masterfile)

    mdat = mdat[ok] # only keep line we want!
    catname = mdat['maskPhotCat'][0]

    rootdir = os.getenv('GoGREEN_data')
    filename = "%s/Data/reduced/%s/Catalogues/Masks/%s"%(rootdir,name,catname)
    return filename

def getzSpecFilename(name):


    #photfileExpr = "'%s/Data/reduced/%s/Catalogues/Masks/%s_phot.fits'%(os.getenv('GoGREEN_data'),name,name)"

    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
#    masterfile = eval(cfg.masterfileExpr) # needs to be set before cfg is declared
    mdat = fits.getdata(masterfile)
    #print mdat['cluster'],name
    #print np.where(mdat['cluster']==name)[0]
    try:
        ok=np.where(mdat['cluster']==name)[0]
    except:
        print '%s not found in %s'%(name,masterfile)
        return ''

    mdat = mdat[ok] # only keep line we want!
    catname = mdat['zSpecCat'][0]

    rootdir = os.getenv('GoGREEN_data')
    filename = "%s/Data/reduced/%s/Catalogues/Masks/%s"%(rootdir,name,catname)
    return filename



def getMaskDir(name):
    tmaskdir = getPhotFilename(name)
    maskdir = string.join(tmaskdir.split('/')[0:-1],'/')+'/'
    return maskdir

def colz(redshift,col='zI1'):
    # return z-IRAC1 colour (by default) at redshift, redshift
    # colour -- redshift relation(s) digitized from Muzzin+ 2013, Fig. 1

    #redshift col_z_36u
    col_zI1="""
    0.00691823492079946, -0.4238365993601798
    0.07962413708246441, -0.28139854162079736
    0.12670025515160166, -0.15278659223665692
    0.18659741739952596, -0.01005711769353379
    0.2592910152961445, 0.11797199808312442
    0.32345775751531525, 0.260604333691667
    0.38764910826458143, 0.432054553225659
    0.4561346475151855, 0.6322255177505207
    0.5160687225582508, 0.8181818181818175
    0.5631694491574817, 0.9756116514914055
    0.6231035242005469, 1.1615679519227027
    0.7129246590423395, 1.3468442798119384
    0.7728218212902633, 1.4895737543550611
    0.8198733308293069, 1.5893678198137517
    0.8583979846909044, 1.7037651051043263
    0.918319755468922, 1.8753124635728988
    0.9696408449792124, 1.975009390097009
    1.0295010944319967, 2.0745120387519584
    1.068062661088733, 2.2321361499307066
    1.1109061119817638, 2.4040720641376
    1.1665583027885353, 2.5757165615407525
    1.2136590293877654, 2.7331463948503405
    1.2735315831055973, 2.847057985468015
    1.3590585294460484, 3.0036135683663816
    1.453124635728995, 3.159974873395588
    1.5600117861907283, 3.3304537035837787
    1.628435804116101, 3.458579958295017
    1.7182323304277998, 3.6150384022588034
    1.8036977554430185, 3.6995492753435477
    1.9105849059047517, 3.870028105531738
    1.9662001839163818, 3.998445777046717
    """

    #redshift 36u_45u
    col_I2I1="""
    0.00691823492079946, -0.4238365993601798
    0.07962413708246441, -0.28139854162079736
    0.12670025515160166, -0.15278659223665692
    0.18659741739952596, -0.01005711769353379
    0.2592910152961445, 0.11797199808312442
    0.32345775751531525, 0.260604333691667
    0.38764910826458143, 0.432054553225659
    0.4561346475151855, 0.6322255177505207
    0.5160687225582508, 0.8181818181818175
    0.5631694491574817, 0.9756116514914055
    0.6231035242005469, 1.1615679519227027
    0.7129246590423395, 1.3468442798119384
    0.7728218212902633, 1.4895737543550611
    0.8198733308293069, 1.5893678198137517
    0.8583979846909044, 1.7037651051043263
    0.918319755468922, 1.8753124635728988
    0.9696408449792124, 1.975009390097009
    1.0295010944319967, 2.0745120387519584
    1.068062661088733, 2.2321361499307066
    1.1109061119817638, 2.4040720641376
    1.1665583027885353, 2.5757165615407525
    1.2136590293877654, 2.7331463948503405
    1.2735315831055973, 2.847057985468015
    1.3590585294460484, 3.0036135683663816
    1.453124635728995, 3.159974873395588
    1.5600117861907283, 3.3304537035837787
    1.628435804116101, 3.458579958295017
    1.7182323304277998, 3.6150384022588034
    1.8036977554430185, 3.6995492753435477
    1.9105849059047517, 3.870028105531738
    1.9662001839163818, 3.998445777046717
    """



    if col== 'zI1':
        dat = ascii.read(col_zI1)
    elif col=='I2I1':
        dat = ascii.read(col_I2I1)
    else:
        print "col should be 'zI1' or 'I2I1'"
        exit(1)

    return np.interp(redshift,dat['col1'],dat['col2'])


def readMaster(name):
    # read master table of cluster properties and return equivalent info to fInfo in early versions of code:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
#    masterfile = eval(cfg.masterfileExpr) # needs to be set before cfg is declared
    mdat = fits.getdata(masterfile)
    try:
        ok=np.where(mdat['cluster']==name)[0]
    except:
        print '%s not found in %s'%(name,masterfile)

    mdat = mdat[ok] # only keep line we want!
    fInfo = {}
    fInfo['grpra'] = mdat['RA_target_deg']
    fInfo['grpdec'] = mdat['DEC_target_deg']
    fInfo['grpz'] = mdat['Redshift']
    fInfo['cra'],fInfo['cdec'] = mdat['RA_gmos_deg'],mdat['DEC_gmos_deg']
    fInfo['rotang'] = mdat['PA_deg']
    fInfo['RSslope'],fInfo['RSintcpt'] = mdat['RSSlope'],mdat['RSintcpt']
    fInfo['Detector'] = mdat['Detector']
    print fInfo
    return fInfo
    
def congrid(a, newdims, method='linear', centre=False, minusone=False):
    '''Arbitrary resampling of source array to new dimension sizes.
    Currently only supports maintaining the same number of dimensions.
    To use 1-D arrays, first promote them to shape (x,1).
    
    Uses the same parameters and creates the same co-ordinate lookup points
    as IDL''s congrid routine, which apparently originally came from a VAX/VMS
    routine of the same name.

    method:
    neighbour - closest value from original data
    nearest and linear - uses n x 1-D interpolations using
                         scipy.interpolate.interp1d
    (see Numerical Recipes for validity of use of n 1-D interpolations)
    spline - uses ndimage.map_coordinates

    centre:
    True - interpolation points are at the centres of the bins
    False - points are at the front edge of the bin

    minusone:
    For example- inarray.shape = (i,j) & new dimensions = (x,y)
    False - inarray is resampled by factors of (i/x) * (j/y)
    True - inarray is resampled by(i-1)/(x-1) * (j-1)/(y-1)
    This prevents extrapolation one element beyond bounds of input array.
    '''
    if not a.dtype in [np.float64, np.float32]:
        a = np.cast[float](a)

    m1 = np.cast[int](minusone)
    ofs = np.cast[int](centre) * 0.5
    old = np.array( a.shape )
    ndims = len( a.shape )
    if len( newdims ) != ndims:
        print "[congrid] dimensions error. " \
              "This routine currently only support " \
              "rebinning to the same number of dimensions."
        return None
    newdims = np.asarray( newdims, dtype=float )
    dimlist = []

    if method == 'neighbour':
        for i in range( ndims ):
            base = np.indices(newdims)[i]
            dimlist.append( (old[i] - m1) / (newdims[i] - m1) \
                            * (base + ofs) - ofs )
        cd = np.array( dimlist ).round().astype(int)
        newa = a[list( cd )]
        return newa

    elif method in ['nearest','linear']:
        # calculate new dims
        for i in range( ndims ):
            base = np.arange( newdims[i] )
            dimlist.append( (old[i] - m1) / (newdims[i] - m1) \
                            * (base + ofs) - ofs )
        # specify old dims
        olddims = [np.arange(i, dtype = np.float) for i in list( a.shape )]

        # first interpolation - for ndims = any
        mint = scipy.interpolate.interp1d( olddims[-1], a, kind=method )
        newa = mint( dimlist[-1] )

        trorder = [ndims - 1] + range( ndims - 1 )
        for i in range( ndims - 2, -1, -1 ):
            newa = newa.transpose( trorder )

            mint = scipy.interpolate.interp1d( olddims[i], newa, kind=method )
            newa = mint( dimlist[i] )

        if ndims > 1:
            # need one more transpose to return to original dimensions
            newa = newa.transpose( trorder )

        return newa
    elif method in ['spline']:
        oslices = [ slice(0,j) for j in old ]
        oldcoords = np.ogrid[oslices]
        nslices = [ slice(0,j) for j in list(newdims) ]
        newcoords = np.mgrid[nslices]

        newcoords_dims = range(np.rank(newcoords))
        #make first index last
        newcoords_dims.append(newcoords_dims.pop(0))
        newcoords_tr = newcoords.transpose(newcoords_dims)
        # makes a view that affects newcoords

        newcoords_tr += ofs

        deltas = (np.asarray(old) - m1) / (newdims - m1)
        newcoords_tr *= deltas

        newcoords_tr -= ofs

        newa = scipy.ndimage.map_coordinates(a, newcoords)
        return newa
    else:
        print "Congrid error: Unrecognized interpolation type.\n", \
              "Currently only \'neighbour\', \'nearest\',\'linear\',", \
              "and \'spline\' are supported."
        return None


