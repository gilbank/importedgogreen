from astropy.io import fits,ascii
from astropy import wcs

import os,sys
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle

try:
    from astropy.coordinates import SkyCoord
except:
    print 'We seem to have an older version of astropy (probably from Ureka).\nTrying to work around'
    from astropy.coordinates import ICRS as SkyCoord

import astropy.units as u
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from GoGREEN_checkPhotCat import getZphWts
from GoGREEN_utils import getPhotFilename, colz


try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)

def mkcatPreim(name):
    # Simple script to just apply SEx flags to Adam's catalogue:

    photfile = getPhotFilename(name)
    gal = fits.getdata(photfile)

    """
    # from Adam's catalogue we have:
        name = 'CLASS_STAR'; format = 'E'
        name = 'SEXTRACTOR_FLAGS'; format = 'E'
    """

    #Just reject things we don't want to  use and overwrite catalogue with new name:
    # updated following telecon 20150824:
    use = np.where(gal['SEXTRACTOR_FLAGS']<8)[0]
    gal=gal[use]

#    fits.writeto('foo.fits',gal,clobber=True)

    # Need to fix table-writing problem as MLB did for mkcatCat:
#    ra = gal['RA']
#    dec = gal['DEC']
#    irac1 =

    # MLB: Attempt to make table readable by IRAF, FV, others.
    # Following http://astropy.readthedocs.org/en/v1.0.3/io/fits/
    prihdu=fits.PrimaryHDU(header=fits.Header())
    col1=fits.Column(name='ID',format='1J',disp='I11',array=gal['ID'].astype(int))
    col2=fits.Column(name='RA',format='E',array=gal['RA'])
    col3=fits.Column(name='DEC',format='E',array=gal['DEC'])
    col4=fits.Column(name='IRAC1',format='E',array=gal['IRAC1'])
    col5=fits.Column(name='EIRAC1',format='E',array=gal['EIRAC1'])
    col6=fits.Column(name='ZMAG',format='E',array=gal['ZMAG'])
    col7=fits.Column(name='EZMAG',format='E',array=gal['EZMAG'])
    col8=fits.Column(name='ZPH',format='E',array=gal['ZPH'])
    col9=fits.Column(name='ZPHLO',format='E',array=gal['ZPHLO'])
    col10=fits.Column(name='ZPHHI',format='E',array=gal['ZPHHI'])
    col11=fits.Column(name='IZPH',format='E',array=gal['IZPH'])
    try:
        col12=fits.Column(name='ZS',format='E',array=gal['ZS'])
    except:
        col12=fits.Column(name='ZS',format='E',array=-np.ones(len(gal)))
    col13=fits.Column(name='AQSTAR_CAND',format='1J',disp='I11',array=gal['AQSTAR_CAND'])
    col14=fits.Column(name='XP',format='E',array=gal['XP'])
    col15=fits.Column(name='YP',format='E',array=gal['YP'])
    cols=fits.ColDefs([col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15])
    #cols=fits.ColDefs(obj)
    tbhdu=fits.BinTableHDU.from_columns(cols)
    thdulist=fits.HDUList([prihdu,tbhdu])
    thdulist.writeto('foo.fits',clobber=True)

    print 'Now update %s using foo.fits'%photfile



if __name__ == "__main__":

    name = sys.argv[1]
#    imagename = sys.argv[2]
#    incat = sys.argv[3]

    mkcatPreim(name)
#    cat2gmmpsE2v(name,imagename,incat)
#    plotField(name) # Need to close this plot window manually. Encourage the user to look at results!

