# ============================================================================
# CL 2014/08/02:  GoGREEN library
# ============================================================================

import subprocess as sp
import sys
from astropy.io import fits
import numpy as np

def returnDicElement(key,value): return dict([(key[i],value[i]) for i in range(len(key))])

def buildDictionary(file):
    # Read in a parameter file
    try:
        file=open(file,'r')
        lines=file.readlines()
        file.close()
        parameters=[]
        for line in lines:
            if line[0] not in'#' and line!= '' and len(line) > 0:
                parameters.append(line.split()[0:2])
    except:
        parameters=[]
    return dict(parameters)

def setup(dir='setup'):
    cmd = 'cp '+dir+'/* .'
    try:
        sp.call(cmd,shell=True)
    except:
        pass
    
    return
    
def makeCat(image,weight=None,detect=None,SExParams=None):
    # Set up the SExtractor command

    flags=''
    for key in SExParams.keys():
        flags=flags+' -'+key+' '+SExParams[key]


    print flags
    cmd='sex ' +flags+' '+image

    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "SExtractor was terminated by signal", -retcode
        else:
            print >>sys.stderr, "SExtractor returned", retcode
    except OSError, e:
        print >>sys.stderr, "SExtractor failed:", e

    return retcode

def buildRegionFile(catName):
    # select objects with ...
    data=fits.getdata(catName,0,header=False)
    output=catName.replace('_cat.fits','_test.reg')
    file=open(output,'w')
    select=(data.field('FLUX_BEST') < 0.9)
    for source in data[select]:
        file.write('circle(%9.5f,%9.5f,4.5)#text={MAG_APER=%5.2f}\n' % (source.field('X_IMAGE'),source.field('Y_IMAGE'),source.field('FLUX_APER')[0]))
    for source in data[~select]:
        file.write('box(%9.5f,%9.5f,10,10)\n' % (source.field('X_IMAGE'),source.field('Y_IMAGE')))
    file.close()
    return output