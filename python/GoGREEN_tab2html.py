import numpy as np
from astropy.io import fits,ascii
from astropy import wcs as pywcs
import os
from sys import exit
from time import ctime

# Convert master FITS table to html page
# (relevant lines can be pasted into wiki, or whole page could be used)

outfile='mastertable.html'


masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
mdat = fits.getdata(masterfile)

# grab HDU creation date for this file
hd = fits.getheader(masterfile,1)
dateHDU = hd['DATE-HDU'] #/ Date of HDU creation (UTC)

# generate a minimal webpage with just table and generation date (currently just local time written. may want to get fancier)
nrows = len(mdat)
xxx = open(outfile,'w')
xxx.write('<html>\n<body>\n<p>')
now_ct = ctime()
xxx.write('Auto-generated from: %s on %s</p>\n'%(masterfile,now_ct))
print 'Auto-generated from: %s on %s</p>\n'%(masterfile,now_ct)
xxx.write('<p>This version of GOGREEN.fits was created on %s UT</p>\n'%dateHDU)
print '<p>This version of GOGREEN.fits was created on %s UT</p>\n'%dateHDU
print '<table style="width: 100%;" border="1">\n<tbody>\n'
xxx.write('<table style="width: 100%;" border="1">\n<tbody>\n')
ncols = len(mdat.columns)
print '<tr>'
xxx.write('<tr>')
for i in range(ncols):
    print '<td>'+mdat.columns[i].name+'<br /></td>'
    xxx.write('<td>'+mdat.columns[i].name+'</td>')
print '</tr>'
xxx.write('</tr>\n')
for i in range(nrows):
    oline=''
    oline='<tr>'
    for item in mdat[i]:
        oline=oline+'<td>'+str(item)+'<br /></td>'
    oline=oline+'</tr>\n'
    print oline
    xxx.write(oline)
print '</tbody></table>\n'
xxx.write('</tbody>\n</table>\n')
xxx.write('</body>\n</html>\n')
print '\nwritten %s\n'%outfile
xxx.close()