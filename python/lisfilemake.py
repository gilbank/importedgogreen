from os import listdir
from astropy.io import fits
import numpy as np
def lisfilemake(rawpath,redpath,maskname,startdate=0,enddate=20300000):

    '''Written by Gregory Rudnick 14 Feb 2016

    PURPOSE: 

    Reads in all the files in a path of the raw data and creates a file 
    that contains the science, flat, and arc for every file in that directory.  

    The code goes through the list of files in the directory and finds
    the OBSTYPE=object files that have the appropriate mask name
    (input when calling the function).  Then it searches for the
    closest flat in time that has the same mask and central
    wavelength.  Finally, it searches for the closest arc (in time)
    that has the same mask and central wavelength.  Then it writes
    these to a MASKNAME.lis file

    CALLING SEQUENCE
    
    lisfilemake(rawpath,redpath,maskname)

    INPUT PARAMETERS

    rawpath - the path containing the raw fits files

    redpath - the path containing the reduced data.  This is where the
    MASKNAME.lis file will be created

    maskname - the name of the mask.  This will be used to identify
    the mask file and to pick all of the relevant exposures 

    OPTIONAL KEYWORKDS
      
    none

    OUTPUTS

    an output file 

    EXAMPLES
 
    PROCEDURE

    REQUIRED PYTHON MODULES

    astropy
    os

    NOTES

    '''

    #load contents of directory into a list
    filelist = np.sort(listdir(rawpath))
    #open outfile
    lisfile = redpath + maskname + '.lis'
    #print "writing to " + lisfile
    fo = open(lisfile, "w")
    # Feb 23, 2016: MLB made major modifications to allow consideration of files between given dates, and generally to speed things up.
    isci=np.array([],dtype=int)
    iarc=np.array([],dtype=int)
    iflat=np.array([],dtype=int)
    gtrwave={}
    for i in range(len(filelist)):
        #print "Trying ",filelist[i]
        fname = rawpath + filelist[i]
        try:
            date=int(filelist[i][1:9])
        except:
            date=-999
        if ((date >= startdate) & (date <= enddate)):
            try:
                hdulist = fits.open(fname)
                gtrwave[i]=hdulist[0].header['WAVELENG']
                if (hdulist[0].header['MASKNAME'] == maskname):
                    if hdulist[0].header['OBSTYPE'] == 'OBJECT' and \
                        hdulist[0].header['OBSCLASS'] == 'science':
                        isci=np.append(isci,i)
                        print 'Found science frame: ',filelist[i]
                    if hdulist[0].header['OBSTYPE'] == 'ARC':
                        iarc = np.append(iarc,i)
                        print 'Found arc frame: ',filelist[i]
                    if hdulist[0].header['OBSTYPE'] == 'FLAT':
                        iflat = np.append(iflat,i)
                        print 'Found flat frame: ',filelist[i]
            except:
                print "Problem opening file ",fname
    for i in range(len(isci)):
        iscience=int(isci[i])
        print 'object identified ' + filelist[iscience]
        #check if previous or next file was a flat with the same
        #grating
        if gtrwave[iscience+1]==gtrwave[iscience] and iscience+1 in iflat:
            flat=filelist[iscience+1]
        elif gtrwave[iscience-1]==gtrwave[iscience] and iscience-1 in iflat:
            flat=filelist[iscience-1]
        print 'flat ' + flat + ' identified for ' + filelist[iscience]
        for j in iarc:
            if gtrwave[j]==gtrwave[iscience]:
                arc=filelist[j]
                print 'arc ' + arc + ' identified for ' + filelist[iscience]
        #trim off ".fits" extension and print
        str1 = filelist[iscience].split(".fits",1)[0]
        str2 = flat.split(".fits",1)[0]
        str3 = arc.split(".fits",1)[0]
        fo.write('{} {} {}\n'.format(str1, str2, str3))


# Close opend file
    fo.close()

    return;
            
