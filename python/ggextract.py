from optparse import OptionParser
import Constants
import GoGREEN_Library as GoGREEN
from astropy.io import fits,ascii
from astropy import wcs
import GoGREEN_spectra as ggsp
import numpy as np
import os
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
import getopt
import re

parser=OptionParser()
parser.add_option("-c", "--configFile", dest="configFile",
                  default=None, help="Configuration file")
parser.add_option("-r", "--refslit", dest="ref",
                  default=None,help="Slit number that defines wlc parameters to which all spectra will be interpolated")
parser.add_option("-p", "--prefix", dest="prefix",
                  default='rfcs_', help="prefix for stacked file")
(options, args) = parser.parse_args()

try:
    cname=str(args[0])
except:
    cname = raw_input('Cluster name? e.g. SXDF64')
try:
    maskname=str(args[1])
except:
    maskname = raw_input('Mask name? e.g. GS2014BLP001-08')
try:
    traceorder=str(args[2])
except:
    traceorder = 0
try:
    preimage=str(args[3])
except:
    preimage = ""

constants=Constants.GoGREENPath()
dir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"
params=GoGREEN.buildDictionary(options.configFile)
imname=options.prefix+maskname
#imname="rfcs_"+maskname
#imname="cs_"+maskname
try:
    with open(options.configFile) as file:
        pass
except IOError as e:
    print "Configuration file not specified or does not exist; using defaults"
        
# 1. Parameters that determine what is computed and output/shown
if 'showplots' not in params.keys(): params['showplots']=True
showplots = bool(params['showplots'])
if 'weight_method' not in params.keys(): params['weight_method']='no_weight'
weight_method = params['weight_method']
# 2.  Data-specific parameters
if 'width_arcsec' not in params.keys(): params['width_arcsec']=.7 # Default sigma width in arcseconds of spatial profile
width_arcsec = np.float(params['width_arcsec'])
if 'slittrim' not in params.keys(): params['slittrim']=2.      # number of pixels at slit edge to ignore
slittrim = np.int(params['slittrim'])
# 3. Fitting parameters
if 'f0' not in params.keys(): params['f0']=0.5 # f0-f1 is the range of pixels (as fraction of total) to use for a single best spatial profile
f0p = np.float(params['f0'])
if 'f1' not in params.keys(): params['f1']=0.85
f1p = np.float(params['f1'])
if 'dxmax' not in params.keys(): params['dxmax'] = 2.      # max shift in predicted spatial position, in pixels
dxmax = np.float(params['dxmax'])
if 'dxstep' not in params.keys(): params['dxstep']=0.1   # step to search for spatial shift
dxstep = np.float(params['dxstep'])
if 'dwmax' not in params.keys(): params['dwmax']=0.2    # max shift in spatial profile (sigma), in arcseconds
dwmax = np.float(params['dwmax'])
if 'dwstep' not in params.keys(): params['dwstep']=0.025 # step to search for sigma shift
dwstep = np.float(params['dwstep'])
if 'trace1' not in params.keys(): params['trace1']=0.2    # trace1 and trace2 are limits (in fraction)within which to fit the trace
trace1 = np.float(params['trace1'])
if 'trace2' not in params.keys(): params['trace2']=.85
trace2 = np.float(params['trace2'])
if 'trace_step' not in params.keys(): params['trace_step']=0.05 # step (in fraction) for fitting trace.
trace_step = np.float(params['trace_step'])
if 'stackedprefix' not in params.keys(): params['stackedprefix']='stacked_' # prefix for vertically-stacked 2D image
stackedprefix=params['stackedprefix']
if 'onedprefix' not in params.keys(): params['onedprefix']='oned_'    # prefix for oned MEF
onedprefix = params['onedprefix']
if 'twodprefix' not in params.keys(): params['twodprefix']='twod_'    # prefix for reordered twod MEF
twodprefix = params['twodprefix']
#########################################################################################################################################################
#########################################################################################################################################################
Version=2.4
# Jan 17, 2018 update.  Allow user to specify bad pixel ranges, by modifying dq.  Limit output range to >6000A
# Oct 26, 2017 update.  Increase var array if pos-neg is greater than 5 sigma to help account for systematics
onedfile=dir+onedprefix+imname+".fits"
twodfile=dir+twodprefix+imname+".fits"
try:
    reducedfile=dir+imname+".fits.gz"
    if not os.path.isfile(reducedfile):
        reducedfile=reducedfile.replace('.gz','')
    mdf,mdfhd = fits.getdata(reducedfile,header=True,ext=('mdf',1))
    hdulinput=fits.open(reducedfile)
except:
    print 'No fits file ',reducedfile
    sys.exit()
#hdulist=fits.open(dir+imname+".fits")
header_primary=hdulinput[0].header
#hdulist.close()
nsciext=header_primary['NSCIEXT']
header_primary['NEXTEND']=1+3*nsciext
if ((header_primary['NODAYOFF']<0) | (header_primary['DETECTOR']=='GMOS + e2v DD CCD42-90')):
#if ((header_primary['NODAYOFF']<0) | (header_primary['INSTRUME']=='GMOS-N')):
#    reverse_NODAB=True
    reverse_NODAB=True
else:
    reverse_NODAB=False
# Read GOGREEN.fits to get name of preimage:
try:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mdat = fits.getdata(masterfile)
except:
    print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    print "Stopping"
    stop()
#preimage_root=mdat['Preim_name'][np.where(mdat['cluster']==cname)]
preimage_root=mdat['z_image'][np.where(mdat['cluster']==cname)]
imdir=os.getenv('GoGREEN_data')+'Data/reduced/'+cname+'/Imaging/GMOS/Z/'
preimage=imdir+preimage_root[0]+'.fits.gz'
if not os.path.isfile(preimage):
    preimage=preimage.replace('.gz','')
try:
    preim,preim_header=fits.getdata(preimage,header=True)
    imagewcs=wcs.WCS(preim_header)
except:
    print 'No preimage ',preimage
    sys.exit()
preim_xscale=(np.abs(preim_header['CD1_1'])+np.abs(preim_header['CD1_2']))*3600
preim_yscale=(np.abs(preim_header['CD2_1'])+np.abs(preim_header['CD2_2']))*3600
preim=preim-np.median(preim)

lisfile=dir+maskname+'.lis'
ref_sciimage=ascii.read(lisfile,data_start=0)[0][0]
ref_flatimage=ascii.read(lisfile,data_start=0)[0][1]
try:
    scimosaic_fn=dir+'mgs'+ref_sciimage+'.fits.gz'
    flatmosaic_fn=dir+'rg'+ref_flatimage+'_comb.fits.gz'
    if not os.path.isfile(scimosaic_fn):
        scimosaic_fn=scimosaic_fn.replace('.gz','')
    if not os.path.isfile(flatmosaic_fn):
        flatmosaic_fn=flatmosaic_fn.replace('.gz','')
    #print flatmosaic_fn
    scimosaic=fits.getdata(scimosaic_fn,ext=('sci',1))
    flatmosaic,flathd=fits.getdata(flatmosaic_fn,header=True,ext=('MDF',1))
    scimosaic=np.transpose(scimosaic)
    mosaic_exists='yes'
except:
    print 'No mosaic image',scimosaic_fn,' exists.  Ignoring.'
    mosaic_exists='no'
hdul = fits.HDUList()
hdul.append(fits.PrimaryHDU(header=header_primary))
hdul.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))
hdul2d = fits.HDUList()
hdul2d.append(fits.PrimaryHDU(header=header_primary))
hdul2d.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))
# Derive a few quantities from the input parameters
pixscale=header_primary['PIXSCALE']
nodpix=header_primary['NODAYOFF']*2/pixscale
nodpix=np.abs(nodpix)
width_pix=width_arcsec/pixscale
dx = np.arange(-dxmax,dxmax,dxstep)
dw = np.arange(-dwmax,dwmax,dwstep)/pixscale # in pixels
Gratinglimit=[4756,10650]
spTrimBluelimit=6400
spRedlimit=10500
for i in range(1,nsciext+1):
    #subim,hd = fits.getdata(dir+imname+".fits",header=True,ext=('sci',i))
    #subvar,varhd = fits.getdata(dir+imname+".fits",header=True,ext=('var',i))
    #subdq,dqhd = fits.getdata(dir+imname+".fits",header=True,ext=('dq',i))
    subim=hdulinput['sci',i].data
    hd=hdulinput['sci',i].header
    subvar=hdulinput['var',i].data
    varhd=hdulinput['var',i].header
    subdq=hdulinput['dq',i].data
    dqhd=hdulinput['dq',i].header
    spTrimBluelimitpix=int((spTrimBluelimit-hd['crval1'])/hd['cd1_1']+hd['crpix1'])
    l0=hd['crval1']+(spTrimBluelimitpix-hd['crpix1'])*hd['cd1_1']
    hd['crval1']=l0
    varhd['crval1']=l0
    dqhd['crval1']=l0
    subim=subim[:,spTrimBluelimitpix:]
    subvar=subvar[:,spTrimBluelimitpix:]
    subdq=subdq[:,spTrimBluelimitpix:]
    hd['naxis1']=hd['naxis1']-spTrimBluelimitpix
    varhd['naxis1']=hd['naxis1']-spTrimBluelimitpix
    dqhd['naxis1']=hd['naxis1']-spTrimBluelimitpix
    pix=np.arange(1,hd['naxis1']+1)
    ll=hd['crval1']+(pix-hd['crpix1'])*hd['cd1_1']
    bluepixshift=(spTrimBluelimit-Gratinglimit[0])/hd['cd1_1']
    redpixshift=(spRedlimit-Gratinglimit[1])/hd['cd1_1']  
    dqgrid=np.indices(np.shape(subdq))
    # --- Try to find best offset of profile pair within slit:####
    npix=subim.shape[0]
    # Centre of slit.  Convention is that index starts at 0 and references the bottom of the pixel
    #slit_centre=(npix/2.)
    # Hmm... why do I say that?  imshow treats index as representing centre of pixel.  Why doesn't that make sense?
    slit_centre=((npix-1)/2.)
    # Offset is expected position of the spectrum at the bottom of the slit
    offset=slit_centre-nodpix/2.0
    #print offset+dx[pk],width_pix+dw[pw],nodpix
    # Try looking for solution that varies with wavelength
    refit='yes'    
    cnt=0
    dofit=True
    subdq_orig=np.copy(subdq)
# Read userbad from header if present
    try:
        outim,hdx = fits.getdata(onedfile,header=True,ext=('sci',i))
        userbadinput=hdx['USERBAD']
        badarray=np.array(userbadinput.split()).astype(int)
        Nbadreg=np.size(badarray)/4
        condition=np.array([])
        for ibad in np.arange(Nbadreg):
            (x1,x2,y1,y2)=badarray[ibad*4:ibad*4+4]
            if ibad==0:
                condition=((dqgrid[1]>x1)& (dqgrid[1]<x2) & (dqgrid[0]>y1) & (dqgrid[0]<y2))
            else:
                condition=(condition) | (dqgrid[1]>x1)& (dqgrid[1]<x2) & (dqgrid[0]>y1) & (dqgrid[0]<y2)
        userbad=np.where(condition)
        print 'Read bad coords: ',userbadinput
    except:
        #print 'No userbad coordinates read from file'
        userbad=np.array([])
        badarray=np.array([])
    while refit=='yes':
        cnt=cnt+1
        dxarray=[]
        dwarray=[]
        pixarray=[]
        profarray=[]
        subdq=np.copy(subdq_orig)
        if np.size(userbad)>0: 
            subdq[userbad]=1
        xsec_avg = ggsp.getmedprof(subim,subdq,f0p,f1p)
        vsec_avg = ggsp.getmedprof(subvar,subdq,f0p,f1p)
        (pk,pw,useProf)=ggsp.getprof(xsec_avg,vsec_avg,offset,width_pix,dx,dw,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)        
        dwfixed=dw*0+dw[pw]
        for specfrac in np.arange(trace1,trace2,trace_step):
            xsec = ggsp.getmedprof(subim,subdq,f0=specfrac,f1=specfrac+trace_step)
            vsec = ggsp.getmedprof(subvar,subdq,f0=specfrac,f1=specfrac+trace_step)
            (pktry,pwtry,Proftry)=ggsp.getprof(xsec,vsec,offset,width_pix,dx,dwfixed,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)
            dxarray=np.append(dxarray,offset+dx[pktry])
            dwarray=np.append(dwarray,dw[pwtry])
            pixval=(specfrac+trace_step/2.)*subim.shape[1]
            pixarray=np.append(pixarray,pixval)
            profarray=np.append(profarray,Proftry)
        profarray=np.reshape(profarray,[-1,Proftry.size])
        x_model=[]
        y_model=[]
        if traceorder< 0:
            dofit=False
            fixedvalue=-float(traceorder)
            if fixedvalue==99:fixedvalue=offset
        if dofit is False:
            order=0
            coeff2=[fixedvalue]
            print fixedvalue,coeff2
            bestfit2 = np.polyval(coeff2,pixarray) 
        else:
            if traceorder == 'recall':
                # In the first instance, try to read coefficients from existing file.
                if cnt==1:
                    coeff2=np.array([])
                    try:
                        outim,hdx = fits.getdata(onedfile,header=True,ext=('sci',i))
                        if hdx['TCOEF2']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF2']))
                        if hdx['TCOEF1']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF1']))
                        if hdx['TCOEF0']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF0']))
                        order=np.size(coeff2)-1
                        print "Using existing fit of order ",order," and coefficients ",coeff2
                        bestfit2 = np.polyval(coeff2,pixarray) 
                    except:
                        print'Error: No trace coefficients from before. '
                        traceorder=0
                        order=0
                        (bestfit2,coeff2,x_model,y_model)=ggsp.getFit(pixarray,dxarray,order)
                        # If user has asked to refit, use provided order
                else:
                    print "Refitting with order ",order
                    (bestfit2,coeff2,x_model,y_model)=ggsp.getFit(pixarray,dxarray,order)  
            else:
                if refit=='yes': order=int(traceorder)
                (bestfit2,coeff2,x_model,y_model)=ggsp.getFit(pixarray,dxarray,order)
        #trace=interp1d(pixarray,offset+bestfit2,bounds_error=False)
        postrace=np.polyval(coeff2,np.arange(subim[0,:].size))
        #postrace=trace(np.arange(subim[0,:].size))+1
        negtrace=postrace+nodpix
        (exdata,exvar,exdq,expos,exneg)=ggsp.SpExtract(subim,subvar,subdq,weight_method,postrace,width_pix+dw[pw],nodpix,slittrim,reverse_NODAB=reverse_NODAB)
        exdq=exdq.astype('int16')
        badreg=np.where(np.abs(expos-exneg) > 5.*np.sqrt(exvar))
        #for p,n,d,v in zip(expos[badreg],exneg[badreg],np.sqrt(exvar[badreg]),(expos[badreg]-exneg[badreg])/np.sqrt(exvar[badreg])):
        #    print p,n,d,v
        exvar[badreg]=exvar[badreg]+(expos[badreg]-exneg[badreg])**2.
        target=np.where(mdf['extver']==i)
        RA=mdf['RA'][target][0]*15.
        DEC=mdf['DEC'][target][0]
        radec=np.array([[RA,DEC]])
        pixels=imagewcs.wcs_world2pix(radec,0)
        # I don't know why x and y are reversed here!  Works for GMOS-N Hamamatsu data
        yccd=pixels[0][0]
        xccd=pixels[0][1]
        #yccd=mdf['x_ccd'][target][0]
        #xccd=mdf['y_ccd'][target][0]
        #xccd=mdf['x_ccd'][i-1]
        #yccd=mdf['y_ccd'][i-1]
        defstampsize=5/preim_xscale*np.array([1,1])
        centralstampsize=1/preim_xscale*np.array([1,1])
        ximage=[int(np.max([xccd-defstampsize[0]/2+.5,0])),int(np.min([xccd+defstampsize[0]/2+.5,np.shape(preim)[0]]))]
        yimage=[int(np.max([yccd-defstampsize[1]/2+.5,0])),int(np.min([yccd+defstampsize[1]/2+.5,np.shape(preim)[1]]))]
        xcent=[int(np.max([xccd-centralstampsize[0]/2+.5,0])),int(np.min([xccd+centralstampsize[0]/2+.5,np.shape(preim)[0]]))]
        ycent=[int(np.max([yccd-centralstampsize[1]/2+.5,0])),int(np.min([yccd+centralstampsize[1]/2+.5,np.shape(preim)[1]]))]
        
        #print ximage,yimage,xcent,ycent
        xstamp=xccd-ximage[0]
        ystamp=yccd-yimage[0]
        if showplots is True:
            fig,axarr=plt.subplots(6,gridspec_kw={'height_ratios':[2,1,1,1,1,1]})
            #print "ID: ",mdf['id'][target][0],"ZMAG=",mdf['mag'][target][0],"priority=",mdf['priority'][target][0]
            ax=fig.add_subplot(1,1,1)
            ax.axis('off')
            ax.text(0,1.,"Slit: "+str(i)+" ID: "+str(mdf['id'][target][0]),transform=ax.transAxes)
            ax.text(0,.95,"ZMAG: "+str(mdf['mag'][target][0]),transform=ax.transAxes)
            ax.text(0,.9,"priority: "+str(mdf['priority'][target][0]),transform=ax.transAxes)
            #ax.text(0,1.,'ID='+str(mdf['id'][target][0]),transform=ax.transAxes)
            #ax.text(0,.85,'ZMAG='+str(np.round(mdf['mag'][target][0],1)),transform=ax.transAxes)
            #ax.text(0,0.7,'Priority='+str(np.round(mdf['priority'][target][0],2)),transform=ax.transAxes)
            # Top figure:  postage stamp of target from preimage  
            preim_sub=preim[ximage[0]:ximage[1],yimage[0]:yimage[1]]
            try:
                minval=np.nanmin(preim_sub)
    #            maxval=np.nanmax(preim_sub)
                maxval=np.nanmax(preim[xcent[0]:xcent[1],ycent[0]:ycent[1]])
                vmin=minval
                vmax=0.99*maxval
                #print minval,maxval
                #print vmin,vmax
                #minval=30500
                #maxval=32000
                axarr[0].imshow(preim_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
                axarr[0].add_patch(patches.Rectangle(
                    (xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale),
                    1/preim_xscale,
                    3/preim_yscale,
                    fill=False,
                    edgecolor='red')
                )
                axarr[0].add_patch(patches.Rectangle(
                    (xstamp-1/2./preim_xscale,ystamp-9./4/preim_yscale),
                    1/preim_xscale,
                    3/preim_yscale,
                    fill=False,
                    edgecolor='yellow')
                )
                axarr[0].scatter(xstamp,ystamp,marker='x')
                axarr[0].set_xlim(0,int(defstampsize[0]))
                axarr[0].set_ylim(0,int(defstampsize[1]))
            except:
                print 'Unable to load image'
            # Figure: 2D dispersed image 
            if mosaic_exists=='yes':
                secy1=flatmosaic['SECY1'][target][0]
                secy2=flatmosaic['SECY2'][target][0]
                secx1=flatmosaic['SECX1'][target][0]-redpixshift+0
                secx2=flatmosaic['SECX2'][target][0]-bluepixshift+0
                specx=int(0.5*(secx1+secx2))
                #print bluepixshift,redpixshift
                #print spTrimBluelimitpix,secx1,secx2,specx
                #specy=int(0.5*(secy1+secy2))
                specy=secy1
                #print specx,specy
                #defmossize=np.array([160,1500])
                #defmossize=np.array([1500,160])
                yscale=1
                if options.prefix=='cs_':yscale=2
                defmossize=np.array([int(secx2-secx1),160/yscale])
                # Something is screwy when doing with preimages.  x and y swapped or something? 
                relpixscale=1.
                #if re.compile('SXDF').match(cname) or re.compile('COS').match(cname): relpixscale=2.
                # This didn't work for 17A GMOS-N Hamamatsu
                #xmosimage=[int(np.max([xccd/relpixscale-defmossize[0]/2,0])),int(np.min([xccd/relpixscale+defmossize[0]/2,np.shape(scimosaic)[0]]))]
                #ymosimage=[int(np.max([yccd/relpixscale-defmossize[1]/2,0])),int(np.min([yccd/relpixscale+defmossize[1]/2,np.shape(scimosaic)[1]]))]
                xmosimage=[int(np.max([specx/relpixscale-defmossize[0]/2,0])),int(np.min([specx/relpixscale+defmossize[0]/2,np.shape(scimosaic)[0]]))]
                ymosimage=[int(np.max([specy/relpixscale-defmossize[1]/2,0])),int(np.min([specy/relpixscale+defmossize[1]/2,np.shape(scimosaic)[1]]))]
                mosaic_sub=scimosaic[xmosimage[0]:xmosimage[1],ymosimage[0]:ymosimage[1]]
                #print specx,specy,np.shape(scimosaic),np.shape(mosaic_sub)
                #print xmosimage
                #print ymosimage
                vmin=-1000
                vmax=1000
                #mosaic_sub=np.transpose(mosaic_sub[:][::-1])[::-1]
                mosaic_sub=np.transpose(mosaic_sub[:][::-1])
                #fig3,axarr3=plt.subplots(1)
                #axarr3.imshow(mosaic_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
                axarr[1].imshow(mosaic_sub,aspect='auto',vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'),origin='lower')
                #axarr[4].set_xlim()
                #thismanager=plt.get_current_fig_manager()
                #thismanager.resize(2400,160)
                #plt.show(block=False)
            # Figure: Extracted spectrum
            axarr[2].plot(ll,expos,c='r',linewidth=0.5)
            axarr[2].plot(ll,exneg,c='b',linewidth=0.5)
            #axarr[2].plot(ll,exdata,c='k')
            #baddata=np.where(np.sqrt(exvar)/np.abs(exdata)>10)
            #print ll[baddata],exvar[baddata],exdata[baddata],np.sqrt(exvar[baddata]/exdata[baddata])
            #gooddata=np.where(np.sqrt(exvar)/np.abs(exdata)<=10)
            axarr[2].plot(ll,exdata,c='k',linewidth=1.5)
            axarr[2].scatter(ll[badreg],exdata[badreg],c='y',s=40)
            axarr[2].set_xlim(ll[0],ll[-1])
            #axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
            axarr[2].set_ylim(np.nanmean(exdata[:-200])-2.*np.nanstd(exdata[:-200]),np.nanmean(exdata[:-200])+2.*np.nanstd(exdata[:-200]))
            #axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
            # Figure: spatial profile
            ypix=np.arange(np.size(xsec_avg[slittrim:-slittrim]))
            axarr[3].plot(ypix+slittrim,xsec_avg[slittrim:-slittrim])
            axarr[3].plot(ypix+slittrim,useProf[slittrim:-slittrim])
            extent=[ll[0],ll[-1],1,20]
            # Figure: 2D sky-subtracted slit with trace
            maxval=0.99*np.nanmax(np.abs(xsec_avg[2:18]))
            axarr[4].imshow(subim[:,:],extent=extent,origin='lower',aspect=10,vmax=maxval,vmin=-maxval,cmap=plt.get_cmap('gray'))
            if np.size(userbad)>0: axarr[4].imshow(3.*np.ma.masked_where(subdq==0,subim[:,:]),alpha=1.,extent=extent,origin='lower',aspect=10,vmax=maxval,vmin=-maxval,cmap=plt.get_cmap('seismic'))
            axarr[4].plot(ll,postrace,'r')
            axarr[4].plot(ll,negtrace,'r--')
            axarr[4].set_xlim(ll[0],ll[-1])
            # Figure: trace position of positive flux
            axarr[5].plot(pixarray,dxarray, 'ro')
            try:
                axarr[5].plot(x_model,y_model,'bo')
            except:
                pass
            axarr[5].plot(pixarray,bestfit2,'b')
            axarr[5].set_xlim(0,ll.size)
            axarr[5].set_ylim(offset-dxmax,offset+dxmax)
            thismanager=plt.get_current_fig_manager()
            thismanager.resize(1200, 1000)
            #thismanager.window.SetPosition((500,0))
            plt.show(block=False)
            #fig2,axarr2=plt.subplots(1)
            #preim_sub=preim[ximage[0]:ximage[1],yimage[0]:yimage[1]]
            #minval=np.nanmin(preim_sub)
            #maxval=np.nanmax(preim_sub)
            #vmin=minval
            #vmax=0.01*maxval
            #print minval,maxval
            #print vmin,vmax
            ##minval=30500
            ##maxval=32000
            #axarr2.imshow(preim_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
            #axarr2.add_patch(patches.Rectangle(
            #    (xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale),
            #    1/preim_xscale,
            #    3/preim_yscale,
            #    fill=False,
            #    edgecolor='red')
            #)
            #print preim_xscale,preim_yscale
            #print xstamp,ystamp,xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale
            #axarr2.scatter(xstamp,ystamp,marker='x')
            #axarr2.set_xlim(0,int(defstampsize[0]))
            #axarr2.set_ylim(0,int(defstampsize[1]))
            #thismanager=plt.get_current_fig_manager()
            #thismanager.resize(250,250)
            #plt.show(block=False)
            print "Slit ",i,":"
            #print "ID: ",mdf['id'][target][0],"ZMAG=",mdf['mag'][target][0],"priority=",mdf['priority'][target][0]
            inputerror=True
            while inputerror == True:
                print "Current trace parameters: ",order,trace1,trace2,trace_step
                #print "Current coefficients: ",coeff2
                refitpars=raw_input('Redefine? (n/NO or parameters): ')
                try:
                    refitinput=refitpars.split()[0]
                except:
                    refitinput='n'
                if refitinput == 'N' or refitinput == 'n' or refitinput == 'no':
                    refit='no'           
                    inputerror = False
                elif refitinput == 'NO':
                    refit='no'
                    showplots=False
                    inputerror = False
                else:
                    refit='yes'
                    try:
                        if float(refitinput)<0:
                            order=0
                            dofit=False
                            fixedvalue=-float(refitinput)
                            if fixedvalue==99:fixedvalue=offset
                        else:
                            dofit=True
                            if traceorder=='recall':
                                order=int(refitinput)
                            else:
                                traceorder=refitinput
                        if np.size(refitpars.split()) >= 2: trace1=float(refitpars.split()[1])
                        if np.size(refitpars.split()) >= 3: trace2=float(refitpars.split()[2])
                        if np.size(refitpars.split()) >= 4: trace_step=float(refitpars.split()[3]) 
                        inputerror=False
                    except:
                        inputerror=True
                if trace2<=trace1: inputerror=True
                if trace1<0 or trace2>1:inputerror=True
                if trace_step<=0 or trace_step>1:inputerror=True
            inputerror=True
            while inputerror == True:
                userbadinput=raw_input('Bad regions in pixels: ('+str(badarray)+')')
                if userbadinput=='':
                    # Use existing userbad array
                    inputerror=False
                else:
                    try:
                        badarray=np.array(userbadinput.split()).astype(int)
                        Nbadreg=np.size(badarray)/4
                        for ibad in np.arange(Nbadreg):
                            (x1,x2,y1,y2)=badarray[ibad*4:ibad*4+4]
                            if ibad==0:
                                condition=((dqgrid[1]>x1)& (dqgrid[1]<x2) & (dqgrid[0]>y1) & (dqgrid[0]<y2))
                            else:
                                condition=(condition) | (dqgrid[1]>x1)& (dqgrid[1]<x2) & (dqgrid[0]>y1) & (dqgrid[0]<y2)
                            userbad=np.where(condition)
                        refit='yes'
                        inputerror=False
                    except:
                        print 'Input error'
                        pass
            plt.close('all')
        else:
            refit='no'
    #######################################################################
    #######################################################################
    header=fits.Header()
    header['CRVAL1']=(hd['crval1'],'Wavelength at Ref pix in Angstroms')
    header['CRPIX1']=(hd['crpix1'],'Ref pix of axis 1')
    header['CD1_1']=(hd['cd1_1'],'WCS matrix element 1 1')
    header['EXTVER']=(i,'Extension version')
    header['MDFROW']=(hd['MDFROW'],'Corresponding row in MDF')
    header['SPEXVER']=(Version,'Version of ggextract')
    header['TWIDTH']=(width_pix+dw[pw],'Width in pixels of Gaussian used for extraction')
    header['NEGOFF']=(nodpix,'Separation in pix of n&s spectra')
    header['XMETHOD']=(weight_method,'Spectral extraction method')
    header['TORDER']=(order,'Order of polynomial fit to spectral trace')
    header['TCOEF0']=(coeff2[-1],'0th order coefficient of polynomial fit')
    try:
        header['USERBAD']=' '.join(badarray.astype('str')),'Pixel ranges in 2D identified as bad during extraction'
    except:
        pass
    try:
        header['TCOEF1']=(coeff2[-2],'1st order coefficient of polynomial fit')
    except:
        header['TCOEF1']=(0,'1st order coefficient of polynomial fit')
    try:
        header['TCOEF2']=(coeff2[-3],'2nd order coefficient of polynomial fit')
    except:
        header['TCOEF2']=(0,'2nd order coefficient of polynomial fit')
    hdul.append(fits.ImageHDU(data=exdata,header=header,name='SCI'))
    hdul.append(fits.ImageHDU(data=exvar,header=header,name='VAR'))
    hdul.append(fits.ImageHDU(data=exdq,header=header,name='DQ'))
    hdul2d.append(fits.ImageHDU(data=subim,header=hd,name='SCI'))
    hdul2d.append(fits.ImageHDU(data=subvar,header=varhd,name='VAR'))
    hdul2d.append(fits.ImageHDU(data=subdq,header=dqhd,name='DQ'))
hdulinput.close()
hdul.writeto(onedfile,clobber=True)
hdul2d.writeto(twodfile,clobber=True)
