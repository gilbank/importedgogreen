#! Python program to format the data for runz
# Usage
# GoGREEN_format4RUNZ.py filename.fits
#
# Produces a file called filename.list that is then used with runz
# After running with runz, a file with filenamez_CLi.rz is produced.
#
# Version history
#
# 31/05/2015
# Included an option to copy it to an external directory
#
# 20/06/2015
# Added the z-band magnitude to the FITS header
# Added an option to then add this to the runz file after the
# redshifting has been done
#
# 22/06/2015
# Accounted for duplicate names
#
# 17/03/2016
# Added the option for the user to specify the 2d filename
# The ordering of the extensions in the combined files differs, so we've added an option
# to the program which lets the program know that it is examining a
# combined file
#
# 01/12/2017
# Added try-except blocks around the unzipping of files
#

import sys
import astropy.io.fits as fits
import numpy
from optparse import OptionParser
import os,shutil
import subprocess as sp

parser=OptionParser()
        
parser.add_option("-i", "--inputFile", dest="inputFile",
                  default=None, help="Input file")

parser.add_option("-t", "--twodFile", dest="twodFile",
                  default=None, help="2d input file")

parser.add_option("-d", "--dir", dest="dir",
                  default='.', help="Output directory")

parser.add_option("-z", "--runzFile", dest="runzFile",
                  default=None, help="runz file")

parser.add_option("-c", "--combined", dest="combined",
                  default=False, action='store_true', help="Use this flag if the file is a combined file")

(options, args) = parser.parse_args()

if options.runzFile!=None:
    # Inserting r-band magnitudes into the runz file
    runz=open(options.runzFile)
    lines=runz.readlines()
    runz.close()
    output=open(options.runzFile,'w')
    for line in lines[0:3]:
        output.write(line)
    output.write(lines[3:4][0].replace('Rmag','zmag'))
    for line in lines[4:]:
        if options.combined:
            zmag=fits.getval(line.split()[1].strip(),'MAG',0)
            name=line.split()[1].strip().replace('object_','').replace('.fits','')
            output.write('%s%-30s%s%5.2f%s' % (line[0:4],name,line[34:62],zmag,line[67:]))
        else:
            zmag=fits.getval('object_'+line[4:24].strip()+'.fits','ZMAG',0)
            output.write('%s%5.2f%s' % (line[0:62],zmag,line[67:]))
        
    output.close()
    exit()

repository=os.environ['GoGREEN_data']+'/repository/'
intputDir,input=os.path.split(repository+options.inputFile)
outputDir=options.dir

# The files in the GoGREEN data repository are gzipped
# We want to keep the repository pure since it is a git repository
# 1d extracted spectra
shutil.copy(repository+options.inputFile,input)
try:
    sp.call('gunzip '+input,shell=True)
except:
    pass

# 2d sky-subtracted data
if options.twodFile==None:
    twodspectra=repository+options.inputFile.replace('tcal_oned','twod')
else:
    twodspectra=repository+options.twodFile
shutil.copy(twodspectra,os.path.split(twodspectra)[1])
try:
    sp.call('gunzip '+os.path.split(twodspectra)[1],shell=True)
except:
    pass
    
input=input.replace('.gz','')
data=fits.open(input)

# The first extension on the input file is a table containing information about the
# targets in the slits
# The resulting extensions are group together in three. Within each
# group, one has the data, the variance and the data quality

# There is one output file for each object
# Stars are skipped in the FITS file, but not in the FITS table, so we
# need to match against object the MDF row
# Spectrum goes in extension 0, variance goes in extension 1. 


nSlit=0
filelist=open(input.replace('.fits','.list'),'w')

if options.combined:
    translate={'RA':'RA','DEC':'DEC','ID':'ID','MAG':'MAG'}
    start=1
    end=len(data)-1
    table=len(data)-1
else: 
    translate={'RA':'OBSRA','DEC':'OBSDEC','ID':'TARGET','MAG':'ZMAG'}
    start=2
    end=len(data)
    table=1


objectList=[]
for i in range(start,end,3):
    object=data[i:i+2]
    # Reset the variance
    # Anything with a variance that is more than above 1000. is set to
    # nan
    object[1].data[object[1].data > 1000.]=numpy.nan
    # Primary header
    prihdu = fits.PrimaryHDU(object[0].data)
    mdfrow=object[0].header['MDFROW']
    for keyword in ['CRVAL1','CRPIX1','CD1_1']:
        prihdu.header[keyword]=object[0].header[keyword]

    # In case of identical names
    if data[table].data['ID'][mdfrow-1] in objectList:
        name='%s_a' % (data[table].data['ID'][mdfrow-1])
    else:
        name='%s' % (data[table].data['ID'][mdfrow-1])

    objectList.append(data[table].data['ID'][mdfrow-1])
    prihdu.header[translate['ID']]=name

    for keyword in ['RA','DEC','MAG']:
        prihdu.header[translate[keyword]]=data[table].data[keyword][mdfrow-1]

    # Add the variance array to the header
    hdulist=fits.HDUList([prihdu,object[1]])
        
    output='object_%s.fits' % (name)

    filelist.write(output+'\n')
    hdulist.writeto(outputDir+'/'+output,clobber=True)
    nSlit+=1
    
data.close()

