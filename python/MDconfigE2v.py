# global parameters for Ham CCDs
import numpy as np

det='E2v'
preim='Y'


nSetupStars = 3
#setupStarScheme = 'random'
setupStarScheme = 'mindist'

# shouldn't be used any more:
photfileExpr = "'%s/Data/reduced/%s/Catalogues/Masks/%s_phot.fits'%(os.getenv('GoGREEN_data'),name,name)"
#masterfileExpr = "os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'"

#### need to update for Ham
# global CCD parameters:
""" Ham
ccd_dx=2048./2.0
ccd_xgap=64./2.0 # may be too big from mosaic?
ccd_dy=4068./2.0

ccd_cx=(2.*(ccd_dx+ccd_xgap)+ccd_dx)/2.
ccd_cy=ccd_dy/2.

x0=415
x1=2404
y0=60
y1=2043
####

pxscale = 0.08*2.0 # GMOS Ham binned        
"""

# global CCD parameters:
# e2v from: ../maskDesTests/MyCode/maskdes.py
pxscale = 0.073*2.0 # GMOS e2V binned        
ccd_dx=2048./2.0
ccd_xgap=67./2.0
ccd_dy=4608./2.0

ccd_cx=(2.*(ccd_dx+ccd_xgap)+ccd_dx)/2.
ccd_cy=ccd_dy/2.

x0=460
x1=2665
y0=63
y1=2238
# GMOS field limits:
yminField = 70.0
ymaxField = 2247.0
xminSField = -192. # spec field
#xminSField = -1750.0 # spec field
xmaxSField = 3322.
xminField = 450.0 # imaging field
xmaxField = 2660.0
#


# ** NOTE: padPix is TOTAL padding, i.e. padPix/2.0 at each side of slit (in 'working' pixels, i.e. 2x2 binned for GoGREEN)**
# changed from 1.0 to 2.0 pixels, 17 April 2015
xpadPix=2.0
ypadPix=2.0




# GMOS field limits:
yminField = np.copy(y0)
ymaxField = np.copy(y1)
xminSField = 0. # spec field #*** update. grating, cenwave specific?
xmaxSField = 3126. #***
xminField = np.copy(x0) # imaging field
xmaxField = np.copy(x1)


# ---- Move global mask design parameters here too:
iRadialWt = False

#allocateSchemes = ['first', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt'] # Default
#allocateSchemes = ['bandbri', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt']
#allocateSchemes = ['fillfaint', 'rptfnt', 'rptfnt', 'rptfnt','bandbri','bandbri'] # BandEnd/
allocateSchemes = ['fillfaint', 'rptfnt', 'rptfnt', 'rptfnt','bandbri','bandbri'] # All faint/


nSingleIter=10 # number of iterations to run on each INDIVIDUAL mask (each call to maskdesHam)
nMultiIter=50   # No of iterations to run on each set of nMasks. This is probably most useful number to optimisation
plotFlag=False
#sortScheme='wts'
sortScheme='shuffle'
#sortScheme='shufflewts'

nomsLen = (6.132/pxscale) # whole number of unbinned pixels (42)
nomsWid = (1.0/pxscale)
nomsTilt = 0.0

#starsLen = (4.0/pxscale) # check (2"x2" box)
#starsWid = (2.0/pxscale)

starsLen=np.copy(nomsLen)
starsWid=np.copy(nomsWid)



NSFlag = True 
# shuffle amount needs to be a whole number of unbinned pix. We are working in 2x2 binnned pixels here
#tshuffleOffPix = -nomsLen/8.0   # subtract object position from yobj. This assumes shuffle buffer is ABOVE object and nod direction is 
                            # DOWNWARDS by 1/2 actual slit length = **1/4** virtual slit length used here (i.e. 0.75" for 3" real slit
                            # =6" virtual slit.

#tshuffleOffPixUnbinned = np.ceil(tshuffleOffPix*2.0)
#shuffleOffPix = tshuffleOffPixUnbinned/2.0

# CAREFUL: apply this shift to galaxies only NOT SETUP STARS!!!!
shuffleOffPix=0.0 # make virtual slits same length for setup stars and gals, therefore no offset!



# z magnitude bins
zFnt = np.array((23.5, 24.25))
##zBri = np.array((23.0, 23.5))
zBri = np.array((21.0, 23.5))
#zBri = np.array((20.0, 23.5))
zVBri = np.array((0.0, 23.0))
#zVBri = np.array((20.0, 23.0))
#zVBri = np.array((19.0, 20.0))
#zVFnt = np.array((24.25, 30.0))
zVFnt = np.array((24.25, 24.50))
irac1Lim=22.5
irac1VFnt=23.0


# blue foreground exclusion cut:
slope = -1.5/3.5
intcpt = 2.0 - slope*19.0
blueCoeffs = np.array((slope,intcpt))
#(irac1, z-irac1)

# RS coeffs are now stored in fInfo

dRed=0.2    # first limit past red sequence. Start to downweight gals past here
dRedLim=0.5 # ultimate limit past red-sequence. set weights to zero above this


#GMOS e2v:
cenwave=850 ; linDisp=0.35762352242772855 ; WLcoeff=np.array([-0.077888, 170.010872])
#cenwave=800 ; linDisp=0.35344379303792917 #; coeff=np.array([-0.050880, -12.860031])
#cenwave=750 ; linDisp=0.35402666435139885 ; coeff=np.array([-0.054557, -148.224445])


blueCutA = 700.0
redCutA = 1045.0

