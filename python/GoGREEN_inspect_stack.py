from optparse import OptionParser
from astropy.io import fits,ascii
from astropy import wcs
import numpy as np
import os
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
import getopt
import re
import matplotlib.ticker as plticker
from astropy.convolution import convolve, Box1DKernel
import GoGREEN_plots as gp
import Constants
import GoGREEN_Library as GoGREEN
import GoGREEN_spectra as ggsp

parser=OptionParser()
parser.add_option("-v", "--version", dest="version",
				  default='0.3', help="Data release version")
(options, args) = parser.parse_args()
constants=Constants.GoGREENPath()
dir=constants.reduced+"/Finalspec/v"+options.version+'/'

try:
	cname=str(args[0])
except:
	cname = raw_input('Cluster name? e.g. SXDF64')
try:
	maglim=float(args[1]) # Only inspect galaxies fainter than this limit
except:
	maglim=0
try:
	qop_only=int(args[2])# Only inspect galaxies with z quality equal to this value
except:
	qop_only=-1
# Set maglim to only inspect spectra fainter than this limit.
#maglim=23.5
#maglim=0
Nmaskmax=6
#if mode is 'inspect' or mode is 'expert':
#	flagfile=dir+cname+'.flags'
#	f = open(flagfile, 'wb')
onedprefix='_oned'
onedfile=dir+cname+onedprefix+".fits"
if not os.path.isfile(onedfile):
	onedfile=onedfile+'.gz'
try:
	mdf,mdfhd = fits.getdata(onedfile,header=True,ext=('mdf',1))
	hdulinput=fits.open(onedfile)
except:
	print 'No fits file ',onedfile
	sys.exit()
twodprefix='_twod'
twodfile=dir+cname+twodprefix+".fits"
if not os.path.isfile(twodfile):
	twodfile=twodfile+'.gz'
try:
	twodhduinput=fits.open(twodfile)
except:
	print 'No fits file ',twoddfile
	sys.exit()
header_primary=hdulinput[0].header

nsciext=header_primary['NSCIEXT']
header_primary['NEXTEND']=1+3*nsciext
#if ((header_primary['NODAYOFF']<0) | (header_primary['INSTRUME']=='GMOS-N')):
if ((header_primary['NODAYOFF']<0) | (header_primary['DETECTOR']=='GMOS + e2v DD CCD42-90')):
	reverse_NODAB=True
	#reverse_NODAB=False
else:
	reverse_NODAB=False

# Get Lidman redshifts file
try:
	CLi_redshift_file=dir+'Redshifts/'+cname+'/v0.3/'+cname+'_mz_CLi.mz'
	CLi_z=ascii.read(CLi_redshift_file,delimiter=',',names=('ID','Name','RA','DEC','Mag','Type','AutoTID','AutoTN','AutoZ','AutoXCor','FinTID','FinTN','FinZ','QOP','Comment'))
except:
	print 'No redshift information available'
# Get Cooper redshifts file
try:
	masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
	mdat = fits.getdata(masterfile)
except:
	print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
	print "Stopping"
	stop()
cluster_redshift=mdat['Redshift'][np.where(mdat['cluster']==cname)]
#preimage_root=mdat['Preim_name'][np.where(mdat['cluster']==cname)]
preimage_root=mdat['z_image'][np.where(mdat['cluster']==cname)]
imdir=os.getenv('GoGREEN_data')+'/Data/reduced/'+cname+'/Imaging/GMOS/Z/'
preimage=imdir+preimage_root[0]+'.fits.gz'
if not os.path.isfile(preimage):
	preimage=preimage.replace('.gz','')
try:
	preim,preim_header=fits.getdata(preimage,header=True)
	imagewcs=wcs.WCS(preim_header)
except:
	print 'No preimage ',preimage
	sys.exit()
#preim_xscale=np.max([np.abs(preim_header['CD1_1']),np.abs(preim_header['CD1_2'])])*3600
#preim_yscale=np.max([np.abs(preim_header['CD2_1']),np.abs(preim_header['CD2_2'])])*3600
preim_xscale=(np.abs(preim_header['CD1_1'])+np.abs(preim_header['CD1_2']))*3600
preim_yscale=(np.abs(preim_header['CD2_1'])+np.abs(preim_header['CD2_2']))*3600
#print preim_xscale,preim_yscale
preim=preim-np.median(preim)

# For later implementation:
#lisfile=dir+maskname+'.lis'
#ref_sciimage=ascii.read(lisfile,data_start=0)[0][0]
#ref_flatimage=ascii.read(lisfile,data_start=0)[0][1]
#try:
#	scimosaic_fn=dir+'mgs'+ref_sciimage+'.fits.gz'
# 	flatmosaic_fn=dir+'rg'+ref_flatimage+'_comb.fits.gz'
# 	if not os.path.isfile(scimosaic_fn):
# 		scimosaic_fn=scimosaic_fn.replace('.gz','')
# 	if not os.path.isfile(flatmosaic_fn):
# 		flatmosaic_fn=flatmosaic_fn.replace('.gz','')
# 	scimosaic=fits.getdata(scimosaic_fn,ext=('sci',1))
# 	flatmosaic,flathd=fits.getdata(flatmosaic_fn,header=True,ext=('MDF',1))
# 	scimosaic=np.transpose(scimosaic)
# 	mosaic_exists='yes'
# except:
# 	print 'No mosaic image',scimosaic_fn,' exists.  Ignoring.'
# 	mosaic_exists='no'
# try:
# 	snr_fn=dir+maskname+'.onedsn'
# 	sndat=ascii.read(snr_fn,data_start=2)
# 	snr_exists=True
# except:
# 	snr_exists=False
pixscale=header_primary['PIXSCALE']
nodpix=header_primary['NODAYOFF']*2/pixscale
nodpix=np.abs(nodpix)
try:
	if qop_only > 0: 
		qselect=(CLi_z['QOP']==qop_only)
	else:
		qselect=(CLi_z['QOP']>-1)
except:
	qselect=True
#irange=range(1,nsciext+1)
p1galaxies=np.where((mdf['priority']==1) & (mdf['MAGMASK']>=maglim) & qselect)
irange=mdf['EXTVER'][p1galaxies][np.argsort(mdf['MAGMASK'][p1galaxies])]
usercontinue=True
for i in irange:
	if usercontinue:
		target=np.where(mdf['extver']==i)
		id=mdf['ID'][target][0]
		try:
			CLi_redshift=CLi_z['FinZ'][np.where(CLi_z['Name']==id)][0]
			CLi_q=CLi_z['QOP'][np.where(CLi_z['Name']==id)][0]
		except:
			CLi_redshift=cluster_redshift
			CLi_q=0
		#print i,id,CLi_redshift,CLi_z['QOP'][np.where(CLi_z['Name']==id)][0]
		spectrum=hdulinput['sci',i].data
		twod=twodhduinput['sci',i].data
		hd=hdulinput['sci',i].header
		var=hdulinput['var',i].data
		varhd=hdulinput['var',i].header
		dq=hdulinput['dq',i].data
		dqhd=hdulinput['dq',i].header
		pix=np.arange(1,hd['naxis1']+1)
		ll=hd['crval1']+(pix-hd['crpix1'])*hd['cd1_1']
		badreg=np.where(np.abs(spectrum/np.sqrt(var))>5)
		# --- Try to find best offset of profile pair within slit:####
		npix=spectrum.shape[0]
		# Centre of slit.  Convention is that index starts at 0 and references the bottom of the pixel
		#slit_centre=(npix/2.)
		# Hmm... why do I say that?  imshow treats index as representing centre of pixel.  Why doesn't that make sense?
		slit_centre=((npix-1)/2.)
		# Offset is expected position of the spectrum at the bottom of the slit
		offset=slit_centre-nodpix/2.0
		RA=mdf['RA'][target][0]*15.
		DEC=mdf['DEC'][target][0]
		radec=np.array([[RA,DEC]])
		pixels=imagewcs.wcs_world2pix(radec,0)
		#pixels=imagewcs.wcs_world2pix(radec,1)
		# I don't know why x and y are reversed here!  Works for GMOS-N Hamamatsu data
		yccd=pixels[0][0]
		xccd=pixels[0][1]
		#yccd=mdf['x_ccd'][target][0]
		#xccd=mdf['y_ccd'][target][0]
		defstampsize=5/preim_xscale*np.array([1,1])
		ximage=[int(np.max([xccd-defstampsize[0]/2+.5,0])),int(np.min([xccd+defstampsize[0]/2+.5,np.shape(preim)[0]]))]
		yimage=[int(np.max([yccd-defstampsize[1]/2+.5,0])),int(np.min([yccd+defstampsize[1]/2+.5,np.shape(preim)[1]]))]
		#ximage=[np.max([xccd-defstampsize[0]/2,0]),np.min([xccd+defstampsize[0]/2,np.shape(preim)[0]])]
		#yimage=[np.max([yccd-defstampsize[1]/2,0]),np.min([yccd+defstampsize[1]/2,np.shape(preim)[1]])]
		centralstampsize=1/preim_xscale*np.array([1,1])
		xcent=[int(np.max([xccd-centralstampsize[0]/2+.5,0])),int(np.min([xccd+centralstampsize[0]/2+.5,np.shape(preim)[0]]))]
		ycent=[int(np.max([yccd-centralstampsize[1]/2+.5,0])),int(np.min([yccd+centralstampsize[1]/2+.5,np.shape(preim)[1]]))]
		#xcent=[int(np.max([xccd-centralstampsize[0]/2,0])),int(np.min([xccd+centralstampsize[0]/2,np.shape(preim)[0]]))]
		#ycent=[int(np.max([yccd-centralstampsize[1]/2,0])),int(np.min([yccd+centralstampsize[1]/2,np.shape(preim)[1]]))]
		xstamp=xccd-ximage[0]
		ystamp=yccd-yimage[0]
		#xstamp=xccd-ximage[0]
		#ystamp=yccd-yimage[0]


		fig,axarr=plt.subplots(3,gridspec_kw={'height_ratios':[2,1,1,]})
		# Top figure:  postage stamp of target from preimage  
		preim_sub=preim[ximage[0]:ximage[1],yimage[0]:yimage[1]]
		minval=np.nanmin(preim_sub)
		maxval=np.nanmax(preim[xcent[0]:xcent[1],ycent[0]:ycent[1]])
		vmin=minval
		vmax=0.99*maxval
		#print minval,maxval
		#print vmin,vmax
		#minval=30500
		#maxval=32000
		axarr[0].axis('off')

		# if snr_exists:
		ax=fig.add_subplot(3,2,2)
		#thisgal=np.where(sndat['ID']==mdf['ID'][target])
		ax.scatter(mdf['MAGMASK'],mdf['SNR2'],facecolor='none',edgecolor='k',s=50)
		ax.scatter(mdf['MAGMASK'][target],mdf['SNR2'][target],c='r',s=50)
		ax.set_yscale('log')
		ax.get_yaxis().set_major_formatter(plticker.ScalarFormatter())
		ax.set_ylim(0.2,10)
		plt.ylabel ("S/N@850nm")
		plt.xlabel ("ZMAG")
		#plt.show(block=False)
		ax=fig.add_subplot(3,5,2)
		ax.axis('off')
		ax.imshow(preim_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
		ax.add_patch(patches.Rectangle(
			(xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale),
			1/preim_xscale,
			3/preim_yscale,
			fill=False,
			edgecolor='red')
		)
		ax.add_patch(patches.Rectangle(
			(xstamp-1/2./preim_xscale,ystamp-9./4/preim_yscale),
			1/preim_xscale,
			3/preim_yscale,
			fill=False,
			edgecolor='yellow')
		)

		ax.scatter(xstamp,ystamp,marker='x')
		ax.set_xlim(0,int(defstampsize[0]))
		ax.set_ylim(0,int(defstampsize[1]))
		ax=fig.add_subplot(3,5,1)
		ax.axis('off')
		ax.text(0,1.,'ID='+str(mdf['ID'][target][0]),transform=ax.transAxes)
		ax.text(0,.85,'ZMAG='+str(np.round(mdf['MAGMASK'][target][0],1)),transform=ax.transAxes)
		ax.text(0,0.7,'SNR='+str(np.round(mdf['SNR2'][target][0],2)),transform=ax.transAxes)
		ax.text(0,0.55,'Exposure time (s)='+str(int(mdf['Etime'][target][0])),transform=ax.transAxes)
		ax.text(0,0.4,'Z='+str(CLi_redshift),transform=ax.transAxes)
		ax.text(0,0.25,'ZQ='+str(CLi_q),transform=ax.transAxes)
		# Figure: Extracted spectrum
		smoothed=convolve(spectrum,Box1DKernel(5))
		axarr[1].plot(ll,spectrum+np.sqrt(var),c='yellow')
		axarr[1].plot(ll,spectrum-np.sqrt(var),c='yellow')
		axarr[1].plot(ll,spectrum,c='gray')
		axarr[1].plot(ll,smoothed,c='k')
		#axarr[1].plot(ll,spectrum,c='k')
		#axarr[1].scatter(ll[badreg],spectrum[badreg],c='y',s=20)
		axarr[1].set_xlim(ll[0],ll[-1])
		#axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
		y1=np.nanmean(spectrum[:-200])-2.*np.nanstd(spectrum[:-200])
		y2=np.nanmean(spectrum[:-200])+5.*np.nanstd(spectrum[:-200])
		llz=ll/(1.+CLi_redshift)
		ax2=axarr[1].twiny()
		ax2.plot(llz,smoothed,c='k')
		ax2.set_xlim(llz[0],llz[-1])
		axarr[1].set_ylim(y1,y2)
		y1=np.max([1.e-3,np.min(smoothed)])
		y2=1.2*np.max(smoothed)
		#ax.set_ylim(y1,y2)
		gp.markline(ax2,'MgII',2800,'above',llz,smoothed,y1,y2)
		#gp.markline(ax2,'[NeIII]',3869,'above',llz,smoothed,y1,y2)
		gp.markline(ax2,'[OII]',3727,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'H$\delta$',4102,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,'H$\gamma$',4341,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'',4341,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,'H$\epsilon$+H',3970,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'H',3970,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,'H$\zeta$',3889,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,'H$\eta$',3835,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,r"H$\theta$",3797,'below',llz,smoothed,y1,y2)
		#gp.markline(ax2,'H$\iota$',3770,'above',llz,smoothed,y1,y2)
		gp.markline(ax2,'',3889,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'',3835,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,"",3797,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'',3770,'above',llz,smoothed,y1,y2)
		gp.markline(ax2,'CaK',3934,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'G',4300,'below',llz,smoothed,y1,y2)
		gp.markline(ax2,'FeII',2730,'above',llz,smoothed,y1,y2)
		gp.markline(ax2,'FeII',2850,'above',llz,smoothed,y1,y2)
		#ax2.plot(llz,spectrum,c='k')
		##axarr[2].scatter(llz[badreg],spectrum[badreg],c='y',s=20)
		#axarr[2].set_xlim(llz[0],llz[-1])
		##axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
		#axarr[2].set_ylim(np.nanmean(spectrum[:-200])-2.*np.nanstd(spectrum[:-200]),np.nanmean(spectrum[:-200])+2.*np.nanstd(spectrum[:-200]))
			
		# Figure: 2D sky-subtracted slit with trace
		maxval=0.99*np.nanmax(np.abs(np.nanmedian(twod[2:8,950:1100],1)))
		extent=[ll[0],ll[-1],1,20]
		axarr[2].imshow(twod[:,:],extent=extent,origin='lower',aspect=10,vmax=maxval,vmin=-maxval,cmap=plt.get_cmap('gray'))
		axarr[2].set_xlim(ll[0],ll[-1])
		plt.show(block=False)

		thismanager=plt.get_current_fig_manager()
		thismanager.resize(1200, 1000)
		#thismanager.window.SetPosition((500,0))
		#fig2=plt.figure()
		#fig2.show()
		#print "Slit ",i,":"
		#print "ID: ",mdf['id'][target][0],"ZMAG=",mdf['MAGMASK'][target][0],"priority=",mdf['priority'][target][0]
		masks=np.array([])
		extensions=np.array([])
		for masknum in np.arange(Nmaskmax)+1:
			maskname=mdf['mask'+str(masknum)][target][0]
			maskext=mdf['EXTVER'+str(masknum)][target][0]
			if maskname is not '':
				masks=np.append(masks,maskname)
				extensions=np.append(extensions,maskext)
		mn=1
		for m in masks:
			print mn,': ',m
			mn=mn+1
		examine=True
		while examine:
			try:
				userinput=int(raw_input('Inspect mask number?  Enter to skip: ').split()[0])
			except:
				userinput=0
			if (userinput>0) and (userinput<=np.size(masks)):
				print 'Inspecting mask ',str(masks[userinput-1])
				ggsp.inspect(cname,masks[userinput-1],extensions[userinput-1])
			else:
				examine=False
			userinput='init'
		try:
			userinput=raw_input('Enter to continue. n/N to quit:').split()[0]
		except:
			userinput='y'
		if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': 
			usercontinue=False
		#try:
	   # 	userinput=raw_input('Continue? (n to quit): ').split()[0]
	   # 	if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': usercontinue=False
	   # except:
	   # 	pass
		#flagerror=True
		#while flagerror:
	#		userinput=raw_input('Flag? (integer or n/N to quit): ').split()[0]
#			if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': 
			# 	usercontinue=False
			# 	flagerror=False
			# else:
			# 	try:
			# 		flag=int(userinput)
			# 		flagerror=False
			# 	except:
			# 		flagerror=True
			# 	if not flagerror:
			# 		try:
			# 			comment=raw_input('Comment?')
			# 		except:
			# 			comment=''
			# 	f.write(str(i)+' '+str(flag)+' '+comment+'\n')
			#plt.close('all')
		plt.close('all')
	else:
		plt.close('all')
		#f.close()