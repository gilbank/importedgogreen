import os,glob
from optparse import OptionParser
from astropy.io import fits,ascii
import numpy as np
import time
import matplotlib.pyplot as plt
from scipy.signal import convolve2d
from GoGREEN_redfix_utils import *

def getskyinfo(mdat,skylines,logfile,skipfiles):
    lstring=np.array(['l%s' % (x,) for x in skylines])
    lstring=" ".join(map(str,lstring))    
    if os.path.isfile(logfile):        
        f=open(logfile,'a')
    else:
        f=open(logfile,'w')
        f.write('Cluster Maskname Nodpix Slit filename x_ccd y_ccd Date Airmass Grwlen residual residual_nc '+lstring+'\n')
    for iclus,cluster in enumerate(mdat['cluster']):
        for masknum in np.arange(nmaskmax):
            try:
                maskname=mdat['mask'+str(masknum+1)+'cut'][iclus]
                if np.core.defchararray.ljust(maskname,1) != 'G':
                    maskname='null'
                directory=os.getenv('GoGREEN_data')+'Data/reduced/'+cluster+'/Spectroscopy/'+maskname+'/'
            except:
                directory='null'
                pass
            if (os.path.isdir(directory) and (maskname is not '')):
                skyfiles=glob.glob(directory+'htgs*skyxgs*')
                for skyfile in skyfiles:
                    basename=os.path.basename(skyfile).split('.fits',1)[0]
                    if basename not in skipfiles:
                        scifile=skyfile.replace('sky','n')
                        try:
                            hdulist=fits.open(skyfile)
                            header_primary=hdulist[0].header
                            nsciext=header_primary['NSCIEXT']
                            nodpix=header_primary['NODPIX']
                            airmass=header_primary['AIRMASS']
                            grwlen=header_primary['GRWLEN']
                            #mdf = fits.getdata(skyfile,header=False,ext=('mdf',1))  
                            mdf = hdulist['mdf',1].data
                            #MJD-OBS keyword is missing from some files.
                            try:
                                date=header_primary['MJD-OBS']                    
                            except:
                                date=-999
                        except:
                            print 'Failed to open ',skyfile
                            nsciext=0
                        for i in range(1,nsciext+1):
                            line_strengths,res_strength,res_nc=getskylines(skyfile,scifile,i,skylines)
                            #x_ccd=mdf['SECX1'][np.where(mdf['EXTVER']==i)][0]
                            # Need a better measure of x_ccd
                            x_ccd=getxpos(cluster,maskname,os.path.basename(scifile),i,9800)
                            y_ccd=mdf['SPECCEN'][np.where(mdf['EXTVER']==i)][0]
                            outstr=' '.join(map(str,[cluster,maskname,nodpix,i,os.path.basename(skyfile),x_ccd,y_ccd,date,airmass,grwlen,res_strength,res_nc," "]))+' '.join(map(str,line_strengths))
                            print(outstr)
                            f.write(outstr+'\n')
            else:
                pass
    f.close()

def getskylines(skyfile,scifile,i,skylines):
    skyhdu=fits.open(skyfile)
    sky=skyhdu['sci',i].data
    skyhd=skyhdu['sci',i].header
    skydq=skyhdu['dq',i].data
    scihdu=fits.open(scifile)
    sci=scihdu['sci',i].data
    scidq=scihdu['dq',i].data
    header_primary=scihdu[0].header
    #sky,skyhd = fits.getdata(skyfile,header=True,ext=('sci',i))
    #skydq = fits.getdata(skyfile,header=False,ext=('dq',i))
    crval1=skyhd['CRVAL1']
    crpix1=skyhd['CRPIX1']
    cd1_1=skyhd['CD1_1']                       
    line_strengths=np.array([])
    central_row=int(np.shape(sky)[0]/2)
    dcr=1
    dp=3
    for line in skylines:
        cent_pix=int((line-crval1)/cd1_1+crpix1)
        p1=int(cent_pix-dp/2.)
        p2=int(cent_pix+dp/2.)
        dq=np.sum(skydq[central_row:central_row+dcr,p1:p2])
        if dq>0:
            line_strengths=np.append(line_strengths,np.nan)
        else:
            line_strengths=np.append(line_strengths,np.average(sky[central_row:central_row+dcr,p1:p2]))
    sci_nocont=remove_continuum_withfit(sci,skyhd,header_primary)
    skyintensity,ratio=calc_residual_ratio(sci,sky,rp1=2,rp2=4,sp1=16,sp2=18)
    skyintensity_nc,ratio_nc=calc_residual_ratio(sci_nocont,sky,rp1=2,rp2=4,sp1=16,sp2=18)
    pixels=np.arange(np.size(ratio))
    wavelength=(pixels-crpix1)*cd1_1+crval1
    wlimit=np.array([9800,10000])
    residual=np.mean(ratio[np.where((skyintensity>1500) & (skyintensity < 2500) & (wavelength > wlimit[0]) & (wavelength < wlimit[1]))])
    residual_nocont=np.mean(ratio_nc[np.where((skyintensity>1500) & (skyintensity < 2500) & (wavelength > wlimit[0]) & (wavelength < wlimit[1]))])
    return line_strengths,residual,residual_nocont
def analyze_lines(data,colx,skylines,nodpix):
    # Choose a strong line as the reference
    # First calculate the average ratio relative to this line, for all others
    # Calculate the residual relative to this ratio.  Identify lines with large residuals
    # Look for correlations between residuals
    refline='l9949'
    residual={}
    resref='l9673'
    #colx=residual[resref]
    for line in map(str,skylines):
        ratio=data['l'+line]/data[refline]
        mean=np.nanmean(ratio)
        std=np.nanstd(ratio)
        clipped_mean=np.nanmean(ratio[np.where(np.abs(ratio-mean)<2*std)])
        #print clipped_mean
        residual['l'+line]=ratio-clipped_mean
    m1=np.ma.masked_array(colx,np.isnan(colx))
    good=np.where((data['Nodpix']==nodpix))
    #good=np.where((data['Nodpix']==nodpix) & (data['x_ccd']<2000))
    outlier=np.where((colx>0.05) & (data['Nodpix']==nodpix))
    #print (np.unique(data['Maskname'][outlier]),np.unique(data['filename'][outlier]))
    for line in map(str,skylines):
        m2=np.ma.masked_array(residual['l'+line],np.isnan(residual['l'+line]))
        print line,np.ma.corrcoef(m1,m2,allow_masked=True)
        fig=plt.figure()
        ax=fig.add_subplot(1,1,1)
        ax.scatter(colx[good],residual['l'+line][good])
        ax.scatter(colx[outlier],residual['l'+line][outlier],c='r')
        x1=np.nanpercentile(colx[good],1.)
        x2=np.nanpercentile(colx[good],99.)
        y1=np.nanpercentile(residual['l'+line][good],1.)
        y2=np.nanpercentile(residual['l'+line][good],99.)
        ax.axis([x1,x2,y1,y2])
        fig.show()
        prmpt=raw_input('Next? ')
        if prmpt=='y':
            plt.close()
        else:
            stop()

def dostack(data,good,signal_constraint=np.array([0,1e6,9000,9250]),weight_wave=np.array([9800,10000])):
    cnt=0
    iter=0
    stack=None
    skystack=None
    oldfilename=None
    oldskyfilename=None
    stacklogfile=open("Stacklog"+time.strftime("%Hh%dd%mm%Y")+".log",'w')
    unstacklogfile=open("Unstacklog"+time.strftime("%Hh%dd%mm%Y")+".log",'w')
    hduloutput = fits.HDUList()
    for c,m,fsky,s in zip(data['Cluster'][good],data['Maskname'][good],data['filename'][good],data['Slit'][good]):
        fsci=fsky.replace('sky','n')
        filename=os.getenv('GoGREEN_data')+'Data/reduced/'+c+'/Spectroscopy/'+m+'/'+fsci
        skyfilename=os.getenv('GoGREEN_data')+'Data/reduced/'+c+'/Spectroscopy/'+m+'/'+fsky
        if not os.path.isfile(filename):
            filename=filename.replace('.gz','')
            fsci=fsci.replace('.gz','')
        if not os.path.isfile(filename):
            filename=filename.replace('.fits','.fits.gz')
            fsci=fsci.replace('.fits','.fits.gz')
        if not os.path.isfile(skyfilename):
            skyfilename=skyfilename.replace('.gz','')
            fsky=fsky.replace('.gz','')
        if not os.path.isfile(skyfilename):
            skyfilename=skyfilename.replace('.fits','.fits.gz')
            fsky=fsky.replace('.fits','.fits.gz')
        if not(fsci == oldfilename):
            try:
                hdulist.close()
            except:
                pass
            hdulist=fits.open(filename)
            oldfilename=fsci
        if not(fsky == oldskyfilename):
            try:
                hdusky.close()
            except:
                pass
            hdusky=fits.open(skyfilename)
            oldskyfilename=fsky
        iter=iter+1
        print 'Doing ',iter,' of ',np.size(good),':',c,m,fsky,s
        #subim,hd = fitpwds.getdata(filename,header=True,ext=('sci',s))
        subim=hdulist['sci',s].data
        hd=hdulist['sci',s].header
        #sky,skyhd = fits.getdata(skyfilename,header=True,ext=('sci',s))
        sky=hdusky['sci',s].data
        skyhd=hdusky['sci',s].header
        #dq = fits.getdata(filename,header=False,ext=('dq',s))
        dq=hdulist['dq',s].data
        # Need to be careful of the abs(subim)>100 limit.  Need to parameterise and enable checks.
        baddata=np.isnan(subim) | np.isnan(sky) | (abs(subim) > 100) | (sky < 0)
        dq[np.where(baddata)]=1
        subim_dq=np.ma.masked_array(subim,dq)
        sky_dq=np.ma.masked_array(sky,dq)
        signal_wave=signal_constraint[2:]
        signal_lowerlimit=signal_constraint[0]
        signal_limit=signal_constraint[1]
        if (iter==1):
            header_primary=hdulist[0].header
            crval1=hd['CRVAL1']
            crpix1=hd['CRPIX1']
            cd1_1=hd['CD1_1']
            signal_pix=((signal_wave-crval1)/cd1_1+crpix1).astype(np.int)
            weight_pix=((weight_wave-crval1)/cd1_1+crpix1).astype(np.int)
        signal=np.average(subim[0:int(subim.shape[0]/2.),signal_pix[0]-1:signal_pix[1]-1])
        meansky=np.ma.average(sky_dq[1:-1,weight_pix[0]:weight_pix[1]])
        if ((abs(signal)< signal_limit) & (abs(signal)> signal_lowerlimit) & (meansky > 0)):
            cnt=cnt+1
            #print cnt,signal
            if cnt==1:
                stack=subim_dq
                skystack=sky_dq
                weight=1./meansky
                diagnostic=stack.data[5,750]
                outstr=' '.join([c,m,str(s),fsci,str(signal),str(meansky),str(diagnostic),'\n'])
                stacklogfile.write(outstr)
            else:
                try:
                    stack=np.ma.dstack((stack,subim_dq))
                    skystack=np.ma.dstack((skystack,sky_dq))
                    weight=np.ma.append(weight,1./meansky)
                    diagnostic=np.ma.average(stack,weights=weight,axis=2).data[5,750]
                    outstr=' '.join([c,m,str(s),fsci,str(signal),str(meansky),str(diagnostic),'\n'])
                    stacklogfile.write(outstr)
                except:
                    print 'SOMETHING WRONG WITH',c,m,fsci
                    outstr=' '.join([c,m,str(s),fsci,str(signal),str(meansky),'\n'])
                    unstacklogfile.write(outstr)

        else:
            #print "Signal = ",signal,".  Skipping slit "
            outstr=' '.join([c,m,str(s),fsci,str(signal),str(meansky),'\n'])
            unstacklogfile.write(outstr)
            pass
    print 'Stacked ',cnt,' slits'
    stacklogfile.close()
    unstacklogfile.close()
    try:
        stack=np.ma.average(stack,weights=weight,axis=2)
        skystack=np.ma.average(skystack,weights=weight,axis=2)
        hdulist.close()
        hdusky.close()
        hduloutput.append(fits.PrimaryHDU(header=header_primary))
        hduloutput.append(fits.ImageHDU(data=stack.data,header=hd,name='SCI'))
        hduloutput.append(fits.ImageHDU(data=skystack.data,header=skyhd,name='SKY'))
        return hduloutput
    except:
        return None
        
def fixdata(Cluster,Maskname,Residual,wlimit=9600,nodpixtofix=38,telescopetofix='GN'):
    directory=os.getenv('GoGREEN_data')+'Data/reduced/'+Cluster+'/Spectroscopy/'+Maskname+'/'
    telescope=np.core.defchararray.ljust(Maskname,2)
    if (os.path.isdir(directory)):
        skyfiles=glob.glob(directory+'htgs*skyxgs*')
        for skyfile in skyfiles:
            scifile=skyfile.replace('sky','n')
            if not os.path.isfile(scifile):
                scifile=scifile.replace('.gz','')
            if not os.path.isfile(scifile):
                scifile=scifile.replace('.fits','.fits.gz')
            if not os.path.isfile(skyfile):
                skyfile=skyfile.replace('.gz','')
            if not os.path.isfile(skyfile):
                skyfile=skyfile.replace('.fits','.fits.gz')
            scifilename=os.path.basename(scifile)
            skyfilename=os.path.basename(skyfile)
            try:
                hdudata=fits.open(scifile)
                header_primary=hdudata[0].header
                nsciext=header_primary['NSCIEXT']
                nodpix=header_primary['NODPIX']                                   
                crval1=hdudata[2].header['CRVAL1']
                crpix1=hdudata[2].header['CRPIX1']
                cd1_1=hdudata[2].header['CD1_1']
                mdf=hdudata['mdf',1].data
                mdfhd=hdudata['mdf',1].header
                plimit=int((wlimit-crval1)/cd1_1+crpix1)
            except:
                print 'Error reading ',scifile,'.  Skipping.'
                nsciext=0
            if nodpix != nodpixtofix:
                #print 'Nodpix ',nodpix,' does not equal ',nodpixtofix,'.  Skipping',scifile
                nsciext=0
            if telescope != telescopetofix:
                #print 'Telescope ',telescope,' does not equal ',telescopetofix,'.  Skipping',scifile
                nsciext=0
            if nsciext>0:
                print 'Fixing ',scifile
                fixfile=directory+'rf'+scifilename.replace('.gz','')
                hdul = fits.HDUList()
                hdul.append(fits.PrimaryHDU(header=header_primary))
                hdul.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))
                hdusky=fits.open(skyfile)
            for i in range(1,nsciext+1):
                scidata=hdudata['sci',i].data
                scihd=hdudata['sci',i].header
                vardata=hdudata['var',i].data
                varhd=hdudata['var',i].header
                dqdata=hdudata['dq',i].data
                dqhd=hdudata['dq',i].header
                skydata=hdusky['sci',i].data
                scihd_fixed=scihd
                baddata=np.isnan(skydata) | (skydata<0) | (skydata > 5000)
                sky_dq=np.copy(dqdata)
                sky_dq[np.where(baddata)]=1
                master_residual=Residual['SCI'].data
                master_sky=Residual['SKY'].data
                imid=int(scidata.shape[0]/2.)
                skydata_masked=np.ma.masked_array(skydata,sky_dq)
                igood=np.arange(imid-2,imid+2)
                mean_sky=np.ma.mean(skydata_masked[igood,:],axis=0)
                mean_ressky=np.mean(master_sky[igood,:],axis=0)
                scale_factor=mean_sky.data/mean_ressky
                #kernel=np.ones((5))
                #kernel=kernel/np.size(kernel)
                #scale_factor_sm=np.convolve(scale_factor,kernel,mode='same')
                redfix=master_residual*scale_factor
                redfix[:,:plimit]=0
                #  subtract test from data.
                scidata_fixed=scidata-redfix
                #  Update variance
                #vardata_fixed=vardata+redfix
                vardata_fixed=vardata
                #  Store with new filename
                hdul.append(fits.ImageHDU(data=scidata_fixed,header=scihd_fixed,name='SCI'))
                hdul.append(fits.ImageHDU(data=vardata_fixed,header=varhd,name='VAR'))
                hdul.append(fits.ImageHDU(data=dqdata,header=dqhd,name='DQ'))
                hdul.append(fits.ImageHDU(data=redfix,header=scihd,name='REDFIX'))
            if nsciext>0:
                hdudata.close()
                hdusky.close()
                hdul.writeto(fixfile,overwrite=True,output_verify="fix+warn")
#  Recombine and reextract, possibly using IRAF.
# MAIN
#
skylines=np.array([9673,9689,9716,9792,9872,9913,9949,10013,10074,10122,10165,10178,10202,10215,10289,10368,10419,10468])
nmaskmax=6
#nodpix_tostack=38
nodpix_bandshuffle=1392

parser=OptionParser()
#parser.add_option("-s", "--scale", dest="scale",default='scale_by_key',help="scale_by_key, scale_by_column, or None")
(options, args) = parser.parse_args()
#option=options.scale

try:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mdat = fits.getdata(masterfile)
except:
    print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    print "Stopping"
    stop()
try:
    nodpix_tostack=int(args[0])
except:
    nodpix_tostack=raw_input('Value of nodpix to correct (38 or 40):')
try:
    Telescope=args[1]
except:
    Telescope=raw_input('Telescope: (GS or GN)')
try:
    logfile=str(args[2])
except:
    logfile=time.strftime("%Hh%dd%mm%Y")+".log"
    print 'Creating logfile ',logfile
try:
    clusters=np.array([args[3]])
    masks=np.array([args[4]])
except:
    clusters=np.array([])
    masks=np.array([])
    for iclus,cluster in enumerate(mdat['cluster']):
        for masknum in np.arange(nmaskmax):
            maskname=mdat['mask'+str(masknum+1)+'cut'][iclus]
            if np.core.defchararray.ljust(maskname,1) != 'G':
                maskname='null'
            directory=os.getenv('GoGREEN_data')+'Data/reduced/'+cluster+'/Spectroscopy/'+maskname+'/'
            if os.path.isdir(directory):
                clusters=np.append(clusters,cluster)
                masks=np.append(masks,maskname)

# Measure relevant info on sky lines
if os.path.isfile(logfile):
    olddata=ascii.read(logfile)
    skipfiles=[w.split('.fits',1)[0] for w in np.unique(olddata['filename'])]
else:
    skipfiles=np.array([])
getskyinfo(mdat,skylines,logfile,skipfiles)
data=ascii.read(logfile)
telescope=np.core.defchararray.ljust(data['Maskname'],2)
#analyze_lines(data,data['l9949'],skylines,nodpix_tostack)
#analyze_lines(data,data['x_ccd'],skylines,nodpix_tostack)
#

<<<<<<< HEAD
stackfile='stacked_'+str(Telescope)+'_nodpix_'+str(nodpix_tostack)+'.fits'
continuum_file='stacked_'+str(Telescope)+'_nodpix_'+str(nodpix_bandshuffle)+'.fits'
nocontfile='stacked_'+str(Telescope)+'_nodpix_'+str(nodpix_tostack)+'_continuum_removed.fits'
#continuum_file='stacked_GS_continuum.fits.gz'
#nocontfile='stacked_GN17A_nodpix'+str(nodpix_tostack)+'_continuum_removed.fits'
=======
stackfile='stacked_GN17A_nodpix'+str(nodpix_tostack)+'.fits'
continuum_file='stacked_GN17A_continuum.fits'
#continuum_file='stacked_continuum.fits.gz'
nocontfile='stacked_GN17A_nodpix'+str(nodpix_tostack)+'_continuum_removed.fits'
>>>>>>> 7ffabc958e913a9e54c01cef608959aca13ddac0

#signal_constraint=np.array([30,9000.,9250.])
signal_constraint=np.array([0,2,9000.,9250.])
#csignal_constraint=np.array([20,9000.,9250.])
csignal_constraint=np.array([5,50,9000.,9250.])
weight_wave=np.array([9800,10000])
scalewave=np.array([9000,9280])
#    clusters=np.array(['SXDF64','SXDF76','SXDF87','SPT0205','SpARCS0335','SPT0546','SPT0546','COSMOS-125','COSMOS-221','COSMOS-221'])
#    masks=np.array(['GS2014BLP001-08','GS2014BLP001-02','GS2014BLP001-07','GS2014BLP001-06','GS2014BLP001-01','GS2014BLP001-09','GS2014BLP001-10','GS2015ALP001-02','GS2014BLP001-05','GS2015ALP001-01'])
#clusters=np.array(['SXDF64'])
#masks=np.array(['GS2014BLP001-08'])
if os.path.isfile(stackfile):
    print 'Using existing stackfile ',stackfile
    hdulResidual=fits.open(stackfile)
else:
#    gooddata=(data['Date'] < 57000)|(data['Date'] > 57100) | (data['x_ccd']>1000) | (data['Nodpix']>38)
# 16B data only:
#    gooddata=(data['Date'] > 57680)
# good 17A data only?
    gooddata=(data['Date'] > 0)
    good=np.where((data['Nodpix']==nodpix_tostack) & gooddata & (telescope==Telescope))
    hdulResidual=dostack(data,good,signal_constraint=signal_constraint,weight_wave=weight_wave)
    hdulResidual.writeto(stackfile,clobber=True)
if os.path.isfile(continuum_file):
    print 'Using existing continuum file ',continuum_file
    hdulContinuum=fits.open(continuum_file)
else:
    good_continuum=np.where((data['Nodpix']==nodpix_bandshuffle) & (telescope==Telescope))
    hdulContinuum=dostack(data,good_continuum,signal_constraint=csignal_constraint,weight_wave=weight_wave)
    hdulContinuum.writeto(continuum_file,clobber=True)
if os.path.isfile(nocontfile):
    print 'Using existing continuum subtracted file',nocontfile
    hdulResidual_continuum_removed=fits.open(nocontfile)
else:
    hdulResidual_continuum_removed=remove_continuum(hdulResidual,hdulContinuum,scalewave=scalewave)
    hdulResidual_continuum_removed.writeto(nocontfile,clobber=True)
#print clusters
#print masks
for c,m in zip(clusters,masks):
    print c,m
    if m != 'GN2017ALP004-06':
        fixdata(c,m,hdulResidual_continuum_removed,wlimit=9300,nodpixtofix=nodpix_tostack,telescopetofix=Telescope)
#    fixdata(c,m,hdulResidual_continuum_removed,wlimit=8000,nodpixtofix=nodpix_tostack)
    
