from astropy.io import fits,ascii
from astropy import wcs as pywcs

#from GoGREEN_prepmask import getZphWts

from GoGREEN_utils import getPhotFilename, colz




from matplotlib.patches import Rectangle
import numpy as np
import matplotlib.pyplot as plt

import os,sys

from numpy.lib import recfunctions as recf
#from maskdes import *
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper
#from astropy.coordinates import SkyCoord


import astropy.units as u

# copied from GoGREEN_prepmask for now (needed to change fInfo['Redshift'] --> fInfo['grpz'] !!! ***

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal['ZPH']>0.7) & (gal['ZPH']<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5

        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['Redshift']) & (2.0*gal['ZPHHI']-gal['ZPH'] >= fInfo['Redshift']) )[0]
#        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH']>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt



try:
    from astropy.coordinates import SkyCoord
except:
    print 'We seem to have an older version of astropy (probably from Ureka).\nTrying to work around'
    from astropy.coordinates import ICRS as SkyCoord




# mkcatPreim('SpARCS_0035','SPARCS0035_zband_IRAC2bands_v1.1_starcheck.cat')
# dumb way to do intersection of 2 lines by hacking:
# http://stackoverflow.com/questions/3252194/numpy-and-line-intersections
def line_intersect(coeffs0, coeffs1):
    m0=None ; m1=None
    x0 = np.array(15.0) ; x1 = np.array(20.0)
    p0 = np.array( (x0,np.polyval(coeffs0,x0)) )
    p1 = np.array( (x0,np.polyval(coeffs1,x0)) )
    q0 = np.array( (x1,np.polyval(coeffs0,x1)) )
    q1 = np.array( (x1,np.polyval(coeffs1,x1)) )
#def line_intersect(p0, p1, m0=None, m1=None, q0=None, q1=None):
    ''' intersect 2 lines given 2 points and (either associated slopes or one extra point)
    Inputs:
        p0 - first point of first line [x,y]
        p1 - fist point of second line [x,y]
        m0 - slope of first line
        m1 - slope of second line
        q0 - second point of first line [x,y]
        q1 - second point of second line [x,y]
    '''
    if m0 is  None:
        if q0 is None:
            raise ValueError('either m0 or q0 is needed')
        dy = q0[1] - p0[1]
        dx = q0[0] - p0[0]
        lhs0 = [-dy, dx]
        rhs0 = p0[1] * dx - dy * p0[0]
    else:
        lhs0 = [-m0, 1]
        rhs0 = p0[1] - m0 * p0[0]

    if m1 is  None:
        if q1 is None:
            raise ValueError('either m1 or q1 is needed')
        dy = q1[1] - p1[1]
        dx = q1[0] - p1[0]
        lhs1 = [-dy, dx]
        rhs1 = p1[1] * dx - dy * p1[0]
    else:
        lhs1 = [-m1, 1]
        rhs1 = p1[1] - m1 * p1[0]

    a = np.array([lhs0, 
                  lhs1])

    b = np.array([rhs0, 
                  rhs1])
    try:
        px = np.linalg.solve(a, b)
    except:
        px = np.array([np.nan, np.nan])

    return px



def checkCat(name,dcol=0.0):

#    plt.ion() #*** allow to run from the command line without waiting for window to be closed.

    

    if(1):

#        mm = fits.getdata('%sMaskDesign/maskinfo.fits'%rootdir)

        tabfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
        mm = fits.getdata(tabfile)

        ok=np.where(mm['cluster']==name)[0]
        #name='COS221'
#        rotang = mm['PA'][ok]
        ccoo= SkyCoord(mm['RA_gmos_deg'][ok],mm['DEC_gmos_deg'][ok],unit=(u.deg,u.deg))
        cra,cdec = ccoo.ra.value[0],ccoo.dec.value[0]
        grpcoo= SkyCoord(mm['RA_target_deg'][ok],mm['DEC_target_deg'][ok],unit=(u.deg,u.deg))
        grpra,grpdec = grpcoo.ra.value[0],ccoo.dec.value[0]
        grpz=mm['Redshift'][ok]

        det = mm['Detector'][ok]

        fInfo = mm[ok] # quick fix??







#        print ' ra,dec not known'
#        stop()


    if det=='Ham':
        import MDconfigHam as cfg
    elif det=='E2v':
        import MDconfigE2v as cfg


    print det

    
    
    # nominal RS fit:
    ##--- by eye
    x0,y0=20.15,2.06 ; x1,y1=21.83,1.86
    #rslope=(y1-y0)/(x1-x0)
    rslope=0.0 #***
    #rintcpt=y0-rslope*x0 +0.35+0.2+0.2+dcol
    rintcpt = colz(grpz)+dcol

    TABrslope=fInfo['RSSlope'][0]
    TABrintcpt=fInfo['RSintcpt'][0]
    if(TABrintcpt==np.NaN):
        TABrintcpt = colz(grpz)
#        TABrintcpt=2.81

    print 'Intercept: GOGREEN.fits = %.2f'%TABrintcpt
    print 'Intercept: plotted = %.2f'%rintcpt
    
    if (np.abs(TABrintcpt-rintcpt)>0.01):
        print
        print "Don't forget to update GOGREEN.fits if you want to use this new value!"
        print
    
    print rintcpt
    redSeqCoeffs = np.array((rslope,rintcpt))
    redCoeffs = redSeqCoeffs+np.array((0,cfg.dRed))

#    photfile = eval(cfg.photfileExpr)
    photfile = getPhotFilename(name)

#    obj = fits.getdata(name+'_phot.fits')
    obj = fits.getdata(photfile)
    print photfile



    #xp = xpHam#*sclH - ccdH_cx + ccd_cx
    #yp = ypHam#*sclH - ccdH_cy + ccd_cy
    
  

    
    # TRIM FIELD
    

    inFoV = np.where( (obj['XP']>cfg.xminField) & (obj['XP']<=cfg.xmaxField) &  (obj['YP']>cfg.yminField) & (obj['YP']<=cfg.ymaxField))[0]

    
    
    
    ##**
    class_starThresh=0.8 # not obvious from 0035 hist.! ***
#    okgal = np.where(dat['star']<class_starThresh)[0] #*** 

    try:
        isgal = np.where(obj['AQSTAR_HANDCHECK']==0)[0]
        setup = np.where(obj['AQSTAR_HANDCHECK']==1)[0]
        gal = obj[isgal] # take all objects as gals
        stars = obj[setup]
    except:
        isgal = np.arange(len(obj))
        gal = obj[isgal]


    #print gal

        
    
    dra = (gal['RA'] - fInfo['RA_target_deg'])*np.cos(np.deg2rad(gal['dec']))
    ddec = gal['DEC'] - fInfo['DEC_target_deg']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['Redshift'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
    #    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
    #    del gal
    #    gal = np.copy(tgal)
    #    del tgal
    #adisMpc = disMpc
    gal.disMpc = disMpc # note: not really appended to recarray!
    

    
    
#
#    fits.writeto(name+'info.fits',fInfo,clobber=True)
#    fits.writeto(name+'gals.fits',gal,clobber=True)



    """
    plt.figure()
    plt.plot(gal.xp,gal.yp,'k,')
    ok=np.where( (gal.disMpc<=1.5) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    plt.plot(gal.xp[ok],gal.yp[ok],'ro',alpha=0.5)
    plt.show()
    """

    
    # setup stars:
    try:
        setup = np.where(obj['AQSTAR_HANDCHECK']==1)[0]
        print '%s setup star candidates found'%len(setup)
        ##fits.writeto(name+'setupCands.fits',obj[setup],clobber=True)
    except:
        try:
            setup = np.where(dat['checked']==1)[0]
            print '%s setup star candidates found'%len(setup)
            ##fits.writeto(name+'setupCands.fits',obj[setup],clobber=True)
        except:
            print 'no setup stars found!'
    try:
        okst=setup # assume there MUST be some setup stars by now
    except:
        print "WARNING: no hand-checked setup stars found."
        print 'just using automated AQSTAR_CANDs'
#        print obj
        okst = np.where(obj['AQSTAR_CAND']==1)[0] ; setup=okst


    # region file:
    f=open('%s.reg'%name,'w')
    f.write('# Region file format: DS9 version 4.1\n')
    #    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write("physical\n")
    for kk in range(len(gal)):
        f.write('circle(%.3f,%.3f,%.3f)\n'%(gal['xp'][kk],gal['yp'][kk],5.0))

    for kk in setup:
        f.write('circle(%.3f,%.3f,%.3f) # color={red}\n'%(obj['xp'][kk],obj['yp'][kk],10.0))

    f.close()
    
    
    
    # region file:
    f=open('%s_rd.reg'%name,'w')
    f.write('# Region file format: DS9 version 4.1\n')
    #    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write("fk5\n")
    for kk in range(len(gal)):
        f.write('circle(%.7f,%.7f,%.3f")\n'%(gal['ra'][kk],gal['dec'][kk],2.0))

    for kk in setup:
        f.write('circle(%.7f,%.7f,%.3f") # color={red}\n'%(obj['ra'][kk],obj['dec'][kk],5.0))


    
    
    
    
    #if (fInfo['grpra']<0.0):
    #    print '*** using field centre as clus centre ***'
    #    fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
    
    
    
    
    
    
    
    
    dra = (gal['ra'] - fInfo['RA_target_deg'])*np.cos(np.deg2rad(gal['dec']))
    ddec = gal['dec'] - fInfo['DEC_target_deg']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['Redshift'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
    #    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
    #    del gal
    #    gal = np.copy(tgal)
    #    del tgal
    #adisMpc = disMpc
    gal.disMpc = disMpc # note: not really appended to recarray!
    
    
    
    
    #plotClus()
    # initial plot to identify location of cluster in ra,dec and CMD    
    #def plotClus(gal,fInfo):
    col = gal['zmag'] - gal['irac1'] ; mag = gal['irac1']
    #pri = gal.prob
    xp = gal['xp'] ; yp = gal['yp']

#    print '***',gal['IZPH'],obj['IZPH']

    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal['izph'][0]==1):
        print 'photozs found!'
        #foofInfo = recf.append_fields(fInfo,names=('grpz'),data=(fInfo['Redshift']),asrecarray=True,usemask=False)
        #foofInfo = foofInfo[0]

        phzWt = getZphWts(gal,fInfo)
        #phzWt = getZphWts(gal,foofInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05)  & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim))[0]
    ###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal['zmag']<=cfg.zFnt[1]) & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal['zmag']<=cfg.zFnt[1]) & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3,label='r<1.0Mpc')
        ok=np.where( (gal.disMpc<=0.3) & (gal['zmag']<=cfg.zFnt[1]) & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5,label='r<0.3Mpc')
        ok=np.where( (gal['zmag']<=cfg.zFnt[1]) & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([17.5,23])
    plt.ylim([-2,6])
    
    
    xxx = np.arange(0,30)

    plt.plot(xxx,cfg.zFnt[1]-xxx,'b--')#,label='faint')
    plt.plot(xxx,cfg.zBri[1]-xxx,'b:')#,label='bright')
    plt.plot(xxx,cfg.zBri[0]-xxx,'b:')
    plt.plot(xxx,np.polyval(cfg.blueCoeffs,xxx),'b-')

    print redSeqCoeffs
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r-',lw=2,label='RS',alpha=0.5)
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+0.2')
    #    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+0.5')
    plt.legend(numpoints=1,loc='lower left')
 
    
    # absolute selection box:
    plt.axvline(cfg.irac1Lim,color='m')
    
    # red limit with irac1 lim:
    
    # red lim with zBri
    zBriCoeffs = np.array( (-1.,cfg.zBri[0]) )
    px0,px1 = line_intersect(redCoeffs,zBriCoeffs)
#    print px0,px1
    # zBri with iracBlue
    px2,px3 = line_intersect(zBriCoeffs,cfg.blueCoeffs)
#    print px2,px3
    plt.plot( (px0,px2),(px1,px3), 'k-', lw=3, alpha=0.5 )
    # red lim with zFnt:
    zFntCoeffs = np.array( (-1.,cfg.zFnt[1]) )
    px4,px5 = line_intersect(redCoeffs,zFntCoeffs)
    plt.plot( (px0,px4),(px1,px5), 'k-', lw=3, alpha=0.5 )
    
    # at irac1Lim:
    # intersect redCoeffs:
    px6,px7 = np.array((cfg.irac1Lim,cfg.zFnt[1]-cfg.irac1Lim))
    plt.plot( (px6,px4), (px7,px5), 'k-', lw=3, alpha=0.5 )
    # intersect blueCoeffs:
    px8,px9 = cfg.irac1Lim,np.polyval(cfg.blueCoeffs,cfg.irac1Lim)
    plt.plot( (px8,px6), (px9,px7), 'k-', lw=3, alpha=0.5 )
    # blue lim:
    plt.plot( (px8,px2), (px9,px3), 'k-', lw=3, alpha=0.5 )

#    print xxx[okB]
#    print zBri[0]-xxx[okB]
#    ok=np.where()
    plt.text(21.5,2.25,'Fnt',rotation=-45,verticalalignment='top')
    plt.text(20.5,2.25,'Bri',rotation=0,verticalalignment='top')


    plt.title('%s\nz=%.3f  RSintcpt=%.2f'%(name,fInfo['Redshift'],rintcpt))

    
    # spatial:
    plt.subplot(122)
    plt.plot(gal['ra'],gal['dec'],'k,')
    
    plt.plot(fInfo['RA_target_deg'],fInfo['DEC_target_deg'],'og',ms=20,alpha=0.3)
    
    if (gal['izph'][0]==1):
        phzWt = getZphWts(gal,fInfo)
    #        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
    ###        ok=np.where( (abs(gal.zph-fInfo['grpz']) <=0.05) & (gal.zmag<=zFnt[1])  & (gal.irac1<=irac1Lim) )[0]
    ###        plt.plot( gal.ra[ok],gal.dec[ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal['zmag']<=cfg.zFnt[1])  & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['ra'][ok],gal['dec'][ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal['ra'][ok],gal['dec'][ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal['zmag']<=cfg.zFnt[1])  & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['ra'][ok],gal['dec'][ok], 'or', alpha=0.2,label='Fnt')
        ok=np.where( (gal.disMpc<=1.0) & (gal['zmag']<=cfg.zFnt[1])  & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['ra'][ok],gal['dec'][ok], 'or', alpha=0.3,label='r<1.0Mpc')
        ok=np.where( (gal.disMpc<=5.5)& (gal['zmag']>cfg.zBri[0]) & (gal['zmag']<=cfg.zBri[1])  & (gal['irac1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['ra'][ok],gal['dec'][ok], 'ok', alpha=0.3,label='Bright')
    
    try:
        ok=np.where( np.abs(gal['zs']-fInfo['Redshift'])<=0.03 )[0]
        if(len(ok)>0):
            plt.plot( gal['ra'][ok],gal['dec'][ok], 'ob', alpha=0.7,label='zs')
    except:
        pass # might not have zs column?
    if(len(okst)>0):
        plt.plot(obj['ra'][okst],obj['dec'][okst],'*g',ms=15,label='setup stars')
        for iii in okst:
            plt.text(obj['ra'][iii],obj['dec'][iii],str(obj['ID'][iii]))
        

    plt.xlabel('ra')
    plt.ylabel('dec')
    plt.legend(numpoints=1)
    
    plt.savefig('%s_diag.png'%name)
    plt.show()

    print obj.columns

    
    
    
if __name__ == "__main__":
    #    name='SPT-0546' ; nMasks=5 ; det='Ham' ; isilent=True

    #--
    global cfg
    #--

    name = sys.argv[1]
    try:
        dcol=float(sys.argv[2])
    except:
        dcol=0.
    checkCat(name,dcol=dcol)


