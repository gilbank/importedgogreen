# ============================================================================
# CL 2014/08/02:  Global Constants module
# ============================================================================

import os

class GoGREENPath():
    """GoGREEN file tree."""
    data = os.environ["GoGREEN_data"]
    try: 
        data = os.environ["GoGREEN_data"]
    except:
        data = "."      
    try: 
        code = os.environ["GoGREEN_scripts"]
    except:
        code = "."
    
    IDLscripts = code + "/IDL/"             # Where the scipts live
    PYTHONscripts = code + "/python/"       # Where the scipts live
    IRAFscripts = code + "/IRAF/"           # Where the scipts live
    config = data + "/Config/"              # Configuration file for run preperation
    reduced = data + "/Data/reduced/"
    images = data + "/Data/Imaging/"        # Images
    spectra = data + "/Data/Spectroscopy/"  # Spectra
    catalogues = data + "/Data/Catalogues/" # Catalogues
    models = data + "/Models/"              # Models
    prep = data + "/Preparation/"           # Preparation


