#!/Users/clidman/Science/Programs/Ureka/variants/common/bin/python
# Python program that estimates the scatter in an aperture of a fixed
# size

# Usage
# GoGREEN_DetectionLimits.py -c configFile -d diameter

import Constants
from optparse import OptionParser
import GoGREEN_Library as GoGREEN
import os
import pyfits
import numpy
from astropy import wcs
import matplotlib.pyplot as plt
import time
from pyraf import iraf

def unlearn(tasks):
    for task in tasks:
        iraf.unlearn(task)
    return

parser=OptionParser()
        
parser.add_option("-c", "--configFile", dest="configFile",
                  default=None, help="Configuration file")

parser.add_option("-d", "--diameter", dest="diameter",
                  default=None, help="Diameter of the aperture")

(options, args) = parser.parse_args()

# GoGREEN environment variables
GoGREEN_envVar=os.environ['GoGREEN_data']

# Read in GoGREEN constats - directories and the like
constants=Constants.GoGREENPath()

# Read in the configuration file
params=GoGREEN.buildDictionary(options.configFile)

if 'extension' not in params.keys():
    params['extension']=0

# ----------- Compute aperture corrections ----------------
# Currently, we do this in an interactive manner. One could consider
# a more automatic approach using PSFex

# Display the image and ask the user to select stars
if 'directory' in params.keys():
    image=params['directory']+params['image']
else:
    image=constants.images+params['image']

#'['+params['section']+']'
iraf.display(image=image+'['+params['extension']+']', frame=1, zr='no',zs='no',z1=params['z1'],z2=params['z2'])
print("Select unsaturated and isolated stars. These stars will be used to compute aperture corrections")

iraf.noao()
iraf.digi()
iraf.apphot()
unlearn(['phot','txdump'])
iraf.datapars.scale=float(params['pixscale'])
# Iraf defines the aperture in terms of the radius
iraf.photpars.apertur='%4.3f,4' % (float(options.diameter)/2.0*iraf.datapars.scale)
iraf.photpars.zmag='0'
iraf.fitskypars.salgori='mode'
iraf.fitskypars.annulus='6'
iraf.fitskypars.dannulus='4'
iraf.centerpars.calgori='centroid'
iraf.phot(image=image.replace('.fits','')+'['+params['extension']+']')
mags=os.path.split(image.replace('.fits',params['extension']+'.mag.1'))[1]
stars=iraf.txdump(textfile=mags,fields='MAG',expr='yes',Stdout=1)
# The magnitude error is not very precise
iraf.delete(files=mags,verify='no')

# Compute the magnitude difference. Allow the user to select or
# deselect stars
mag1=[]
mag2=[]
for star in stars:
    mag1.append(float(star.split()[0]))
    mag2.append(float(star.split()[1]))

correction=numpy.array(mag1)-numpy.array(mag2)

print('\n')
print('Select stars from the following list')
for element in list(enumerate(correction)):
    print 'star %d\t%6.3f\t%6.3f\t%6.3f' % (element[0]+1,mag1[element[0]],mag2[element[0]],element[1])
selection=[int(i)-1 for i in raw_input('Enter a comma separated list: ').split(',')]
print('\n')

aperCorr=numpy.median(correction[selection])
print('%d stars\t Aperture correction %6.3f' % (len(correction[selection]),aperCorr))

# Copy accross the configuration files for SExtractor
GoGREEN.setup(GoGREEN_envVar+'/Analysis/DetectionLimits/setup/')

# Set up the input to SExtractor
#image=constants.images+params['image']
if 'weight' in params.keys():
    weight=constants.images+params['weight']
else:
    weight=None

segMap=os.path.split(params['image'])[1].replace('.fits','_seg.fits')
SExParams={'c':'segmentation.config','CHECKIMAGE_NAME':segMap}

# Use SExtractor to create a segmentation map indicating where the
# objects are
# The extension is needed, otherwise SExtractor will derive catalogues
# for all extensions

GoGREEN.makeCat(image+'['+params['extension']+']',weight,None,SExParams)

# Compute the scatter in a aperture of some diameter

# Randomly select 100 regions within the image and check that they are more than halfwidth pixels from any source and more than
# border pixels from the edge

data=pyfits.getdata(segMap,0,header=False)
dataImage=pyfits.getdata(image,0,header=False)

flux=numpy.array([],'float')
nsample=int(params['nsample'])

# Set the region

xrange=numpy.array(params['section'].split(',')[0].split(':')).astype(int)
yrange=numpy.array(params['section'].split(',')[1].split(':')).astype(int)

halfWidth=int(float(options.diameter))*1.2
aperture=float(options.diameter) / 2.0

id=0
skies=[]
while id < nsample:
    x=int(numpy.random.uniform(xrange[0],xrange[1]))
    y=int(numpy.random.uniform(yrange[0],yrange[1]))
    region=data[y-halfWidth:y+halfWidth,x-halfWidth:x+halfWidth]  # Note the pyfits convention here [y,x]
    flag=region==0 # Creates a Boolean array. True if the value is zero
    if not False in flag: 
        id=id+1
        skies.append([x,y])
        # Sum the values within a circular region
        values=numpy.array([],'float')
        regionImage=dataImage[y-halfWidth:y+halfWidth,x-halfWidth:x+halfWidth]  # Note the pyfits convention here [y,x]
        for xp in range(regionImage.shape[0]):
            for yp in range(regionImage.shape[1]):
                if numpy.sqrt((xp-aperture)**2.+(yp-aperture)**2.) <= aperture:
                    values=numpy.append(values,regionImage[yp:yp+1,xp:xp+1])  
                    # Should compute fractional counts in those pixels that are partially within the aperture
        flux=numpy.append(flux,values.sum())
        
# Convert skies positions from x,y to RA,DEC andrite it out to a file
# so that we can check it with image display tools such as ALADIN

w=wcs.WCS(image)
skies_world=w.wcs_pix2world(numpy.array(skies),1)

file=open(segMap.replace('seg.fits','skies.tsv'),'w')
file.write('ID\tRA\tDEC\n')
id=0
for sky in skies_world:
    id+=1
    file.write('sky%003d\t%9.6f\t%9.6f\n' % (id, sky[0],sky[1]))
file.close()

# Compute the median and the region covering the 68.3 percentile

flux.sort()
median=numpy.median(flux)
lowerLimit=median-flux[int(0.16*nsample)]
upperLimit=flux[int(0.84*nsample)]-median
averageLimit=(lowerLimit+upperLimit)/2.

detectionLimit=float(params['zeroPoint'])-2.5*numpy.log10(5.0*averageLimit)-aperCorr

# Write out the results
file=open(options.configFile.replace('.config','_%spix.results' % (options.diameter)),'w')
file.write('This file was written on '+time.asctime(time.localtime(time.time()))+'\n')
file.write('Input   \t%s\n' %(image))
file.write('Diameter\t%5.2f\tpixels\n' %(aperture*2.0))
file.write('Median  \t%5.2e\tADU\n' %(median))
file.write('Sigma   \t%5.2e\tADU\n' %(averageLimit))
file.write('AperCorr\t%5.2f\tmag\n' %(aperCorr))
file.write('\n')
file.write('5 sigma Limit \t%5.2f\n' %(detectionLimit))
file.close()

# Plot a histogram of the sky values
fig=plt.figure()
ax=fig.add_subplot(111)
bins=numpy.arange(median-5*averageLimit,median+5*averageLimit,averageLimit / 2.)
ax.hist(flux,bins)
ax.set_xlabel('Fluxes in radius %4.2f' % aperture)

# Plot the limts found above
x_low=[median-averageLimit,median-averageLimit]
x_high=[median+averageLimit,median+averageLimit]
ax.plot(x_low,ax.get_ylim(),'r')
ax.plot(x_high,ax.get_ylim(),'r')

plt.savefig(options.configFile.replace('.config','_%spix.png' % (options.diameter)))
plt.close()
