

import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
import pickle


clus = 'SpARCS1634'
#clus='COSMOS-221'
#clus = 'SpARCS1634'


file1d = clus+'_oned.fits.gz'
ext=1

#### 1D:
sp1d,hdr = fits.getdata(file1d,ext,header=True)
#*** should prob. loop over all extensions and unpack properly
#    use WCS to convert to lambda
hdulist = fits.open(file1d)
#hdulist.close()

lastnum=0
j=0
specarr = list()
specid = list()
for i in range(len(hdulist)):
    if i==0: continue
    hdr = hdulist[i].header
    try:
        print hdr['NAXIS1'],hdr['OBJID'],hdr['CRVAL1'],hdr['CRPIX1'],\
            hdr['CDELT1'],hdr['BUNIT'],hdr['FLUXSCAL']
    except:
#        print 'problem %s'%i
        # MDF (with redux pars) in final extension:
        tabhdr = hdr
        fitstab = hdulist[i].data    
    if hdr['OBJID']<>lastnum: # need try/except
        j=0
        lastnum = hdr['OBJID']
        tspec = -999.*np.ones((3,hdr['NAXIS1']))
        tspec[j,:]= hdulist[i].data        
        lam =  hdr['CRVAL1'] - (hdr['CRPIX1'] - 1) * hdr['CDELT1'] + \
               np.arange(len(hdulist[i].data ))*hdr['CDELT1']

        plt.plot(lam,tspec[0,:])
        plt.show()
    else:
        j=j+1
        tspec[j,:]= hdulist[i].data        
        #dispersion_start = header['CRVAL1'] - (header['CRPIX1'] - 1) * header['CDELT1']
#        tspec[j,:]= hdulist[i].data
    if j==2:
        specarr.append(tspec)
        try:
            specid.append(hdr['OBJID'])
        except:
            pass
#        stop()
#np.savez('SPT0546_oned.npz',specarr=specarr,specid=specid,mdf=fitstab)

# Need to run manually as previous will crash***
with open('%s_oned.pkl'%clus,'w') as f:
    pickle.dump(specarr,f)
    pickle.dump(specid,f)
    pickle.dump(lam,f)
    pickle.dump(fitstab,f)

# fitstab['ID']
#fitstab['priority']

#### 2D:


file2d = clus+'_twod.fits.gz'
ext=1
sp2d,hdr = fits.getdata(file2d,ext,header=True)
#*** should prob. loop over all extensions and unpack properly
#    use WCS to convert to lambda
hdulist = fits.open(file2d)
#hdulist.close()

lastnum=0
j=0
specarr = list()
specid = list()
for i in range(len(hdulist)):
    if i==0: continue
    hdr = hdulist[i].header
    try:
        print hdr['NAXIS1'],hdr['OBJID'],hdr['CRVAL1'],hdr['CRPIX1'],\
            hdr['CDELT1'],hdr['BUNIT'],hdr['FLUXSCAL']
    except:
#        print 'problem %s'%i
        # MDF (with redux pars) in final extension:
        tabhdr = hdr
        fitstab = hdulist[i].data    
    if hdr['OBJID']<>lastnum: # need try/except
        j=0
        lastnum = hdr['OBJID']
        print hdulist[i].data.shape
        tspec = -999.*np.ones((3,21,hdr['NAXIS1']))
        tspec[j,:]= hdulist[i].data        
        lam =  hdr['CRVAL1'] - (hdr['CRPIX1'] - 1) * hdr['CDELT1'] + \
               np.arange(len(hdulist[i].data ))*hdr['CDELT1']

        plt.plot(lam,tspec[0,:])
        plt.show()
    else:
        j=j+1
        tspec[j,:]= hdulist[i].data        
        #dispersion_start = header['CRVAL1'] - (header['CRPIX1'] - 1) * header['CDELT1']
#        tspec[j,:]= hdulist[i].data
    if j==2:
        specarr.append(tspec)
        try:
            specid.append(hdr['OBJID'])
        except:
            pass
#        stop()
#np.savez('SPT0546_oned.npz',specarr=specarr,specid=specid,mdf=fitstab)

# Need to run manually as previous will crash***
with open('%s_twod.pkl'%clus,'w') as f:
    pickle.dump(specarr,f)
    pickle.dump(specid,f)
    pickle.dump(lam,f)
    pickle.dump(fitstab,f)

##imshow(specarr[10][0])




# fitstab['ID']
#fitstab['priority']
