import os, sys
import numpy
from pyraf import iraf
import subprocess as sp
import sys
from astropy.io import fits
from astropy import wcs
from pyraf import iraf

statSection='[400:1600,400:1600]'

def buildDictionary(file):
    file=open(file,'r')
    lines=file.readlines()
    file.close()
    parameters=[]

    for line in lines:
        if line[0]!='#':
            parameters.append(line.split())

    return dict(parameters)

def mkdir(dir):
    try:
        os.mkdir(dir)
    except:
        pass
    return

def  unlearn(tasks):
    for task in tasks:
        iraf.unlearn(task)
    return

def getlisting(filename):
    f=open(filename)
    lines=f.readlines()
    f.close()
    # Strip spaces and other special characters
    return [line.strip() for line in lines]
    
def setup(params,options):
    # Create subdirectories
    mkdir(params['reduced'])
    mkdir(params['calib'])
    
    # Copy accross the setup files for SExtraxtor
    setupDir=params['setUp']
    if os.path.isdir(setupDir):
        cmd='cp %s/* %s/.' % (setupDir,params['reduced'])
        retcode=sp.call(cmd,shell=True)

    return

def averageDITs(image,weight,saturation,options):
    # Averages the frames, if required, and repack them into a multi-extention fits file for use with SExtractor
    # Uses a mixture of astropy and iraf routines. Could be recoded to use astropy entirely
    # Create the weight map if one does not exists.
    # Creates flag maps
    
    iraf.fitsutil()
    unlearn(['imcom','imdel','imexpr','fxcopy','fxinsert','imcopy'])

    if options.preprocess=='detrended':
        output=image.replace('p.fits','pa')
    else:
        output=image.replace('s.fits','sa')

    image=image.replace('.fits','')
    # Needs to be updated for the case when no weight maps exist
    if weight!=None:
        weight=weight.replace('.fits','')

    
    for quadrant in [1,2,3,4]:
        # Science images, average along the 3rd dimension
        if fits.getval(image+'.fits','NAXIS',quadrant)==3:
            iraf.imcombine(input=image+'['+`quadrant`+']',output=image+'_CHIP'+`quadrant`,combine='average',reject='none',project='yes',scale='none',zero='none',weight='none',lthresh='INDEF',hthresh='INDEF')
        else:
            iraf.imcopy(input=image+'['+`quadrant`+']',output=image+'_CHIP'+`quadrant`)

        # Take the first weight slice
        if weight!=None:
            data,hdr=fits.getdata(weight+'.fits', quadrant, header=True)
            if fits.getval(weight+'.fits','NAXIS',quadrant)==3:
                fits.writeto(weight+'_CHIP'+`quadrant`+'.fits',data[0,:,:],hdr)
                multiDim=True
            else:
                fits.writeto(weight+'_CHIP'+`quadrant`+'.fits',data,hdr)
                multiDim=False
        else:
            # If the weight image does not exist, we use the data frame in its place
            # This part of the code is under development
            data,hdr=fits.getdata(image+'.fits', quadrant, header=True)
            if fits.getval(data+'.fits','NAXIS',quadrant)==3:
                fits.writeto(weight+'_CHIP'+`quadrant`+'.fits',data[0,:,:],hdr)
                multiDim=True
            else:
                fits.writeto(weight+'_CHIP'+`quadrant`+'.fits',data,hdr)
                multiDim=False
            
            exit()
                
        # Create the flag and weight maps
        # Don't use ushort for flag maps. Flag maps will not work in this case

        # We use the first slice in multidimentional images to help define the flag and weight maps
        # For sky subtraction, use a flag map
        # See notes for details
        
        if multiDim:
            a_image='%s[%d][*,*,1]' % (image,quadrant)
            iraf.imexpr(expr='((a > 0 && a < '+`saturation`+') && b > 0.7) ? 0 : 128',output=image+'_flag_CHIP'+`quadrant`,outtype='short',a=a_image,b=weight+'_CHIP'+`quadrant`) 
            iraf.imexpr(expr='((a > 0 && a < '+`saturation`+') && b > 0.7) ? 1 : 0',output=image+'_weight_CHIP'+`quadrant`,outtype='short',a=a_image,b=weight+'_CHIP'+`quadrant`) 
        else:
            iraf.imexpr(expr='((a > 0 && a < '+`saturation`+') && b > 0.7) ? 0 : 128',output=image+'_flag_CHIP'+`quadrant`,outtype='short',a=image+'_CHIP'+`quadrant`,b=weight+'_CHIP'+`quadrant`) 
            iraf.imexpr(expr='((a > 0 && a < '+`saturation`+') && b > 0.7) ? 1 : 0',output=image+'_weight_CHIP'+`quadrant`,outtype='short',a=image+'_CHIP'+`quadrant`,b=weight+'_CHIP'+`quadrant`) 

    if options.preprocess=='detrended':
        # Only if you are not using xdimsum, i.e. you are using the images that already have the sky removed.
        iraf.fxcopy(input=image,output=output+'.fits',groups='0',new_file='yes')  # Copies the primary HDU from the original file
        iraf.fxinsert(input=image+'_CHIP1',output=output+'.fits[0]',groups='')
        iraf.fxinsert(input=image+'_CHIP2',output=output+'.fits[1]',groups='')
        iraf.fxinsert(input=image+'_CHIP3',output=output+'.fits[2]',groups='')
        iraf.fxinsert(input=image+'_CHIP4',output=output+'.fits[3]',groups='')
        iraf.imdel(images=image+'_CHIP*.fits',verify='no')
        
    iraf.fxcopy(input=image+'.fits',output=output+'_flag.fits',groups='0')
    iraf.fxinsert(input=image+'_flag_CHIP1',output=output+'_flag.fits[0]',groups='')
    iraf.fxinsert(input=image+'_flag_CHIP2',output=output+'_flag.fits[1]',groups='')
    iraf.fxinsert(input=image+'_flag_CHIP3',output=output+'_flag.fits[2]',groups='')
    iraf.fxinsert(input=image+'_flag_CHIP4',output=output+'_flag.fits[3]',groups='')

    if options.preprocess=='detrended':
        # We need the individual flag maps for xdimsum
        iraf.imdel(images=image+'_flag_CHIP*.fits',verify='no')

    iraf.fxcopy(input=image+'.fits',output=output+'_weight.fits',groups='0')
    iraf.fxinsert(input=image+'_weight_CHIP1',output=output+'_weight.fits[0]',groups='')
    iraf.fxinsert(input=image+'_weight_CHIP2',output=output+'_weight.fits[1]',groups='')
    iraf.fxinsert(input=image+'_weight_CHIP3',output=output+'_weight.fits[2]',groups='')
    iraf.fxinsert(input=image+'_weight_CHIP4',output=output+'_weight.fits[3]',groups='')

    # We do not need the weight maps split by chip
    iraf.imdel(images=image+'_weight_CHIP*.fits',verify='no')
    # The lines below need testing
    iraf.imdel(images=image+'w_CHIP*.fits',verify='no')
    iraf.imdel(images=image+'w.fits',verify='no')

    return

def qsoflags(params,options):
    # work out how many observaitons were done with then varius flags
    QCflagList={'1':0,'2':0,'3':0,'4':0,'5':0}
    for file in os.listdir(params['raw']+'html'):
        if 'explog-C' in file:
            f=open(params['raw']+'html/'+file)
            contents=f.readlines()
            f.close()
            for i,line in enumerate(contents):
                if "PROCESSED" in line or "DETRENDED_ONLY" in line:
                    if  "DETRENDED_ONLY" in line:
                        # For data taekn before 2009
                        QSO=contents[i+11].replace("<TD>","").strip().replace("</TD>","")
                    else:
                        # For more recent data
                        QSO=contents[i+10].replace("<TD>","").strip().replace("</TD>","")
                    QCflagList[QSO]+=1
    print QCflagList
    return
                        

    
def preprocess(params,options):
        
    # i) Find the good frames, we accept frames with QSO==1
    # We use the HTML logs here as it seems that this information is not kept in the FITS header
    # The code is ugly, but effective

    # ii) Average along the 3rd dimention and produce weight maps and flag maps
    QCflagList=[1]
    
    for file in os.listdir(params['raw']+'html'):
        if 'explog-C' in file:
            f=open(params['raw']+'html/'+file)
            contents=f.readlines()
            f.close()
            for i,line in enumerate(contents):
                if "PROCESSED" in line or "DETRENDED_ONLY" in line:
                    if  "DETRENDED_ONLY" in line:
                        # For data taekn before 2009
                        name=contents[i-11].replace("<TD>","").strip().replace("</TD>","")
                        QSO=contents[i+11].replace("<TD>","").strip().replace("</TD>","")
                        status=contents[i].replace("<TD>","").strip().replace("</TD>","")
                    else:
                        # For more recent data
                        name=contents[i-9].replace("<TD>","").strip().replace("</TD>","")
                        QSO=contents[i+10].replace("<TD>","").strip().replace("</TD>","")
                        status=contents[i].replace("<TD>","").strip().replace("</TD>","")
                        
                        
                    if int(QSO) in QCflagList and ("PROC" in status or "DETRENDED_ONLY" in status):
                        if  "DETRENDED_ONLY" in line:
                            # For data taken before 2009
                            index1=name.index("HREF=")+6
                            index2=name.index(".html")+1
                        else:
                            # For more recent data
                            index1=name.index("HREF=")+6
                            index2=name.index(".html")
                        # Copy across the detrended or pre-sky images first
                        if options.preprocess=='detrended':
                            input=params['raw']+'detrended/'+name[index1:index2]+'s.fz'
                            output=params['reduced']+name[index1:index2]+'s'
                        elif options.preprocess=='pre-sky':
                            input=params['raw']+'pre-sky/'+name[index1:index2-5]+'s.fits.fz'
                            output=params['reduced']+name[index1:index2-5]+'s.fits'
                        else:
                            print 'Please select either pre-sky or detrended'
                            exit()
                            
                        # Copy accross the image first
                        cmd='imcopy %s %s' % (input, output)
                        sp.call(cmd,shell=True)

                        # Copy across the weight maps second
                        input_weight=(params['raw']+'weightmap/'+name[index1:index2]+'s.fz').replace('p.fits.fz','w.fits.fz')
                        output_weight=(params['reduced']+name[index1:index2]+'s').replace('p.fits','w.fits')
                        cmd='imcopy %s %s' % (input_weight, output_weight)
                        try:
                            sp.call(cmd,shell=True)
                        except:
                            output_weight=None
                            
                        # Average along the 3rd dimention and produce the weight and flag maps
                        averageDITs(output,output_weight,40000.0,options)
            
    return



def createLists(parmas,options):
    # This has become a bit messy with sa.fits and pa.fits
    lines=getlisting(options.list)

    # Lists that are used to check the photometry
    # Get fits header information
    entries=[]
    for line in lines:
        hdr=fits.getheader(line.replace('sa.fits','s.fits'))
        entries.append((hdr['DATE-OBS'],hdr['FILTER'],line))

    data=numpy.array(entries,dtype=[('DATE-OBS','a10'),('FILTER','a2'),('filename','a14')])
    
    dates=numpy.unique(data['DATE-OBS'])
    filters=numpy.unique(data['FILTER'])

    filterLists={}
    dateLists={}
    xdimsum={}
    
    for filter in filters:
        selection=(data['FILTER']==filter)
        filterLists[filter]=open(options.list.replace('.list', '_%s_phot.list' % filter), 'w')
        for entry in data[selection]:
            filterLists[filter].write('%s\n' % (entry['filename'].replace('.fits','.0001.resamp.fits')))

        for date in dates:
            selection=(data['FILTER']==filter) & (data['DATE-OBS']==date)
            if selection.sum() > 0:  
                dateLists[date]=open(options.list.replace('.list', '_%s_%s_phot.list' % (filter,date)), 'w')
                xdimsum[date]=open(options.list.replace('.list', '_%s_%s_xdimsum.list' % (filter,date)), 'w')
                for entry in data[selection]:
                    dateLists[date].write('%s\n' % (entry['filename'].replace('.fits','.0001.resamp.fits')))
                    xdimsum[date].write('%s\n' % (entry['filename'].replace('sa.fits','s.fits')))
                    
                
    for filter in filters:
        filterLists[filter].close()
        for date in dates:
            dateLists[date].close()
            xdimsum[date].close()

    # Lists for scamp, swarp, and imcom, and if needed, xdimsum
    # We create one list for each filter
    # We could make this more automatic
    filters=['Ks','J']
    rebuild={}
    scamp={}
    swarp={}
    imcom={}
    
    for filter in filters:
        rebuild[filter]=open(options.list.replace('.list', '_%s_rebuild.list' % filter), 'w')
        scamp[filter]=open(options.list.replace('.list', '_%s_scamp.list' % filter), 'w')
        swarp[filter]=open(options.list.replace('.list', '_%s_swarp.list' % filter), 'w')
        imcom[filter]=open(options.list.replace('.list', '_%s_imcom.list' % filter), 'w')
        
    for line in lines:
        filter=fits.getval(line.replace('sa.fits','s.fits'),'FILTER',0)
        scamp[filter].write('%s\n' % (line.replace('.fits','.cat')))
        rebuild[filter].write('%s\n' % (line.replace('sa.fits','s.fits')))
        swarp[filter].write('%s\n' % (line))
        for ext in ['0001','0002','0003','0004']:
            imcom[filter].write('%s.%s.resamp.fits\n' % (line.replace('.fits',''),ext))

        
    for filter in filters:
        rebuild[filter].close()
        scamp[filter].close()
        swarp[filter].close()
        imcom[filter].close()
        
        
    return


def skySub(params,options):
    # Unlearn IRAF tasks
    iraf.xdimsum()
    unlearn(['xfirstpass','xmaskpass','imcopy','imdel','imexpr','hedit'])

    input=numpy.genfromtxt(options.skySub,dtype=[('filename','a100')])
    
    nFrames=0
    if options.list==None:
        xf=[] # Contain file handles
        for i in range(0,4):
            xf.append(open('xfirstpass_CHIP%d.list' % (i+1),'w'))

        ref=['','','','']

        for f in input['filename']:
            nFrames+=1
            for i in range(0,4):
                output='%s%d' % (f.replace('.fits','_CHIP'),i+1)
                xf[i].write(output+'\n')        
                if ref[i]=='':
                    ref[i]=output

        # Close the files
        for i in range(0,4):
            xf[i].close()
    else:
        print 'Option not yet available'
        exit()

    # Adjust the number of sky frames based on the number of exposures one has to work with

    nskymin=min(nFrames,10)-1
    nmean=min(nFrames,14)-1
    print "Mean number of frames and minimumum number of skies will be",nmean,"and",nskymin,",respectively."

    
    for i in range(0,4):
        inlist='@xfirstpass_CHIP%d.list' % (i+1)
        # Run xfirstpass without measuring the offsets and without a bad pixel mask
        iraf.xfirstpass(inlist=inlist,referen=ref[i],output=ref[i]+'_fp',expmap='.exp',statsec=statSection,nmean=`nmean`,nskymin=`nskymin`,nreject='2',maskfix='no',badpixu='no',mkshift='no',chkshif='no',xnregis='no')
        
        # Measure the offsets. We might replace this code with something automatic
        if os.path.isfile(ref[i]+'.offset'):
            iraf.xfirstpass(inlist=inlist,referen=ref[i],output=ref[i]+'_fp',expmap='.exp',xslm='no',maskfix='no',xzap='no',badpixu='no',mkshift='no',chkshif='no',xnreg='yes',shiftli=ref[i]+'.offset')
            print 'Here I am JH'
        else:
            iraf.xfirstpass(inlist=inlist,referen=ref[i],output=ref[i]+'_fp',expmap='.exp',xslm='no',maskfix='no',xzap='no',badpixu='no',mkshift='yes',chkshif='no',xnreg='yes',shiftli=ref[i]+'.offset')
        
        # Run xmaskpass twice
        # Use it to compute the objects masks
        
        iraf.xmaskpass(input=ref[i]+'_fp',inexpmap=ref[i]+'_fp.exp',seccorn=ref[i]+'_fp.corners',output=ref[i]+'_mp',outexpma='.exp',statsec=statSection,chkmask='no',kpchking='no',xslm='no', maskfix='no', xzap='no', badpixu='no',shiftli=ref[i]+'.offset',mag='1',xnregis='no', negthre='yes')

        # Update the object masks with the flag maps that we created. These maps mark regions that are affected by
        # the guiding window as bad.
        # Alternatively, one could use the information in the FITS header to define these regions.
        
        
        for k,file in enumerate(input['filename']):
            root=file.replace('.fits','')
            flag='%s_flag_CHIP%d.fits' % (root,i+1)

            obm='%s_CHIP%d.sub.obm.pl' % (root,i+1)
            obm_out='%s_CHIP%d.sub.obm_adj.pl' % (root,i+1)
            iraf.imexpr(expr='(a==0 && b==0) ? 0 : 1', output=obm_out , a=obm, b=flag)

            ocm='%s_CHIP%d.sub.ocm.pl' % (root,i+1)
            ocm_out='%s_CHIP%d.sub.ocm_adj.pl' % (root,i+1)
            iraf.imexpr(expr='(a==1 && b==0) ? 1 : 0', output=ocm_out , a=ocm, b=flag)

            iraf.imcopy(input=obm_out,output=obm)
            iraf.imcopy(input=ocm_out,output=ocm)
            imdel(obm_out)
            imdel(ocm_out)

            # To get around the bug reported in http://iraf.net/article.php?story=7337
            iraf.hedit(images=obm,fields='WCSDIM', delete='yes', verify='no')
            iraf.hedit(images=obm,fields='WAXMAP01', delete='yes', verify='no')
            
            
            # For chip 4 only, add remnants to the object masks
            if i==3:
                datafile='%s_CHIP%d.sub.fits' % (root,i+1)
                memoryPos=int(params['memoryPos'])
                negLimit=-500.0

                # We replace anything that is below the negative limit 
                # with 50,000, so that it is detected as an object and masked
                data=fits.open(datafile,mode='update')
                data[0].data[data[0].data < negLimit]=50000.0

                # This also sets flux inside the guiding window to 50000. We reset that back to zero
                ystart=data[0].header['WCPOSY4']
                ywidth=data[0].header['WCSIZEY']
                xstart=data[0].header['WCPOSX4']
                xwidth=data[0].header['WCSIZEX']
                data[0].data[ystart-1:ystart+ywidth,xstart-1:xstart+xwidth]=0.0
                data.close(output_verify='ignore')

                # Run Sextractor
                segfile=datafile.replace('.fits','_seg.fits')
                cmd='sex -c remnant.sex -CHECKIMAGE_NAME '+segfile+' '+datafile
                try:
                    retCode=sp.call(cmd,shell=True)
                    if retCode < 0:
                        print >>sys.stderr, "SExtractor was terminated by signal", -retCode
                    else:
                        print >>sys.stderr, "SExtracxtor returned", retCode
                except OSError, e:
                    print >>sys.stderr, "SExtractor failed:", e

                # Update the obm, ocm and crm masks
                # The crm masks are used later to create the bpm masks, which are used in the flag and weight maps.
                for j in range(k+1,k+memoryPos): 
                    if j>= 0 and j <  len(input['filename']) and j!=k:
                        
                        root2=input['filename'][j].replace('.fits','')
                        obm='%s_CHIP%d.sub.obm.pl' % (root2,i+1)
                        obm_out='%s_CHIP%d.sub.obm_adj.pl' % (root2,i+1)
                        iraf.imexpr(expr='(a==0 && b==0) ? 0 : 1', output=obm_out , a=obm, b=segfile)

                        ocm='%s_CHIP%d.sub.ocm.pl' % (root2,i+1)
                        ocm_out='%s_CHIP%d.sub.ocm_adj.pl' % (root2,i+1)
                        iraf.imexpr(expr='(a==1 && b==0) ? 1 : 0', output=ocm_out , a=ocm, b=segfile)

                        crm='%s_CHIP%d.sub.crm.pl' % (root2,i+1)
                        crm_out='%s_CHIP%d.sub.crm_adj.pl' % (root2,i+1)
                        iraf.imexpr(expr='(a==0 && b==0) ? 0 : 1', output=crm_out , a=crm, b=segfile)
                        
                        iraf.imcopy(input=obm_out,output=obm)
                        iraf.imcopy(input=ocm_out,output=ocm)
                        iraf.imcopy(input=crm_out,output=crm)
                        imdel(obm_out)
                        imdel(ocm_out)
                        imdel(crm_out)

                
        # Run xmaskpass with the updated masks
        iraf.xmaskpass(input=ref[i]+'_fp',inexpmap=ref[i]+'_fp.exp',seccorn=ref[i]+'_fp.corners',output=ref[i]+'_mp',outexpma='.exp',statsec=statSection, mkmask='no', chkmask='no',kpchking='no', maskder='no', nmean=`nmean`,nskymin=`nskymin`,nreject='1', maskfix='no', badpixu='no',shiftli=ref[i]+'.offset',mag='1',xnregis='yes')
        # For chip 4 only, add remnants to the cosmic ray mask
        for k,file in enumerate(input['filename']):
            root=file.replace('.fits','')
            if i==3:
                datafile='%s_CHIP%d.sub.fits' % (root,i+1)
                memoryPos=int(params['memoryPos'])
                segfile=datafile.replace('.fits','_seg.fits')

                # Update the obm, ocm and crm masks
                # The crm masks are used later to create the bpm masks, which are used in the flag and weight maps.
                for j in range(k+1,k+memoryPos): 
                    if j>= 0 and j <  len(input['filename']) and j!=k:
                        
                        root2=input['filename'][j].replace('.fits','')
                        crm='%s_CHIP%d.sub.crm.pl' % (root2,i+1)
                        crm_out='%s_CHIP%d.sub.crm_adj.pl' % (root2,i+1)
                        iraf.imexpr(expr='(a==0 && b==0) ? 0 : 1', output=crm_out , a=crm, b=segfile)
                        
                        iraf.imcopy(input=crm_out,output=crm)
                        imdel(crm_out)

        
    return

def pack(params,options):
    # Unlearn IRAF tasks
    unlearn(['imcopy','hedit','imexpr'])

    # Get the list of exposures
    images=getlisting(options.pack)
        
    # ---------- Create bad pixel masks
    # This could be made more efficient
    for image in images:
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            crm='%s_%s.sub.crm.pl' % (image.replace('.fits',''),chip)
            flag='%s_flag_%s.fits' % (image.replace('.fits',''),chip)
            bpm='%s_%s.sub.bpm.fits' % (image.replace('.fits',''),chip)
            iraf.imexpr(expr='(a==0 && b==0) ? 1 : 0',output=bpm,outtype='short',a=crm,b=flag)

    # ---------- merge into a single FITS files ------------
    for image in images:
        # First, the image
        outputImage='%sa.fits' % (image.replace('.fits',''))
        hduList=fits.HDUList(fits.PrimaryHDU())
        header=fits.getheader(image)
        hduList[0].header=header
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            input='%s_%s.sub.fits' % (image.replace('.fits',''),chip)
            data,header=fits.getdata(input,header=True)
            hduList.append(fits.ImageHDU(data=data,header=header))
        hduList.writeto(outputImage,clobber=True,output_verify='ignore')

        # Update the weight and flag maps with the badpixel map
        # This includes remnants, which are saved in the crm, as well as cosmic rays
        outputFlag='%sa_flag.fits' % (image.replace('.fits',''))
        outputWeight='%sa_weight.fits' % (image.replace('.fits',''))
        for i, chip in enumerate(['CHIP1','CHIP2','CHIP3','CHIP4']):
            input='%s_%s.sub.bpm.fits' % (image.replace('.fits',''),chip)
            hdulist2=fits.open(input)
            selection=(hdulist2[0].data < 1)

            #Weight map
            hdulist=fits.open(outputWeight,mode='update')
            hdulist[i+1].data[selection]=0
            hdulist.close()  # Closing the file automatically makes the updates

            #Flag map
            hdulist=fits.open(outputFlag,mode='update')
            hdulist[i+1].data[selection]=1
            hdulist.close()  # Closing the file automatically makes the updates
            hdulist2.close()
            
    return

def createLists_tmp(params,options):
    # Create lists for scamp, swarp and imcombine
    scamp=open('scamp.list','w')
    swarp=open('swarp.list','w')
    listofDates=open('date.list','w')
    previousfilter=''
    previousdate=''
    filterlist=[]
    datelist=[]
    for line in listing:
        base=line.split()[0].replace('.fits','')
        currentfilter=line.split()[1]
        currentdate=line.split()[2]
        if currentdate != previousdate or currentfilter != previousfilter:
            previousdate=currentdate
            datelist.append(currentfilter+'_'+currentdate)
            listofDates.write(currentfilter+'_'+currentdate+'.list\n')
        if currentfilter != previousfilter:
            previousfilter=currentfilter
            filterlist.append(currentfilter)
        scamp.write(base+'.cat\n')
        swarp.write(base+'.fits\n')
    scamp.close()
    swarp.close()
    listofDates.close()

    # List based on filter, all chips
    for filter in filterlist:
        combine=open(filter+'.list','w')
        for line in listing:
            file=line.split()[0].replace('.fits','')
            if line.split()[1] == filter:
                for ext in ['0001','0002','0003','0004']:
                    combine.write(file+'.'+ext+'.resamp.fits\n')
        combine.close()    

    # List based on filter, chip 0001 ony
    for filter in filterlist:
        combine=open(filter+'_alldates.list','w')
        for line in listing:
            file=line.split()[0].replace('.fits','')
            if line.split()[1] == filter:
                ext='0001'
                combine.write(file+'.'+ext+'.resamp.fits\n')
        combine.close()    
        
    # List based on date, chip 0001 only
    for entry in datelist:
        combine=open(entry+'.list','w')
        for line in listing:
            file=line.split()[0].replace('.fits','')
            if line.split()[2] in entry and line.split()[1] in entry:
                ext='0001'
                combine.write(file+'.'+ext+'.resamp.fits\n')
        combine.close()    



    return

def SExtractor(params,options):
    f=open(options.SExtractor)
    lines=f.readlines()
    f.close()
    
    for line in lines:
        file=line.strip().replace('.fits','')
        cmd='sex -c scamp.sex -CATALOG_NAME '+file+'.cat -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE '+file+'_weight.fits -FLAG_IMAGE '+file+'_flag.fits '+file+'.fits'
        print cmd
        try:
            retCode=sp.call(cmd,shell=True)
            if retCode < 0:
                print >>sys.stderr, "SExtractor was terminated by signal", -retCode
            else:
                print >>sys.stderr, "SExtracxtor returned", retCode
        except OSError, e:
            print >>sys.stderr, "SExtractor failed:", e

    return

def swarp(params,options):
    if options.pixelScale==None:
        cmd='swarp `cat '+options.swarp+'` -c swarp.config  -RESAMPLE Y -COMBINE N -DELETE_TMPFILES N -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_SUFFIX _weight.fits'
    else:
        cmd='swarp `cat '+options.swarp+'` -c swarp.config  -RESAMPLE Y -COMBINE N -DELETE_TMPFILES N -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_SUFFIX _weight.fits -PIXELSCALE_TYPE MANUAL -PIXEL_SCALE '+options.pixelScale

    retcode=sp.call(cmd,shell=True)

    return

def digitise(params,options):
    listing=getlisting(options.digitise)
    for line in listing:
        file=line.split()[0].replace('fits','')
        for ext in ['0001','0002','0003','0004']:
            name=file+ext+'.resamp'
            iraf.imexp(expr='(a==0) ? 0 : 1',output=name+'.weight.pl',a=name+'.weight.fits')
            iraf.hedit(images=name+'.fits',fields='BPM',value=name+'.weight.pl',add='yes',verify='no')
    return


def removeBiasOffsets(params,options,method="IRAF"):
    # The imcombine task is an alternative
    # Use the obm mask to mask out objects

    from astropy.stats import sigma_clipped_stats
    
    listing=getlisting(options.removeBiasOffsets)
    nlow=500
    nhigh=500
    unlearn=(['imcopy'])
    if method=="IRAF":
        unlearn(['imcom','imdel'])

    
    for file in listing:
        image=fits.open(file,mode='update')
        if method=="IRAF":
            for ext in [1,2,3,4]:
                iraf.imcom(input=file+'['+`ext`+']',output='tmp.fits',project='yes',combine='median',reject='minmax',nlow=nlow, nhigh=nhigh)
                image2=fits.getdata('tmp.fits',0)
                # Data that are not combined in an earlier step do not have the float type
                # For some reason, the type is coverted to double?
                image[ext].data=image[ext].data.astype(float)-image2
                iraf.imdel(images='tmp.fits',verify='no')
        else:
            # Use numpy
            for ext in [1,2,3,4]:
                # Treat one chip at a time
                obm=file.replace('a.fits','_CHIP%d.sub.obm' % (ext))
                imdel(obm+'.fits')
                iraf.imcopy(input=obm+'.pl',output=obm+'.fits')
                mask=fits.getdata(obm+'.fits')
                data=image[ext].data
                # More aggressive clipping
                # This did not work as expected
                # Add the 500 lowest and 500 highest pixels to the mask
                #data_copy=numpy.copy(data)
                #data_copy[mask]=numpy.nan
                #p25=numpy.nanpercentile(data_copy, 25, axis=0)
                #p75=numpy.nanpercentile(data_copy, 75, axis=0)
                #print numpy.mean(p25),numpy.mean(p75)
                # maskedData=(mask==1) | (data < p25) | (data > p75)
                maskedData=(mask==1)
                # By default, this does five iterations.
                image2=sigma_clipped_stats(data,maskedData,sigma=3.0,axis=0)
                median=numpy.array(image2[1])
                bad=numpy.isnan(median)
                # If all values are nan, what does image2 return?
                median[bad]=0.0
                # print numpy.average(median)
                image[ext].data=image[ext].data-median
                
        image.close()
    
    return


def maskRemnants(params,options):
    # Find remnants, and update the flag and weight maps.
    # We need to run this on the images before they are swarped, because the relative image dimentions change after swarping
    listing=getlisting(options.maskRemnants)
    memoryPos=int(params['memoryPos'])
    memoryNeg=int(params['memoryNeg'])
    for i in range(len(listing)-1):
        file=listing[i].split()[0].replace('.0001.resamp.fits','')
        segfile=file+'_seg.fits'
        weightfile=file+'_weight.fits'
        datafile=file+'.fits'

        # We replace anything that is below the negative limit because of saturation
        # with 50,000, so that it is detected as an object and masked
        # Only chip4 appears to be affected by remnants
        extList=[4]
        negLimit=-500.0
        data=fits.open(datafile,mode='update')
        for ext in extList:
            data[ext].data[data[ext].data < negLimit]=50000.0
            # This also sets flux inside the guiding window to 50000. We reset that back to zero
            ystart=data[ext].header['WCPOSY4']
            ywidth=data[ext].header['WCSIZEY']
            xstart=data[ext].header['WCPOSX4']
            xwidth=data[ext].header['WCSIZEX']
            data[ext].data[ystart-1:ystart+ywidth,xstart-1:xstart+xwidth]=0.0
 
        data.close(output_verify='ignore')
        
        
        # Run Sextractor
        cmd='sex -c remnant.sex -CHECKIMAGE_NAME '+segfile+' '+datafile
        try:
            retCode=sp.call(cmd,shell=True)
            if retCode < 0:
                print >>sys.stderr, "SExtractor was terminated by signal", -retCode
            else:
                print >>sys.stderr, "SExtracxtor returned", retCode
        except OSError, e:
            print >>sys.stderr, "SExtractor failed:", e
            
        # Update the flag and weight maps of the images that precede and follow
        # This should no longer be necessary as this is now part of the code that creates the flag and weight maps
        for j in range(i-memoryNeg,i+memoryPos): 
            if j> 0 and j < len(listing) and j!=i:
                file2=listing[j].split()[0].replace('.0001.resamp.fits','')
                weightfile2=file2+'_weight.fits'
                flagfile=file2+'_flag.fits'
                
                print 'Updating the flag and weight maps for %s' % file2
                for ext in extList:
                    weight=fits.open(weightfile2,mode='update')
                    flag=fits.open(flagfile,mode='update')
                    hdulist2=fits.open(segfile)
                    # Converts the segmenataion map, so that one gets a zero if there was a bright object.
                    # One then adds this to the weight map, good regions=1
                    selection=(hdulist2[ext].data>0)
                    weight[ext].data[selection]=0
                    flag[ext].data[selection]=1
                    weight.close()  
                    flag.close()  # Closing the file automatically makes the updates
                    hdulist2.close()
        
    return

def nmad(data):
    return 1.4826 * numpy.nanmedian(numpy.abs(data-numpy.nanmedian(data)))

def selectStars(datafile,cat):
    unlearn(['display','tvmark','center','txdump'])
    # Display the image
    root=datafile.replace('.fits','')
    iraf.display(image=datafile+'[1]',frame='1',bpmask='',z1=-100, z2=1000, zscale='no',zrange='no')
    
    # Overlay the unsaturated SExtractor detections on the image
    detections=fits.getdata(cat,1)
    coo=root+'.coo'
    f=open(coo,'w')
    for detection in detections:  
        f.write('%7.2f %2.2f\n' % (detection['XWIN_IMAGE'],detection['YWIN_IMAGE']))
    f.close()
    iraf.tvmark(frame='1',coords=coo,label='yes',color=205,mark='circle',nxoffse=40,radii=20,pointsize=2,txsize=3)

    # Ask the user to select 5 stars
    iraf.noao()
    iraf.digi()
    iraf.apphot()
    iraf.datapars.scale='0.3'
    iraf.center(image=root+'[1]')
    centres=root+'1.ctr.1'
    xcenterIn=iraf.txdump(textfile=centres,fields='XCENTER',expr='yes',Stdout=1)
    ycenterIn=iraf.txdump(textfile=centres,fields='YCENTER',expr='yes',Stdout=1)
    iraf.delete(files=centres,verify='no')

    # Write these positions to a file
    ctr=root+'.coo'
    f=open(ctr,'w')
    for x,y in zip(xcenterIn,ycenterIn):
        f.write('%s %s\n' % (x,y))
    f.close()
    
    return ctr

def measureFWHM(baseImage, image, stars):
    unlearn(['radprof','txdump'])
    iraf.noao()
    iraf.digi()
    iraf.apphot()
    iraf.datapars.scale='0.3'

    root=image.replace('.fits','')
    pfr=root+'1.pfr.1'
    if baseImage==None:
        # No offsets to apply
        iraf.radprof(image=image+'[1]',radius='4',step='0.3',coords=stars,output=pfr,interac='no',verify='no',verbose='yes',update='no')
        pfwhm=iraf.txdump(textfile=centres,fields='PFWHM',expr='yes',Stdout=1)
    else:
        # Determine the offsets that need to be applied using the swarp header files
        # Code not yet finished
        iraf.radprof(image=image+'[1]',radius='4',step='0.3',coords=stars,output=pfr,interac='no',verify='no',verbose='yes',update='no')
        pfwhm=iraf.txdump(textfile=centres,fields='PFWHM',expr='yes',Stdout=1)
        
    return

def quality(params,options):
    # Run SExtractor
    listing=getlisting(options.quality)
    output=open(options.quality.replace('_swarp.list','.qc'),'w')
    output.write('#Frame          \tchip1\tchip2\tchip3\tchip4\tFWHM\tScale\n')

    firstFile=True
    for datafile in listing:
    
##        segfile=datafile.replace('.fits','_seg.fits')
##        cmd='sex -c depth.sex -CATALOG_TYPE FITS_1.0 -CHECKIMAGE_NAME '+segfile+' '+datafile 
##        try:
##            retCode=sp.call(cmd,shell=True)
##            if retCode < 0:
##                print >>sys.stderr, "SExtractor was terminated by signal", -retCode
##            else:
##                print >>sys.stderr, "SExtracxtor returned", retCode
##        except OSError, e:
##a            print >>sys.stderr, "SExtractor failed:", e

        # There are two approaches.
        # Use the FWHM in the FITS headers
        # Compute the FWHM directly

        # Direct computation
        ## We use the output of SExtractor to locate the bright stars, but we use the IRAF task radprof to measure the
        ## quality of a user defeined subsample. We could investigatge using PSFEX

        ##fwhm=[]
        ## Use the first file to define the initial list
        ##if firstFile:
        ##    stars=selectStars(datafile,'test.cat')
        ##    firstFile=False
        ##    # Measure the PSF with radprof
        ##    fwhm.append(measureFWHM(None,datafile,stars))
        ##else:
        ##    fwhm.append(measureFWHM(listing[0],datafile,stars))

        # We use the object mask created by xdimsum, if it exists to help compute the noise
    
        nmadList=[]
        data=fits.open(datafile)
        for chip in range(0,4):
            maskfile=datafile.replace('a.fits', '_CHIP%d.sub.obm.fits' % (chip+1))
            try:
                mask=(fits.getdata(maskfile)==0)
                nmadList.append('%5.1f' % (nmad(data[chip+1].data[mask])))
            except:
                nmadList.append('%5.1f' % (nmad(data[chip+1].data)))
                
        # Image quality
        nmadList.append('%5.3f' % (fits.getval(datafile,'WCFWHMX1',0)))

        # We also add the scale factor
        resampName=datafile.replace('.fits','.0001.resamp.fits')
        try:
            scale1=fits.getval(resampName,'SCALE',0)
        except:
            scale1=1.0

        try:
            scale2=fits.getval(resampName,'SCALE2',0)
        except:
            scale2=1.0
        
        try:
            scale3=fits.getval(resampName,'SCALE3',0)
        except:
            scale3=1.0

        nmadList.append('%5.3f' % (scale1*scale2*scale3))
        output.write('%s\t%s\n' % (datafile,'\t'.join(nmadList)))
        
        data.close()
    output.close()

    return

def normalise(params,options):
    offsetfile=open(options.normalise,'r')
    offsets=offsetfile.readlines()
    offsetfile.close()

    for offset in offsets:
        if offset[0]!='#':
            image=offset.split()[0]
            scale=10**(float(offset.split()[6])/-2.5)
            print 'Applying scale',scale, 'for file', image
            for ext in ['0001','0002','0003','0004']:
                name=image+'.'+ext+'.resamp'
                try:
                    fits.getval(input,'SCALE')
                    print 'Scale factor has already been applied'
                except:
                    iraf.imarith(operand1=name,op='*',operand2=scale,result=name)
                    iraf.hedit(images=name,fields='SCALE',value=`scale`,add='yes',verify='no')
    return

def normaliseChips(params,options):
    # Normalise the chips sensitivities
    unlearn(['hedit','imarith'])
    factor=[float(x) for x in options.factors.split(',')]
    
    offsetfile=open(options.normaliseChips,'r')
    offsets=offsetfile.readlines()
    offsetfile.close()
    ext=['0001','0002','0003','0004']
    print factor
    for offset in offsets:
        if offset[0]!='#':
            image=offset.split()[0]
            for i in range(0,4):
                name=image.replace('.fits','.'+ext[i]+'.resamp.fits')
                scale=10**(float(factor[i])/-2.5)
                print 'Applying scale',scale, 'for file', name
                iraf.imarith(operand1=name,op='*',operand2=scale,result=name)
                iraf.hedit(images=name,fields='SCALE2',value=scale,add='yes',verify='no')
    return

def normaliseRuns(params,options):
    imageList=open(options.normaliseRuns,'r')
    images=imageList.readlines()
    imageList.close()

    for image in images:
        scale=10**(float(options.factors)/-2.5)
        print 'Applying scale',scale, 'for file', image.strip()
        for ext in ['0001','0002','0003','0004']:
                name=image.strip().replace('.fits','.'+ext+'.resamp.fits')
#                print name,scale
                iraf.imarith(operand1=name,op='*',operand2=scale,result=name)
                iraf.hedit(images=name,fields='SCALE3',value=`scale`,add='yes',verify='no')


def combineChipByChip(params,options):
    unlearn(['imcom','imcopy'])
    # Create one combined image per chip 
    listing=getlisting(options.combine)
    for ext in ['0001','0002','0003','0004']:
        file=open('tmp.list','w')
        for line in listing:
            file.write(line.replace('.fits','.%s.resamp.fits\n' % (ext)))
        file.close()
        output=options.combine.replace("_swarp.list","")
        # Delete existing images
        imdel(output+'_'+ext+'.fits')
        imdel(output+'_'+ext+'.exp.pl')
        imdel(output+'_'+ext+'.exp.fits')
       
        iraf.imcom(input='@tmp.list',output=output+'_'+ext,expmask=output+'_'+ext+'.exp',offsets='wcs',combine='average',reject='sigclip',masktype='goodvalue',maskval='1',lthresh='-1000',lsigma='5',hsigma='5')
        iraf.imcopy(input=output+'_'+ext+'.exp.pl',output=output+'_'+ext+'.exp.fits')
    return

def imdel(image):
    unlearn(['imdel'])
    if os.path.isfile(image):
        iraf.imdel(images=image,verify='no')
    return

def findIndex(exposure,qc):
    for i,filename in enumerate(qc['filename']):
        start=filename.find('.')
        if filename[0:start]==exposure[0:start]:
            return i
    return None

def combineCoAdds(params,options):
    unlearn(['imcom'])
    # It is not clear if imcombine can combine images that can weight individual pixels using the exposure map
    # We might be able to use Swarp for this ....
    # So we just average for now, which is far from ideal.
    output=options.combine.replace('_coadd.list','')
    
    # Remove output images, if they exist
    imdel(output+'.fits')
    imdel(output+'.exp.pl')

    # Image first
    iraf.imcom(input='@'+options.combine,output=output,offsets='wcs',combine='average',reject='none')

    # Then the exposure maps
    output_pl=options.combine.replace('_coadd.list','_coadd_pl.list')
    filenames=numpy.genfromtxt(options.combine,dtype=[('filename','a100')])
    
    f=open(output_pl,'w')
    for filename in filenames['filename']:
        f.write('%s\n' % (filename.replace('.fits','.exp.pl')))
    f.close()

    iraf.imcom(input='@'+output_pl,output=output+'.exp.pl',offsets='wcs',combine='average',reject='none')
    
    return

def combineAll(params,options):
    unlearn(['imcom','imar','imcopy'])
    
    
    # We apply the same weight to each of the chips is a single exposure
    if options.type=='All':
        # No weighting
        weight='none'
        output=options.combine.replace("_imcom.list","_nw")
    elif options.type=='FWHM':
        # Weighting by FWHM
        output=options.combine.replace("_imcom.list","_wf")
        weightfile=options.combine.replace("_imcom.list","_wf")
    elif options.type=='noise':
        # Weighting by noise
        output=options.combine.replace("_imcom.list","_wn")
        weightfile=options.combine.replace("_imcom.list","_wn")
    elif options.type=='FWHM_and_noise':
        # Weighting by both
        output=options.combine.replace("_imcom.list","_wb")
        weightfile=options.combine.replace("_imcom.list","_wb")
    else:
        print 'Not a valid option'
        exit()
        
    listing=getlisting(options.combine)
    if options.type!='All':
        qc=numpy.genfromtxt(options.qc,usecols=(0,1,5,6),skip_header=1,dtype=[('filename','a100'),('noise',float),('FWHM',float),('Scale',float)])

    # The scale is included, because the noise is computed on the image before it is swarped and scaled.    
        
    if options.type!='All':
        totalWeight=0
        weight='@'+weightfile
        f=open(weightfile,'w')
        for i, exposure in enumerate(listing):
            index=findIndex(exposure,qc)
            if index==None:
                print "Could not find file"
                exit()
#            index=divmod(i,4)[0]
            if options.type=='FWHM':
                # Weighting by FWHM
                if qc['FWHM'][index]==0:
                    f.write('%f\n' % (1.0))
                    totalWeight+=1.0
                else:
                    f.write('%f\n' % (1.0 / (qc['FWHM'][index])**2.0))
                    totalWeight+=(1.0 / (qc['FWHM'][index])**2.0)

            elif options.type=='noise':
                # Weighting by noise
                f.write('%f\n' % (1.0 / (qc['noise'][index]*qc['Scale'][index])**2.))
                totalWeight+=1.0 / (qc['noise'][index]*qc['Scale'][index])**2.
            else:
                # Weighting by noise and FWHM
                if qc['FWHM'][index]==0:
                    f.write('%f\n' % (1.0 / (qc['noise'][index]*qc['Scale'][index])**2.))
                    totalWeight+=1.0 / (qc['noise'][index]*qc['Scale'][index])**2.
                else:
                    f.write('%f\n' % (1.0 / ((qc['noise'][index]*qc['Scale'][index]*qc['FWHM'][index])**2.0)))
                    totalWeight+=1.0 / (qc['noise'][index]*qc['Scale'][index]*qc['FWHM'][index])**2.0

        f.close()

    # Remove output images, if they exist
    imdel(output+'.fits')
    imdel(output+'.exp.pl')
    imdel(output+'.exp.fits')
    print "Total weight % 5.3f" % totalWeight 
    
    iraf.imcom(input='@'+options.combine,output=output,expmask=output+'.exp',offsets='wcs',combine='average',reject='sigclip',masktype='goodvalue',maskval='1',zero='median',statsec='[400:1600,400:1600]',weight=weight,lthresh='-1000',lsigma='5',hsigma='5')
    iraf.imcopy(input=output+'.exp.pl',output=output+'.exp.fits')
    iraf.imar(operand1=output+'.exp.fits',op='*',operand2=totalWeight,result=output+'.exp.fits')
#    iraf.imcom(input='@'+options.combine,output=output,expmask=output+'.exp',offsets='wcs',combine='average',reject='minmax',masktype='goodvalue',maskval='1',zero='median',statsec='[400:1600,400:1600]',lthresh='-1000',nlow='3',nhigh='3')

    return

def merge(params,options):
    input=options.merge
    if options.output==None:
        output=options.merge.replace(".fits",'_v%s' % (params['version']))
    else:
        output=options.output
        
    iraf.fitsutil()
    unlearn(['imcopy','fxcopy','fxinsert'])
    iraf.imdel(images=output+'_ipe.fits',verify='no')
    iraf.imcopy(input=input,output=output+'_ipe.fits')
    if os.path.isfile(input.replace('.fits','.exp.pl')):
##        iraf.imcopy(input=input.replace('.fits','.exp.pl'),output=output+'.exp.fits')
        iraf.fxinsert(input=output+'.exp.fits[0]',output=output+'_ipe.fits[0]',groups=0)
    else:
        iraf.fxinsert(input=options.merge.replace('.fits','.weight.fits[0]'),output=output+'_ipe.fits[0]',groups=0)

    
    return

def cleanup(params,options):
    cmd='rm *resamp* *~ temp* tmp* test* *seg.fits'
    retcode=sp.call(cmd,shell=True)
    cmd='rm *CHIP?.fits'
    retcode=sp.call(cmd,shell=True)
    cmd='rm *sub*pl'
    retcode=sp.call(cmd,shell=True)
    cmd='rm *sub*fits'
    retcode=sp.call(cmd,shell=True)
    cmd='rm *s.fits'
    retcode=sp.call(cmd,shell=True)
    cmd='rm *w.fits'
    retcode=sp.call(cmd,shell=True)

    return

def deepcleanup(params,options):
    cmd='rm *pl *head *cat *.fits'
    retcode=sp.call(cmd,shell=True)

    return

def zeroPoint(params,options):
    # compute the ZP using 2MASS
    filter=options.passband
    CRVAL=numpy.matrix([[fits.getval(options.zeroPoint,'CRVAL1',0)],[fits.getval(options.zeroPoint,'CRVAL2')]])
    CRPIX=numpy.matrix([[fits.getval(options.zeroPoint,'CRPIX1',0)],[fits.getval(options.zeroPoint,'CRPIX2')]])
    CD=numpy.matrix([[fits.getval(options.zeroPoint,'CD1_1',0),0.0],[0.0,fits.getval(options.zeroPoint,'CD2_2',0)]])
    # Find stars in the 2MASS catalogue
    #print CD,CRVAL,CRPIX

    cmd='find2mass -r %s -c %9.5f %9.5f' % (params['radius'],CRVAL[0,0],CRVAL[1,0])
    p1=sp.Popen(cmd, shell=True, stdout=sp.PIPE)
    stars=parseCDC(p1.communicate(0)[0],options.zeroPoint.replace('.fits','.2MASS'),filter,params)
    
    
    # Determine the x,y position of the 2MASS stars
    # The coordinate transformation in the header is
    # c-c_0 = M * (x-x_0)

    tvmarkFile=options.zeroPoint.replace('.fits','.txt')
    file=open(tvmarkFile,'w')
    xcenterRef=[]
    ycenterRef=[]

    for star in stars:
        coord=numpy.matrix([[star['ra']],[star['dec']]])
        proj=numpy.matrix([[numpy.cos(coord[1,0]*numpy.pi/180.),0.0],[0.0,1.0]])
        x=numpy.linalg.inv(CD)*proj*(coord-CRVAL)+CRPIX
        file.write('%7.2f %7.2f %s\n' % (x[0,0],x[1,0],star['id']))
        xcenterRef.append(x[0,0])
        ycenterRef.append(x[1,0])


    file.close()
    unlearn(['display','tvmark'])
    
    iraf.display(image=options.zeroPoint+'[0]',frame=1,zr='no',zs='no',z1=-100,z2=300)
    iraf.tvmark(frame='1',coords=tvmarkFile,label='yes',color=205,mark='circle',nxoffse=40,radii=20,pointsize=2,txsize=3)
    
    # If we do not trust the coordinates, then we ask the user to select stars in the image
    trust=True
    iraf.noao()
    iraf.digi()
    iraf.apphot()
    unlearn(['phot','txdump'])
    iraf.fitskypars.salgori='median'
    iraf.fitskypars.annulus='4'
    iraf.fitskypars.dannulu='6'
    iraf.datapars.scale='0.16'
    iraf.photpars.apertur='4'
    iraf.photpars.zmag='0'
    if not trust:
        iraf.phot(image=options.zeroPoint.replace('.fits','[0]'))
    else:
        iraf.phot(image=options.zeroPoint.replace('.fits','[0]'),coords=tvmarkFile,interact='no')
        
    mags=options.zeroPoint.replace('.fits','0.mag.1')
    xcenterIn=iraf.txdump(textfile=mags,fields='XCENTER',expr='yes',Stdout=1)
    ycenterIn=iraf.txdump(textfile=mags,fields='YCENTER',expr='yes',Stdout=1)
    mag=iraf.txdump(textfile=mags,fields='MAG',expr='yes',Stdout=1)
    # The magnitude error is not very precise
    magerr=iraf.txdump(textfile=mags,fields='MERR',expr='yes',Stdout=1)

    iraf.delete(files=mags,verify='no')

    # ZP
    ZP=[]
    ZP_err=[]
    sum1=0
    sum2=0
    phot=open(options.zeroPoint.replace('.fits','.phot'),'w')
    phot.write('ID\tRA\tDEC\tmag_INST\terr_mag_INST\tmag_2MASS\terr_mag_2MASS\tQfl\tZP\terr_ZP\n')
    # Only A class stars are considered
    use=[]
    for i in range(len(xcenterIn)):
         if stars[i]['flag'] in ['A'] and stars[i]['magerr'] > 0 :
             reply=raw_input('Using star # %d?' % (stars[i]['id']))
             if reply=='y':
                 use.append(i)

    for i in use:
        ZP.append(stars[i]['mag']-float(mag[i]))
        ZP_err.append(numpy.sqrt(stars[i]['magerr']**2.+float(magerr[i])**2.))
        print 'Using star #',stars[i]['id']
        phot.write('%s\t%9.5f\t%9.5f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%s\t%6.3f\t%6.3f\n' %(stars[i]['id'],stars[i]['ra'],stars[i]['dec'],float(mag[i]),float(magerr[i]),stars[i]['mag'],stars[i]['magerr'],stars[i]['flag'],ZP[-1],ZP_err[-1]))
        sum1+=ZP[-1]/ZP_err[-1]**2.
        sum2+=1.0/ZP_err[-1]**2.

    phot.close()
    

    # The error bars are probably understimated
    if len(ZP) > 0:
        iraf.hedit(images=options.zeroPoint+'[0]',fields='ZP',value=sum1/sum2,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='err_ZP',value=numpy.sqrt(1.0/sum2),add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='scatt_ZP',value=numpy.std(ZP)/numpy.sqrt(len(ZP)),add='yes',verify='no')
    else:
        iraf.hedit(images=options.zeroPoint+'[0]',fields='ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='err_ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='scatt_ZP',value=-9.99,add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='n_ZP',value=len(ZP),add='yes',verify='no')

    #iraf.hedit(images=options.zeroPoint,fields='AIRMASS',value=airmass,verify='no')
    
    # Add some keywords

    iraf.hedit(images=options.zeroPoint+'[0]',fields='MAGSYS',value='Vega',add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='VERSION',value='v'+params['version'],add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='REDUCED',value=params['reducer'],add='yes',verify='no')

    return

def parseCDC(input,output,filter,params):
    lines=input.split('\n')
    file=open(output,'w')
    ra=[]
    dec=[]
    mag=[]
    magerr=[]
    flag=[]
    magIndex={'J':3,'J1':3,'Ks':5}
    flagIndex={'J':0,'J1':0,'Ks':2}
    file.write('ID\tRA\tDEC\tmag\tmag_err\tQfl\n')
    for line in lines:
        if len(line) > 0:
            if line[0]!='#':
                entries=line.split('|')
                TwoMASSflag=entries[6].split()[0][flagIndex[filter]:flagIndex[filter]+1]
                try:
                    m=float(entries[magIndex[filter]].split()[0])
                    me=float(entries[magIndex[filter]].split()[1])
                except:
                    continue

                if TwoMASSflag in ['A'] and me < 0.1 and m > float(params['magLower']) and m < float(params['magUpper']):
                    ra.append(float(entries[0].split()[0]))
                    dec.append(float(entries[0].split()[1]))
                    mag.append(m)
                    if entries[5].split()[1]=='---':
                        magerr.append(-9.99)
                    else:
                        magerr.append(float(entries[magIndex[filter]].split()[1]))
                    flag.append(TwoMASSflag)
                    file.write('%s\t%9.5f\t%9.5f\t%s\t%s\t%s\n' % (entries[2],ra[-1],dec[-1],mag[-1],magerr[-1],flag[-1]))

    
    file.close()
    stars=numpy.zeros(len(ra),dtype=[('id','int'),('ra','float'),('dec','float'),('mag','float'),('magerr','float'),('flag','a3')])
    stars['ra']=ra
    stars['dec']=dec
    stars['mag']=mag
    stars['magerr']=magerr
    stars['flag']=flag
    stars['id']=numpy.arange(1,len(ra)+1)

    return stars
