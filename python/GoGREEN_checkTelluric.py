from optparse import OptionParser
import Constants
import GoGREEN_Library as GoGREEN
from astropy.io import fits,ascii
import GoGREEN_spectra as ggsp
import numpy as np
import os
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
import getopt
import re
import matplotlib.ticker as plticker
from astropy.convolution import convolve, Box1DKernel

parser=OptionParser()
(options, args) = parser.parse_args()

try:
    cname=str(args[0])
except:
    cname = raw_input('Cluster name? e.g. SXDF64')
try:
    maskname=str(args[1])
except:
    maskname = raw_input('Mask name? e.g. GS2014BLP001-08')
try:
    tellname=str(args[2])
except:
    tellname = raw_input('Telluric name? e.g. LTT2415_15B_Telluric')
try:
	apnum=int(args[3])
except:
	apnum=0
traceorder=0

constants=Constants.GoGREENPath()
dir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"

calonedprefix='cal_oned_'
tcalonedprefix='tcal_oned_'
imname="rfcs_"+maskname
calonedfile=dir+calonedprefix+imname+".fits"
if not os.path.isfile(calonedfile):
	calonedfile=calonedfile+'.gz'
if not os.path.isfile(calonedfile):
	imname="cs_"+maskname
	calonedfile=dir+calonedprefix+imname+".fits"
if not os.path.isfile(calonedfile):
	calonedfile=calonedfile+'.gz'

tcalonedfile=dir+tcalonedprefix+imname+".fits"
if not os.path.isfile(tcalonedfile):
	tcalonedfile=tcalonedfile+'.gz'
if not os.path.isfile(tcalonedfile):
	imname="cs_"+maskname
	tcalonedfile=dir+tcalonedprefix+imname+".fits"
if not os.path.isfile(tcalonedfile):
	tcalonedfile=tcalonedfile+'.gz'

calibrated=fits.open(calonedfile)
telluric_corrected=fits.open(tcalonedfile)
header_primary=calibrated[0].header
nsciext=header_primary['NSCIEXT']
header_primary['NEXTEND']=1+3*nsciext
mdf=calibrated['mdf'].data
#try:
telluric=fits.open(constants.reduced+'Telluric/'+tellname+'.fits')
tpix=np.arange(1,telluric[0].header['naxis1']+1)
tll=telluric[0].header['crval1']+(tpix-telluric[0].header['crpix1'])*telluric[0].header['cd1_1']
tlims=np.where((tll>7500) & (tll <10100))
plottell=True
#except:
#	plottell=False
if apnum > 0: 
	irange=range(apnum,nsciext+1)
else:
	irange=range(1,nsciext+1)
	magsort=True
	p1galaxies=np.where(mdf['priority']=='1')
	if magsort: irange=mdf['EXTVER'][p1galaxies][np.argsort(mdf['MAG'][p1galaxies])]
usercontinue=True
for i in irange:
	if usercontinue:
		cal=calibrated['sci',i].data
		tcal=telluric_corrected['sci',i].data
		calhd=calibrated['sci',i].header
		pix=np.arange(1,calhd['naxis1']+1)
		ll=calhd['crval1']+(pix-calhd['crpix1'])*calhd['cd1_1']
		npix=cal.shape[0]
		fig,ax=plt.subplots(1)
		lims=np.where((ll>7500) & (ll <10100))
		if plottell:
			plt.plot(tll[tlims],telluric[0].data[tlims]*np.mean(cal[lims]),c='r',lw=1)
		plt.plot(ll[lims],convolve(cal,Box1DKernel(5))[lims],lw=1)
		plt.plot(ll[lims],convolve(tcal,Box1DKernel(5))[lims],c='k')

		thismanager=plt.get_current_fig_manager()
		thismanager.resize(1200, 500)
		plt.show(block=False)
		try:
			userinput=raw_input('Continue? (n to quit): ').split()[0]
			if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': usercontinue=False
		except:
			pass
		plt.close('all')
