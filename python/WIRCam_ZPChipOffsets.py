#!/Users/clidman/Science/Programs/anaconda3/envs/iraf27/bin/python
# Python script to process WIRcam data

from optparse import OptionParser
import numpy as np

def ZPoffsets(options):
    import subprocess as sp
    import WIRCam_library as WIRCam
    import matplotlib.pyplot as plt
    import astropy.io.fits as fits

    # Add these parameters ...
    aperture=20
    lower=-13
    upper=-10
    plot=False
    
    if options.config!=None:
        params=WIRCam.buildDictionary(options.config)
        WIRCam.setup(params,options)
    else:
        params={}
    
    input=options.input
        
    
    # Run SExtractor in singleimage mode

    for ext in ['0001','0002','0003','0004']:
        fitsfile=input+'_'+ext+'.fits'
        weight=input+'_'+ext+'.exp.fits'
        catfile=input+'_'+ext+'.cat'
        cmd='sex -c zp.sex -DETECT_MINAREA 5 -DETECT_THRESH 10 -ANALYSIS_THRESH 10 -CATALOG_NAME %s -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE %s -PHOT_APERTURES %3.1f %s' % (catfile,weight,aperture,fitsfile)
        retcode=sp.call(cmd,shell=True)

    # Match the catalogues in RA and compute the offset in magnitude

    output=input+'.chipoffsets'

    file=open(output,'w')

    file.write('Lower: %4.1f\n' % lower)
    file.write('Upper: %4.1f\n' % upper)
    file.write('Aperture: %3.1f' % aperture)

    for comb in [['0001','0002'],['0001','0003'],['0001','0004'],['0002','0003'],['0003','0004']]:
        firstcat=input+'_'+comb[0]+'.cat'
        master=catalogue()
        master.columns,master.data=master.read(firstcat)
        secondcat=input+'_'+comb[1]+'.cat'
        slave=catalogue()
        slave.columns,slave.data=slave.read(secondcat)
        mag1,magdiff,magerr=match(master,slave)
        if comb==['0001','0002']:
            plt.subplot(511)
        elif comb==['0001','0003']:
            plt.subplot(512)
        elif comb==['0001','0004']:
            plt.subplot(513)
        elif comb==['0002','0003']:
            plt.subplot(514)
        else:
            plt.subplot(515)

        plt.errorbar(mag1,magdiff,magerr,None,marker='o',mfc='b',linestyle='None',ecolor='k')
        plt.plot([-17,-8],[0,0])
        plt.ylim(-0.3,0.3)

        offset=np.array([],dtype='float32')
        sum1=0.0
        sum2=0.0
        num3=0
        for j in range(len(mag1)):
            if mag1[j] > lower and mag1[j] < upper and magdiff[j] < 0.5 and magdiff[j]>-0.5:
                sum1=sum1+magdiff[j]/magerr[j]**2.
                sum2=sum2+1/magerr[j]**2.
                num3=num3+1
                offset=np.append(offset,magdiff)


        if num3 > 0:
            print firstcat+' - '+secondcat
            print 'Number of stars: '+ `num3`[0:3]
            print 'Weighted mean and error: '+`sum1/sum2`[0:6]+' '+`np.sqrt(1/sum2)`[0:6]
            print 'Scatter: '+`np.std(offset)`[0:6]
            print 'Median: '+`np.median(offset)`[0:6]
            file.write(firstcat+' - '+secondcat+'\n')
            file.write('Number of stars: '+ `num3`[0:3]+'\n')
            file.write('Weighted mean and error: '+`sum1/sum2`[0:6]+' '+`np.sqrt(1/sum2)`[0:6]+'\n')
            file.write('Scatter: '+`np.std(offset)`[0:6]+'\n')
            file.write('Median: '+`np.median(offset)`[0:6]+'\n')

    plt.show()
    plt.close()

    file.close()

    return
    
class catalogue:
    """A catalog"""
    def __init__(self):
        self.columns=[] 
        self.data=[]

    def read(self,cat):
        file=open(cat,'r')
        for line in file.readlines():
            entries=line.split()
            if entries[0]=="#":
                self.columns.append(entries[2])
            else:
                self.data.append(entries)
        file.close()
        return self.columns,self.data

def match(cat1,cat2):
    # Find the indeces
    thresh=0.2

    raindex=cat1.columns.index('XWIN_WORLD')
    decindex=cat1.columns.index('YWIN_WORLD')
    magindex=cat1.columns.index('MAG_APER')
    magerrindex=cat1.columns.index('MAGERR_APER')
    flagindex=cat1.columns.index('FLAGS')

    mag=[]
    magerr=[]
    magdiff=[]

    for i in range(0,len(cat1.data)):
        for j in range(0,len(cat2.data)):
            r= np.sqrt((float(cat1.data[i][raindex])-float(cat2.data[j][raindex]))**2*np.cos(float(cat1.data[i][decindex])*np.pi/180.)+(float(cat1.data[i][decindex])-float(cat2.data[j][decindex]))**2)*3600. 
            if r < thresh and cat1.data[i][flagindex]=='0' and cat2.data[j][flagindex]=='0':
                mag.append(float(cat1.data[i][magindex]))
                magdiff.append(float(cat1.data[i][magindex])-float(cat2.data[j][magindex]))
                magerr.append(np.sqrt(float(cat1.data[i][magerrindex])**2+float(cat2.data[j][magerrindex])**2))
#                print r,cat1.data[i][magindex],cat1.data[i][magerrindex],cat2.data[j][magindex],cat2.data[j][magerrindex]

    return mag,magdiff,magerr

if __name__ == '__main__':

    parser=OptionParser()


    parser.add_option('-i','--input',dest='input',
                      default=None,help='Input file')

    parser.add_option('-c','--config',dest='config',
                      default=None,help='Configuration file')

    (options,args)=parser.parse_args()

    ZPoffsets(options)
