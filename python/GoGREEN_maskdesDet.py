
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle,Circle
import sys,os
from astropy.io import ascii as asciitable
 
import logging  # only needed for debugging



#logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
logging.basicConfig(filename='tmp.log', filemode='w', level=logging.DEBUG)

# Ham: totx =3136


from GoGREEN_plotmask import drawHam,showSlits

dbglvl = 0


def showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0):
    tx0=x0-cfg.xpadPix/2.
    tx1=x0+xWidth+cfg.xpadPix/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-cfg.ypadPix/2.
    ty1=y0+yWidth+cfg.ypadPix/2.

    figDEBUG = plt.figure()
    axDEBUG = figDEBUG.add_subplot(111)
    xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0
    #plt.plot(xCen,yCen,'k,') # these points are spectral box centres)

    okMask = np.where( inMask==1 )[0]

    # plot objects scaled by weighting:
    ok=np.where(wt>0.0)[0]
    for kk in ok:
        sz = 50.*wt[kk]
#        cap = np.where(sz>120)[0]
#        sz[cap]=120.
        col='r'
        if sz > 70.: 
            sz=70.0 
            col='b'

        circ = Circle((xobj[kk],yobj[kk]),sz,color=col,alpha=0.3)
        #plt.text(xobj[kk],yobj[kk],'%.0f'%id[kk])
        plt.text(xobj[kk],yobj[kk],'%.0f'%kk)
        axDEBUG.add_patch(circ)
    bad = np.where(wt==0.0)[0]
    if (len(bad)>0):
        plt.plot(xobj[bad],yobj[bad],'rx')

    if (len(okMask)>0):
        for kk in okMask:
#            print kk
            # SPECTRUM:

            sz = 50.*wt[kk]
            if sz > 70.: sz=70.0
            circ = Circle((xobj[kk],yobj[kk]),sz,color='b',alpha=0.3)
            axDEBUG.add_patch(circ)

            rect = Rectangle((x0[kk],y0[kk]),xWidth[kk],yWidth[kk],color='g',ec='none',alpha=0.3)
            axDEBUG.add_patch(rect)
            # slit:
            sx0=x0-swid/2.+xToLeft
            sx1=swid
            # in wavelength space
            slit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='m',ec='none')
            axDEBUG.add_patch(slit)
            # in direct image/mask
            sx0=xobj-swid/2.
            sx1=swid
            rslit=Rectangle((sx0[kk],y0[kk]),sx1[kk],yWidth[kk],color='none',ec='k')
            axDEBUG.add_patch(rslit)

            plt.axis('equal')
            plt.plot(xobj,yobj,'k,')
            plt.plot(xobj[okMask],yspe[okMask],'k,')
            plt.xlabel('GMOS x (Ham. binned pix)')
            plt.ylabel('GMOS y (Ham. binned pix)')
        #    if (title):
        #        plt.title(title)
            plt.title('%.0f slits'%np.sum(inMask))
            axDEBUG.set_xlim(-50,3000)
            axDEBUG.set_ylim(-50,2300)

##    drawHam(axDEBUG)
    plt.show()



def defineGMOSspec(xobj,yobj,slen,wt,NSmode='micro',fillMode=False):
    # -- Use optical model to obtain wavelength limits (converted to pixels)
    # based on grating and filter selected. 

    # GMOS HAS RED AND BLUE REVERSED FROM DIRECTION GOD INTENDED!!
#    from config import *


    if(NSmode=='band'):
        print 'BAND shuffle'
        # band shuffle instead of micro-shuffle:
        slen=slen/2.0
        yHt=cfg.ymaxField-cfg.yminField
        yF0=cfg.yminField+0.33*yHt
#        ymaxField=cfg.yminField+0.67*yHt
        # need to allow for slit and padding:
        # slen could be a vector:
        try:
            uslen = np.min(slen)
        except:
            uslen = slen
        ymaxField=cfg.yminField+0.67*yHt - uslen/2.0 - cfg.ypadPix/2.0
        #yminField = yF0
        yminField = yF0 + uslen/2.0 + cfg.ypadPix/2.0
    else:
        ymaxField = cfg.ymaxField
        yminField = cfg.yminField

    # R150, cenwave=850nm, +RG610:
#    cenwave=850 ; linDisp=0.39353344971414417 ; coeff=np.array([-7.78878524e-02,   1.54497292e+02])
#    blueCutA = 610.0
#    redCutA = 1045.0
    
    #Spectrum Length: 1105.3698238764111 (pixels)
    #(1050.-610.)/linDisp = 1118.
    # w. OG515. Spectrum Length: 1346.7724290907997 (pixels)
    #In [77]: (1050-515.)/0.39353344971414417
    #Out[77]: 1359.4778293652412
    # OKAY, actual red cutoff in gmmps seems to be 1045A rather than 1050, so update.

    if (fillMode):
#        blueCutA = 750.0 #nm
        blueCutA = 900.0 #nm
        redCutA = 850.0 #nm
        lenRightPix = (cfg.cenwave - blueCutA)/cfg.linDisp  # REVERSED FOR GMOS!
        lenLeftPix = (redCutA - cfg.cenwave)/cfg.linDisp 
    else:
        lenRightPix = (cfg.cenwave - cfg.blueCutA)/cfg.linDisp  # REVERSED FOR GMOS!
        lenLeftPix = (cfg.redCutA - cfg.cenwave)/cfg.linDisp 

#    xcen=3000. #**** made-up!
    xcen=cfg.ccd_cx #**** made-up!
#    xspec=xobj #(xobj-xcen)+(-0.2)*(xobj-xcen)+xcen # make up anamorphic factor
#    xspec = xobj -np.polyval(coeff,xobj) # measured anamorphic factor  # REVERSED FOR GMOS!
    xspec = xobj +np.polyval(cfg.WLcoeff,xobj) # measured anamorphic factor
    yspec=yobj 

    xlo = lenLeftPix * 2.0 # binning
    xhi = lenRightPix * 2.0 # binning
    

    # -- start off with ylo/yhmaxi half slit length
    yhi=slen/2.
    ylo=slen/2.



    # check for objects where spectra fall out of field:
    keepflag=np.ones(len(xobj))
    outy = np.where( (yobj < yminField) | (yobj > ymaxField) )[0]
    keepflag[outy]=0
    xLeft = xspec-xlo
    xRight = xspec+xhi
    outLeft = np.where(xLeft< cfg.xminSField)[0]
    keepflag[outLeft]=0 
    outRight = np.where(xRight> cfg.xmaxSField)[0] 
    keepflag[outRight]=0 
    
    # also, object must be visible in field. DOH!
    outFoV=np.where( (xobj<cfg.xminField) | (xobj>cfg.xmaxField) | (yobj<yminField) | (yobj>ymaxField) )[0]
    keepflag[outFoV]=0
    sStars = np.where(wt==9.0)[0]
    if(len(sStars)>0):
        keepflag[sStars]=1.0 # keep setup stars regardless. does not matter that their spectra fall off array

#    print len(xLeft),' ',len(xlo)
    axlo = np.repeat(xlo,len(xLeft))
    axhi = np.repeat(xhi,len(xRight))
#    print len(axlo)

    return xspec,yspec,axlo,axhi,ylo,yhi, keepflag


def resolveCollisions(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=50.0,ypad=50.0):
    # coords are lower left and widths of boxes;
    # wt is P(include in mask); inMask is flag for already accepted objects
    
    # Assume inMask=1.0 (/pri>=1.0) objects have already been pre-cleaned for collisions.
    # Loop over list and fit in spectra where possible:
    
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.
    
    if(dbglvl>1):
        figDEBUG = plt.figure()
        axDEBUG = figDEBUG.add_subplot(111)
        xCen = (tx0+tx1)/2.0 ; yCen = (ty0+ty1)/2.0 
        plt.plot(xCen,yCen,'k.')
        plt.plot(xobj,yobj,'ko')
        if (dbglvl>1): 
            for i in range(len(x0)): plt.text(xCen[i],yCen[i],'%.0f'%i)

        # plot slits currently in mask:
        okMask = np.where( inMask==1 )[0]
        if (len(okMask)>0):
            for kk in okMask:
                print kk
                rect = Rectangle((tx0[kk],ty0[kk]),xWidth[kk],yWidth[kk],color='b',ec='none',alpha=0.3)
                axDEBUG.add_patch(rect) 
#                axDEBUG.draw()

    # Run 1 Monte-Carlo realisation based on wt.
    # generate N random numbers [0,1]
    # if ith number < wt, include:
    wtMC = np.random.random(len(x0))
    
    for jj in range(len(x0)):
        ii=ss[jj]
#        ii=jj

        if (inMask[ii]==1):
            if(dbglvl>0): logging.debug('this object is already in the mask. skipping.')
            continue

        if( wtMC[ii] > wt[ii] ): continue
        
        # take one slit at a time from candidate list and compare with *all* existing slits currently in mask:        
        # http://tech-read.com/2009/02/06/program-to-check-rectangle-overlapping/
        r1x1=tx0[ii] ; r1x2=tx1[ii] ; r1y1=ty0[ii] ; r1y2=ty1[ii]

        okMask = np.where( inMask==1 )[0]
        if(dbglvl>0): logging.debug('%sth slit'%ii)
        if (len(okMask)<1):
            if(dbglvl>0): logging.debug('no objects currently in mask. accepting this new slit')
            inMask[ii]=1
        else:
            if (dbglvl>0): logging.debug('%s slits currently in mask'%(len(okMask)))

            r2x1=tx0[okMask] ; r2x2=tx1[okMask] ; r2y1=ty0[okMask] ; r2y2=ty1[okMask]
            olap = np.where( (r1x2 >= r2x1) & (r1y2 >= r2y1) & (r1x1 <= r2x2) & (r1y1 <= r2y2) )[0]
            if (len(olap)>0):
                if (dbglvl>0): 
                    logging.debug('%s slit collisions. Rejecting this new slit'%(len(olap)))
                    logging.debug('collides with: %s'%okMask[olap])
                    logging.debug('edges of current slit: %.2f %.2f'%(r1y1,r1y2))
                    logging.debug('edges of colliding slit(s): %.2f %.2f'%(r2y1[olap][0],r2y2[olap][0]))
                if(dbglvl>2): 
###                    rect = Rectangle((x0[ii],y0[ii]),xWidth[ii],yWidth[ii],color='r',ec='none',alpha=0.2)
                    rect = Rectangle((xCen[ii],ty0[ii]),10.0,yWidth[ii],color='r',ec='none',alpha=0.2)
                    axDEBUG.add_patch(rect) 
                continue
            else:
                inMask[ii]=1
                if (dbglvl>0): logging.debug('ADDING THIS SLIT!')
                if(dbglvl>1):
                    rect = Rectangle((x0[ii],ty0[ii]),xWidth[ii],yWidth[ii],color='y',ec='none',alpha=0.3)
                    axDEBUG.add_patch(rect) 
                    print 'here'
#                    axDEBUG.draw()
    
    okMask = np.where( inMask==1 )[0]
    if(dbglvl>0): 
        logging.debug('%s slits in mask'%(len(okMask)))
        print okMask
    if(dbglvl>1): plt.show()
    return inMask



def mask2Reg(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0):
    tx0=x0-xpad/2.
    tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
    ty0=y0-ypad/2.
    ty1=y0+yWidth+ypad/2.
    
    f=open('ds9.reg','w')
    f.write('# Region file format: DS9 version 4.1\n')
#    f.write('global color=green dashlist=8 3 width=1 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\n')
    f.write("physical\n")
    
    okMask = np.where( inMask==1 )[0]

    for kk in okMask:
        f.write('polygon(%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f\n'%(tx0[kk],ty0[kk],tx0[kk],ty1[kk],tx1[kk],ty1[kk],tx1[kk],ty0[kk],tx0[kk],ty0[kk]))
        f.write('circle(%.3f,%.3f,%.3f\n'%(xobj[kk],yobj[kk],5.0))
    
        # slit:
        f.write('polygon(%.3f,%.3f,%.3f,%.3f\n'%(xobj[kk],yobj[kk]-slen[kk]/2.0,xobj[kk],yobj[kk]+slen[kk]/2.0)) 

    f.close()
        
###def setupStars(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=xpadPix,ypad=ypadPix,nMinStars=4):
def setupStars(wt, xp, yp, inMask,nMinStars=4,method='topbot',NSmode='micro'):
        # options: firstN, top and bottom-most, random, bestSpread (L shape), mindist (require min spacing)
        # all of these go through rejection step for bad stars first
        
        def verifyStars(starxp,staryp, nReq=nMinStars):
            # Check that selected stars are sensible, i.e not too close to each other or too linear:
            
            dyMax = np.max(staryp) - np.min(staryp)            
            dxMax = np.max(starxp) - np.min(starxp)            
            
            #*** check for too many in line?? ***
            ok=1
            if ( (dyMax<500.0) | (dxMax<500.0) ):
                 print 'star selection no good. try again'
                 #stop()
                 ok=0
            else: 
                print 'verified %s stars'%len(starxp)
            return ok
    
        def randomN(starxp,staryp, nReq=nMinStars, NSmode=NSmode):
            for i in range(10):
                print 'randomN %s'%(len(starxp))
                ss = np.argsort(np.random.random(len(starxp)))
                sss =ss[0:nReq]
                #inMask[candStars]=0.0 ; wt[candStars]=0.0
                #inMask[sss]=1.0 ; wt[sss]=9.0
                istatus = verifyStars(starxp[sss], staryp[sss])
                print i,istatus,sss
                if( istatus ):
                    return sss
                    break

                # if we get here, then we didn't find a suitable solution. jussssssssst take last oone
            return sss



        # First need, to restrict to central third if band shuffle is selected:
        if(NSmode=='band'):
            print 'BAND shuffle'
            # band shuffle instead of micro-shuffle:
            slen = cfg.nomsLen/2.0 #*** this is hardwired here *** FIX LATER?
            yHt=cfg.ymaxField-cfg.yminField
            yF0=cfg.yminField+0.33*yHt
            #        ymaxField=cfg.yminField+0.67*yHt
            # need to allow for slit and padding:
            ymaxField=cfg.yminField+0.67*yHt - slen/2.0 - cfg.ypadPix/2.0
            #yminField = yF0
            yminField = yF0 + slen/2.0 + cfg.ypadPix/2.0


            outFoV=np.where( (yp<yminField) | (yp>ymaxField) )[0]
            if(len(outFoV)>0):
                wt[outFoV]=0.0 # is this just for stars (xp,yp,wt - i assume so!)
            else:
                pass
        else:
            pass


        # check if there are more than nMinStars, and if so, attempt to choose:
        candStars = np.where(wt==9.0)[0]
        nCandStars = len(candStars)
        if (nCandStars>nMinStars):
            if (method=='firstN'):
                # this is just a testing mode. we probably don't ever want to use this method!
                #*** just take first 4 for now:
    #            candStars = candStars[0:nMinStars]
                #***
                inMask[candStars]=0.0 # set all stars to 0 initially
                wt[candStars]=0.0
                candStars = candStars[0:nMinStars]
                inMask[candStars]=1.0 
                wt[candStars]=9.0
                print 'keeping first %s stars'%nMinStars
            elif (method=='random'):
                print candStars
                useStars = randomN(xp[candStars],yp[candStars],nReq=nMinStars)
                print useStars
                print inMask
                inMask[candStars]=0.0
                wt[candStars]=0.0
                uu = candStars[useStars]
                inMask[uu]=1.0 ; wt[uu]=9.0
            elif (method=='mindist'):
                mindist=700
                minstarsep=0.
                maxcnt=20
                cnt=1
                while ((minstarsep < mindist and cnt < maxcnt) or cnt==1):
                    useStars = randomN(xp[candStars],yp[candStars],nReq=nMinStars)
                    for i in useStars:
                        for j in useStars:
                            if j != i:
                                xdist=np.sqrt((xp[candStars[i]]-xp[candStars[j]])**2.+(yp[candStars[i]]-yp[candStars[j]])**2.)
                                minstarsep=np.minimum(minstarsep,xdist)
                    cnt=cnt+1
                if (cnt>maxcnt): print "NO WELL SEPARATED SETUP STARS FOUND"
                print 'Method = mindist: '
                print useStars
                for i in useStars:
                    print i,xp[candStars[i]],yp[candStars[i]]
                print inMask
                inMask[candStars]=0.0
                wt[candStars]=0.0
                uu = candStars[useStars]
                inMask[uu]=1.0 ; wt[uu]=9.0
                
            else:
                print 'setup star method, %s, not found'%(method)
                stop()
        
            candStars=np.where(wt==9.0)[0]
#            verifyStars(xp[candStars], yp[candStars])
        
        
        
        else:
            # force into mask.
            # we don't care aboot collisions between setup stars
            # BUT we should check later that these don't collide with must-haves
            inMask[candStars]=1.0
#        pass
        return inMask,wt
    
#def verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=-99.0,ypad=-99.0):
#def verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=20.0,ypad=20.0):
#def verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=cfg.xpadPix,ypad=cfg.ypadPix):
def verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj):
        # do some pre-checking of must-have objects, as optimisation code before passing
        # to runmask/resolveCollisions
        
#        if (xpad<0.):
#            xpad = cfg.xpadPix
#        if (ypad<0.):
#            ypad = cfg.ypadPix

        xpad = np.repeat(cfg.xpadPix,len(x0))
        ypad = np.repeat(cfg.ypadPix,len(x0))
        stars = np.where(wt>1.0)[0]
        xpad[stars] = cfg.xpadPix + 0. # add a little extra padding round setup stars
        ypad[stars] = cfg.ypadPix + 0.


#        okMask = np.where(wt>=1.0)[0]
        okMask = np.where(wt==1.0)[0] # don't include setup stars yet
        #print 'okmask',okMask
        if (len(okMask)==0):
            # no must haves or acq objects
            return inMask,wt
        
        # run resolveCollisions on only musthave objects:
        tx0 = np.copy(x0[okMask]) ; ty0 = np.copy(y0[okMask])
        txWidth = np.copy(xWidth[okMask]) ;  tyWidth = np.copy(yWidth[okMask])
        twt = np.copy(wt[okMask]) ; tinMask = np.zeros(len(okMask)) ; tss=np.arange(len(okMask))
        txobj = np.copy(xobj[okMask]) ;   tyobj = np.copy(yobj[okMask])
        txpad = np.copy(xpad[okMask]) ; typad = np.copy(ypad[okMask])
        inMH = resolveCollisions(tx0,txWidth,ty0,tyWidth,twt,tinMask,tss,txobj,tyobj,xpad=txpad,ypad=typad)
        if (inMH.min()<1.0):
            print '**** not all must-haves could be allocated! ****'
            stop()            
            # *** actually we should be a bit cleverer here:
            # collisions between setup stars are fine. 
            # collisions with any science slits is not
         
            # take each setup star in turn and check it does not collide with a must-have science slit:
        stars = np.where(wt==9.0)[0]
        CPstars = np.copy(stars)
        oinMask=np.copy(inMask)
        if(len(stars)>0):
            print len(np.where(wt==1.0)[0])
            print 'checking each setup star'
            for i in range(len(stars)):
                print 'star %s'%i
                #inMask[stars[i]]=1.0
                wt[stars[i]]=1.0
                print wt[stars]
                print 'testing %s objects'%(len(np.where(inMask==1.0)[0]))
                #--
                okMask = np.where(wt==1.0)[0]
                print 'len(okmask)=%s'%len(okMask)
                print okMask
                #--
                tx0 = np.copy(x0[okMask]) ; ty0 = np.copy(y0[okMask])
                txWidth = np.copy(xWidth[okMask]) ;  tyWidth = np.copy(yWidth[okMask])
                twt = np.copy(wt[okMask]) ; tinMask = np.zeros(len(okMask)) ; tss=np.arange(len(okMask))
                txobj = np.copy(xobj[okMask]) ;   tyobj = np.copy(yobj[okMask])
                txpad = np.copy(xpad[okMask]) ; typad = np.copy(ypad[okMask])
                #txpad = cfg.xpadPix+10.0 ; typad = cfg.ypadPix+10.0#***
                inMH = resolveCollisions(tx0,txWidth,ty0,tyWidth,twt,tinMask,tss,txobj,tyobj,xpad=txpad,ypad=typad)
                print np.sum(inMH)
                if (inMH.min()<1.0):
#                if(np.sum(inMH)<np.sum(inMask)):
                    print '**** star collided with must have science slit. REJECTED ****'
                    wt[stars[i]]=0.0
                    inMask[stars[i]]=0.0
                    inMH=0.0#
                else:
                    wt[stars[i]]=9.0
                okMask=np.where(wt>=1.0)[0]
                inMask[okMask]=1.0
#        inMask[CPstars] = 0.0 # don't force any stars in yet!
        return inMask,wt
         
        
 
#def runMask(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=-99.0,ypad=-99.0,sortScheme='shuffle'):
def runMask(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=-99.0,ypad=-99.0,sortScheme='shuffle'):
    # run a few iterations of mask to ensure gaps are filled:

#    if (xpad<-90.):
#        xpad = cfg.xpadPix
#    if (ypad<-90.):
#        ypad = cfg.ypadPix
    xpad = np.repeat(cfg.xpadPix,len(x0))
    ypad = np.repeat(cfg.ypadPix,len(x0))
    stars = np.where(wt==9.0)[0]
    xpad[stars] = cfg.xpadPix + 0.0
    ypad[stars] = cfg.ypadPix + 0.0

    # THIS BIT IS CRUCIAL
    # Set the order in which objects are examined. As soon as a slit is assigned in a realisation it cannot
    # be removed again. So, the order in which objects are tested is vital.
    #
    # I have previously done this as a function of:
    #  - distance from cluster centre
    #  - sorting by priority, ...
    #
    # Now I think optimal is probably randomising order, but sorting in bins of priority from low to high    
#    ss = getShuffle(wt,sortScheme)
    ss = np.argsort(np.random.random(len(wt))) # just randomise order

    # (need to reject wt==0.0 points below) 
    
    inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=xpad,ypad=ypad)
    inMaski = np.copy(inMask1)
    del inMask1
    # Run 5 iterations with original weights, followed by 5 with essentially no weights (to fill in gaps).
    # May require more if a lot of the weights are v low (resulting in slits not being assigned).
    for kk in range(15):
#        wt09 = np.copy(wt)*0.0+0.9999
#        del ss
#        ss = getShuffle(wt,sortScheme)

        
        inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMaski,ss,xobj,yobj,xpad=xpad,ypad=ypad)
        inMaski = np.copy(inMask1)
        del inMask1
        okMask=np.where(inMaski==1.0)[0]
        ##print len(okMask)
    for kk in range(10):
        wt09 = np.copy(wt)*0.0+0.9999
        bad = np.where(wt==0.0) ; wt09[bad]=0.0
#        del ss
#        ss = getShuffle(wt,sortScheme)
        
        inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt09,inMaski,ss,xobj,yobj,xpad=xpad,ypad=ypad)
        #inMask1 = resolveCollisions(x0,xWidth,y0,yWidth,wt,inMaski,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix)
        inMaski = np.copy(inMask1)
        del inMask1
        okMask=np.where(inMaski==1.0)[0]
        ##print len(okMask)
    
    
    
    okMask=np.where(inMaski==1.0)[0]
    print '%s slits in mask'%(len(okMask))
    return inMaski

#def getStars(starfile,id,xobj,yobj,ra,dec,wt,slen,swid,stilt):
#    id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)

def desmask(cra,cdec,rotang,infile,det='Ham',niter=10,iplot=False,sortScheme='shuffle',starfile=None,NSmode='micro'):

    global cfg
     
    if det=='Ham':
        import MDconfigHam as cfg
    elif det=='E2v':
        import MDconfigE2v as cfg


#    reload(cfg)
 
    # NOTE: rotang is not actually needed for GoGREEN GMOS masks, since we're designing from pixel coords directly
 
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'test1in.txt')
    #(cra,cdec,rotang,infile) = (150.5702, 2.4986000000000002, 0.0, 'COS221in1.txt')
    
    id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(infile,unpack=True)
    
    ## load setup stars and add to arrays:
#    if (starfile):
#        id,xobj,yobj,ra,dec,wt,slen,swid,stilt = getStars(starfile,id,xobj,yobj,ra,dec,wt,slen,swid,stilt)
    
    
    xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = defineGMOSspec(xobj,yobj,slen,wt,NSmode=NSmode)
    # set weight of keepflag==0 objects to 0.0
    rej=np.where(keepFlag==0.0)[0]
    if (len(rej)>0): wt[rej]=0.0
    
    x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
    y0 = yspe-yBelow ; yWidth = yAbove+yBelow
    
    
    
    inMask = np.zeros(len(id)) ; ok=np.where(wt>=1.0)[0] ; inMask[ok]=1.0
    print len(ok)
    
#    inMask,wt = setupStars(wt, xobj, yobj, inMask, 4, method='random')
###    inMask,wt = setupStars(wt, xobj, yobj, inMask, cfg.nSetupStars, method=cfg.setupStarScheme, NSmode=NSmode)
    
    # check for collisions between must-have objects
    tinMask,twt = verifyMustHaves(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj)
    #print 'timMask',tinMask
    inMask = np.copy(tinMask)
    wt = np.copy(twt)

    #--
    # just run setupStars after verifying which stars don't collide with mustHaves:
    print '%s available stars'%(len(np.where(wt==9.0)[0]))
    inMask,wt = setupStars(wt, xobj, yobj, inMask, cfg.nSetupStars, method=cfg.setupStarScheme, NSmode=NSmode)
    print '%s viable stars'%(len(np.where(wt==9.0)[0]))
    #--

    for i in range(niter):
        inMask = np.zeros(len(id)) ; ok=np.where(wt>=1.0)[0] ; inMask[ok]=1.0
        #*** need to check for collisions between must have objects (and setup stars) and exit gracefully if problem ***
             
        inMask1 = runMask(x0,xWidth,y0,yWidth,wt,inMask,xobj,yobj,xpad=cfg.xpadPix,ypad=cfg.ypadPix,sortScheme=sortScheme)
    
        if (i==0):
            storeRes = np.copy(inMask1)
        else:
            #print np.sum(inMask)
            storeRes = np.vstack((storeRes,inMask1))
        
        okMask = np.where(inMask1==1.0)[0]
        print 'ITER: %s'%i
        print '%s slits in Mask'%len(okMask)
    
    if niter>1:    
        tots = np.sum(storeRes,1)
        totWts = np.sum(storeRes*wt,1)
        
        bestN = np.argmax(tots)
        bestWt = np.argmax(totWts)
        
        print 'best N mask (i=%s): N=%s, Tot(wt)=%.2f'%(bestN,tots[bestN],totWts[bestN])
        print 'best Wt mask (i=%s): Tot(wt)=%.2f, N=%s'%(bestWt,totWts[bestWt],tots[bestWt])
        
        # restore best mask:
        del inMask
        inMask = np.ravel(storeRes[bestN])
    else: 
        inMask = np.copy(inMask1)
    
    
    outname = infile.replace('in','out')
    if (infile==outname):
        outname='maskout.txt'
    np.savetxt(outname,np.transpose((ra[okMask],dec[okMask],wt[okMask],id[okMask])),fmt='%.7f %.7f %.4f %s')
    gmmpsname = infile.replace('in','gmmps')
    mag = np.repeat(20,len(okMask))
    pri = np.ones(len(okMask))
    pstars = np.where(wt[okMask]>=9.0)[0]
    if (len(pstars)>0):
        pri[pstars]=0.0 # is this right?
    if (NSmode=='micro'):
        slenfac=2.0
    else:
        slenfac=2.0 # same for both!
    # pixels in unbinned coords. nod offset***?
    
    np.savetxt(gmmpsname,np.transpose((id[okMask],ra[okMask]/15.0,dec[okMask],\
                                     xobj[okMask]*2.0,yobj[okMask]*2.0,mag,pri,slen[okMask]*cfg.pxscale/slenfac,\
                                     swid[okMask]*cfg.pxscale,stilt[okMask])),\
                                     fmt='%.0f %.8f %.7f %.4f %.4f %.3f %s %.3f %.3f %.3f')
    
    
    
    # Plot final mask:
    if(iplot):
        showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0)
        showSlits(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=cfg.xpadPix,ypad=cfg.ypadPix)

        
    mask2Reg(id,xobj,yobj,yspe,x0,xToLeft,xWidth,y0,yWidth,wt,inMask,swid,slen,xpad=0.0,ypad=0.0)

        
        
    
    if(dbglvl>1):
        rejMask = np.where(inMask==0.0)[0]
        for i in rejMask:
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]-swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]+yAbove[i]], 'r-')
            plt.plot([xobj[i]+swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]+yAbove[i]], 'r-')
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]-yBelow[i],yobj[i]-yBelow[i]], 'r-')
            plt.plot([xobj[i]-swid[i]/2.0,xobj[i]+swid[i]/2.0], [yobj[i]+yAbove[i],yobj[i]+yAbove[i]], 'r-')
            plt.text(xobj[i],yobj[i],i,color='r')
        plt.show()

