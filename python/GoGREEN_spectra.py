import Constants
import GoGREEN_Library as GoGREEN
import numpy as np
import scipy
from scipy.interpolate import interp1d, interp2d
from scipy.optimize import minimize
import warnings
def getmedprof(im,dq,f0=0.25,f1=0.75):
    # Get spatial cross section of a slit
    # take f0%--f1% of range:
    warnings.filterwarnings('ignore')
    sx = np.shape(im)[1]
    x0 = int(np.ceil(float(sx)*f0))
    x1 = int(np.floor(float(sx)*f1))
    dqx=dq[:,x0:x1]
    isgood=np.where(dqx==0,im[:,x0:x1],np.nan)
    xsec = np.nanmedian(isgood,1)
    return xsec

def gaussian(x, a, mean, sigma, y0):
    return (a*np.exp(-(x-mean)*(x-mean)/sigma/sigma) + y0)
    

def ABprof(npix=20,nodDis=9.0625,offset=5.5,width=4.,amp=12.):
    # create A+B profile from two gaussians
    x = np.arange(npix)
    gA = gaussian(x,amp,offset,width,0.)
    gB = gaussian(x,-amp,offset+nodDis,width,0.)
    return gA+gB

def getprof(x,v,offset,w,dx,dw,width_pix,nodpix,trim=0,reverse_NODAB=False):
    warnings.filterwarnings('ignore')
    arrProfs = np.zeros( (len(dx), len(dw),len(x)) )
    for j,off in enumerate(dx):
        for k,woff in enumerate(dw):
### ZZZ MLB: set ABprof to -ABprof if reverse_NODAB=True
            res = ABprof(offset=offset+off,nodDis=nodpix,npix = len(x),width=width_pix+woff)
            if reverse_NODAB:
                res=-res
            # -- truncate outer edges of gaussians to prevent spurious peaks from xcor with slit edge junk
            res[0:trim]=0 ; res[-trim::]= 0 
            # --
            arrProfs[j,k,:] = res
            #print offset+off,width_pix+woff,np.nansum(x*res)
    #xcorval = np.sum(x*arrProfs/v,axis=2)
    xcorval = np.nansum(x*arrProfs,axis=2)
    idx = np.argmax(xcorval)
    (pk,pw)=np.unravel_index(idx,xcorval.shape)
    #amp=np.average(abs(x),weights=1./v)/np.average(abs(arrProfs[pk][pw]),weights=1./v)
    si=np.sign(arrProfs[pk][pw])
    amp=np.average(x[trim:-trim]*si[trim:-trim])/np.average(abs(arrProfs[pk][pw][trim:-trim]))
    return (pk,pw,amp*arrProfs[pk][pw])

def getFit(pixarray,dxarray,order):
    y_mean=[] # Arrays for (preliminary) linear mean fit
    x_mean=[]
    y_bestfit=[] # Arrays for fit of the order desired
    x_bestfit=[]
    coeff = np.polyfit(pixarray,dxarray,abs(order))
    bestfit = np.polyval(coeff,pixarray)
    stddev=np.nanstd(bestfit-dxarray)    
    x_model=pixarray[np.where(abs(bestfit-dxarray)<2.*stddev)]
    y_model=dxarray[np.where(abs(bestfit-dxarray)<2.*stddev)]
    if np.size(x_model)<3:
        x_model=pixarray
        y_model=dxarray
#    mean = np.polyfit(pixarray,dxarray,0) # Polyfit creates coefficients
#    mean_a = np.polyval(mean,pixarray) # Polyval evaluates coefficients with a given 'x'-axis array
#    coeff = np.polyfit(pixarray,dxarray,abs(order))
#    bestfit = np.polyval(coeff,pixarray)
#    outliersm, outliersb = 10, 10 
#    for j in range(len(dxarray)):         
#        print j,mean_a[j],bestfit[j],dxarray[j]
#        if abs(mean_a[j] - dxarray[j]) <= 0.8: # 0.8 is the 'threshold' of error, set via trial and error (seems to be a good limit)
#            outliersm -= 1
#            y_mean.append(dxarray[j])
#            x_mean.append(pixarray[j])
#        if abs(bestfit[j] - dxarray[j]) <= 0.8:
#            outliersb -= 1
#            y_bestfit.append(dxarray[j])
#            x_bestfit.append(pixarray[j])
#    if outliersm <= 3:
#        y_model = y_mean
#        x_model = x_mean
#    elif outliersm > 3:
#        y_model = y_bestfit
#        x_model = x_bestfit
        
            # Generally the linear fit is more reliable, so it picks it automatically when it has less than 3 outliers.
            # Could do something similar where the method with least outliers is picked:
##            if outliersm <= outliersb:
##                y_model = y_mean
##                x_model = x_mean
##            elif outliersm > outliersb:
##                y_model = y_bestfit
##                x_model = x_bestfit
            
    coeff2 = np.polyfit(x_model,y_model,order)
    bestfit2 = np.polyval(coeff2,pixarray)  
    return (bestfit2,coeff2,x_model,y_model)
    #coeff2 = np.polyfit(pixarray,dxarray,order)
    #bestfit2 = np.polyval(coeff2,pixarray)  
    #return (bestfit2,coeff2,pixarray,dxarray)

def SpExtract(subim,subvar,subdq,weight_method,postrace,width,nodpix,slittrim,reverse_NODAB=False):
#1) Inverse variance-weighted.  Take the weight of pixel i to be w_i = dq_i/var_i.  Then for each pixel the weighted average value is Yavg = Sum(s_i*w_i*y_i)/Sum(w_i), where s_i=sign(p_i).
#To convert this to an appropriate sum, rather than an average, we should multiply by the weighted number of pixels contributing to the average, npix=Sum(w_i)/max(w_i).  So the sum we're interested in is just ysum=Sum(s_i*w_i*y_i)/max(w_i)
#
#2) Profile-weighted.  In this case w_i = dq_i*abs(p_i) and again Yavg = Sum(s_i*w_i*y_i)/Sum(w_i).
#To convert to a sum, the normalization factor is N=Sum(w_i)/Sum(w_i^2).  [Verify that if the data are a scaled version of the assumed profile, y_i = Ap_i, then N*xavg=Sum(y_i), which is what we want.]  So Ysum=Sum(s_i*w_i*y_i)*Sum(w_i)/Sum(w_i*w_i).
#
#3) Profile-fit.  This is probably the one I will want to use.  The idea is to fit the normalization of the assumed profile p_i to minimize the chisquared difference from the data.  Then the total flux is just the sum of the profile.
#
#So, find A that minimizes the function f=Sum[ dq_i*(y_i-A*p_i)^2/var_i].  Then return the value Ysum=A*Sum(s_i*p_i). 
    
    x = subim.shape[1] #1510
    y = subim.shape[0] #20
    Profiles=np.copy(subim)*0
    goodval=np.copy(subdq)
    goodval[0:slittrim]=1
    goodval[-slittrim::]=1
    goodval[np.where(subvar<=0)]=1

    for j in range(x):
        Profiles[:,j]=ABprof(offset=postrace[j],width=width,nodDis=nodpix,npix = y)
        if reverse_NODAB:
            Profiles[:,j]=-Profiles[:,j]
        s_pix=np.sign(Profiles)
        for i in range(y):
            if goodval[i,j] == 0:
                goodval[i,j] = 1.
            else:
                goodval[i,j] = 0.
    goodpos=np.copy(goodval)
    goodneg=np.copy(goodval)
    goodpos[np.where(Profiles<0)]=0
    goodneg[np.where(Profiles>0)]=0
    if weight_method == 'prof_fit':
        A=np.zeros(x)
        for j in range(x):
            fun = lambda A: np.sum(goodval[:,j]*(subim[:,j]-A*Profiles[:,j])**2/(subvar[:,j]+1.e-10))
            A[j]=minimize(fun,[1.0]).x
        y_sum=A*np.sum(goodval*s_pix*Profiles,axis=0)
        y_var=np.sum(goodval*subvar,axis=0)
        y_dq=np.sum(goodval,axis=0)
        y_pos=A*np.sum(goodpos*s_pix*Profiles,axis=0)
        y_neg=A*np.sum(goodneg*s_pix*Profiles,axis=0)
    else:
        if weight_method == 'no_weight':
            w_pix=goodval*(subim*0+1)
            norm=np.size(goodval[slittrim:-slittrim,0])
            w_pos=goodpos*(subim*0+1)
            norm_pos=np.size(goodpos[slittrim:-slittrim,0])
            w_neg=goodneg*(subim*0+1)
            norm_neg=np.size(goodneg[slittrim:-slittrim,0])
        if weight_method == 'var_weight':
            w_pix=goodval*np.where(subvar>0,1./(subvar+1.e-10),1e10)
            w_pos=goodpos*np.where(subvar>0,1./(subvar+1.e-10),1e10)
            w_neg=goodneg*np.where(subvar>0,1./(subvar+1.e-10),1e10)
            norm=1.
            norm_pos=1.
            norm_neg=1.
        if weight_method == 'prof_weight':
            w_pix=goodval*np.abs(Profiles)
            w_pos=goodpos*np.abs(Profiles)
            w_neg=goodneg*np.abs(Profiles)
            #norm=np.sum(w_pix,axis=0)*np.sum(w_pix,axis=0)/(np.sum(w_pix*w_pix,axis=0)+1.e-10)
            norm=np.sum(w_pix,axis=0)*np.sum(np.abs(Profiles),axis=0)/(np.sum(w_pix*w_pix,axis=0)+1.e-10)
            norm_pos=np.sum(w_pos,axis=0)*np.sum(np.abs(Profiles),axis=0)/(np.sum(w_pos*w_pos,axis=0)+1.e-10)
            norm_neg=np.sum(w_neg,axis=0)*np.sum(np.abs(Profiles),axis=0)/(np.sum(w_neg*w_neg,axis=0)+1.e-10)
        y_avg = np.sum(w_pix*s_pix*subim,axis=0)/(np.sum(w_pix,axis=0)+1.e-10)
        y_sum = y_avg*norm
        y_var= norm*np.sum(w_pix*subvar,axis=0)/(np.sum(w_pix,axis=0)+1.e-10)
        y_dq=np.sum(goodval,axis=0)
        y_pos=norm_pos*np.sum(w_pos*s_pix*subim,axis=0)/(np.sum(w_pos,axis=0)+1.e-10)
        y_neg=norm_pos*np.sum(w_neg*s_pix*subim,axis=0)/(np.sum(w_neg,axis=0)+1.e-10)
    return (y_sum,y_var,y_dq,y_pos,y_neg)
def inspect(cname,maskname,apnum):
    from optparse import OptionParser
    import Constants
    import GoGREEN_Library as GoGREEN
    from astropy.io import fits,ascii
    from astropy import wcs
    import numpy as np
    import os
    import matplotlib.colors as mc
    import matplotlib.pyplot as plt
    import matplotlib.patches as patches
    import sys
    import getopt
    import re
    import matplotlib.ticker as plticker
    from astropy.convolution import convolve, Box1DKernel
    traceorder=0
    constants=Constants.GoGREENPath()
    dir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"
    configFile=constants.reduced+'ggextract.config'
    params=GoGREEN.buildDictionary(configFile)
    try:
        with open(configFile) as file:
            pass
    except IOError as e:
        print "Configuration file does not exist; using defaults"

    showplots=True
    if 'weight_method' not in params.keys(): params['weight_method']='no_weight'
    weight_method = params['weight_method']
    # 2.  Data-specific parameters
    if 'width_arcsec' not in params.keys(): params['width_arcsec']=.7 # Default sigma width in arcseconds of spatial profile
    width_arcsec = np.float(params['width_arcsec'])
    if 'slittrim' not in params.keys(): params['slittrim']=2.      # number of pixels at slit edge to ignore
    slittrim = np.int(params['slittrim'])
    # 3. Fitting parameters
    if 'f0' not in params.keys(): params['f0']=0.5 # f0-f1 is the range of pixels (as fraction of total) to use for a single best spatial profile
    f0p = np.float(params['f0'])
    if 'f1' not in params.keys(): params['f1']=0.85
    f1p = np.float(params['f1'])
    if 'dxmax' not in params.keys(): params['dxmax'] = 2.      # max shift in predicted spatial position, in pixels
    dxmax = np.float(params['dxmax'])
    if 'dxstep' not in params.keys(): params['dxstep']=0.1   # step to search for spatial shift
    dxstep = np.float(params['dxstep'])
    if 'dwmax' not in params.keys(): params['dwmax']=0.2    # max shift in spatial profile (sigma), in arcseconds
    dwmax = np.float(params['dwmax'])
    if 'dwstep' not in params.keys(): params['dwstep']=0.025 # step to search for sigma shift
    dwstep = np.float(params['dwstep'])
    if 'trace1' not in params.keys(): params['trace1']=0.2    # trace1 and trace2 are limits (in fraction)within which to fit the trace
    trace1 = np.float(params['trace1'])
    if 'trace2' not in params.keys(): params['trace2']=.85
    trace2 = np.float(params['trace2'])
    if 'trace_step' not in params.keys(): params['trace_step']=0.05 # step (in fraction) for fitting trace.
    trace_step = np.float(params['trace_step'])

    trace1=0.1
    trace2=0.95
    trace_step=0.05
    onedprefix='oned_'
    imname="rfcs_"+maskname
    onedfile=dir+onedprefix+imname+".fits"
    if not os.path.isfile(onedfile):
        onedfile=onedfile+'.gz'
    if not os.path.isfile(onedfile):
        imname="cs_"+maskname
        onedfile=dir+onedprefix+imname+".fits"
    if not os.path.isfile(onedfile):
        print onedfile
    try:
        reducedfile=dir+imname+".fits.gz"
        if not os.path.isfile(reducedfile):
            reducedfile=reducedfile.replace('.gz','')
        mdf,mdfhd = fits.getdata(reducedfile,header=True,ext=('mdf',1))
        hdulinput=fits.open(reducedfile)
    except:
        print 'No fits file ',reducedfile
        sys.exit()
    header_primary=hdulinput[0].header

    nsciext=header_primary['NSCIEXT']
    header_primary['NEXTEND']=1+3*nsciext
    #if ((header_primary['NODAYOFF']<0) | (header_primary['INSTRUME']=='GMOS-N')):
    if ((header_primary['NODAYOFF']<0) | (header_primary['DETECTOR']=='GMOS + e2v DD CCD42-90')):
        reverse_NODAB=True
        #reverse_NODAB=False
    else:
        reverse_NODAB=False

    try:
        masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
        mdat = fits.getdata(masterfile)
    except:
        print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
        print "Stopping"
        stop()
    #preimage_root=mdat['Preim_name'][np.where(mdat['cluster']==cname)]
    preimage_root=mdat['z_image'][np.where(mdat['cluster']==cname)]
    imdir=os.getenv('GoGREEN_data')+'/Data/reduced/'+cname+'/Imaging/GMOS/Z/'
    preimage=imdir+preimage_root[0]+'.fits.gz'
    if not os.path.isfile(preimage):
        preimage=preimage.replace('.gz','')
    try:
        preim,preim_header=fits.getdata(preimage,header=True)
        imagewcs=wcs.WCS(preim_header)
    except:
        print 'No preimage ',preimage
        sys.exit()
    #preim_xscale=np.max([np.abs(preim_header['CD1_1']),np.abs(preim_header['CD1_2'])])*3600
    #preim_yscale=np.max([np.abs(preim_header['CD2_1']),np.abs(preim_header['CD2_2'])])*3600
    preim_xscale=(np.abs(preim_header['CD1_1'])+np.abs(preim_header['CD1_2']))*3600
    preim_yscale=(np.abs(preim_header['CD2_1'])+np.abs(preim_header['CD2_2']))*3600
    #print preim_xscale,preim_yscale
    preim=preim-np.median(preim)

    lisfile=dir+maskname+'.lis'
    ref_sciimage=ascii.read(lisfile,data_start=0)[0][0]
    ref_flatimage=ascii.read(lisfile,data_start=0)[0][1]
    try:
        scimosaic_fn=dir+'mgs'+ref_sciimage+'.fits.gz'
        flatmosaic_fn=dir+'rg'+ref_flatimage+'_comb.fits.gz'
        if not os.path.isfile(scimosaic_fn):
            scimosaic_fn=scimosaic_fn.replace('.gz','')
        if not os.path.isfile(flatmosaic_fn):
            flatmosaic_fn=flatmosaic_fn.replace('.gz','')
        scimosaic=fits.getdata(scimosaic_fn,ext=('sci',1))
        flatmosaic,flathd=fits.getdata(flatmosaic_fn,header=True,ext=('MDF',1))
        scimosaic=np.transpose(scimosaic)
        mosaic_exists='yes'
    except:
        print 'No mosaic image',scimosaic_fn,' exists.  Ignoring.'
        mosaic_exists='no'
    try:
        snr_fn=dir+maskname+'.onedsn'
        sndat=ascii.read(snr_fn,data_start=2)
        snr_exists=True
    except:
        snr_exists=False
    pixscale=header_primary['PIXSCALE']
    nodpix=header_primary['NODAYOFF']*2/pixscale
    nodpix=np.abs(nodpix)
    width_pix=width_arcsec/pixscale
    dx = np.arange(-dxmax,dxmax,dxstep)
    dw = np.arange(-dwmax,dwmax,dwstep)/pixscale # in pixels  
    i=int(apnum)
    subim=hdulinput['sci',i].data
    hd=hdulinput['sci',i].header
    subvar=hdulinput['var',i].data
    varhd=hdulinput['var',i].header
    subdq=hdulinput['dq',i].data
    dqhd=hdulinput['dq',i].header
    pix=np.arange(1,hd['naxis1']+1)
    ll=hd['crval1']+(pix-hd['crpix1'])*hd['cd1_1']
    # --- Try to find best offset of profile pair within slit:####
    npix=subim.shape[0]
    # Centre of slit.  Convention is that index starts at 0 and references the bottom of the pixel
    #slit_centre=(npix/2.)
    # Hmm... why do I say that?  imshow treats index as representing centre of pixel.  Why doesn't that make sense?
    slit_centre=((npix-1)/2.)
    # Offset is expected position of the spectrum at the bottom of the slit
    offset=slit_centre-nodpix/2.0
    xsec_avg = getmedprof(subim,subdq,f0p,f1p)
    vsec_avg = getmedprof(subvar,subdq,f0p,f1p)
    (pk,pw,useProf)=getprof(xsec_avg,vsec_avg,offset,width_pix,dx,dw,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)
    dofit=True
    dxarray=[]
    dwarray=[]
    pixarray=[]
    dwfixed=dw*0+dw[pw]
    profarray=[]
    for specfrac in np.arange(trace1,trace2,trace_step):
        xsec = getmedprof(subim,subdq,f0=specfrac,f1=specfrac+trace_step)
        vsec = getmedprof(subvar,subdq,f0=specfrac,f1=specfrac+trace_step)
        (pktry,pwtry,Proftry)=getprof(xsec,vsec,offset,width_pix,dx,dwfixed,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)
        dxarray=np.append(dxarray,offset+dx[pktry])
        dwarray=np.append(dwarray,dw[pwtry])
        pixval=(specfrac+trace_step/2.)*subim.shape[1]
        pixarray=np.append(pixarray,pixval)
        profarray=np.append(profarray,Proftry)
    profarray=np.reshape(profarray,[-1,Proftry.size])
    x_model=[]
    y_model=[]
    coeff2=np.array([])
    try:
        outim,hdx = fits.getdata(onedfile,header=True,ext=('sci',i))
        if hdx['TCOEF2']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF2']))
        if hdx['TCOEF1']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF1']))
        if hdx['TCOEF0']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF0']))
        order=np.size(coeff2)-1
        #print "Using existing fit of order ",order," and coefficients ",coeff2
        bestfit2 = np.polyval(coeff2,pixarray) 
    except:
        #print'WARNING: No existing trace coefficients in',onedfile
        #print 'Using order=0 to fit'
        traceorder=0
        order=0
        (bestfit2,coeff2,x_model,y_model)=getFit(pixarray,dxarray,order)
    postrace=np.polyval(coeff2,np.arange(subim[0,:].size))
    negtrace=postrace+nodpix
    (exdata,exvar,exdq,expos,exneg)=SpExtract(subim,subvar,subdq,weight_method,postrace,width_pix+dw[pw],nodpix,slittrim,reverse_NODAB=reverse_NODAB)
    badreg=np.where(np.abs(expos-exneg) > 5.*np.sqrt(exvar))
    target=np.where(mdf['extver']==i)
    RA=mdf['RA'][target][0]*15.
    DEC=mdf['DEC'][target][0]
    radec=np.array([[RA,DEC]])
    pixels=imagewcs.wcs_world2pix(radec,0)
    #pixels=imagewcs.wcs_world2pix(radec,1)
    # I don't know why x and y are reversed here!  Works for GMOS-N Hamamatsu data
    yccd=pixels[0][0]
    xccd=pixels[0][1]
    #yccd=mdf['x_ccd'][target][0]
    #xccd=mdf['y_ccd'][target][0]
    defstampsize=5/preim_xscale*np.array([1,1])
    ximage=[int(np.max([xccd-defstampsize[0]/2+.5,0])),int(np.min([xccd+defstampsize[0]/2+.5,np.shape(preim)[0]]))]
    yimage=[int(np.max([yccd-defstampsize[1]/2+.5,0])),int(np.min([yccd+defstampsize[1]/2+.5,np.shape(preim)[1]]))]
    #ximage=[np.max([xccd-defstampsize[0]/2,0]),np.min([xccd+defstampsize[0]/2,np.shape(preim)[0]])]
    #yimage=[np.max([yccd-defstampsize[1]/2,0]),np.min([yccd+defstampsize[1]/2,np.shape(preim)[1]])]
    centralstampsize=1/preim_xscale*np.array([1,1])
    xcent=[int(np.max([xccd-centralstampsize[0]/2+.5,0])),int(np.min([xccd+centralstampsize[0]/2+.5,np.shape(preim)[0]]))]
    ycent=[int(np.max([yccd-centralstampsize[1]/2+.5,0])),int(np.min([yccd+centralstampsize[1]/2+.5,np.shape(preim)[1]]))]
    #xcent=[int(np.max([xccd-centralstampsize[0]/2,0])),int(np.min([xccd+centralstampsize[0]/2,np.shape(preim)[0]]))]
    #ycent=[int(np.max([yccd-centralstampsize[1]/2,0])),int(np.min([yccd+centralstampsize[1]/2,np.shape(preim)[1]]))]
    xstamp=xccd-ximage[0]
    ystamp=yccd-yimage[0]
    #xstamp=xccd-ximage[0]
    #ystamp=yccd-yimage[0]


    fig,axarr=plt.subplots(5,gridspec_kw={'height_ratios':[2,1,1,1,1]})
    # Top figure:  postage stamp of target from preimage  
    preim_sub=preim[ximage[0]:ximage[1],yimage[0]:yimage[1]]
    minval=np.nanmin(preim_sub)
    maxval=np.nanmax(preim[xcent[0]:xcent[1],ycent[0]:ycent[1]])
    vmin=minval
    vmax=0.99*maxval
    #print minval,maxval
    #print vmin,vmax
    #minval=30500
    #maxval=32000
    axarr[0].axis('off')
    ax=fig.add_subplot(4,2,1)
    ax.axis('off')
    ax.imshow(preim_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
    ax.add_patch(patches.Rectangle(
        (xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale),
        1/preim_xscale,
        3/preim_yscale,
        fill=False,
        edgecolor='red')
    )
    ax.add_patch(patches.Rectangle(
        (xstamp-1/2./preim_xscale,ystamp-9./4/preim_yscale),
        1/preim_xscale,
        3/preim_yscale,
        fill=False,
        edgecolor='yellow')
    )

    ax.scatter(xstamp,ystamp,marker='x')
    ax.set_xlim(0,int(defstampsize[0]))
    ax.set_ylim(0,int(defstampsize[1]))
    # Figure: 2D dispersed image 
    if mosaic_exists=='yes':
        secy1=flatmosaic['SECY1'][target][0]
        secy2=flatmosaic['SECY2'][target][0]
        secx1=flatmosaic['SECX1'][target][0]
        secx2=flatmosaic['SECX2'][target][0]
        specx=int(0.5*(secx1+secx2))
        #specy=int(0.5*(secy1+secy2))
        # This references the centre of the n&s pair
        specy=secy1
        defmossize=np.array([1500,160])
        # Something is screwy when doing with preimages.  x and y swapped or something?
        relpixscale=1.
        #if re.compile('SXDF').match(cname) or re.compile('COS').match(cname): relpixscale=2.

        xmosimage=[int(np.max([specx/relpixscale-defmossize[0]/2,0])),int(np.min([specx/relpixscale+defmossize[0]/2,np.shape(scimosaic)[0]]))]
        ymosimage=[int(np.max([specy/relpixscale-defmossize[1]/2,0])),int(np.min([specy/relpixscale+defmossize[1]/2,np.shape(scimosaic)[1]]))]
        mosaic_sub=scimosaic[xmosimage[0]:xmosimage[1],ymosimage[0]:ymosimage[1]]
        vmin=-1000
        vmax=1000               
        #mosaic_sub=np.transpose(mosaic_sub[:][::-1])[::-1]
        # Following is right for SXDF64
        mosaic_sub=np.transpose(mosaic_sub[:][::-1])

        #fig3,axarr3=plt.subplots(1)
        #axarr3.imshow(mosaic_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
        #axarr[1].imshow(mosaic_sub[:,::-1],aspect=0.88,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'),origin='fuck')
        axarr[1].imshow(mosaic_sub,aspect='auto',vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'),origin='lower')
        #axarr[4].set_xlim()
        #thismanager=plt.get_current_fig_manager()
        #thismanager.resize(2400,160)
        #plt.show(block=False)
    # Figure: Extracted spectrum
    axarr[2].plot(ll,expos,c='r',linewidth=0.5)
    axarr[2].plot(ll,exneg,c='b',linewidth=0.5)
    axarr[2].plot(ll,convolve(exdata,Box1DKernel(5)),c='k')
    axarr[2].scatter(ll[badreg],exdata[badreg],c='y',s=20)
    axarr[2].set_xlim(ll[0],ll[-1])
    #axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
    axarr[2].set_ylim(np.nanmean(exdata[:-200])-2.*np.nanstd(exdata[:-200]),np.nanmean(exdata[:-200])+2.*np.nanstd(exdata[:-200]))
        
    # Figure: spatial profile
    ypix=np.arange(np.size(xsec_avg[slittrim:-slittrim]))
    #axarr[3].plot(ypix+slittrim,xsec_avg[slittrim:-slittrim])
    #axarr[3].plot(ypix+slittrim,useProf[slittrim:-slittrim])
    extent=[ll[0],ll[-1],1,20]
    # Figure: 2D sky-subtracted slit with trace
    maxval=0.99*np.nanmax(np.abs(xsec_avg[2:18]))
    axarr[3].imshow(subim[:,:],extent=extent,origin='lower',aspect=10,vmax=maxval,vmin=-maxval,cmap=plt.get_cmap('gray'))
    axarr[3].plot(ll,postrace,'r')
    axarr[3].plot(ll,negtrace,'r--')
    axarr[3].set_xlim(ll[0],ll[-1])
    # Figure: trace position of positive flux
    axarr[4].plot(pixarray,dxarray, 'ro')
    try:
        axarr[4].plot(x_model,y_model,'bo')
    except:
        pass
    axarr[4].plot(pixarray,bestfit2,'b')
    axarr[4].set_xlim(0,ll.size)
    axarr[4].set_ylim(offset-dxmax,offset+dxmax)
    thismanager=plt.get_current_fig_manager()
    thismanager.resize(1200, 1000)
    #thismanager.window.SetPosition((500,0))
    if snr_exists:
        ax=fig.add_subplot(4,2,2)
        thisgal=np.where(sndat['ID']==mdf['ID'][target])
        ax.scatter(sndat['Mag'],sndat['S/N'],facecolor='none',edgecolor='k',s=50)
        ax.scatter(sndat['Mag'][thisgal],sndat['S/N'][thisgal],c='r',s=50)
        ax.set_yscale('log')
        ax.get_yaxis().set_major_formatter(plticker.ScalarFormatter())
        ax.set_ylim(0.5,500)
        plt.ylabel ("S/N@850nm")
        plt.xlabel ("ZMAG")
    plt.show(block=False)
    #fig2=plt.figure()
    #fig2.show()
    #print "Slit ",i,":"
    #print "ID: ",mdf['id'][target][0],"ZMAG=",mdf['mag'][target][0],"priority=",mdf['priority'][target][0]
    
    #plt.close('all')
