from astropy.io import fits,ascii
import numpy as np
import matplotlib.pyplot as plt
import sys
import pyfits
from scipy.interpolate import interp1d

imname=str(sys.argv[1])

# Get primary header, and header of a science extension:
#
hdulist=fits.open(imname)
header_primary=hdulist[0].header
hdulist.close()
#header_primary=pyfits.getheader(imname)
nsciext=header_primary['NSCIEXT']

try:
    ref=int(sys.argv[2])
except:
    ref=int(nsciext/2)
subim,hd1 = fits.getdata(imname,header=True,ext=('sci',ref))

# Initialize 2D image based on guess of total size.
hh=0
startx=0
ny = (hd1['naxis2']+2)*nsciext
nx = hd1['naxis1']
out2D = np.zeros((ny+1,nx+1))

# Set WLC parameters based on reference slit:
wmin=hd1['crval1']
disp=hd1['cd1_1']
ipix=np.arange(1,hd1['naxis1']+1)
refpix=hd1['crpix1']
reflambda=wmin+(ipix-refpix)*disp

for i in (range(1,nsciext+1)):
    subim,hd = fits.getdata(imname,header=True,ext=('sci',i))
    hh1 = hh+np.shape(subim)[0]
    while hh1 >ny:
# In case we didn't make the image big enough in y
        ny=ny+1
        out2D=np.vstack([out2D,np.zeros(np.shape(out2D)[1])])
    pix=np.arange(1,hd['naxis1']+1)
    ll=hd['crval1']+(pix-hd['crpix1'])*hd['cd1_1']
    y=interp1d(ll,subim,bounds_error=False)
    out2D[hh:hh1,startx:nx] = y(reflambda)
    hh=hh1

#plt.figure()
#plt.imshow(out2D,origin='lower',aspect='auto',vmax=100)
#plt.show()

outfile = 'stacked_'+imname

hdul2 = fits.HDUList()
header2=header_primary
header2['NSCIEXT']=1
header2['NEXTEND']=2
#hd1.update('naxis2',ny)
#hd1.update('naxis1',nx)
#hd1.update('EXTVER',1)
hd1['naxis2']=ny
hd1['naxis1']=nx
hd1['EXTVER']=1
del hd1['DETSEC']
del hd1['DATASEC']
#del hd1['AIRMASS']
hdul2.append(fits.PrimaryHDU(header=header2))
hdul2.append(fits.ImageHDU(data=out2D[0:hh1,:],header=hd1,name='SCI'))
hdul2.writeto(outfile,clobber=True)

#fits.writeto(outfile,out2D[0:hh1,:],clobber=True)
#fits.setval(outfile, keyword='CRVAL1', value=hd1['crval1'])
#fits.setval(outfile, keyword='CRPIX1', value=hd1['crpix1'])
#fits.setval(outfile, keyword='CD1_1', value=hd1['cd1_1'])
#fits.setval(outfile, keyword='CUNIT1', value='Angstroms')
#fits.setval(outfile, keyword='CTYPE1', value='WAVE')




