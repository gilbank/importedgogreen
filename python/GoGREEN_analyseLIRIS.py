#!/Users/clidman/Science/Programs/Ureka/variants/common/bin/python
# Python script to anlayse reduced LIRIS data

# Usage GoGREEN_analyseLIRIS.py

import matplotlib.pyplot as plt
import os
import pyfits,numpy
import sys

ZP=[]
errZP=[]
scattZP=[]
date=[]
sum1=0.
sum2=0.
extinction={'Ks':0.08}

# The error in the ZPs is taken to be the maximum of the uncertainty
# in the mean and the formal uncertainty in the ZP

# We correct for exposure time. Set to an integration time of 1 second

try:
    startMJD=float(sys.argv[1])
    endMJD=float(sys.argv[2])
except:
    startMJD=0
    endMJD=100000

for dir in os.listdir('reduced'):
    if os.path.isdir('reduced/'+dir):
        for file in os.listdir('reduced/'+dir):
            if 'LIRIS' in file and 'fits' in file:
                hdr=pyfits.getheader('reduced/'+dir+'/'+file,0)
                try:
                    if float(hdr['ZP']) > 21.0 and float(hdr['ERR_ZP']) > 0 and hdr['MJD-OBS'] > startMJD and hdr['MJD-OBS'] < endMJD:
                                        
                        print dir,file
                        # Removes points taken through clouds and those fields without brights stars
                        ZP.append(hdr['ZP']+hdr['AIRMASS']*extinction['Ks']-2.5*numpy.log10(hdr['EXPTIME']))
                        errZP.append(max(hdr['err_ZP'],hdr['scatt_ZP']))
                        date.append(hdr['MJD-OBS'])
                        sum1+=ZP[-1]/errZP[-1]**2.
                        sum2+=1.0/errZP[-1]**2.
                except:
                    pass

fig=plt.figure()
ax=fig.add_subplot(111)
ax.errorbar(date,ZP,errZP,None,marker='o',mfc='b',linestyle='None',ecolor='k')
ax.set_xlabel('MJD data')
ax.set_ylabel('ZP')

# Not corrected for extintion
# Compute the weighted ZP

print 'Average ZP %5.2f +/- %5.2f' % (sum1/sum2,numpy.std(ZP))

ax.plot([min(date),max(date)],[sum1/sum2,sum1/sum2],'b',linestyle='--')
ax.set_ylim(22.5,23.5)
    
plt.show()
plt.close()
