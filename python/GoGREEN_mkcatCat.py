from astropy.io import fits,ascii
from astropy import wcs

import os,sys
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle

try:
    from astropy.coordinates import SkyCoord
except:
    print 'We seem to have an older version of astropy (probably from Ureka).\nTrying to work around'
    from astropy.coordinates import ICRS as SkyCoord

import astropy.units as u
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from GoGREEN_checkPhotCat import getZphWts


try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)


ilogin=False
ilogintmp=False
if not os.path.isfile('login.cl'):
    try:
        irafhome = os.getenv('HOME')+'/iraf'
#        irafhome = os.getenv('UR_DIR')#+'/iraf'
        if(os.path.isfile(irafhome+'/login.cl')):
            ilogin=True
            ilogintmp=True # need to delete at home
            os.system('cp '+irafhome+'/login.cl .')
        else:
            pass # no login found. need to die
            print
            print "no login.cl found. copy to current directory to run this task"
            print
            exit(1)
    except:
        print
        print "no login.cl found. copy to current directory to run this task"
        print

else:
    pass
    # got login.cl here. okay


try:
    from pyraf import iraf
    iraf.gemini()
    iraf.gmos()
    iraf.mostools()
    ipyraf = True
except:
    ipyraf=False
    print
    print "The required gemini IRAF/pyraf tasks are not available"
    print
    exit(1)






import MDconfigE2v as cfg

# COSMOS cat:
#dat = fits.getdata('/home/gilbank/Proj/goGreen/PubCats/photoz_zml_vers1.8_250909.fits')
# ULTRAVISTA cat:
#dat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.cat')
# http://www.strw.leidenuniv.nl/galaxyevolution/ULTRAVISTA/Ultravista/K-selected.html


def flux2ab(flux):
    ok = np.where(flux>0.0)[0]
    ab = np.zeros(len(flux))+99.0
    ab[ok] =  -2.5*np.log10(flux[ok])+25.0
    return ab

def eflux2eab(eflux,flux,minerr=0.01):
    ok = np.where(flux>0.0)[0]
    eab = np.zeros(len(eflux))+99.0
    eab[ok] = eflux[ok] *2.5/flux[ok]/np.log(10)
    return np.sqrt(eab**2 + minerr**2)




def crudeSky2xy(cra,cdec,rotang,ra,dec):
    print '\n*** WARNING: only doing crude conversion to pixel coords. Use IRAF Gemini task for final cats ***\n'
    w = wcs.WCS(naxis=2)
    w.wcs.crpix = [cfg.ccd_cx,cfg.ccd_cy]

    print cfg.ccd_cx,cfg.ccd_cy
    print cra,cdec

    w.wcs.crval = [cra, cdec]
    w.wcs.ctype = ["RA---TAN", "DEC--TAN"]
    #wcs.wcs.crota = [rotang, rotang] #** CHECK **
    # [for some reason, setting the rotval keyword no longer seems to work, so manipulate CD matrix directly instead!
    
    theta_rad=np.deg2rad(rotang[0])  #*** need to CHECK sign!
    cd = [ (-cfg.pxscale*np.cos(theta_rad)/3600.0,-cfg.pxscale*np.sin(theta_rad)/3600.0), \
            (-cfg.pxscale*np.sin(theta_rad)/3600.0,cfg.pxscale*np.cos(theta_rad)/3600.0)]
    w.wcs.cd = cd
#    wcs.wcs.print_contents()

#    xpix,ypix = wcs.wcs_world2pix(ra,dec, 1)  
    xpix,ypix = w.wcs_world2pix(ra,dec, 1)  
    return xpix,ypix


    """
    gmskcreate ("cos221.rd",
    " GN-2007A-Q-4", "gmos-s", 150.5702, 2.4986, 90., fl_getim=no, inimage="",
    outimage="", fl_getxy=yes, outcoords="coss21.txt", outtab="coss21OT",
    iraunits="degrees", fraunits="degrees", slitszx=1., slitszy=5.,
    logfile="logfile", glogpars="", fl_inter=yes, verbose=yes, fl_debug=no,
    status=0, scanfile="")
    """

def radec2xyGmskcreate(clusname,tmpout,foo):


    # -- get mask info from GOGREEN.fits
    tabfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    tab = fits.getdata(tabfile)
    #print tab.field

    ok = np.where(tab['cluster'] == clusname)[0]
    tab = tab[ok] # delete all other lines for convenience

    rafield = tab['RA_gmos_deg'] ; decfield = tab['dec_gmos_deg']
    pa = tab['pa_deg']
    image = tab['z_image']
    #nORs = tab['Preimage'] #*** This needs to be set to telescope on which mask will be observed
    nORs = tab['spec_inst'][0]
#    instrument = ('gmos-%s'%nORs).lower()
    instrument = ('%s'%nORs).lower()

    print 'instrument is %s'%instrument
    # IF THIS IS UNDEFINED IN GOGREEN.fits TASK SHOULD FAIL!

    cname = clusname

#    tmpout="_tmp.txt"
    #gprgid="Q"
    gprgid="GN-2015B-LP-4"
##    instrument="gmos-s"
#    instrument="gmos-n" #***

#    foo = fits.getdata(incat)
    """
    The input data file must contain one line per object. Each line must contain values for 'ID', 'RA', 'DEC', 'MAG' in that order and separated by spaces, where 'ID' is an integer ID number for the object,
    'RA' and 'DEC' are the Right Ascension and Declination of the object, and 'MAG' is the magnitude of the object.
    """

    np.savetxt('_tmp.txt',np.transpose( (foo['ID'],foo['RA'],foo['DEC'],foo['ZMAG']) ),fmt='%i %.7f %.7f %.3f')

    try:
        os.system('rm -f '+cname+"_pseudo.fits")
        os.system('rm -f '+cname+"_e2xy.cat")
        os.system('rm -f '+cname+"_GMI.fits")
    except:
        pass

    yes = "YES" ; no = "NO"

    print 'running gmskcreate...'
    fullimname="\""+(os.getenv('GoGREEN_data')+'/Data/reduced/'+cname+'/Imaging/'+image)[0]+"\""
    # MLB: For some reason gmkscreate task below is not finding the image via the full pathname
    # Workaround is to make sure the image is in the working directory.
    print tmpout,gprgid,instrument,rafield,decfield,pa,fullimname
    ss = iraf.gmskcreate(tmpout,gprgid,instrument,\
                 rafield,decfield,pa,\
                 fl_getim=yes, inimage=image[0].strip()+".fits[SCI]", outimage=cname+"_pseudo.fits",\
                 #fl_getim=yes, inimage=fullimname, outimage=cname+"_pseudo.fits",\
                 fl_getxy=yes,outcoor=cname+"_e2xy.cat",\
                 outtab=cname+"_GMI.fits",\
                 iraunits="degrees",fraunits="degrees",
                 verbose=yes)

    xp,yp = np.loadtxt(cname+"_e2xy.cat", unpack=True)
#    assert len(xp) == len(foo)
    print "done"

    # correct for binning factor in pix coords:
    #outtab = recf.append_fields(foo,('XP','YP'),(xp/2.0,yp/2.0),asrecarray=True,usemask=False)
    #fits.writeto('%s_phot.fits'%clusname, outtab, clobber=True)



    if(ilogintmp):
        os.system('rm -f login.cl')


    return xp/2.,yp/2.

    #print tab


def getCOSMOS():
        # cosmos data/ ultravista
#        dat = fits.getdata('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.fits.gz')
        COSMOSdir = os.getenv('GoGREEN_data')+'/Data/PubCats/COSMOS/'
        dat = fits.getdata(COSMOSdir+'UVISTA_final_v4.1.fits.gz')
        zdat = fits.getdata(COSMOSdir+'UVISTA_final_v4.1_zout.fits')

        ###
        use = dat['USE']
        #dat=dat[use]
        #zdat=zdat[use]
        #MLB edit Oct 26 2015
#       MLB: USE=1 is for galaxies only so we need to change this to make use of the stars as well
        usedat=dat[np.where(use | dat['star'])]
        zdat=zdat[np.where(use | dat['star'])]
        ###

        id = usedat['id']
        ra = usedat['ra']
        dec = usedat['dec']
        irac1 = flux2ab(usedat['ch1'])
        eirac1 = eflux2eab(usedat['ech1'],usedat['ch1'])
        zmag = flux2ab(usedat['zp'])
        ezmag = eflux2eab(usedat['ezp'],usedat['zp'])

        #    bad=np.where(dat['ch1_colf']<=0.0)[0]
        #    if(len(bad)>0): irac1[bad]=0.0
        #    bad=np.where(dat['z_colf']<=0.0)[0]
        #    if(len(bad)>0): zmag[bad]=0.0

        Kstot = flux2ab(usedat['Ks_tot'])
        #    Kstot = -2.5*np.log10(dat['K_totf'])+25.0
        #    bad=np.where(dat['K_totf']<=0.0)[0]
        #    if(len(bad)>0): Kstot[bad]=0.0

        Kap = flux2ab(usedat['Ks'])
        #    Kap = -2.5*np.log10(dat['K_colf'])+25.0
        #    bad=np.where(dat['K_colf']<=0.0)[0]
        #    if(len(bad)>0): Kap[bad]=0.0

        # convert both ap mags to total
        irac1 = irac1-Kap +Kstot
        zmag = zmag-Kap+Kstot

        #zdat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.zout')
        #zdat = fits.getdata('%s/PubCats/UDS_DR8/udsz.fits'%rootdir)

        ###zph = zdat['z_a'] #*** is this best choice???
        zph = zdat['z_p'] #*** is this best choice???
        zphLo = zdat['l68']
        zphHi = zdat['u68']
        ###    izph = True
        izph = np.ones(len(usedat))
        #izs = False

        zs = zdat['z_spec'] #-np.ones(len(id))
        #***
        #zsq = -np.ones(len(id)) # zs quality flag ACTUALLY LET'S JUST ASSUME THE MASK DESIGN CODE ONLY GETS GIVEN GOOD ZSs
        #    xp,yp = MaskpixfromSky(fInfo['cra'][0],fInfo['cdec'][0],fInfo['rotang'][0],obj.ra,obj.dec)
#        xp,yp = crudeSky2xy(cra,cdec,rotang,ra,dec)

        #    obj = recf.append_fields(t,('ID','ra','dec','irac1','eirac1','zmag','ezmag','zph','zphLo','zphHi','izph','zs','xpHam','ypHam'),\
#        obj = recf.append_fields(t,('ID','RA','DEC','IRAC1','EIRAC1','ZMAG','EZMAG','ZPH','ZPHLO','ZPHHI','IZPH','ZS','XP','YP'),\
#                                 (id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,xp,yp),\
#                                   asrecarray=True,usemask=False)
#
#        obj = recf.drop_fields(obj,'f0',asrecarray=True,usemask=False)
        starflag = usedat['star']

        return usedat,id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,starflag


def getSXDFuds():
        SXDFdir = os.getenv('GoGREEN_data')+'/Data/PubCats/SXDF/'
        catfits='%suds8_v0.2.test.fits'%SXDFdir
        dat = fits.getdata(catfits)

        id = dat['id'].astype(int)
        ra = dat['ra']
        dec = dat['dec']
    #    irac1 = -2.5*np.log10(dat['ch1_colf'])+25.0
        irac1 = flux2ab(dat['ch1_colf'])
        #eirac1 = dat['ch1_colfe']#***
        eirac1 = eflux2eab(dat['ch1_colfe'],dat['ch1_colf'])
    #    zmag = -2.5*np.log10(dat['z_colf'])+25.0
        zmag = flux2ab(dat['z_colf'])
    #    ezmag = dat['z_colfe']#***
        ezmag = eflux2eab(dat['z_colfe'],dat['z_colf'])

    #    bad=np.where(dat['ch1_colf']<=0.0)[0]
    #    if(len(bad)>0): irac1[bad]=0.0
    #    bad=np.where(dat['z_colf']<=0.0)[0]
    #    if(len(bad)>0): zmag[bad]=0.0

        Kstot = flux2ab(dat['K_totf'])
    #    Kstot = -2.5*np.log10(dat['K_totf'])+25.0
    #    bad=np.where(dat['K_totf']<=0.0)[0]
    #    if(len(bad)>0): Kstot[bad]=0.0

        Kap = flux2ab(dat['K_colf'])
    #    Kap = -2.5*np.log10(dat['K_colf'])+25.0
    #    bad=np.where(dat['K_colf']<=0.0)[0]
    #    if(len(bad)>0): Kap[bad]=0.0

        # convert both ap mags to total
        irac1 = irac1-Kap +Kstot
        zmag = zmag-Kap+Kstot

        #zdat = ascii.read('/home/gilbank/Proj/goGreen/PubCats/UVISTA_final_v4.1.zout')
        zdat = fits.getdata('%s/udsz.fits'%SXDFdir)

        ###zph = zdat['z_a'] #*** is this best choice???
        zph = zdat['z_p'] #*** is this best choice???
        zphLo = zdat['l68']
        zphHi = zdat['u68']
        ###    izph = True
        izph = np.ones(len(dat))
        #izs = False

        zs = zdat['z_spec'] #-np.ones(len(id))
        #***
        #zsq = -np.ones(len(id)) # zs quality flag ACTUALLY LET'S JUST ASSUME THE MASK DESIGN CODE ONLY GETS GIVEN GOOD ZSs
        #    xp,yp = MaskpixfromSky(fInfo['cra'][0],fInfo['cdec'][0],fInfo['rotang'][0],obj.ra,obj.dec)
 #       xp,yp = crudeSky2xy(cra,cdec,rotang,ra,dec)

    #    obj = recf.append_fields(t,('ID','RA','DEC','IRAC1','EIRAC1','ZMAG','EZMAG','ZPH','ZPHLO','ZPHHI','IZPH','ZS','XP','YP'),\
    #                             (id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,xp,yp),\
    #                               asrecarray=True,usemask=False)
#
#        obj = recf.drop_fields(obj,'f0',asrecarray=True,usemask=False)

        starflag = np.zeros(len(dat))
        classStarMin = 0.92 # from quick hist:
        star = np.where(dat['Kstarclass']> classStarMin)
        starflag[star]=1.0

        return dat,id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,starflag



def mkcatCat(name):

    global cfg

    # grab relevant data from master FITS table:
    tabfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mm = fits.getdata(tabfile)

    #*** Ureka uses an older version of astopy so SkyCoord fails! ***
    ok=np.where(mm['cluster']==name)[0]
    ccoo= SkyCoord(mm['RA_gmos_deg'][ok],mm['DEC_gmos_deg'][ok],unit=(u.deg,u.deg))
    cra,cdec = ccoo.ra.value[0],ccoo.dec.value[0]
    grpcoo= SkyCoord(mm['RA_target_deg'][ok],mm['DEC_target_deg'][ok],unit=(u.deg,u.deg))
    print cra,cdec
    rotang = mm['PA_deg'][ok]
    grpra,grpdec = grpcoo.ra.value[0],ccoo.dec.value[0]
    grpz=mm['Redshift'][ok]
    det = mm['Detector'][ok]
    assert det=='E2v'
    fInfo = mm[ok] # quick fix??

    if (name[0:3]=='COS'):
        dat,id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,starflag = getCOSMOS()

    elif (name[0:3]=='SXD'):
        # SXDF data
        dat,id,ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,starflag = getSXDFuds()


    else:
        print
        print "don't know what field %s is!"%name
        print
        exit(1)

    t = np.recarray(0,formats=('float'))
    obj = recf.append_fields(t,('ID','RA','DEC','IRAC1','EIRAC1','ZMAG','EZMAG','ZPH','ZPHLO','ZPHHI','IZPH','ZS','AQSTAR_CAND'),\
                             (id.astype(int),ra,dec,irac1,eirac1,zmag,ezmag,zph,zphLo,zphHi,izph,zs,starflag),\
                             asrecarray=True,usemask=False)
    obj = recf.drop_fields(obj,'f0',asrecarray=True,usemask=False)



    # convert ra,dec to x,y
    # let's do crude at first, then transform cropped positions:

##    xp,yp = crudeSky2xy(cra,cdec,rotang,ra,dec)
    xp,yp = crudeSky2xy(cra,cdec,(-rotang),ra,dec) #*** hmm, don't know where this came from ***
    #plt.figure()
    #plt.plot(xp,yp,'k.')
    #plt.show()
    inFoV = np.where( (xp>cfg.xminField) & (xp<=cfg.xmaxField) &  (yp>cfg.yminField) & (yp<=cfg.ymaxField))[0]
    obj = obj[inFoV] ; dat=dat[inFoV]
    print '%s objects in field of view'%len(obj)


    # Now let's do it accurately with gmskcreate:
    # (and add to obj)
    np.savetxt('_tmp.txt',np.transpose( (obj['ID'],obj['RA'],obj['DEC'],obj['ZMAG']) ),fmt='%i %.7f %.7f %.3f')


    xp,yp = radec2xyGmskcreate(name,'_tmp.txt',obj)
    assert len(xp) == len(obj)
    os.system('rm -f _tmp.txt')

    #**** star/gal sep. setup star cands:
    # go from starflags (based on SEx CLASS_STAR) to candidate AQSTARs (including mag cuts):
    candSS = np.where( (obj['AQSTAR_CAND']==1) & (obj['ZMAG']<20.3) )[0]
    obj['AQSTAR_CAND']=0
    print '%s candidate AQSTARs found'%(len(candSS))
    if (len(candSS)> 0):
        obj['AQSTAR_CAND'][candSS]=1

    obj = recf.append_fields(obj,('XP','YP'),\
                             (xp,yp),\
                             asrecarray=True,usemask=False)
    # MLB: Attempt to make table readable by IRAF, FV, others.
    # Following http://astropy.readthedocs.org/en/v1.0.3/io/fits/
    prihdu=fits.PrimaryHDU(header=fits.Header())
    col1=fits.Column(name='ID',format='1J',disp='I11',array=id[inFoV].astype(int))
    col2=fits.Column(name='RA',format='E',array=ra[inFoV])
    col3=fits.Column(name='DEC',format='E',array=dec[inFoV])
    col4=fits.Column(name='IRAC1',format='E',array=irac1[inFoV])
    col5=fits.Column(name='EIRAC1',format='E',array=eirac1[inFoV])
    col6=fits.Column(name='ZMAG',format='E',array=zmag[inFoV])
    col7=fits.Column(name='EZMAG',format='E',array=ezmag[inFoV])
    col8=fits.Column(name='ZPH',format='E',array=zph[inFoV])
    col9=fits.Column(name='ZPHLO',format='E',array=zphLo[inFoV])
    col10=fits.Column(name='ZPHHI',format='E',array=zphHi[inFoV])
    col11=fits.Column(name='IZPH',format='E',array=izph[inFoV])
    col12=fits.Column(name='ZS',format='E',array=zs[inFoV])
    col13=fits.Column(name='AQSTAR_CAND',format='1J',disp='I11',array=obj['AQSTAR_CAND'])
    col14=fits.Column(name='XP',format='E',array=xp)
    col15=fits.Column(name='YP',format='E',array=yp)
    cols=fits.ColDefs([col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15])
    #cols=fits.ColDefs(obj)
    tbhdu=fits.BinTableHDU.from_columns(cols)
    thdulist=fits.HDUList([prihdu,tbhdu])
    thdulist.writeto('foo.fits',clobber=True)
        
    #fits.writeto('foo.fits',obj, clobber=True)

    plt.figure()
    plt.title(name)
    plt.subplot(121)
    plt.plot(obj['RA'],obj['DEC'],'k.')
    plt.plot(obj['RA'][candSS],obj['DEC'][candSS],'r*')
    plt.xlabel('ra (deg)')
    plt.ylabel('dec (deg)')

    plt.subplot(122)
    plt.plot(obj['XP'],obj['YP'],'k.')
    plt.plot(obj['XP'][candSS],obj['YP'][candSS],'r*')
    plt.xlabel('xp (2 x binned e2v pixels)')
    plt.ylabel('yp (2 x binned e2v pixels)')

    ax = plt.gca()

    cenXshift=0.0

    rect=Rectangle((0+cenXshift,0),cfg.ccd_dx,cfg.ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+cfg.ccd_dx+cfg.ccd_xgap+cenXshift,0),cfg.ccd_dx,cfg.ccd_dy,color='none',ec='r')
    ax.add_patch(rect)
    rect=Rectangle((0+2.*(cfg.ccd_dx+cfg.ccd_xgap)+cenXshift,0),cfg.ccd_dx,cfg.ccd_dy,color='none',ec='r')
    ax.add_patch(rect)

    # GMOS imaging field:
    plt.plot((cfg.x0,cfg.x0),(cfg.y0,cfg.y1),'k-',lw=2)
    plt.plot((cfg.x0,cfg.x1),(cfg.y1,cfg.y1),'k-',lw=2)
    plt.plot((cfg.x1,cfg.x1),(cfg.y1,cfg.y0),'k-',lw=2)
    plt.plot((cfg.x1,cfg.x0),(cfg.y0,cfg.y0),'k-',lw=2)



    print
    print '---------- Field info -----------'
    print 'Mask centre     ra,dec = %.6f, %.6f; rotang = %.2f'%(cra,cdec,rotang)
    print 'Cluster centre: ra,dec = %.6f, %.6f; redshift = %.4f'%(grpra,grpdec,grpz)
    print '---------------------------------'
    print

    plt.show()



if __name__ == "__main__":

    name = sys.argv[1]
#    imagename = sys.argv[2]
#    incat = sys.argv[3]

    mkcatCat(name)
#    cat2gmmpsE2v(name,imagename,incat)
#    plotField(name) # Need to close this plot window manually. Encourage the user to look at results!

