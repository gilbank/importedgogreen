from GoGREEN_utils import getPhotFilename,getMaskDir
from astropy.io import fits,ascii
import numpy as np
import os,glob
from numpy.lib import recfunctions as recf
from optparse import OptionParser

parser=OptionParser()
(options,args) = parser.parse_args()
try:
	name=str(args[0])
except:
	name = raw_input('Cluster name? e.g. SXDF64')
try:
	masknum=int(args[1])
except:
	masknum=int(raw_input('Mask number?'))
photfile = getPhotFilename(name)
photdata = fits.getdata(photfile)
stars = photdata[np.where(photdata['AQSTAR_CAND']==1)[0]]
#MLB Kludge to add specific objects at this stage.
#stars = photdata[np.where((photdata['AQSTAR_CAND']==1) | (photdata['ID']==564) | (photdata['ID']==388))[0]]
maskdir=getMaskDir(name)
OTdir=maskdir+"OT/"
OTfiles=glob.glob(OTdir+name+"_mask"+str(masknum)+"_ver*_GMI_OT.fits")
versions=np.array([],dtype='int')
for OTfile in OTfiles:
	versions=np.append(versions,int(OTfile.split('ver')[1][0]))
latestversion=str(np.max(versions))
newversion=str(np.max(versions)+1)
latestOT=OTdir+name+"_mask"+str(masknum)+"_ver"+latestversion+"_GMI_OT.fits"
newOT=OTdir+name+"_mask"+str(masknum)+"_ver"+newversion+"_GMI_OT.fits"
#maskdata=fits.getdata(latestOT)
maskHDU=fits.open(latestOT)
maskPH=maskHDU[0].header
maskdata=maskHDU[1].data
maskheader=maskHDU[1].header
aqstar_id=maskdata[np.where(maskdata['priority']=='0')[0]]['ID']
dtype=maskdata.dtype
#formats=maskdata.formats
#columns=maskdata.columns
#tbhdu=fits.BinTableHDU.from_columns(columns)
for id,ra,dec,zmag,x,y in zip(stars['ID'],stars['RA'],stars['DEC'],stars['ZMAG'],stars['XP'],stars['YP']):
	if id not in aqstar_id: 
		print id,ra/15.,dec,zmag,x,y,1,3,0,"4"
		newdata=np.array((id,ra/15.,dec,zmag,x,y,1,3,0,"2"),dtype=dtype)
		maskdata=np.append(maskdata,newdata)
		#maskdata[32]=newdata
		maskheader['NAXIS2']=maskheader['NAXIS2']+1
hduloutput=maskHDU
#hduloutput=fits.HDUList()
#hduloutput.append(fits.PrimaryHDU())
hduloutput[0].header=maskPH
hduloutput[1].data=maskdata
#hduloutput.append(fits.BinTableHDU(data=maskdata))
#hduloutput[1].header=maskheader
hduloutput.writeto(newOT,clobber=True)
maskHDU.close()