#! Python program to format the data for runz
# Usage
# GoGREEN_format4Marz.py filename.fits
#

# Version history
#
# 01/12/2017
# Copied from GoGREEN_format4RUNZ.py
#

import sys
import astropy.io.fits as fits
import numpy
from optparse import OptionParser
import os,shutil
import subprocess as sp

parser=OptionParser()
        
parser.add_option("-i", "--inputFile", dest="inputFile",
                  default=None, help="Input file")

parser.add_option("-o", "--outputFile", dest="outputFile",
                  default=None, help="Output file")

parser.add_option("-O", "--observatory", dest="observatory",
                  default=None, help="Observatory")

parser.add_option("-t", "--twodFile", dest="twodFile",
                  default=None, help="2d input file")

parser.add_option("-w", "--waveStart", dest="waveStart",
                  default=None, help="Starting Wavelength")

parser.add_option("-d", "--dir", dest="dir",
                  default='.', help="Output directory")

(options, args) = parser.parse_args()

repository=os.environ['GoGREEN_data']+'/repository/'
intputDir,input=os.path.split(repository+options.inputFile)
outputDir=options.dir

# The files in the GoGREEN data repository can be gzipped
shutil.copy(repository+options.inputFile,input)
try:
    sp.call('gunzip '+input,shell=True)
except:
    pass

# 2d sky-subtracted data
if options.twodFile==None:
    twodspectra=repository+options.inputFile.replace('tcal_oned','twod')
else:
    twodspectra=repository+options.twodFile
shutil.copy(twodspectra,os.path.split(twodspectra)[1])

try:
    sp.call('gunzip '+os.path.split(twodspectra)[1],shell=True)
except:
    pass
    
input=input.replace('.gz','')
if options.outputFile==None:
    output=input.replace('_oned.fits','_mz.fits')
else:
    output=options.outputFile

data=fits.open(input)

# The first extension on the input file is a table containing information about the
# targets in the slits
# The resulting extensions are grouped together in three. Within each
# group, one has the data, the variance and the data quality

# There is one output file for each object
# Stars are skipped in the FITS file, but not in the FITS table, so we
# need to match against object the MDF row
# Spectrum goes in extension 0, variance goes in extension 1. 

start=1
end=len(data)-1
table=len(data)-1

# For trimming the spectra

header=data[start].header
if options.waveStart!=None:
    waveStart=float(options.waveStart)
    pixStart=int((waveStart-header['CRVAL1']) / header['CD1_1'] + header['CRPIX1'])
    # Recompute the actual wavelength corresponfing to this pixel
    waveStart=(pixStart-header['CRPIX1']) * header['CD1_1'] + header['CRVAL1']
else:
    pixStart=1
    waveStart=header['CRVAL1']

print pixStart,waveStart

# Set up the dimensions of the output arrays

nSpec=(end-start) / 3
nBins=data[1].data.shape[0] - pixStart + 1

# ToDo: Add a check to see that CRVAL1 does not change

flux=numpy.zeros(nSpec*nBins).reshape(nSpec,nBins)
variance=numpy.zeros(nSpec*nBins).reshape(nSpec,nBins)

objectNames=[]
objectMags=[]
objectRAs=[]
objectDECs=[]

deg2rad=numpy.pi / 180.

    
for index,i in enumerate(range(start,end,3)):
    flux[index]=data[i].data[pixStart-1:]
    variance[index]=data[i+1].data[pixStart-1:]
    if data[i+2].data[pixStart-1:].sum()==0:
        print "No valid data for object #%d" % (index+1)
        flux[index]=numpy.nan
        variance[index]=numpy.nan
    else:
        # Anything with a variance that is more than above 1000. is set to nan
        # This could be done at the end
        variance[index][variance[index] > 1000.]=numpy.nan

    # Check wavelength solution
    if data[i].header['CRVAL1'] != data[start].header['CRVAL1'] or \
       data[i].header['CRPIX1'] != data[start].header['CRPIX1'] or \
       data[i].header['CD1_1'] != data[start].header['CD1_1']:
        print "Inconistent wavelegth solutions"
        exit()

    mdfrow=data[i].header['MDFROW']
    name='%s' % (data[table].data['ID'][mdfrow-1])
    mag='%5.3f' % (data[table].data['MAG'][mdfrow-1])
    ra='%9.5f' % (data[table].data['RA'][mdfrow-1]*deg2rad)
    dec='%9.5f' % (data[table].data['DEC'][mdfrow-1]*deg2rad)
    objectNames.append(name)
    objectMags.append(mag)
    objectRAs.append(ra)
    objectDECs.append(dec)

# Create the primary hdu
prihdu = fits.PrimaryHDU(flux)

for keyword in ['CRVAL1','CRPIX1','CD1_1']:
    prihdu.header[keyword]=data[start].header[keyword]
    
if options.waveStart!=None:
    prihdu.header['CRVAL1']=waveStart

# Additional keywords
# Turn off heliocentric corrections in Marz

# If the observatory is not specified, we turn off heliocentric corrections
if options.observatory==None:
    prihdu.header['DO_HELIO']=False
else:
    print "The option to enter observatory parameters is not available yet"
    
# Add the variance array to the header
header=fits.Header()
header['EXTNAME']='VARIANCE'

# Create the FITS table
c1 = fits.Column(name='NAME', array=objectNames, format='20A')
c2 = fits.Column(name='RA', array=objectRAs, format='E')
c3 = fits.Column(name='DEC', array=objectDECs, format='E')
c4 = fits.Column(name='MAGNITUDE', array=objectMags, format='E')
c5 = fits.Column(name='COMMENT', array=objectNames, format='20A')

tbhdu = fits.BinTableHDU.from_columns(fits.ColDefs([c1,c2,c3,c4,c5]))
tbhdu.header['EXTNAME']='FIBRES'

hdulist=fits.HDUList([prihdu,fits.ImageHDU(data=variance,header=header),tbhdu])

hdulist.writeto(outputDir+'/'+output,clobber=True)
    
data.close()

