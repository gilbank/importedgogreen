#
# Read in photometric catalogue and set up weighting scheme ready for maskdes
# Make checking plots (spatial, CMD,..)
#
import os,sys,string
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from astropy.io import fits,ascii
from astropy.io.fits import Column
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper


from numpy.lib import recfunctions as recf
from glob import glob

import GoGREEN_maskdesDet as MD
from GoGREEN_plotmask import plotmask
from GoGREEN_addfillers import addFillers
from GoGREEN_utils import getPhotFilename, colz, getzSpecFilename

#from GoGREEN_cats2OT import cats2OT

"""
#*** TODO:
- batch run several fields?

- Sort out reading of catalogues and setup star candidates 
    **(input catalogue/master table formats)**

- deal with weighting for targets with goGreen redshifts

- maskdesDet:
 optimise setup star choices

- write gmmps format file: pyraf thru Ureka?


--- minor 

- store statistics from best prepMask run?
-  remove a lot of useless code
#***
"""


plt.ion() #*** allow to run from the command line without waiting for window to be closed.


# *** This seems to miss the first match sometimes?!?! this didn't seem to be the case before.
#     Better to explicitly use a one-off list comprehension match?? ***
def match(a,b,badval=-1):
    # much faster matching using dictionary!
    good=np.where(b <> badval)[0]
    d=dict(zip(b[good], np.arange(len(b))[good]  ))
    okd = [d.get(x) for x in a]
    # result will be none if there is no match. need to deal
    okddd=np.array(okd)
    ok=np.array(okddd.nonzero()).astype('int64')
    
    okb=(okddd[ok]).astype('int64')
    oka=ok
        
#    return np.array(oka),np.array(okb)
    return np.reshape(np.array(oka),-1),np.reshape(np.array(okb),-1)


def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal['ZPH']>0.7) & (gal['ZPH']<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH'] >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH']>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt


def genWeights(gal,fInfo,scheme='faint'):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] ; zmag = gal['ZMAG']
    pri = gal.prob
    
    print scheme

    # -- redder than CMR:
#    redSeqCoeffs = np.array((fInfo['RSslope'],fInfo['RSintcpt']))
#    redCoeffs = redSeqCoeffs+np.array((0,dRed))
#    redLimCoeffs = redSeqCoeffs+np.array((0,dRedLim))

    yfit = np.polyval(redCoeffs,mag)
#    rej = np.where(col > yfit)[0]


    if (scheme=='first'):
        # set bright high priority
        fntWt = 0.01
        briWt = 0.8
        ok = inBin(zmag,cfg.zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,cfg.zBri) ; pri[ok]=briWt
        rej = np.where( (col > yfit) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 
#        ok = inBin(zmag,zVBri) ; pri[ok]=0.05
#        ok = np.where( (gal.inMask>0) & (gal['ZMAG']<zBri[1]) )[0]
#        print '%s bright gals in previous mask being ignored'%(len(ok))
#        pri[ok]=0.0

        
    elif (scheme=='fillfaint'):
        
        fntWt = 0.8
        briWt = 0.1
        ok = inBin(zmag,cfg.zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,cfg.zBri) ; pri[ok]=briWt        
        rej = np.where( (col > yfit) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zVFnt[1]) )[0]
#        rej = np.where( (col > yfit) & (zmag>zVBri[0]) & (zmag<zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal['ZMAG']>=cfg.zFnt[0]) )[0]
        print '%s faint gals in previous mask being force-repeated'%(len(ok))
        pri[ok]=1.0    
        ok = np.where( (gal.inMask>0) & (gal['ZMAG']<cfg.zBri[1]) )[0]
        print '%s bright gals in previous mask(s) being ignored'%(len(ok))
        pri[ok]=0.0 
        
        
    elif(scheme=='rptfnt'):
        # don't add any new faint objects (just repeat)
        # set weighting high for bright to try to squeeze in
        briWt = 0.8
        fntWt = 0.0
        ok = inBin(zmag,cfg.zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,cfg.zBri) ; pri[ok]=briWt

        rej = np.where( (col > yfit) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal['ZMAG']>=cfg.zFnt[0]) )[0]
        print '%s faint gals in previous mask being force-repeated'%(len(ok))
        pri[ok]=1.0
        ok = np.where( (gal.inMask>0) & (gal['ZMAG']<cfg.zBri[1]) )[0]
        print '%s bright gals in previous mask being ignored'%(len(ok))
        pri[ok]=0.0  
# MLB edit May 9, 2016
        ok = np.where( (gal.inMask>0) & (gal['ZS']>0) & (gal['SPZ_QUAL']<3) )[0]
        print '%s gals in previous mask(s) with existing good redshifts being ignored'%(len(ok))
        pri[ok]=0.0

    elif(scheme=='bandbri'):
        # don't add any new faint objects (just repeat)
        # set weighting high for bright to try to squeeze in
        briWt = 0.8
        fntWt = 0.0
        ok = inBin(zmag,cfg.zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,cfg.zBri) ; pri[ok]=briWt

        rej = np.where( (col > yfit) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 

        ok = np.where( (gal.inMask>0) & (gal['ZMAG']<cfg.zBri[1]) )[0]
        print '%s bright gals in previous mask being ignored'%(len(ok))
        pri[ok]=0.0
        if (cfg.iRadialWt):
            radwt =  (1. - 0.4*gal.disMpc)**2 #*** probably not extreme enough
            notMustHaves = np.where(pri<1.0)[0]
            pri[notMustHaves] = pri[notMustHaves]* radwt[notMustHaves]

        
    else:
        print 'scheme %s not found'%scheme
        print flibble # to crash
    
        rej = np.where( (col > yfit) & (zmag>cfg.zVBri[0]) & (zmag<cfg.zVFnt[1]) )[0]
        pri[rej] = 0.0###1#5 
    
    
###    ok = inBin(zmag,zVFnt) ; pri[ok]=0.#1
###    ok = inBin(zmag,zVBri) ; pri[ok]=0.01#5
    
    # -- photoz weighting
    if (gal['IZPH'][0]==1): # hmm, it seems izph is an array of len(gal), but all elements should be either 1 or 0
##***
#        print 'bypassing zph wts!!!!'
#        phzWt = np.zeros(len(gal))
#        phzWt[:]=0.99
        print 'Using photo-z weights'
        phzWt = getZphWts(gal,fInfo)

        """
        phzWt = np.zeros(len(gal))
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal['ZPH']>0.7) & (gal['ZPH']<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH'] >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH']>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0

        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        """
        fairGame = np.where(pri<1.0)[0] # CAREFUL! don't downweight pri=1 objects!!
#        pri = pri * phzWt
        pri[fairGame] = pri[fairGame] * phzWt[fairGame]
    # --


    #-- external zspec weighting (New Jan 2016):
    if (np.max(gal['ZS'])>0):
        # *** currently let's just weight everything (with non-zero ZS) by 0.1 ***
        zsWtFactor = 0.1
        print 'external ZS being used in priority weighting'
#        fairGame = np.where(pri<1.0)[0] # CAREFUL! don't downweight pri=1 objects!!
#        fairGame = np.where( (pri<1.0) & (gal['ZS']>0.0) )[0] # CAREFUL! don't downweight pri=1 objects!!
# MLB edit Jan 25, 2016.  Only downweight if zquality=1 or 2.  Applicable to GCLASS catalogues only.
        fairGame = np.where( (pri<1.0) & (gal['ZS']>0.0) & (gal['SPZ_QUAL']<3))[0] # CAREFUL! don't downweight pri=1 objects!!
        print '%s objects being downweighted'%(len(fairGame))
        pri[fairGame] = pri[fairGame] * zsWtFactor
    #--



    # -- redder than CMR:
    yfit = np.polyval(redCoeffs,mag)
#    rej = np.where(col pwd> yfit)[0]
    rej = np.where( (col > yfit) & (zmag>cfg.zVBri[0]) & (zmag<cfg.zVFnt[1]) )[0]
    pri[rej] = 0.0###1#5 

    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>cfg.zVBri[0]) & (zmag<cfg.zVFnt[1]) )[0]
    pri[rej] = 0.00 

    # --
    
    
    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfit = np.polyval(cfg.blueCoeffs,mag)
    rej = np.where(col < yfit)[0]
    pri[rej]=0.0
    # --
    
    # -- irac1 exclusion cut:
    rej = np.where(mag>cfg.irac1VFnt)[0]
    pri[rej]=0.0
    bad = np.where(mag>cfg.irac1Lim)[0]
    pri[bad]=pri[bad]*0.#1 # downweight v faintest bin

    
    bad = np.where(mag<0.0)[0]
    pri[bad]=0.0 # IRAC undetected?***
    # --

    # -- very last cut is to reject all non-gals (this may just be expolicit setup stars, but could also be artefacts, etc.):
    nongal = np.where(gal['AQSTAR_HANDCHECK']==1)[0]
    pri[nongal]=0.0
    # --


    
    

    noRpt = np.where(gal.noRpt==1)[0]
    if len(rej)>0:
        pri[noRpt]=0.0

    # update weights:
    gal.prob = pri
    





def plotCMD(gal,fInfo,axname):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] 
    pri = gal.prob
    axname.plot(mag,col,'k,')
    axname.set_xlabel('[3.6]')
    axname.set_ylabel('z - [3.6]')
    axname.set_xlim([19,23])
    axname.set_ylim([-2,6])
    
    xxx = np.arange(0,30)
    axname.plot(xxx,cfg.zFnt[1]-xxx,'b--',label='faint')
    axname.plot(xxx,cfg.zBri[1]-xxx,'b:',label='faint')
    axname.plot(xxx,np.polyval(cfg.blueCoeffs,xxx),'b-')
    

    axname.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:')
    axname.plot(xxx,np.polyval(redCoeffs,xxx),'r-')
    axname.plot(xxx,np.polyval(redLimCoeffs,xxx),'r--')
    
    # wts:
    sz=pri*100 +0.1
    axname.scatter(mag,col,s=sz,alpha=0.5,color='b')
    

# initial plot to identify location of cluster in ra,dec and CMD    
def plotClus(gal,fInfo):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] 
    pri = gal.prob
    xp = gal['XP'] ; yp = gal['YP']
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal['IZPH'][0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.05)  & (gal['ZMAG']<=zFnt[1])  & (gal['IRAC1']<=irac1Lim))[0]
###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=0.3) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5, label='r<0.3Mpc')
        ok=np.where( (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([19,23])
    plt.ylim([-2,6])
    plt.legend(numpoints=1)
    
    xxx = np.arange(0,30)
    plt.plot(xxx,cfg.zFnt[1]-xxx,'b--',label='faint')
    plt.plot(xxx,cfg.zBri[1]-xxx,'b:',label='bright')
    plt.plot(xxx,np.polyval(cfg.blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:',label='RS')
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+%.2f'%dRed)
    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+%.2f'%dRedLim)
    plt.legend(numpoints=1)
    
    # spatial:
    plt.subplot(122)
    plt.plot(gal['RA'],gal['DEC'],'k,')

    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3)
    if (gal['IZPH'][0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.05) & (gal['ZMAG']<=zFnt[1])  & (gal['IRAC1']<=irac1Lim) )[0]
###        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal['ZMAG']<=cfg.zFnt[1])  & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal['ZMAG']<=cfg.zFnt[1])  & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=0.2)
        
 
    plt.xlabel('ra')
    plt.ylabel('dec')
    
    
    plt.show()
    
    
# in pixels
def drawMpcCircle(xc,yc,nMpc,fInfo,axname):
    radius = (nMpc*1000.0*asperkpc(fInfo['grpz'])).value # arcsec
    radius = radius/pxscale # pix
    circ = Circle((xc,yc),radius, fill=False, facecolor='none', edgecolor='r')
    axname.add_patch(circ)

def calcdisMpc(fInfo,gal):
    # add distance of every gal from rabcg, decbcg in Mpc
    
    
    dra = (gal['RA'] - fInfo['grpra'])*np.cos(np.deg2rad(gal['DEC']))
    ddec = gal['DEC'] - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
#    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
#    del gal
#    gal = np.copy(tgal)
#    del tgal
    adisMpc = disMpc
    
    if(0): # use spm instead
        from astrometry.libkd import spherematch as spm
        oka,okb,disdeg = spm.match_radec(gal['RA'],gal['DEC'],fInfo['grpra'],fInfo['grpdec'],1.0,nearest=False)
        adisMpc = np.ones(len(gal))
        disMpc = disdeg*3600.0 / convfac /1000.0
        adisMpc[oka] = disMpc # needed with spm
    

    return adisMpc

def plotSpatial(gal,fInfo,axname):
    xp = gal['XP'] ; yp = gal['YP'] 
    pri = gal.prob
    axname.plot(xp,yp,'k,')
    axname.set_xlabel('xp')
    axname.set_ylabel('yp')
#    axname.set_xlim([19,23])
#    axname.set_ylim([-2,6])

    # wts:
    sz=pri*100 +0.1
    axname.scatter(xp,yp,s=sz,alpha=0.5,color='b')

    MD.drawHam(axname)
    
    # draw 0.5 Mpc radius circle
    ##xc,yc = MD.MaskpixfromSky(fInfo['cra'],fInfo['cdec'],fInfo['rotang'],fInfo['grpra'],fInfo['grpdec'])
    xc = cfg.ccd_cx ; yc = cfg.ccd_cy
    drawMpcCircle(xc,yc,0.5,fInfo,axname)
    drawMpcCircle(xc,yc,1.0,fInfo,axname)
    drawMpcCircle(xc,yc,2.0,fInfo,axname)

#def writeInput(inname,gal,fInfo,sLen=cfg.nomsLen,sWid=cfg.nomsWid,sTilt=cfg.nomsTilt,stars=None):
def writeInput(inname,gal,fInfo,stars=None):
    # write the input file for desmask()
    #ID,xobj,yobj,ra,dec,pri,slen,swid,stilt = np.loadtxt('tmpCDFS41.dat',unpack=True)


    sLen = cfg.nomsLen ; sWid=cfg.nomsWid ; sTilt=cfg.nomsTilt

#    use=np.where(gal.prob>0.0)[0]
    use=np.where( (gal.prob>0.0) & (gal['AQSTAR_HANDCHECK']<1) )[0]
#    np.savetxt(inname,np.transpose( (gal.ID,gal['XP'],gal['YP'],gal['RA'],gal['DEC'],gal.prob,\
#                                             np.repeat(sLen,len(gal)), np.repeat(sWid,len(gal)), np.repeat(sTilt,len(gal)) ) ) )
    stars = np.where(gal['AQSTAR_HANDCHECK']==1)[0]

    print 'here:',len(stars)
    print stars
#    print use


    f = open(inname,'w')
    np.savetxt(f,np.transpose( (gal.ID[use],gal['XP'][use],gal['YP'][use],gal['RA'][use],gal['DEC'][use],gal.prob[use],\
                                             np.repeat(sLen,len(use)), np.repeat(sWid,len(use)), np.repeat(sTilt,len(use)) ) ),\
                                                      fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )

    if len(stars)>0:
        gal.prob[stars]=9.0
        np.savetxt(f,np.transpose( (gal['ID'][stars],gal['XP'][stars],gal['YP'][stars],gal['RA'][stars],gal['DEC'][stars],\
                                        gal.prob[stars],\
                                              np.repeat(cfg.starsLen,len(stars)), np.repeat(cfg.starsWid,len(stars)), np.repeat(0.0,len(stars)) ) ),\
                                              fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    else:
        pass
    f.close()
    """
    f = open(inname,'w')
    np.savetxt(f,np.transpose( (gal.ID[use],gal['XP'][use],gal['YP'][use],gal['RA'][use],gal['DEC'][use],gal.prob[use],\
                                             np.repeat(sLen,len(use)), np.repeat(sWid,len(use)), np.repeat(sTilt,len(use)) ) ),\
                                             fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    try: #(len(stars))
        use=np.arange(len(stars)) ; stars.prob = np.repeat(9.0,len(stars))
        try:
            uid = np.loadtxt(name+'.stars')
#            ok1,ok2 = match(uid,stars.ID)
            ok2 = [x for x,st in enumerate(stars.ID) if st in uid]     
            stars = stars[ok2]
            use=np.arange(len(stars)) ; stars.prob = np.repeat(9.0,len(stars))
        except:
            pass # no .stars file
            
                
        
        np.savetxt(f,np.transpose( (stars.ID[use],stars.xp[use],stars.yp[use],stars.ra[use],stars.dec[use],stars.prob[use],\
                                              np.repeat(starsLen,len(use)), np.repeat(starsWid,len(use)), np.repeat(0.0,len(use)) ) ),\
                                              fmt='%s %.2f %.2f %.7f %.7f %.4f %.2f %.2f %.3f' )
    except:
        pass
    f.close()
    """
    
def tagMask(gal,fInfo,mID,masknum=1):    
    # add inMask bitflag:
    ok1,ok2 = match(gal.ID,mID)
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal['ZMAG']
    # update inMask flag with binary to indicate which number(s) mask in
    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def evalCat(gal,fInfo):
    col = gal['ZMAG'] - gal['IRAC1']
    
    # Find number of objects in appropriate mag,distance, etc. bins in whole catalogue (with appropriate cuts)
    # for completeness calculations:
    inner = (gal.disMpc<=0.5)
    outer = ( (gal.disMpc>0.5) & (gal.disMpc<=1.0) )
    outsk = ( (gal.disMpc>1.0) )
    
    bri = ( (gal['ZMAG'] > cfg.zBri[0]) & (gal['ZMAG'] <= cfg.zBri[1]) )
    fnt = ( (gal['ZMAG'] > cfg.zFnt[0]) & (gal['ZMAG'] <= cfg.zFnt[1]) )
    
    # -- redder than CMR:
#    yfitRedLim = np.polyval(redLimCoeffs,gal['IRAC1'])
    yfitRedLim = np.polyval(redCoeffs,gal['IRAC1'])

    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfitBlueLim = np.polyval(cfg.blueCoeffs,gal['IRAC1'])

    if (gal['IZPH'][0]==1):
        zphWts = getZphWts(gal,fInfo)

    # othCuts must be applied to each condition!
    if (gal['IZPH'][0]==1):
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal['IRAC1']<cfg.irac1VFnt) \
                    & (gal['IRAC1']>0.0) \
                    & (zphWts>0.4) )
    else:
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal['IRAC1']<cfg.irac1VFnt) \
                    & (gal['IRAC1']>0.0) \
                    )


    hasSlit = (gal.inMask>0)

    innBri = np.sum( inner & bri & othCuts )
    outerBri = np.sum( outer & bri & othCuts )
    outskBri = np.sum( outsk & bri & othCuts )
    innFnt = np.sum( inner & fnt & othCuts )
    outerFnt = np.sum( outer & fnt & othCuts )
    outskFnt = np.sum( outsk & fnt & othCuts )
#    print 'len(innBri)=%s'%len(innBri)
    #print innBri,outerBri,innFnt,outerFnt,outskFnt
    innBriS = np.sum( inner & bri & othCuts & hasSlit)
    outerBriS = np.sum( outer & bri & othCuts & hasSlit)
    outskBriS = np.sum( outsk & bri & othCuts & hasSlit)
    innFntS = np.sum( inner & fnt & othCuts & hasSlit)
    outerFntS = np.sum( outer & fnt & othCuts & hasSlit)
    outskFntS = np.sum( outsk & fnt & othCuts & hasSlit)
    #print innBriS,outerBriS,innFntS,outerFntS,outskFntS

    print
    print 'Bright:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innBriS,innBri, outerBriS,outerBri, outskBriS,outskBri )
    print 'Faint:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innFntS,innFnt, outerFntS,outerFnt, outskFntS,outskFnt )

    return np.array((innBriS, outerBriS, outskBriS)),\
            np.array((innBri, outerBri, outskBri)),\
            np.array((innFntS, outerFntS, outskFntS)),\
            np.array((innFnt, outerFnt, outskFnt)),\


def evalMask(gal,fInfo,masknum=0,nTotMasks=3):
#    # evaluate mask:

    # if masknum==0, evaluate all objects inMask, otherwise only specified mask number:
    if (masknum):
#        reqBit = 2**(masknum-1)
        bitFlag = [np.binary_repr(y,nTotMasks) for y in gal.inMask.astype('int')]        
#        ok1 = np.where(gal.inMask==reqBit)[0]
        ok1 = [j for j,y in enumerate(bitFlag) if y[nTotMasks-masknum]=='1' ]
        #ok1 = [j for j,y in enumerate(maskFlag) if y[2]=='1']
    else:
        ok1 = np.where(gal.inMask>0)[0]
    
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal['ZMAG']
    # as fn of zmag:
#    print '%s fnt; %s bri; %s vFnt; %s vBri'\
#        %( len(inBin(mag,zFnt)),len(inBin(mag,zBri)),len(inBin(mag,zVFnt)),len(inBin(mag,zVBri)) )
    # as fn of distance:

    ###oka,okb,dis = spm.match_radec(mgal['RA'],mgal['DEC'],fInfo['cra'],fInfo['cdec'],1.0,nearest=False)

    dra = (gal['RA'] - fInfo['grpra'])/np.cos(np.deg2rad(gal['DEC']))
    ddec = gal['DEC'] - fInfo['grpdec']
    dis = np.sqrt(dra**2 + ddec**2)
    
    disMpc = dis*3600.0 / (1.0*1000.0*asperkpc(fInfo['grpz'])).value
    inn = np.where( disMpc <= 0.5)[0]
#    print dis
#    print disMpc
#    print '%s within 0.5 Mpc'%len(inn)
#    print

    # update inMask flag with binary to indicate which number(s) mask in
#    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def bitMask(x):
    # turn N array of floats (up to 2**(M-1))into
    # into N*M bitmask array 
    strBits = [binary_repr(y,3) for y in x.astype('int')]
    

def writeGMMPS(det,id,ra,dec,x_ccd,y_ccd,mag,priority,slitsize_x,slitsize_y,slittilt):
    # write _ODF1.cat file

    headerE2v="""
QueryResult

# Config entry for original catalog server:
serv_type: local
long_name: /Users/mbalogh/projects/GOGREEN/masks/SXDF76/OT/SXDF76_mask1_GMI_OTODF1.cat
short_name: SXDF76_mask1_GMI_OTODF1.cat
url: /Users/mbalogh/projects/GOGREEN/masks/SXDF76/OT/SXDF76_mask1_GMI_OTODF1.cat
symbol: {x_ccd y_ccd priority} {circle white {} {} {} {$priority == "3"}} {15 {}} : {x_ccd y_ccd priority} {triangle blue {} {} {} {$priority == "1"}} {15 {}} : {x_ccd y_ccd priority} {diamond cyan {} {} {} {$priority == "0"}} {20 {}} : {x_ccd y_ccd priority} {cross yellow {} {} {} {$priority == "X"}} {15 {}} : {x_ccd y_ccd priority} {square green {} {} {} {$priority == "2"}} {15 {}}
# Fits keywords
#fits FILTSPEC= 'OG515   '
#fits GRATING = 'R150    '
#fits WAVELENG=                  850
#fits SPEC_LEN= '2951.8299815688761'
#fits ANAMORPH= '1.0844667613950845'
#fits SPOCMODE= 'Off     '
#fits GMMPSVER= '0.402 F2-Support and Slit Placement Optimization Release'
#fits MASK_PA =                  0.0
#fits PERS_ODF= 'mbalogh '
#fits DATE_ODF= '2014-10-30T20:09:22'
#fits FILE_OT = 'SXDF76_mask1_GMI_OT.cat.fits'
#fits DET_IMG = 'UNKNOWN '
#fits DET_SPEC= 'EEV2037-06-03EEV8194-19-04EEV8261-07-04'
#fits RA      =             34.73612 / Right Ascension
#fits DEC     =              -5.3218 / Declination of Target
#fits EQUINOX =                2000.
#fits BDFILE  = 'tmp.bands'
#fits SHUFSIZE=                   43 / Shuffle distance in unbinned pixels
#fits FILENAME= '/Users/mbalogh/projects/GOGREEN/masks/SXDF76/OT/SXDF76_mask1_GMI_OT'
#fits FILE_OT = 'SXDF76_mask1_GMI_OTODF1.fits'
#fits TIME_ODF= '2014-10-30T16:09:22.000'
#fits PIXSCALE = 0.072700
# End fits keywords
# Curved slits
# End curved slits
# End config entry

ID	RA	DEC	x_ccd	y_ccd	specpos_x	specpos_y	slitpos_x	slitpos_y	slitsize_x	slitsize_y	slittilt	MAG	priority	slittype	wave_ccd
---	---	----	------	------	----------	----------	----------	----------	-----------	-----------	---------	----	---------	---------	---------
"""


    # Ham .cat file has not been tested yet, as I can't get even tested files to work with my build of gmmps!
    headerHam="""
QueryResult

# Config entry for original catalog server:
serv_type: local
long_name: /home/gilbank/Proj/goGreen/bitbucket/gogreen/python/tmp/SPT-0205_p1_mask2_GMI_OTODF1.cat
short_name: SPT-0205_p1_mask2_GMI_OTODF1.cat
url: /home/gilbank/Proj/goGreen/bitbucket/gogreen/python/tmp/SPT-0205_p1_mask2_GMI_OTODF1.cat
symbol: {x_ccd y_ccd priority} {circle white {} {} {} {$priority == "3"}} {15 {}} : {x_ccd y_ccd priority} {triangle blue {} {} {} {$priority == "1"}} {15 {}} : {x_ccd y_ccd priority} {diamond cyan {} {} {} {$priority == "0"}} {20 {}} : {x_ccd y_ccd priority} {cross yellow {} {} {} {$priority == "X"}} {15 {}} : {x_ccd y_ccd priority} {square green {} {} {} {$priority == "2"}} {15 {}}
#SpectrumLength: 1360
# Fits keywords
#fits INSTRUME = GMOS-S
#fits PIXSCALE = 0.160000
#fits DET_IMG  = BI5-36-4k-2,BI11-33-4k-1,BI12-34-4k-1
#fits DET_SPEC = BI5-36-4k-2,BI11-33-4k-1,BI12-34-4k-1
#fits BDFILE   = tmp.bands
#fits SHUFSIZE  = 38 / Shuffle distance in unbinned pixels
# End fits keywords
# End config entry

ID	RA	DEC	x_ccd	y_ccd	specpos_x	specpos_y	slitpos_x	slitpos_y	slitsize_x	slitsize_y	slittilt	MAG	priority	slittype	wave_ccd
------	---------	---------	---------	---------	-------	-------	------	------	---------	---------	--------	---	--------	--------	--------
"""
    if(det=='Ham'):
        print 'not yet implemented'
        print 'need to check fitting coeffs'
        return

        header=headerHam
    elif(det=='E2v'):
        coeff = np.array([ -7.78878447e-02,   6.03175079e+02])
        tcoeff = np.array([ 0.92211214,  241.15820665])
        header=headerE2v
    else:
        print 'unknown det %s'%det
        return


    xxx=open('test_ODF1.cat','w')
    for item in header:
        xxx.write(item)

    fmt='%s\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%.6f\t%s\t%s\t%.6f\n'
    # id,ra,dec,x_ccd,y_ccd,mag,priority,slitsize_x,slitsize_y,slittilt
    for i in np.arange(len(id)):
        #print fmt%(id,ra,dec,x_ccd,y_ccd,specpos_x,specpos_y,slitpos_x,slitpos_y,slitsize_x,slitsize_y,slittilt,mag,priority,slittype,wave_ccd)
        # need to sort out geometry between my code and gmmps expectation. binning?
        #
        slitpos_x = np.repeat(0.,len(id)) ; slitpos_y = np.repeat(0.,len(id)) ; specpos_y = np.repeat(0.,len(id))
        slittype=np.repeat('R',len(id))
        # specpos_x needs to be derived from gmosdefinespec
        # from defineGMOSspec()


        """
        In [67]: cfg.WLcoeff
        Out[67]: array([ -7.78880000e-02,   1.70010872e+02])
        In [68]: cfg.linDisp
        Out[68]: 0.35762352242772855
        # Why aren't these coeffs exactly same as ones below???
        """

        #specpos_x = x_ccd +np.polyval(cfg.WLcoeff,x_ccd) # measured anamorphic factor
        """
        In [44]: coeff = np.polyfit(dat['x_ccd'],dat['specpos_x'],1)
        In [45]: coeff
        Out[45]: array([ -7.78878447e-02,   6.03175079e+02])
        """
#        coeff = np.array([ -7.78878447e-02,   6.03175079e+02])
        specpos_x = np.polyval(coeff,x_ccd)

        # i think wave_ccd is the wavelength on the (left?) edge of the detector?
        # let's just set to 0 for now"
        #wave_ccd = x_ccd*0.0+100.0
        #print cfg.pxscale
#        lenLeftPix = (cfg.redCutA - cfg.cenwave)/cfg.linDisp
#        xlo = lenLeftPix * 2.0 # binning
#        xLeft = specpos_x-xlo
#        wave_ccd = xLeft
#        wave_ccd = np.polyval(cfg.WLcoeff,specpos_x)+x_ccd #*** this isn't quite right!
        """
        In [39]: coeff = np.polyfit(dat['x_ccd'],dat['wave_ccd'],1)
        In [40]: coeff
        Out[40]: array([   0.92211214,  241.15820665])
        """
        #tcoeff = np.array([ 0.92211214,  241.15820665])
        wave_ccd = np.polyval(tcoeff,x_ccd)
#       print wave_ccd

        # ra(h)-->deg
        # NO? pixel coords / 2

        # specpos_x is WRONG
        xxx.write(fmt%(id[i],15.*ra[i],dec[i],x_ccd[i],y_ccd[i],\
                specpos_x[i],specpos_y[i],slitpos_x[i],slitpos_y[i],slitsize_x[i],slitsize_y[i],\
                slittilt[i],mag[i],priority[i],slittype[i],wave_ccd[i]))

    xxx.close()


def BADwriteGMMPS(id,ra,dec,x_ccd,y_ccd,mag,priority,slitsize_x,slitsize_y,slittilt):
    #**** NOT WORKING ******
    # let's just try a FITS table of the type that's supposed to work:
    """
    The Object Table is a FITS table that contains the following mandatory columns:
    ID    Unique object id (integer)
    RA    RA in hours (real)
    DEC    Dec in degrees (real)
    x_ccd    X coordinate of object position in pixels (real)
    y_ccd    Y coordinate of object position in pixels (real)
    MAG    (Relative) magnitude of object (real)
    priority    Priority of object (char*1; "0/1/2/3/X")
    The order of the columns is important. The column names are case sensitive.

    Other columns may be present in the table and may help the user selecting objects for the mask. The user may find the following columns useful:
    slitsize_x	Slit width in arcsec (real)
    slitsize_y	Slit length in arcsec (real)
    slittilt	Slit position angle in degrees (real)
    slitpos_y	Slit position in the Y-direction (spatial) relative to the object position in arcsec (real)


    NOTE: If the image is binned, the Object Table MUST contain a header keyword "PIXSCALE" containing
    the pixel scale in arcsec/pixel.
    """

    #*** pixelscale in header not implemented yet!
    
    nn=len(id)
    out = np.array( [id,ra,dec,x_ccd,y_ccd,mag,priority], \
                      dtype=[('ID',np.int16), ('RA',np.float32), ('DEC',np.float32),('x_ccd',np.float32),\
                             ('y_ccd',np.float32), ('MAG', np.float32), ('priority',np.int16)] )
    """
    c1 = Column(name='ID', format='20A', array=id)
    c2 = Column(name='RA', format='E', array=ra)
    c3 = Column(name='DEC', format='E', array=dec)
    c4 = Column(name='x_ccd', format='E', array=x_ccd)
    c5 = Column(name='y_ccd', format='E', array=y_ccd)
    c6 = Column(name='MAG', format='E', array=mag)
    c7 = Column(name='priority', format='E', array=priority)
    """

    c1 = Column(name='ID', format='20A', array=id, disp='I9', unit='##')
    c2 = Column(name='RA', format='E', array=ra, disp='G15.7', unit='H')
    c3 = Column(name='DEC', format='E', array=dec, disp='G15.7',unit='deg')
    c4 = Column(name='x_ccd', format='E', array=x_ccd, disp='G15.7', unit='pixels')
    c5 = Column(name='y_ccd', format='E', array=y_ccd, disp='G15.7', unit='pixels')
    c6 = Column(name='MAG', format='E', array=mag, disp='G15.7', unit='magnitudes')
    c7 = Column(name='priority', format='1A', array=priority, disp='I1')

    c8 = Column(name='slitsize_x', format='E', array=slitsize_x, disp='G15.7', unit='arcsec')
    c9 = Column(name='slitsize_y', format='E', array=slitsize_y, disp='G15.7', unit='arcsec')
    c10 = Column(name='slittilt', format='E', array=slittilt, disp='G15.7',unit='deg')

#    coldefs = fits.ColDefs([c1, c2, c3, c4, c5, c6, c7])
    coldefs = fits.ColDefs([c1, c2, c3, c4, c5, c6, c7, c8, c9, c10])
    tbhdu = fits.BinTableHDU.from_columns(coldefs)
    tbhdu.writeto('testODF1.fits', clobber=True)



#    coldefs = fits.ColDefs([c1, c2, c3, c4, c5, c6, c7])
#    tbhdu = fits.BinTableHDU.from_columns(coldefs)
#    tbhdu.writeto('test.fits')
#    fits.writeto('testODF1.fits',out,clobber=True)


def verifyDes(maskname): # maskname should really be, e.g. bestSpARCS_0035in1.txt, to check the best design of each
    id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(maskname,unpack=True)
    plt.figure()
    plt.plot(xp,yp,'k,')
    sz=wt*100.+0.1
    plt.scatter(xp,yp,s=sz,color='r',alpha=0.3)

    outname=maskname.replace('in','out')
#    outname=maskname.replace('des','out')
    sra,sdec,sdec,sid = np.loadtxt(outname,unpack=True)

    ok1,ok2=match(id,sid)
    plt.scatter(xp[ok1],yp[ok1],s=sz[ok1],color='b',alpha=0.3)
    plt.show()



def updateinMask(gal,maskfile):
    mra,mdec,mpri,mID = np.loadtxt(maskfile, unpack=True)
    ok1,ok2=match(gal.ID,mID)
    gal.inMask[ok1]=1.0

## neat way of adding variables to an object!
#class fInfo():
#    pass


def readMaster(name,all=False):
    # read master table of cluster properties and return equivalent info to fInfo in early versions of code:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
#    masterfile = eval(cfg.masterfileExpr) # needs to be set before cfg is declared
    mdat = fits.getdata(masterfile)
    try:
        ok=np.where(mdat['cluster']==name)[0]
    except:
        print '%s not found in %s'%(name,masterfile)

    mdat = mdat[ok] # only keep line we want!
    if (all): return mdat
    fInfo = {}
    fInfo['grpra'] = mdat['RA_target_deg']
    fInfo['grpdec'] = mdat['DEC_target_deg']
    fInfo['grpz'] = mdat['Redshift']
    fInfo['cra'],fInfo['cdec'] = mdat['RA_gmos_deg'],mdat['DEC_gmos_deg']
    fInfo['rotang'] = mdat['PA_deg']
    fInfo['RSslope'],fInfo['RSintcpt'] = mdat['RSSlope'],mdat['RSintcpt']
    fInfo['Detector'] = mdat['Detector']
    print fInfo
    return fInfo



def mkHeader(name):
    # Make a comments header of all relevant pars used in creation of mask:

    d=vars(cfg)
    yyy=open('_tmphd.txt','w')
    for k,v in d.iteritems():
        if not k.startswith('__') :
            #print '# %s = %s'%(k,v)
            yyy.write('# %s = %s\n'%(k,v))
    # add explicit name of maskphot file:
    photfile = getPhotFilename(name)
    yyy.write('# %s\n'%photfile)
    yyy.close()



def procOnedsnFile(onedsnfile,gal):
    # for new catmatch files:
    onedsnfile = onedsnfile.replace('.onedsn','_onedsn_catmatch.fits')

    # Read onedsnfile and update weights.
    # Need to consider several things.
    # Fnt targets to repeat will have P=1 and mag>=23.5 (hardwired now)
    # Bri targets to exclude will have P=1 and mag<23.5
    # Filler targets to exclude will have P=3
    # ******************************************
    # Actually, we don't care. We just need Keep
    # ******************************************

    # Match and update inMask and noRpt flags accordingly
    donedat = fits.getdata(onedsnfile)
    #
    if(1):
        noRPT = np.where(donedat['KEEP']==0)[0]
        doneidx = [iii for iii,a in enumerate(gal['ID']) if a in donedat['ID']]
        NORPTdoneidx = [iii for iii,a in enumerate(gal['ID']) if a in donedat['ID'][noRPT]]
        # I could just set inMask=0 for the fnt objects already observed, but this won't stop them potentially being repeated in future.
        # Can I set to -ve numbers without problems??
        print doneidx
        gal.inMask[doneidx] = 1
        gal.inMask[NORPTdoneidx] = 0
        # need to then work out how to implement adding new fnt targets on all subsequent masks.
        gal.noRpt[NORPTdoneidx] = 1


    #***




def OLDprocOnedsnFile(onedsnfile,gal):
    # Read onedsnfile and update weights.
    # Need to consider several things.
    # Fnt targets to repeat will have P=1 and mag>=23.5 (hardwired now)
    # Bri targets to exclude will have P=1 and mag<23.5
    # Filler targets to exclude will have P=3
    # ******************************************
    # Actually, we don't care. We just need Keep
    # ******************************************

    # Match and update inMask and noRpt flags accordingly
    try:
        donedat = ascii.read(onedsnfile)
    except:
        print 'default format for %s did not work; trying ===='%onedsnfile
        ff = open(onedsnfile,'r')
        foo1 = ff.readline()
        foo2 = ff.readline()
        if foo2[0]=='=':
            print 'alternate format verified'
            # looks like alternate format of header\n======\ndata is okay
        else:
            print "DON'T KNOW HOW TO DEAL WITH THIS .onedsn FILE. CRASHING..."
            stop()
        ff.close()
        donedat = ascii.read(onedsnfile,header_start=0,data_start=2)
    #donedat = ascii.read('GS2014BLP001-06.fnt') #*** hardwired for now:
    # faint objects only for now:
    #*** also, need to use STOP flags for fnts

    #
    if(0): # I think this was working, but MLB's mags were wrong
        noRPT = np.where(donedat['Keep']==0)[0]
        doneidx = [iii for iii,a in enumerate(gal['ID']) if a in donedat['ID']]
        NORPTdoneidx = [iii for iii,a in enumerate(gal['ID']) if a in donedat['ID'][noRPT]]
        # I could just set inMask=0 for the fnt objects already observed, but this won't stop them potentially being repeated in future.
        # Can I set to -ve numbers without problems??
        print doneidx
        gal.inMask[doneidx] = 1
        gal.inMask[NORPTdoneidx] = 0
        # need to then work out how to implement adding new fnt targets on all subsequent masks.
        gal.noRpt[NORPTdoneidx] = 1
    else:
        print "!!!force repeating all faints regardless of spec s/n measured!!!"
        # let's just test method works. Force all fnts to be repeated and brights to not:
        # MLB's mags are wrong. Get from cat
        for iii in range(len(donedat)):
            ook = np.where(gal['ID']==donedat['ID'][iii])[0]
            if (gal['ZMAG'][ook]>=23.5):
                gal.noRpt[ook] = 0
                gal.inMask[ook] = 1
            elif (gal['ZMAG'][ook]<23.5):
                gal.noRpt[ook] = 1
                gal.inMask[ook] = 0
            else:
                print "don't know how i got here!"
                stop()


    #***



def prepMask(name,nMasks=5,isilent=True):
#if(1):    
    
    global redSeqCoeffs,redCoeffs,redLimCoeffs

    # Check for already-observed masks:
    mdat = readMaster(name,all=True)
    print cfg.allocateSchemes
    print 'Checking for pre-existing masks'
    aMaskNums = np.zeros(0)#[]
    aDoneMaskNums= np.zeros(0)#[]
    for i in np.arange(nMasks)+1:
        keymask = 'mask%sobserved'%i
        # Need to get must-haves (faints) from .fnt (and already-observed bris from previous mask):


        if (mdat[keymask]==0):
            aMaskNums = np.append(aMaskNums,i)
        else:
            aDoneMaskNums = np.append(aDoneMaskNums,i)
            print 'mask %s already observed! skipping..'%i
    aMaskNums = aMaskNums.astype('int')
    aDoneMaskNums = aDoneMaskNums.astype('int')
    #    aMaskNums = np.arange(nMasks) #****
    print aMaskNums
    aMaskNums = aMaskNums-1 # 0-based indexxing needed!
    aDoneMaskNums = aDoneMaskNums-1 # 0-based indexxing needed!

    # Update allocation scheme to fill with faints after last already-observed mask:
    #**** Need some checking to not overwrite band shuffle, etc.

    # check for mask5 (assumed band-shuffled):
    ok=np.where(aDoneMaskNums>=4)[0] # assuming band-shuffle mask >=5 (=4)
    if(len(ok)>0):
        print 'band-shuffled mask found: ',aDoneMaskNums[ok]+1
        #*** need to set NORPT for brights:

#    if (len(aDoneMaskNums)>0):
    if ( (len(aDoneMaskNums)>0)): #check for any non-band-shuffled masks
        if (np.min(aDoneMaskNums)<4):
            ok=np.where(aDoneMaskNums<4)[0] # assuming band-shuffle mask >=5 (=4)
            nextNew = aDoneMaskNums[ok].max()+1
            if(cfg.allocateSchemes[nextNew]=='rptfnt'):
                print 'Updating allocation scheme based on previous masks:'
                cfg.allocateSchemes[nextNew]='fillfaint'
    print cfg.allocateSchemes


    #gal = fits.getdata('%sgals.fits'%name)
    # store array of inMask flags for each iteration, so we can reconstruct best mask:
    #ainMask = np.zeros((len(gal),nMultiIter))
    
    # Clean up old results files:
    os.system('rm -f %s*in*txt'%name)
    os.system('rm -f %s*out*txt'%name)
    os.system('rm -f best%s*txt'%name)
    os.system('rm -f %s*reg'%name)


    # ---- check catalogue. If it doesn't contain AQSTAR_HANDCHECK - bail
    photfile = getPhotFilename(name)
    gal = fits.getdata(photfile) # NOTE: this now includes all objects, including setup stars****

    try:
        foo = gal['AQSTAR_HANDCHECK']
    except:
        print photfile
        print 'AQSTAR_HANDCHECK not present in catalogue.\nABORTING\nPlease verify some acquisition stars and update.\n\n'
        sys.exit(1)
    # ----

    # ---- New Jan 2016, check if spec. zs exists from external catalogues. If so, update in current catalogue:
    zspecfile = getzSpecFilename(name)
    try:
        extzsdat = fits.getdata(zspecfile)
        izs = True
    except:
        izs = False
      
    gal=recf.append_fields(gal,'SPZ_QUAL',0.*gal['ZS'],asrecarray=True,usemask=False)
    if(izs):
        print 'Matching external zs from %s'%zspecfile
        # match to phot cat, based on ID number, and update in gal['ZS']:
        # match gal['ID'] to extzsdat['GG_ID']
#        mtchIndx = [ii for ii,xx in enumerate(gal['ID']) if xx in extzsdat['GG_ID']]
        mtchIndx=-np.ones(len(extzsdat)).astype('int64')
        for ii in range(len(extzsdat)):
            ok = np.where(gal['ID']==extzsdat['GG_ID'][ii])[0]
            if len(ok)<>1:
                print 'error on matching zspecs! GG_ID = %s'%extzsdat['GG_ID'][ii]
            mtchIndx[ii] = ok

###     gal['ZS'][mtchIndx] = extzsdat['GG_ID']
        gal['ZS'][mtchIndx] = extzsdat['SPZ'] #*** currently no cut on spec quality flag***  
        gal['SPZ_QUAL'][mtchIndx] = extzsdat['QUAL']
        #print extzsdat['SPZ']
        #print np.max(gal['ZS'])
        # NOTE: need to do this again inside the iteration loop, when cat is re-read each time!
    else:
        pass
    # ----


    iplot=1
    bestScore = 0 # (let's do this old-fashioned way and ignore bitFlags)
#    print ,
    for kk in range(cfg.nMultiIter): # run this many iterations of the full set of masks
#        print 'ITER(multi) =%s'%kk
#        print 'ITER(multi) =%s\r'%kk,
###        print 'ITER(multi) ={}\r'.format(kk),
        sys.stdout.write('\r'+'ITER(multi) =%s'%kk)
        sys.stdout.flush()
        # force silent:
        if (isilent):
            sys.stdout = open(os.devnull, "w") # suppress printing for each mask
    
    #    fInfo = fits.getdata('%sinfo.fits'%name) ; fInfo=fInfo[0] # convert to scalars
        fInfo = readMaster(name)
        #*** ZZZ need to change to Adam's new FITS catalogue
        ##photfile = '%s/Catalogues/%s/%s_phot.fits'%(os.getenv('GOGREEN_data'),name,name)
###        photfile = eval(cfg.photfileExpr)
        photfile = getPhotFilename(name)
#        gal = fits.getdata('%sgals.fits'%name)
        gal = fits.getdata(photfile) # NOTE: this now includes all objects, including setup stars****

        #-- need to re-match in ZS, if applicable
        gal=recf.append_fields(gal,'SPZ_QUAL',0.*gal['ZS'],asrecarray=True,usemask=False)
        if(izs):
            gal['ZS'][mtchIndx] = extzsdat['SPZ']
        #MLB:
            gal['SPZ_QUAL'][mtchIndx] = extzsdat['QUAL']

        #--

        """
        try:
            stars = fits.getdata('%ssetupCands.fits'%name)
#            if(glob('%s.star*'%name)):

            if(len(stars)>4):
                # take two highest and lowest in y:
                sss=np.argsort(stars.yp)
                suse = np.hstack((sss[0:2],sss[-2::]))
                stars = stars[suse]

#                stars=stars[0:4] #*** kludge
#                useStars = np.loadtxt('%s.stars'%name)
#                sok1,sok2 = match(stars.ID,useStars)i
#                stars = stars[sok1]
        except:
        stars = None
        """
        print
##        stars = np.where(gal['setupStar']==1)[0]
        stars = np.where(gal['AQSTAR_HANDCHECK']==1)[0]
        print gal['AQSTAR_HANDCHECK']

        if (len(stars)<3):
            stars = None
        #****

        redSeqCoeffs = np.array((fInfo['RSslope'][0],fInfo['RSintcpt'][0]))
        redCoeffs = redSeqCoeffs+np.array((0,cfg.dRed))
        redLimCoeffs = redSeqCoeffs+np.array((0,cfg.dRedLim))
    
        
    
        if (cfg.NSFlag):
            print "applying shuffle offset (%.2f pixels) to galaxy positions"%cfg.shuffleOffPix
            print gal.field
            gal['YP'] = gal['YP'] + cfg.shuffleOffPix
            
    
        print
        print '---------- Field info -----------'
        print 'Mask centre     ra,dec = %.6f, %.6f; rotang = %.2f'%(fInfo['cra'],fInfo['cdec'],fInfo['rotang'])
        if (fInfo['grpra']<0.0):
            print '*** using field centre as clus centre ***'
            fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
        print 'Cluster centre: ra,dec = %.6f, %.6f; redshift = %.4f'%(fInfo['grpra'],fInfo['grpdec'],fInfo['grpz'])
        print '---------------------------------'
        print
        
        # CAREFUL: this must only be reset at the start of 
        inMask = np.zeros(len(gal)) # flag whether gal has been assigned to a mask or not:
        prob = np.zeros(len(gal)) # probability of being included in mask
        
        noRpt = np.zeros(len(gal))

        # calculate distances:
        disMpc = calcdisMpc(fInfo,gal)
        
        # add all these other arrays to gal array, to make sure everything stays in sync:
        gal = recf.append_fields(gal,('inMask','prob','disMpc','noRpt'),(inMask,prob,disMpc,noRpt),asrecarray=True,usemask=False)
        # trim to field:
        #print gal['XP']
        inFoV = np.where( (gal['XP'] > cfg.xminField) & (gal['XP'] <= cfg.xmaxField) &  (gal['YP'] > cfg.yminField) & (gal['YP'] <= cfg.ymaxField))[0]
        gal = gal[inFoV]

        #** for debugging:
        fits.writeto('tgal.fits',gal, clobber=True)


        print '%s gals in full catalogue within imaging field'%(len(gal))
        print 
#        if (kk==0):
#            plotClus(gal,fInfo)
    
        #**
        # Need to update inMask flags based on already observed masks
        if(len(aDoneMaskNums)>0):
            print 'updating catalogues based on observed slits...'
            tmaskdir = getPhotFilename(name)
            maskdir = string.join(tmaskdir.split('/')[0:-1],'/')+'/'
            for mskNum in aDoneMaskNums:
                mdat = readMaster(name,all=True)
                keyname = 'mask%scut'%(mskNum+1)
                onedsnfile = maskdir+mdat[keyname][0]+'.onedsn'

                procOnedsnFile(onedsnfile,gal)

            # Need to add new Fnt must-haves if this is the last mask already done***

        else:
            'no already-observed mask found'
        #**


###        for jj in range(nMasks):
        for jj in aMaskNums:
        
            print 'Mask %s:'%(jj+1) 
            print   
            # Mask 1 - allocate bright targets first:
            #genWeights(gal,fInfo, scheme='bright')
            genWeights(gal,fInfo, scheme=cfg.allocateSchemes[jj])
            
            
            if(cfg.allocateSchemes[jj][0:4]=='band'):
                NSmode='band'
            else:
                NSmode='micro'

            print 'stars: ',stars
#            stop()
    
            # run design:
            inname = name+'in%s.txt'%(jj+1)
            writeInput(inname,gal,fInfo,stars=stars)
            
#            print 'nSI ',cfg.nSingleIter
            MD.desmask( fInfo['cra'],fInfo['cdec'],fInfo['rotang'],inname, det=cfg.det, \
                        niter=cfg.nSingleIter ,iplot=cfg.plotFlag, sortScheme=cfg.sortScheme, NSmode=NSmode )
#            MD.desmask( fInfo['cra'],fInfo['cdec'],fInfo['rotang'],inname, cfg,iplot=cfg.plotFlag, sortScheme=cfg.sortScheme, NSmode=NSmode )
            
            # evaluate design:
            maskname = inname.replace('in','out')
            mra,mdec,mpri,mID = np.loadtxt(maskname, unpack=True)
            #ok1,ok2 = match(gal.ID,mID)
            tagMask(gal,fInfo,mID,masknum=(jj+1))
    #        evalMask(gal,fInfo,masknum=(jj+1),nTotMasks=nMasks)
            ok1 = np.where(gal.inMask==1)[0]
            
            updateinMask(gal,maskname)
            
            print '%s: %s slits; total weight=%.2f'%(maskname,len(mra),np.sum(mpri))
            print
            
            os.system('cp -f ds9.reg %s'%(maskname.replace('.txt','.reg')))
            
            (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)       
        #    writeGMMPS(gal.ID[ok1],gal['RA'][ok1],gal['DEC'][ok1],gal['XP'][ok1],gal['YP'][ok1],gal['ZMAG'][ok1],np.ones(len(ok1)))
        
        # evaluate this set of masks:    
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
        """
        print 'HERE!'
        
        print np.sum(nFntS[0:2])
        print np.sum(nBriS[0:2])
        print
        print (nBriS,nBri, nFntS, nFnt)
        """
        
        # *** FINAL SCORE ***
    ###    Score = np.copy(np.sum(nFntS[0:2])+0.1*(np.sum(nBriS[0:2])))
        if (np.sum(nFntS[0:2])<11):
    #        Score=0.0
            Score = np.copy(0.5*np.sum(nFntS[0:2])+0.05*(np.sum(nBriS[0:2])))
        else:
            Score = np.copy(0.2*np.sum(nFntS[0:2])+1.0*(np.sum(nBriS[0:2])))

        print 'Score: %.2f'%Score

        if (Score > bestScore):
            bestScore = np.copy(Score)
###            for i in range(nMasks):
            for i in aMaskNums:
                ###print 'new bestScore %s'%bestScore
                ###os.system('ls -lrt | tail')

                inname = name+'in%s.txt'%(i+1)
                os.system('cp -f %s best%s'%(inname,inname))
                maskname = inname.replace('in','out')
                os.system('cp -f %s best%s'%(maskname,maskname))
                regname = maskname.replace('.txt','.reg')
                os.system('cp -f %s best%s'%(regname,regname))
                gmmpsname = inname.replace('in','gmmps')
                os.system('cp -f %s best%s'%(gmmpsname,gmmpsname))

    
        
        
        # un-mute:
        sys.stdout = sys.__stdout__
        
    
    
    # read in masks to check:
    for i in aMaskNums:
        maskname = 'best%sout%s.txt'%(name,i+1)
        mra,mdec,mpri,mid = np.loadtxt(maskname,unpack=True)
        gal.inMask[:]=0.0
        ok1,ok2 = match(gal.ID,mid)
        gal.inMask[ok1]=1.0
        
        print
        print '%s:'%maskname
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
    
    # run again without setting to zero in between to evaluate total set:
    
    print
    print
    print "Best Mask Set (all %s masks, not just the filename below!):"%nMasks
    for i in aMaskNums:
        maskname = 'best%sout%s.txt'%(name,i+1)
        mra,mdec,mpri,mid = np.loadtxt(maskname,unpack=True)
    #    gal.inMask[:]=0.0
        ok1,ok2 = match(gal.ID,mid)
        gal.inMask[ok1]=1.0
        
        print
        print '%s:'%maskname
        if(i<nMasks-1): 
            sys.stdout = open(os.devnull, "w") # suppress printing for each mask
        else:
            sys.stdout = sys.__stdout__
        (nBriS,nBri, nFntS, nFnt) = evalCat(gal,fInfo)
    
        
    # add header:
    mkHeader(name)
##    for i in np.arange(nMasks):
    for i in aMaskNums:
        maskname = 'best%sout%s.txt'%(name,i+1)
        print 'cat _tmphd.txt %s > _tmp.txt'%maskname
        os.system('cat _tmphd.txt %s > _tmp.txt'%maskname)
        os.system('mv -f _tmp.txt %s'%maskname)

    
    
    
    print
    print
    print 'NOTE: galaxy positions have been offset in y by %.2f arcsec \nto allow for shuffle buffer. VERIFY NOD DISTANCE AND DIRECTION'%(cfg.shuffleOffPix*cfg.pxscale)
    print ''
    
    
    
    # Simple checking of masks in best set:
    
###    for ii in range(nMasks):
    for ii in aMaskNums:
        #bestinfile = 'best%sin%s.txt'%(name,ii+1)
        #bestoutfile = 'best%sout%s.txt'%(name,ii+1)
    
        #id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(bestinfile,unpack=True)
        #mra,mdec,mpri,mID = np.loadtxt(bestoutfile, unpack=True)

        bestgmmpsfile = 'best%sgmmps%s.txt'%(name,ii+1)
        id,rah,dec,xccd,yccd,mag,pri,slen,swid,stilt = np.loadtxt(bestgmmpsfile,unpack=True)

        """
        # write to GMMPS ODF format:
        writeGMMPS(cfg.det,id,rah,dec,xccd,yccd,mag,pri,swid,slen,stilt) # note: reordering of width (slitsize_x) and len (...y)
        #odfname = bestgmmpsfile.replace('.txt','ODF1.fits')
        #os.system('mv -f testODF1.fits %s'%odfname)
        odfname = bestgmmpsfile.replace('.txt','ODF1.cat')
        os.system('mv -f test_ODF1.cat %s'%odfname)
        """

        # clean up intermediate files to avoid confusion later!
        os.system('rm -f %s*in*txt'%name)
        os.system('rm -f %s*out*txt'%name)
        os.system('rm -f %s*reg'%name)
        os.system('rm -f %s*gmmps*txt'%name)



        
def runMasks(name,det='Ham',nMasks=5, isilent=True):
        # still need to make this global??***


    global cfg
#    isilent=1
#    nMasks=5
    #name='SpARCS_0035'
    #name='SPT-0205'
    #name='CDFS-41'
#    name='SPT-0546'
    
    #nMasks=3
    #name='COS-221'
    #name='SXDF64'
    #name='SXDF76'
    #name='SXDF87'

    fInfo = readMaster(name)
    #    det = fInfo['Det']
    # Hmm, it looks like this can't be decided at runtime. Need to call prepMask with appropriate det already set! ***
    #fInfo['grpra'] = mdat['RA_target_deg']

    #*** reload()?
    if det=='Ham':
        import MDconfigHam as cfg
    elif det=='E2v':
        import MDconfigE2v as cfg
    
#    reload(cfg)

    prepMask(name,nMasks=nMasks,isilent=isilent)
##
#    return
##

    # Check for already-observed masks:
    # need to do this again, since previously done inside prepMask
    mdat = readMaster(name,all=True)
    #print 'Checking for pre-existing masks'
    aMaskNums = np.zeros(0)#[]
    for i in np.arange(nMasks)+1:
        keymask = 'mask%sobserved'%i
        if (mdat[keymask]==0):
            aMaskNums = np.append(aMaskNums,i)
        else:
            print
    #        print 'mask %s already observed! skipping..'%i
    aMaskNums = aMaskNums.astype('int')
    #    aMaskNums = np.arange(nMasks) #****
    aMaskNums = aMaskNums-1 # 0-based indexxing needed!

    print aMaskNums

###    for i in np.arange(nMasks)+1:
    for i in aMaskNums+1:
        if(cfg.allocateSchemes[i-1][0:4]=='band'):
            NSmode='band'
        else:
            NSmode='micro'
        inname = 'best%sin%s.txt'%(name,i)
        plotmask('best%sin%s.txt'%(name,i),name, det=det, fInfo=fInfo, NSmode=NSmode)

    plt.close('all') # save memory
    if(1): # let's skip adding fillers now
###        addFillers(name,nMasks=nMasks,det=det,fInfo=fInfo,isilent=True)
            addFillers(name,aMaskNums=aMaskNums,det=det,fInfo=fInfo,isilent=True)
###        for i in np.arange(nMasks)+1:
    for i in aMaskNums+1:
            if(cfg.allocateSchemes[i-1][0:4]=='band'):
                NSmode='band'
            else:
                NSmode='micro'
            inname = 'best%sin%s.txt'%(name,i)
            plotmask('best%sin%s.txt'%(name,i),name, det=det, fInfo=fInfo, NSmode=NSmode)

            # Make OT FITS table:
            # only use this routine for masks designed from public cats (not pre-images)
            # if cfg.det=='E2v':
            #if cfg.preim=='N':
            #    cats2OT(name,i)
                #elif cfg.det=='Ham':
            #else:
            #    print 'preim2OT not implemented yet'
#            else:
#                print "don't know which detector we're using! Not attempting to make OT FITS"

    plt.close('all') # save memory




        
        
"""    
def doall():
    clusnames = ['SpARCS_0035','SPT-0205',]
    for name in clusnames:
        prepMask(name,nMasks=5)
        
    grpnames = ['COS-221','SXDF64','SXDF76','SXDF87']
    for name in grpnames:
        prepMask(name,nMasks=3)
"""    
    
    
    #name='SPT-0205'
#name='SpARCS_0035'
#name='COS-221'
#name='SXDF64'
#name='SXDF76'

if __name__ == "__main__":
    #    name='SPT-0546' ; nMasks=5 ; det='Ham' ; isilent=True

    #--
    global cfg
    #--

    name = sys.argv[1]
#    name='SXDF76' ; nMasks=3 #; det='E2v' ;
#    isilent=True
    isilent=False

    foo = readMaster(name)
    print foo
    det = foo['Detector']

    # I've updated this manually for the clusters we did on 1st run
    if( (name[0:4]=='SXDF') | (name[0:3]=='COS') ):
###        det = 'E2v'
        nMasks = 3
    else:
###        det = 'Ham'
        nMasks = 5
    #

    # assume 3 masks for E2v, 5 masks for Ham unless overridden
    try:
        nMasks = int(sys.argv[2])
    except:
        pass
    runMasks(name, det=det, nMasks=nMasks, isilent=isilent)
    


    
