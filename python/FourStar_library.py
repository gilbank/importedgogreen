import os
import numpy
from pyraf import iraf
import subprocess as sp
import sys
from astropy.io import fits
from astropy import wcs

trimSection={'c1':'[6:1980,6:1990]','c2':'[6:2000,75:2041]','c3':'[55:2042,55:2041]','c4':'[90:2020,6:1990]'}

def buildDictionary(file):
    file=open(file,'r')
    lines=file.readlines()
    file.close()
    parameters=[]

    for line in lines:
        if line[0]!='#':
            parameters.append(line.split())

    return dict(parameters)

def setup(setupDir):
    # Copy accross the setup files for SExtraxtor
    if os.path.isdir(setupDir):
        cmd='cp '+setupDir+'/* .'
        retcode=sp.call(cmd,shell=True)
    return

def parseCDC(input,output,filter):
    lines=input.split('\n')
    file=open(output,'w')
    ra=[]
    dec=[]
    mag=[]
    magerr=[]
    flag=[]
    magIndex={'J':3,'J1':3,'Ks':5}
    flagIndex={'J':0,'J1':0,'Ks':2}
    file.write('ID\tRA\tDEC\tmag\tmag_err\tQfl\n')
    for line in lines:
        if len(line) > 0:
            if line[0]!='#':
                entries=line.split('|')
                TwoMASSflag=entries[6].split()[0][flagIndex[filter]:flagIndex[filter]+1]
                if TwoMASSflag in ['A'] and float(entries[magIndex[filter]].split()[1]) < 0.1:
                    ra.append(float(entries[0].split()[0]))
                    dec.append(float(entries[0].split()[1]))
                    mag.append(float(entries[magIndex[filter]].split()[0]))
                    if entries[5].split()[1]=='---':
                        magerr.append(-9.99)
                    else:
                        magerr.append(float(entries[magIndex[filter]].split()[1]))
                    flag.append(TwoMASSflag)
                    file.write('%s\t%9.5f\t%9.5f\t%s\t%s\t%s\n' % (entries[2],ra[-1],dec[-1],mag[-1],magerr[-1],flag[-1]))

    
    file.close()
    stars=numpy.zeros(len(ra),dtype=[('id','int'),('ra','float'),('dec','float'),('mag','float'),('magerr','float'),('flag','a3')])
    stars['ra']=ra
    stars['dec']=dec
    stars['mag']=mag
    stars['magerr']=magerr
    stars['flag']=flag
    stars['id']=numpy.arange(1,len(ra)+1)

    return stars

def makeCat(image,weight,config,params=None,test=False):

    if test:
        catName=image.replace('.fits','_test.cat')
        flag='-CATALOG_TYPE ASCII_HEAD'
    else:
        catName=image.replace('.fits','.cat')
        flag=''

    catName=os.path.split(catName)[1]

    if params!=None:
        for key in params.keys():
            flag=flag+' -'+key+' '+params[key]

    print flag

    if weight==None:
        cmd='sex -c '+config+' '+flag+' -CATALOG_NAME '+catName+' ' + image
    else:
        cmd='sex -c '+config+' '+flag+' -CATALOG_NAME '+catName+' -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE '+ weight + ' ' + image

    print cmd
    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "SExtractor was terminated by signal", -retcode
        else:
            print >>sys.stderr, "SExtractor returned", retcode
    except OSError, e:
        print >>sys.stderr, "SExtractor failed:", e

    return catName
        

def unlearn(tasks):
    for task in tasks:
        iraf.unlearn(task)
    return

def filelist(params,options):
    #print out some fundamental info about each image after they have
    #been preprocessed. 
    
    # Create a list of FITS files
    imageList=numpy.sort([image for image in os.listdir(params['reduced']) if '.fits' == os.path.splitext(image)[1]])
    #only get the name for c1 assuming that the header for that is the
    #same as for c2,c3, and c4 - I checked this for one image
    prefixes=numpy.array([image[0:8] for image in imageList])
    uniquePrefix=numpy.unique(prefixes)
    #print uniquePrefix
    #image = numpy.array(upref + '_c1.fits' for upref in uniquePrefix)

    outfile = params['reduced'] + 'file_info.list'
    file=open(outfile,'w')
    file.write('#image\tOBJECT\tOBSTYPE\tEXPTIME\tFILT\n' )

    # Combine images with the same prefix
    for prefix in uniquePrefix:
        image = prefix + '_c4.fits'
        #print image

        obj = fits.getval(params['reduced']+image,'OBJECT').strip()
        obstype = fits.getval(params['reduced']+image,'OBSTYPE').strip()
        exptime = fits.getval(params['reduced']+image,'EXPTIME')
        filt = fits.getval(params['reduced']+image,'FILTER').strip()
        file.write('%s\t%s\t%s\t%f\t%s\n' % (prefix,obj,obstype,exptime,filt))
    file.close()

    return

def preProcess(params,options):
    # We assume that everything with the same sequence
    # and chip number can be coadded
    unlearn(['imcombine'])
    
    # Create a list of FITS files
    imageList=numpy.sort([image for image in os.listdir(params['raw']) if '.fits' == os.path.splitext(image)[1]])
    prefixes=numpy.array([image[0:8]+'_'+image[12:14] for image in imageList])
    uniquePrefix=numpy.unique(prefixes)
    
    # Combine images with the same prefix
    for prefix in uniquePrefix:

        file=open('imcombine.list','w')
        for image in imageList[prefixes==prefix]:
            file.write('%s%s\n' % (params['raw'],image))
        file.close()
        output=params['reduced']+prefix+'.fits'

        # If the image is a dark then combine with pixel rejection
        if fits.getval(params['raw']+image,'FILTER').strip() == 'dark':
            reject='minmax'
            nlow=1
            nhigh=1
        else:
            reject='none'
            nlow=0
            nhigh=0

        if not os.path.exists(output):
            iraf.imcom(input='@imcombine.list',output=output,combine='average',reject=reject,scale='none',zero='none',nlow=nlow,nhigh=nhigh)
        else:
            print 'Skipping %s' % (prefix)
    return

def createDark(params,options):
    # Not yet implemented
    return

def createTwilightFlat(params,options):

    saturation = 40000
    minimumNumber = 10
    # Unlearn the various IRAF tasks
    unlearn(['imstat','imcom','imarith','imdel','imcopy'])

    # Compute stats. We do this in one chip, aan dassume that the
    # results in the other three will be similar
    file=open(options.twilightFlat)
    filenames=file.readlines()
    file.close()

    #
    stats=[]
    for file in filenames:
        stats.append(float(iraf.imstat(images=file.strip(),fields='midpt',format='no',Stdout=1)[0]))

    # Cull saturated files

    filenames2=[]
    stats2=[]
    for i in range(len(stats)):
        if stats[i] < saturation:
            filenames2.append(filenames[i].strip())
            stats2.append(stats[i])
        else:
            print "Rejecting frame %s as too bright" % (filenames[i].strip())
            
    if len(filenames2) < minimumNumber:
        print 'Not enough frames to create a flat'
        return
    else:
        nhigh=numpy.minimum(5, len(filenames2) / 2 - 4)
    
    # Determine the filter from the first file
    filter=fits.getval(filenames2[0],'FILTER',0).strip()
    
    
    # Split into two groups (high counts and low counts) - assumes monatonically increasing or decreasing counts
    # We need a way to find flats that are affected by clouds
    split = int(len(filenames2)/2.)

    for chip in ['c1','c2','c3','c4']:
#    for chip in ['c2','c3','c4']:
        tempfile1=open('temp1','w')
        tempfile2=open('temp2','w')
        j=0
        for filename in filenames2:
            if j < split:
                tempfile1.write(filename.replace('c1',chip)+'\n')
            else:
                tempfile2.write(filename.replace('c1',chip)+'\n')
            j=j+1

        tempfile1.close()
        tempfile2.close()
        print 'There are '+ `len(filenames2)`+' files'
        print 'The median of the first and last frames ',stats2[0],stats2[len(stats2)-1]
        # Add this to the header


        iraf.imcom(input='@temp1',output='flat_high',combine='average',reject='minmax',scale='median',zero='none',statsec='[200:1800,200:1800]',nlow='1',nhigh=nhigh)
        iraf.imcom(input='@temp2',output='flat_low',combine='average',reject='minmax',scale='median',zero='none',statsec='[200:1800,200:1800]',nlow='1',nhigh=nhigh)

        iraf.imar(operand1='flat_high',op='-',operand2='flat_low',result='flat')
        iraf.hedit(images='flat',fields='FLATSTAT',value=`stats2[0]`+','+`stats2[len(stats2)-1]`,add='yes',verify='no')
        # Add a warning if the ratio is too low. ToDo
        median=float(iraf.imstat(images='flat',fields='midpt',format='no',Stdout=1)[0])
        # A negative value means that the counts were increasing, i.e. a morning flat
        output='flat_%s_%s' % (filter,chip)
        # Test for output filename. If it exists, append _1 to name. ToDo
        iraf.imar(operand1='flat',op='/',operand2=median,result=output)
        print 'Median Value:',median
        iraf.hedit(images=output,fields="MEDIAN",value=median,add='yes',verify='no')
        iraf.imdel(images='flat_low,flat_high,flat',verify='no')

    return

def createSciFlat(params,options):

    # Unlearn the various IRAF tasks
    unlearn(['imcom','imar','imdel'])

    file=open(options.sciFlat)
    filenames=file.readlines()
    file.close()

    # Determine the filter from the first file
    filter=fits.getval(filenames[0].strip(),'FILTER',0).strip()
    
    
    for chip in ['c1','c2','c3','c4']:
        tempfile1=open('temp1','w')
        for filename in filenames:
            tempfile1.write(filename.strip().replace('c1.fits',chip+"_red.fits")+'\n')
        tempfile1.close()

        print 'There are '+ `len(filenames)`+' files'
        
        if options.output==None:
            output='flat_%s_%s' % (filter,chip)
        else:
            output='%s_%s' % (options.output,chip)

        iraf.imcom(input='@temp1',output='flat',combine='average',reject='minmax',scale='median',zero='none',statsec='[200:1800,200:1800]',nlow='5',nhigh='5')

        median=float(iraf.imstat(images='flat',fields='midpt',format='no',Stdout=1)[0])
        iraf.imar(operand1='flat',op='/',operand2=median,result=output)
        print 'Median Value:',median
        iraf.hedit(images=output,fields="MEDIAN",value=median,add='yes',verify='no')
        iraf.imdel(images='flat',verify='no')

    return

def createBadPixelMasks(params,options):
    unlearn(['imexpr','imcopy'])
    
    for chip in ['c1','c2','c3','c4']:
        input=params['flat'].replace('c1',chip)
        output='bad_%s.fits' % (chip)
        iraf.imexpr(expr="a>2 || a<0.5 ? 0 : 1",output=output,a=input)
        iraf.imcopy(input=output,output=output.replace('fits','pl'))
    return

def findDark(image,params,options):
    # We find the file that has the same integration
    for f in os.listdir(params['calib']):
        if ".fits" in f:
            calibFile=params['calib']+f
            filt=fits.getval(calibFile,"FILTER")
            exptime=fits.getval(calibFile,"EXPTIME")
            if filt=="dark" and fits.getval(image,"EXPTIME")==exptime:
                return calibFile

    return None

def findFlat(image,params,options):
    # We find the file that has the same filter
    for f in os.listdir(params['calib']):
        if "flat" in f and ".fits" in f:
            calibFile=params['calib']+f
            filt=fits.getval(calibFile,"FILTER")
            if filt==fits.getval(image,"FILTER"):
                return calibFile

    return None

def detrend(params,options):
    # Unlearn the various IRAF tasks
    unlearn(['hedit','imcopy'])
    
    file=open(options.detrend)
    images=[line.strip() for line in file.readlines()]
    file.close()
    
    for image in images:
        # Find the dark
        dark=findDark(image,params,options)
        flat=findFlat(image,params,options)
        for chip in ['c1','c2','c3','c4']:
            input=image.replace('c1',chip)
            output=input.replace('.fits','_red.fits')
            # Determine if a dark and or flat need to be applied
            # This can occur if one creates science flats, as one has to subtract the dark, but not flat field
            applyDark=False
            try:
                fits.getval(output,'DARK',0)
            except:
                if dark!=None:
                    applyDark=True
                    d=dark.replace('c1',chip)

            applyFlat=False
            try:
                fits.getval(output,'FLAT',0)
            except:
                if flat!=None:
                    applyFlat=True
                    f=flat.replace('c1',chip)
                
            if options.flat!=None:
                # Use the specified flat
                f=params['calib']+options.flat.replace('c1',chip)


            if applyDark:
                print "Using dark %s" % d
                iraf.imar(operand1=input,op='-',operand2=d,result=output)
                iraf.hedit(images=output,fields='DARK',value=os.path.split(d)[1],add='yes',verify='no')
            else:
                print "Not subtracting dark"

            if applyFlat and not options.noFlat:
                print "Using flat %s" % f
                iraf.imar(operand1=output,op='/',operand2=f,result=output)
                iraf.hedit(images=output,fields='FLAT',value=os.path.split(f)[1],add='yes',verify='no')
            else:
                print "Not applying dome flat"
                
    # We trim the data later
    
    return

def skySubtract(params,options):

    # Copy accross the bad pixel maps
    for chip in ['c1','c2','c3','c4']:
        file=params['flat'].replace('flat','bad').replace('c1',chip).replace('fits','pl')
        iraf.copy(input=file,output='.')


    # Set up the input file lists for xdimsum
    xf={} # Contains file handles
    for chip in ['c1','c2','c3','c4']:
        xf[chip]=open('xfirstpass_'+chip+'.list','w')

    ref={'c1':'','c2':'','c3':'','c4':''}

    file=open(options.skySubtract)
    images=[line.strip() for line in file.readlines()]
    file.close()

    for image in images:
        for chip in ['c1','c2','c3','c4']:
            output=image.replace('c1',"%s_red" % (chip))
            if ref[chip]=='':
                ref[chip]=output
            xf[chip].write(output+'\n')

    for chip in ['c1','c2','c3','c4']:
        xf[chip].close()

    iraf.xdimsum()
    unlearn(['xfirstpass','xmaskpass'])

    for chip in ['c1','c2','c3','c4']:
#    for chip in ['c1','c2','c3']:

        # Adjust the number of sky frames based on the number of exposures one has to work with
        nskymin=min(len(images),8)-1
        nmean=min(len(images),12)-1
        print "Mean number of frames and minimumum number of skies will be",nmean,"and",nskymin,",respectively."

        # Run xfirstpass first
        stub=ref[chip].replace('.fits','_fp')
        if not options.skipFirstPass:
            iraf.xfirstpass(inlist='@xfirstpass_'+chip+'.list',referen=ref[chip],output=stub,expmap='.exp',statsec=params['statSec'],nmean=nmean,nskymin=nskymin,nreject='2',bpmask='bad_'+chip+'.pl',badpixu='no',mkshift='yes',chkshif='no',xnreg='yes',shiftli=stub+'.offset')

        # Run xmaskpass second
        iraf.xmaskpass(input=stub,inexpmap=stub+'.exp',seccorn=stub+'.corners',output=stub+'_mp',outexpma='.exp',statsec=params['statSec'],chkmask='no',kpchking='no',nmean=nmean,nskymin=nskymin,nreject='1',bpmask='bad_'+chip+'.pl',badpixu='no',shiftli=stub+'.offset',mag='1',xnregis='yes')

    return

def normaliseGains(params,options):
    # Unlearn the various IRAF tasks
    unlearn(['hedit','imar'])
    
    file=open(options.normaliseGains)
    images=[line.strip() for line in file.readlines()]
    file.close()
    # Get the name of the flat that was used to do the flatfielding
    
    flatField=fits.getval(images[0].replace('.fits','_red.fits'),'FLAT',0)
    # Get the median values
    scales={}
    for chip in ['c1','c2','c3','c4']:
        flat=params['calib']+flatField.replace('c1',chip)
        scales[chip]=fits.getval(flat,'MEDIAN')

    maxscale=numpy.max(scales.values())
    for chip in scales.keys():
        scales[chip]=scales[chip] / maxscale
        
    if options.combined:
        # We apply the corrections to the combined images
        for chip in ['c1','c2','c3','c4']:
            input=images[0].replace('c1',chip+'_red_fp_mp')
            if 'GAINCORR' not in fits.getheader(input):
                print 'Applying %5.3f scale to %s' % (scales[chip],input)
                iraf.imar(operand1=input,op='/',operand2=scales[chip],result=input)
                iraf.hedit(images=input,fields='GAINCORR',value=scales[chip],add='yes',verify='no')
            else:
                print 'Gain correction already applied to %s' % input
                    
    else:
        for image in images:
            for chip in ['c1','c2','c3','c4']:
                input=image.replace('c1',chip+'_red.sub')
                if 'GAINCORR' not in fits.getheader(input):
                    print 'Applying %5.3f scale to %s' % (scales[chip],input)
                    iraf.imar(operand1=input,op='/',operand2=scales[chip],result=input)
                    iraf.hedit(images=input,fields='GAINCORR',value=scales[chip],add='yes',verify='no')
                else:
                    print 'Gain correction already applied to %s' % input
    return

def getBadRegions(badRegionFile):
    f=open(badRegionFile)
    badRegions=f.readlines()
    f.close
    return badRegions

def removeFile(file):
    if os.path.isfile(file):
        iraf.imdel(images=file,verify='no')
    return

def createWeightMaps(params,options):
    file=open(options.weights)
    images=[line.strip() for line in file.readlines()]
    file.close()

    unlearn(['imexpr','imcopy','imreplace'])

    # Create the special bad region masks
    if options.regions!=None:
        regions=getBadRegions(options.regions)
        for image in images:
            stub=image.replace('c1.fits','')
            for chip in ['c1','c2','c3','c4']:
                # Start with the generic bad pixel map
                sbpm=stub+chip+'_red.sub.sbpm.pl'
                removeFile(sbpm)
                iraf.imcopy(input='bad_'+chip+'.pl',output=sbpm)

            for region in regions:
                # We use simple rectangular regions
                entries=region.split()
                chip=entries[0]
                region=entries[1]
                sbpm=stub+chip+'_red.sub.sbpm.pl'
                iraf.imreplace(images=sbpm+region,value=0)

    for image in images:
        for chip in ['c1','c2','c3','c4']:
            stub=image.replace('c1.fits','')
            crm=stub+chip+'_red.sub.crm.pl'
            bad='bad_'+chip+'.pl'
            sbpm=stub+chip+'_red.sub.sbpm.pl' # sbpm - used for satellite trails and bad resgions

            bpm=stub+chip+'_red.sub.bpm.fits' # Output bad pixel mask
            removeFile(bpm)

            # Combine all masks, 0 - bad, 1 - good
            
            if os.path.isfile(sbpm):
                iraf.imexpr(expr='(a==0 && b==1 && c==1) ? 1 : 0',output=bpm,outtype='short',a=crm,b=bad,c=sbpm)
                print 'Using special mask:',sbpm
            else:
                # One could space using lower bit integers - currently, it is using 32 bit integers
                iraf.imexpr(expr='(a==0 && b==1) ? 1 : 0',output=bpm,outtype='short',a=crm,b=bad)

            input=bpm+trimSection[chip]
            output=bpm
            iraf.imcopy(input=input,output=output)

            input=stub+chip+'_red.sub'+trimSection[chip]
            output=stub+chip+'_red.sub'
            iraf.imcopy(input=input,output=output)

    return

def Merge(params,options):
    file=open(options.Merge)
    images=[line.strip() for line in file.readlines()]
    file.close()

    # Information used to build an approximate astrometric solution
    # We need to find the best values of crpix in a more automatic fashion
    angBin=4.44e-5
    # 2016
    crpix={'c1':[-70.,-55.],'c2':[-70.,2080.],'c3':[2080.,2080.],'c4':[2040.,-80.]}

    #    if 'spt' in options.Merge:
#        crpix={'c1':[+5.,-95.],'c2':[+10.,2020.],'c3':[2150.,2020.],'c4':[2120.,-120.]}
#    else:
#        crpix={'c1':[+20.,-95.],'c2':[+30.,2020.],'c3':[2180.,2020.],'c4':[2140.,-120.]}
  
    for image in images:
        # First, the images
        # Create an HDUList object
        hdulist=fits.HDUList(fits.PrimaryHDU())
        output=image.replace('_c1.fits','_red.sub.fits')
        for chip in ['c1','c2','c3','c4']:
            input=image.replace('c1.fits',chip+'_red.sub.fits')
            # Create a WCS
            hdu=fits.open(input)
            w = wcs.WCS(naxis=2)

            # North up and East left is defined by
            cd=numpy.array([[-angBin,0],[0,angBin]])
            # Rotate
            angle=-numpy.radians(hdu[0].header['ROTANGLE'])
            c=numpy.cos(angle)
            s=numpy.sin(angle)
            w.wcs.cd=numpy.dot(numpy.array([[c, -s], [s, c]]),cd)
            w.wcs.crval=numpy.array([hdu[0].header['RA'],hdu[0].header['DEC']])
            w.wcs.ctype=["RA---TAN","DEC--TAN"]
            w.wcs.crpix=numpy.array([crpix[chip][0],crpix[chip][1]])
            
            # Add it to the FITS header
            hdu[0].header['CRPIX1']=w.wcs.crpix[0]
            hdu[0].header['CRPIX2']=w.wcs.crpix[1]
            hdu[0].header['CD1_1']=w.wcs.cd[0,0]
            hdu[0].header['CD1_2']=w.wcs.cd[0,1]
            hdu[0].header['CD2_1']=w.wcs.cd[1,0]
            hdu[0].header['CD2_2']=w.wcs.cd[1,1]
            hdu[0].header['CRVAL1']=w.wcs.crval[0]
            hdu[0].header['CRVAL2']=w.wcs.crval[1]
            hdu[0].header['CTYPE1']=w.wcs.ctype[0]
            hdu[0].header['CTYPE2']=w.wcs.ctype[1]
            del hdu[0].header['SKYMED']
            del hdu[0].header['EPOCH']
            del hdu[0].header['EQUINOX']
            hdulist.append(hdu[0])
            hdu.close()
        hdulist.writeto(output,clobber=True)

        # The the bad pixel maps
        hdulist=fits.HDUList(fits.PrimaryHDU())
        output=image.replace('_c1.fits','_red.sub.bpm.fits')
        for chip in ['c1','c2','c3','c4']:
            input=image.replace('c1.fits',chip+'_red.sub.bpm.fits')
            hdu=fits.open(input)
            hdulist.append(hdu[0])
            hdu.close()
        hdulist.writeto(output,clobber=True)
            
    return

def findObjects(params,options):
    
    # Copy accross the configuration files
    setup(params['setUp'])
    
    file=open(options.findObjects)
    images=[line.strip() for line in file.readlines()]
    file.close()

    # Run SExtractor
    for image in images:
        input=image.replace('_c1.fits','_red.sub.fits')
        weight=input.replace('.fits','.bpm.fits')
        print input,weight
        makeCat(input,weight,'scamp.sex')
            

    return

def addFrames(params,options):
    setup(params['setUp'])

    unlearn(['imcom','imcopy','imdel','hedit','imexpr'])
    # Run scamp, done manually for now
    # Run swarp, done manually for now

    file=open(options.addFrames)
    images=[line.strip() for line in file.readlines()]
    file.close()

    # Combine the frames with iraf imcombine
    rootname=options.addFrames.replace('.list','')

    # Define the list
    chipNo={'c1':'0001','c2':'0002','c3':'0003','c4':'0004'}
    file=open(rootname+'.list4imcom','w')
    for image in images:
        for chip in ['c1','c2','c3','c4']:
            stub=image.replace('_c1.fits','_red.sub')+'.'+chipNo[chip]
            file.write(stub+'.resamp.fits\n')
            # Create the bad pixel maps
            iraf.imexpr(expr="a > 0 ? 1 : 0", output=stub+".resamp.weight.pl", a=stub+".resamp.weight.fits")
            iraf.hedit(images=stub+".resamp.fits",fields='BPM',value=stub+".resamp.weight.pl",add='yes',verify='no',update='yes')
    file.close()

    # Combine the data
    version='_v'+params['version']
    output=rootname+version
    removeFile(output+'.fits')
    removeFile(output+'_exp.pl')
    removeFile(output+'_exp.fits')
    iraf.imcom(input='@'+rootname+'.list4imcom',output=output,expmasks=output+'_exp',combine='average',reject='minmax',offsets='wcs',masktyp='badvalue',maskval='0',scale='none',zero='median',statsec=params['statSec'],nlow='2',nhigh='2')

    iraf.imdel(images=output+'_exp.fits',verify='no')
    iraf.imcopy(input=output+'_exp.pl',output=output+'_exp.fits')

    return

def mergeFrames(params,options):
    # Merge the exposure map with the science frame
    iraf.fitsutil()
    unlearn(['fxinsert','imcopy','imdel'])
    input=options.mergeFrames
    output=options.mergeFrames.replace('.resamp.fits','').replace('.fits','')

    iraf.imdel(images=output+'_ipe.fits',verify='no')
    iraf.imcopy(input=input,output=output+'_ipe.fits')
    if 'resamp' in output:
        iraf.fxinsert(input=output+'.resamp.weight.fits[0]',output=output+'_ipe.fits[0]',groups=0)
    else:
        iraf.fxinsert(input=output+'_exp.fits[0]',output=output+'_ipe.fits[0]',groups=0)
        
    return

def zeroPoint(params,options):
    # compute the ZP using 2MASS
    filter=options.passband
    CRVAL=numpy.matrix([[fits.getval(options.zeroPoint,'CRVAL1',0)],[fits.getval(options.zeroPoint,'CRVAL2')]])
    CRPIX=numpy.matrix([[fits.getval(options.zeroPoint,'CRPIX1',0)],[fits.getval(options.zeroPoint,'CRPIX2')]])
    CD=numpy.matrix([[fits.getval(options.zeroPoint,'CD1_1',0),0.0],[0.0,fits.getval(options.zeroPoint,'CD2_2',0)]])
    # Find stars in the 2MASS catalogue
    #print CD,CRVAL,CRPIX

    cmd='find2mass -r 5.0 -c %9.5f %9.5f' % (CRVAL[0,0],CRVAL[1,0])
    p1=sp.Popen(cmd, shell=True, stdout=sp.PIPE)
    stars=parseCDC(p1.communicate(0)[0],options.zeroPoint.replace('.fits','.2MASS'),filter)

    
    # Determine the x,y position of the 2MASS stars
    # The coordinate transformation in the header is
    # c-c_0 = M * (x-x_0)

    tvmarkFile=options.zeroPoint.replace('.fits','.txt')
    file=open(tvmarkFile,'w')
    xcenterRef=[]
    ycenterRef=[]
    for star in stars:
        coord=numpy.matrix([[star['ra']],[star['dec']]])
        proj=numpy.matrix([[numpy.cos(coord[1,0]*numpy.pi/180.),0.0],[0.0,1.0]])
        x=numpy.linalg.inv(CD)*proj*(coord-CRVAL)+CRPIX
        file.write('%7.2f %7.2f %s\n' % (x[0,0],x[1,0],star['id']))
        xcenterRef.append(x[0,0])
        ycenterRef.append(x[1,0])


    file.close()
    unlearn(['display','tvmark'])
    
    iraf.display(image=options.zeroPoint+'[0]',frame=1,zr='no',zs='no',z1=-100,z2=300)
    iraf.tvmark(frame='1',coords=tvmarkFile,label='yes',color=205,mark='circle',nxoffse=40,radii=20,pointsize=2,txsize=3)
    
    # If we do not trust the coordinates, then we ask the user to select stars in the image
    trust=True
    iraf.noao()
    iraf.digi()
    iraf.apphot()
    unlearn(['phot','txdump'])
    iraf.fitskypars.salgori='median'
    iraf.fitskypars.annulus='4'
    iraf.fitskypars.dannulu='6'
    iraf.datapars.scale='0.16'
    iraf.photpars.apertur='4'
    iraf.photpars.zmag='0'
    if not trust:
        iraf.phot(image=options.zeroPoint.replace('.fits','[0]'))
    else:
        iraf.phot(image=options.zeroPoint.replace('.fits','[0]'),coords=tvmarkFile,interact='no')
        
        mags=options.zeroPoint.replace('.fits','0.mag.1')
    xcenterIn=iraf.txdump(textfile=mags,fields='XCENTER',expr='yes',Stdout=1)
    ycenterIn=iraf.txdump(textfile=mags,fields='YCENTER',expr='yes',Stdout=1)
    mag=iraf.txdump(textfile=mags,fields='MAG',expr='yes',Stdout=1)
    # The magnitude error is not very precise
    magerr=iraf.txdump(textfile=mags,fields='MERR',expr='yes',Stdout=1)

    iraf.delete(files=mags,verify='no')

    # ZP
    ZP=[]
    ZP_err=[]
    sum1=0
    sum2=0
    phot=open(options.zeroPoint.replace('.fits','.phot'),'w')
    phot.write('ID\tRA\tDEC\tmag_INST\terr_mag_INST\tmag_2MASS\terr_mag_2MASS\tQfl\tZP\terr_ZP\n')
    # Only A class stars are considered
    use=[]
    for i in range(len(xcenterIn)):
         if stars[i]['flag'] in ['A'] and stars[i]['magerr'] > 0 :
             reply=raw_input('Using star # %d?' % (stars[i]['id']))
             if reply=='y':
                 use.append(i)

    for i in use:
        ZP.append(stars[i]['mag']-float(mag[i]))
        ZP_err.append(numpy.sqrt(stars[i]['magerr']**2.+float(magerr[i])**2.))
        print 'Using star #',stars[i]['id']
        phot.write('%s\t%9.5f\t%9.5f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%s\t%6.3f\t%6.3f\n' %(stars[i]['id'],stars[i]['ra'],stars[i]['dec'],float(mag[i]),float(magerr[i]),stars[i]['mag'],stars[i]['magerr'],stars[i]['flag'],ZP[-1],ZP_err[-1]))
        sum1+=ZP[-1]/ZP_err[-1]**2.
        sum2+=1.0/ZP_err[-1]**2.

    phot.close()
    

    # The error bars are probably understimated
    if len(ZP) > 0:
        iraf.hedit(images=options.zeroPoint+'[0]',fields='ZP',value=sum1/sum2,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='err_ZP',value=numpy.sqrt(1.0/sum2),add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='scatt_ZP',value=numpy.std(ZP)/numpy.sqrt(len(ZP)),add='yes',verify='no')
    else:
        iraf.hedit(images=options.zeroPoint+'[0]',fields='ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='err_ZP',value=-9.99,add='yes',verify='no')
        iraf.hedit(images=options.zeroPoint+'[0]',fields='scatt_ZP',value=-9.99,add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='n_ZP',value=len(ZP),add='yes',verify='no')

    #iraf.hedit(images=options.zeroPoint,fields='AIRMASS',value=airmass,verify='no')
    
    # Add some keywords

    iraf.hedit(images=options.zeroPoint+'[0]',fields='MAGSYS',value='Vega',add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='VERSION',value='v'+params['version'],add='yes',verify='no')
    iraf.hedit(images=options.zeroPoint+'[0]',fields='REDUCED',value=params['reduced'],add='yes',verify='no')

    return

def examineOffsets(params,options):
    # Work out the offset filename
    f=open(options.examineOffsets)
    firstLine=f.readline()
    f.close()

    f=open(firstLine.strip().replace('.fits','_red_fp.offset'))
    lines=f.readlines()
    f.close()

    x=[]
    y=[]
    for line in lines:
        entries=line.split()
        x.append(float(entries[1]))
        y.append(float(entries[2]))

    # Plot the results
    import matplotlib.pyplot as plt
    fig=plt.figure()
    ax=fig.add_subplot(111)
    ax.plot(x,y)
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    plt.show()
    plt.close()

    return
