#
# Read in photometric catalogue and set up weighting scheme ready for maskdes
# Make checking plots (spatial, CMD,..)
#
import os,sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from astropy.io import fits,ascii
from astropy.io.fits import Column
#from astropy.cosmology import arcsec_per_kpc_proper as asperkpc
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from numpy.lib import recfunctions as recf
from glob import glob

#from config import *
#from configE2v import *

##from astrometry.libkd import spherematch as spm

import GoGREEN_maskdesDet as MD
from GoGREEN_utils import getPhotFilename, colz


"""
#import maskdesE2v as MD2
# try to add fillers into existing masks



isilent=1
nMasks=5
#name='SpARCS_0035'
#name='SPT-0205'
#name='CDFS-41'
name='SPT-0546'

#nMasks=3
#name='COS-221'
#name='SXDF64'
#name='SXDF76'
#name='SXDF87'
"""

# //////////////////////////////////////////////////////////////////////
# ---- Parameters for mask design setup
#nMasks=5 # number of masks in set for this cluster
# weighting scheme for each mask in sequence. Probably want different scheme for bright mask at start, followed by same for all subsequent
#allocateSchemes=np.repeat('bright2         ',nMasks)
#allocateSchemes[0]='faint'
#allocateSchemes[0]='bright   '
#allocateSchemes[0]='brightcore'

#allocateSchemes = ['first', 'fillfaint', 'rptfnt', 'rptfnt', 'rptfnt']


#xc,yc = ccd_cx,ccd_cy

# //////////////////////////////////////////////////////////////////////


import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage


#from match import simpmatch as match


# check***
"""
def match(a,b):
    ok1 = [i for i,x in enumerate(a) if x in b]
    ok2 = [x for x in b if x in a]
    return ok1,ok2
"""

def inBin(x,bin):
    ok=np.where( (x>bin[0]) & (x<=bin[1]) )[0]
    return ok

def getZphWts(gal,fInfo):
        phzWt = np.zeros(len(gal)) + 0.05
        # -- 2nd highest prioirty photoz (within: 0.7--2.0):
        ok = np.where( (gal['ZPH']>0.7) & (gal['ZPH']<2.0) )[0]
        #sel2 = np.where( (dat.zPDF>0.7) & (dat.zPDF<2.0) )[0]
        phzWt[ok] = 0.5
        
        # -- highest priority photo-z (within 2sigma of grpz):
        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH'] >= fInfo['grpz']) )[0]
#        ok = np.where( (2.0*gal['ZPHLO']-gal['ZPH'] <= fInfo['grpz']) & (2.0*gal['ZPHHI']-gal['ZPH']>=grpz), 1.0, 0.0 )[0]
        #   sel = np.where( (dat.zPDF>0) & (2.0*dat.zPDF_l68-dat.zPDF <= grpz) & (2.*dat.zPDF_u68-dat.zPDF>=grpz) & (dat.TYPE==0) )[0]
        phzWt[ok] =1.0
        # *** NOTE: gals with phzs outside this range (or bad photozs) are hence completely rejected! ***
        return phzWt

def getAllBris(name,gal):
    # find all bright targets from all masks in set
    from glob import glob
    files = glob("best%sout?.txt"%name)
    ainds=[]
    for filename in files:
        dat = ascii.read(filename,names=('ra','dec','pwt','id'))
        id = dat['id'].astype('int')
        inds = [i for i,x in enumerate(gal['ID']) if x in dat['id']]
        # now check which are bright. actually just match all. we don't want fnt targets in fillers!
        ainds=np.append(ainds,inds)
    return inds


def genWeights(name,gal,fInfo,scheme='filler'):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] ; zmag = gal['ZMAG']
    pri = gal.prob

    print scheme

    # -- redder than CMR:

    # increase red limit
    yfit = np.polyval(redCoeffs,mag)
    yfitLim = np.polyval(redLimCoeffs,mag)
    bluefitLim = np.polyval(cfg.blueCoeffs,mag)

#    rej = np.where(col > yfit)[0]


    if (scheme=='filler'):
        # set bright high priority
        fntWt = 0.05
        briWt = 0.95
        ok = inBin(zmag,cfg.zFnt) ; pri[ok]=fntWt
        ok = inBin(zmag,cfg.zBri) ; pri[ok]=briWt
        rej = np.where( (col > yfitLim) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zBri[1]) )[0]
##        pri[rej] = 0.0###1#5 
#        ok = inBin(zmag,zVBri) ; pri[ok]=0.05
#        ok = np.where( (gal.inMask>0) & (gal['ZMAG']<zBri[1]) )[0]
#        print '%s bright gals in previous mask being ignored'%(len(ok))
#        pri[ok]=0.0

        ok=np.where(zmag<cfg.zBri[1])[0]
        pri[ok]=0.95
# MLB added nuance to filler priorities
#  Want to give max priority to galaxies that would otherwise have been selected, but for the wavelength restriction.  This restriction is relaxed (removed) for filler objects.  So, give reduced but non-zero priority to other types of galaxies:
        #rej = np.where( (col > yfitLim) | (zmag<cfg.zVBri[0]) | (zmag>cfg.zBri[1]) )[0]
# Give decreasing priority as you go redder from the red limit
        redgal=np.where(col > yfitLim)
        pri[redgal]=pri[redgal]*np.maximum((1.-(col[redgal]-yfitLim[redgal])/1.5),0.0)
        vredgal=np.where(col > yfitLim+1.5)
        pri[vredgal]=pri[vredgal]*0.05
# Give decreasing priority as you go bluer from the blue limit
        bluegal=np.where(col < bluefitLim)
        pri[bluegal]=pri[bluegal]*np.maximum((1.-(bluefitLim[bluegal]-col[bluegal])/0.5),0.0)
        vbluegal=np.where(col < bluefitLim-0.5)
        pri[vbluegal]=pri[vbluegal]*0.01
# Give decreasing priority as you go brighter than the bright limit
        brightgal=np.where(zmag<cfg.zBri[0])
        pri[brightgal]=pri[brightgal]*np.maximum((1.-(cfg.zBri[0]-zmag[brightgal])/3.),0.0)
        vbrightgal=np.where(zmag<cfg.zVBri[0])
        pri[vbrightgal]=pri[vbrightgal]*0.01
# Give low and decreasing priority as you go fainter than the faint limit
        faintgal=np.where(zmag>cfg.zBri[1])
        pri[faintgal]=pri[faintgal]*np.maximum((1.-(zmag[faintgal]-cfg.zBri[1])/2.),0.)
        vfaintgal=np.where(zmag>cfg.zBri[1]+2)
        pri[vfaintgal]=pri[vfaintgal]*0.01
    else:
        print 'scheme %s not found'%scheme
        print flibble # to crash
        
    
    
###    ok = inBin(zmag,zVFnt) ; pri[ok]=0.#1
###    ok = inBin(zmag,zVBri) ; pri[ok]=0.01#5
    
    
    # -- redder than CMR:
    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>cfg.zVBri[0]) & (zmag<cfg.zVFnt[1]) )[0]
#    pri[rej] = 0.0###1#5 

    yfit = np.polyval(redLimCoeffs,mag)
#    rej = np.where(col > yfit)[0]
    rej = np.where( (col > yfit) & (zmag>cfg.zVBri[0]) & (zmag<cfg.zVFnt[1]) )[0]
##    pri[rej] = 0.00 
    
    # --
    
    
    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfit = np.polyval(cfg.blueCoeffs,mag)
    rej = np.where(col < yfit)[0]
##    pri[rej]=0.0
    # --
    
    # -- irac1 exclusion cut:
    rej = np.where(mag>cfg.irac1VFnt)[0]
##    pri[rej]=0.0
    bad = np.where(mag>cfg.irac1Lim)[0]
##    pri[bad]=pri[bad]*0.#1 # downweight v faintest bin

    
    bad = np.where(mag<0.0)[0]
##    pri[bad]=0.0 # IRAC undetected?***
    # --

    
    radwt =  (1. - 0.4*gal.disMpc)**2 #*** probably not extreme enough
    

    # reject bright targets which are already allocated in *any* mask in set:
    briDone = getAllBris(name,gal)
    pri[briDone]=0.01 # v. low weighting
    print '***',rej


    # update weights:
    gal.prob = pri
    
    if (cfg.iRadialWt):
        notMustHaves = np.where(pri<1.0)[0]
        gal.prob[notMustHaves] = pri[notMustHaves]* radwt[notMustHaves]
        





def plotCMD(gal,fInfo,axname):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] 
    pri = gal.prob
    axname.plot(mag,col,'k,')
    axname.set_xlabel('[3.6]')
    axname.set_ylabel('z - [3.6]')
    axname.set_xlim([19,23])
    axname.set_ylim([-2,6])
    
    xxx = np.arange(0,30)
    axname.plot(xxx,cfg.zFnt[1]-xxx,'b--',label='faint')
    axname.plot(xxx,cfg.zBri[1]-xxx,'b:',label='faint')
    axname.plot(xxx,np.polyval(cfg.blueCoeffs,xxx),'b-')
    

    axname.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:')
    axname.plot(xxx,np.polyval(redCoeffs,xxx),'r-')
    axname.plot(xxx,np.polyval(redLimCoeffs,xxx),'r--')
    
    # wts:
    sz=pri*100 +0.1
    axname.scatter(mag,col,s=sz,alpha=0.5,color='b')
    

# initial plot to identify location of cluster in ra,dec and CMD    
def plotClus(gal,fInfo):
    col = gal['ZMAG'] - gal['IRAC1'] ; mag = gal['IRAC1'] 
    pri = gal.prob
    xp = gal['XP'] ; yp = gal['YP']
    
    plt.figure()
    plt.subplot(121)
    plt.plot(mag,col,'k,')
    if (gal['IZPH'][0]==1):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.05)  & (gal['ZMAG']<=zFnt[1])  & (gal['IRAC1']<=irac1Lim))[0]
###        plt.plot( mag[ok],col[ok], 'or', alpha=1, label='|zph-grpz|<=0.05')
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=1.0) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3, label=r'|zph-grpz|<=2$\sigma$')
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=1.0) )[0]
        plt.plot( mag[ok],col[ok], 'or',alpha=0.1,label='zph= 0.7--2.0')
    else:
        ok=np.where( (gal.disMpc<=1.0) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.3)
        ok=np.where( (gal.disMpc<=0.3) & (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'ok', alpha=0.5, label='r<0.3Mpc')
        ok=np.where( (gal['ZMAG']<=cfg.zFnt[1]) & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( mag[ok],col[ok], 'or', alpha=0.1)
        
    plt.xlabel('[3.6]')
    plt.ylabel('z - [3.6]')
    plt.xlim([19,23])
    plt.ylim([-2,6])
    plt.legend(numpoints=1)
    
    xxx = np.arange(0,30)
    plt.plot(xxx,cfg.zFnt[1]-xxx,'b--',label='faint')
    plt.plot(xxx,cfg.zBri[1]-xxx,'b:',label='bright')
    plt.plot(xxx,np.polyval(cfg.blueCoeffs,xxx),'b-')
    
    plt.plot(xxx,np.polyval(redSeqCoeffs,xxx),'r:',label='RS')
    plt.plot(xxx,np.polyval(redCoeffs,xxx),'r--',label='RS+%.2f'%cfg.dRed)
    plt.plot(xxx,np.polyval(redLimCoeffs,xxx),'r-',label='RS+%.2f'%cfg.dRedLim)
    plt.legend(numpoints=1)
    
    # spatial:
    plt.subplot(122)
    plt.plot(gal['RA'],gal['DEC'],'k,')

    plt.plot(fInfo['grpra'],fInfo['grpdec'],'og',ms=20,alpha=0.3)
    if (gal['IZPH'][0]):
        phzWt = getZphWts(gal,fInfo)
#        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.03)  & (gal.disMpc<=0.5) )[0]
###        ok=np.where( (abs(gal['ZPH']-fInfo['grpz']) <=0.05) & (gal['ZMAG']<=zFnt[1])  & (gal['IRAC1']<=irac1Lim) )[0]
###        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=1)
        ok=np.where( (phzWt>0.6) & (gal.disMpc<=5.5) & (gal['ZMAG']<=cfg.zFnt[1])  & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=0.2)
        ok=np.where( (phzWt>0.4) & (gal.disMpc<=5.5) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or',alpha=0.1)
    else:  
        ok=np.where( (gal.disMpc<=5.5) & (gal['ZMAG']<=cfg.zFnt[1])  & (gal['IRAC1']<=cfg.irac1Lim) )[0]
        plt.plot( gal['RA'][ok],gal['DEC'][ok], 'or', alpha=0.2)
        
 
    plt.xlabel('ra')
    plt.ylabel('dec')
    
    
    plt.show()
    
    
# in pixels
def drawMpcCircle(xc,yc,nMpc,fInfo,axname):
    radius = (nMpc*1000.0*asperkpc(fInfo['grpz'])).value # arcsec
    radius = radius/cfg.pxscale # pix
    circ = Circle((xc,yc),radius, fill=False, facecolor='none', edgecolor='r')
    axname.add_patch(circ)

def calcdisMpc(fInfo,gal):
    # add distance of every gal from rabcg, decbcg in Mpc
    
    
    dra = (gal['RA'] - fInfo['grpra'])*np.cos(np.deg2rad(gal['DEC']))
    ddec = gal['DEC'] - fInfo['grpdec']
    disdeg = np.sqrt(dra**2 + ddec**2)
    
    convfac = (asperkpc(fInfo['grpz'])).value
    disMpc = disdeg*3600.0 / convfac /1000.0
#    tgal = recf.append_fields(gal,'disMpc',disMpc,asrecarray=True,usemask=False)
#    del gal
#    gal = np.copy(tgal)
#    del tgal
    adisMpc = disMpc
    
    if(0): # use spm instead
        from astrometry.libkd import spherematch as spm
        oka,okb,disdeg = spm.match_radec(gal['RA'],gal['DEC'],fInfo['grpra'],fInfo['grpdec'],1.0,nearest=False)
        adisMpc = np.ones(len(gal))
        disMpc = disdeg*3600.0 / convfac /1000.0
        adisMpc[oka] = disMpc # needed with spm
    

    return adisMpc

def plotSpatial(gal,fInfo,axname):
    xp = gal['XP'] ; yp = gal['YP'] 
    pri = gal.prob
    axname.plot(xp,yp,'k,')
    axname.set_xlabel('XP')
    axname.set_ylabel('YP')
#    axname.set_xlim([19,23])
#    axname.set_ylim([-2,6])

    # wts:
    sz=pri*100 +0.1
    axname.scatter(xp,yp,s=sz,alpha=0.5,color='b')

#    MD.drawGMOS()
    MD.drawHam(axname)
    
    # draw 0.5 Mpc radius circle
    ##xc,yc = MD.MaskpixfromSky(fInfo['cra'],fInfo['cdec'],fInfo['rotang'],fInfo['grpra'],fInfo['grpdec'])
    drawMpcCircle(cfg.ccd_cx,cfg.ccd_cy,0.5,fInfo,axname)
    drawMpcCircle(cfg.ccd_cx,cfg.ccd_cy,1.0,fInfo,axname)
    drawMpcCircle(cfg.ccd_cx,cfg.ccd_cy,2.0,fInfo,axname)

    
    
def tagMask(gal,fInfo,mID,masknum=1):    
    # add inMask bitflag:
#    ok1,ok2 = match(gal['ID'],mID)
    
    ok1 = [x for x in gal['ID'] if x in mID]
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal['ZMAG']
    # update inMask flag with binary to indicate which number(s) mask in
    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def evalCat(gal,fInfo):
    col = gal['ZMAG'] - gal['IRAC1']
    
    # Find number of objects in appropriate mag,distance, etc. bins in whole catalogue (with appropriate cuts)
    # for completeness calculations:
    inner = (gal.disMpc<=0.5)
    outer = ( (gal.disMpc>0.5) & (gal.disMpc<=1.0) )
    outsk = ( (gal.disMpc>1.0) )
    
    bri = ( (gal['ZMAG'] > cfg.zBri[0]) & (gal['ZMAG'] <= cfg.zBri[1]) )
    fnt = ( (gal['ZMAG'] > cfg.zFnt[0]) & (gal['ZMAG'] <= cfg.zFnt[1]) )
    
    # -- redder than CMR:
    yfitRedLim = np.polyval(redLimCoeffs,gal['IRAC1'])

    # -- blue exclusion cut:
    # (careful, this is in terms of irac1 mag, not z, as above)
    yfitBlueLim = np.polyval(cfg.blueCoeffs,gal['IRAC1'])

    if (gal['IZPH'][0]):
        zphWts = getZphWts(gal,fInfo)

    # othCuts must be applied to each condition!
    if (gal['IZPH'][0]):
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal['IRAC1']<cfg.irac1VFnt) \
                    & (gal['IRAC1']>0.0) \
                    & (zphWts>0.4) )
    else:
        othCuts = ( (col<=yfitRedLim) & (col>=yfitBlueLim) & (gal['IRAC1']<cfg.irac1VFnt) \
                    & (gal['IRAC1']>0.0) \
                    )


    hasSlit = (gal.inMask>0)

    innBri = np.sum( inner & bri & othCuts )
    outerBri = np.sum( outer & bri & othCuts )
    outskBri = np.sum( outsk & bri & othCuts )
    innFnt = np.sum( inner & fnt & othCuts )
    outerFnt = np.sum( outer & fnt & othCuts )
    outskFnt = np.sum( outsk & fnt & othCuts )
#    print 'len(innBri)=%s'%len(innBri)
    #print innBri,outerBri,innFnt,outerFnt,outskFnt
    innBriS = np.sum( inner & bri & othCuts & hasSlit)
    outerBriS = np.sum( outer & bri & othCuts & hasSlit)
    outskBriS = np.sum( outsk & bri & othCuts & hasSlit)
    innFntS = np.sum( inner & fnt & othCuts & hasSlit)
    outerFntS = np.sum( outer & fnt & othCuts & hasSlit)
    outskFntS = np.sum( outsk & fnt & othCuts & hasSlit)
    #print innBriS,outerBriS,innFntS,outerFntS,outskFntS

    print
    print 'Bright:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innBriS,innBri, outerBriS,outerBri, outskBriS,outskBri )
    print 'Faint:'
    print 'r<0.5Mpc: %.0f/%.0f;  r:0.5--1.0Mpc: %.0f/%.0f;  r>1.0Mpc: %.0f/%.0f'%( innFntS,innFnt, outerFntS,outerFnt, outskFntS,outskFnt )

    return np.array((innBriS, outerBriS, outskBriS)),\
            np.array((innBri, outerBri, outskBri)),\
            np.array((innFntS, outerFntS, outskFntS)),\
            np.array((innFnt, outerFnt, outskFnt)),\


def evalMask(gal,fInfo,masknum=0,nTotMasks=3):
#    # evaluate mask:

    # if masknum==0, evaluate all objects inMask, otherwise only specified mask number:
    if (masknum):
#        reqBit = 2**(masknum-1)
        bitFlag = [np.binary_repr(y,nTotMasks) for y in gal.inMask.astype('int')]        
#        ok1 = np.where(gal.inMask==reqBit)[0]
        ok1 = [j for j,y in enumerate(bitFlag) if y[nTotMasks-masknum]=='1' ]
        #ok1 = [j for j,y in enumerate(maskFlag) if y[2]=='1']
    else:
        ok1 = np.where(gal.inMask>0)[0]
    
#    mag = zmag[ok1]
    mgal = gal[ok1]
    mag = mgal['ZMAG']
    # as fn of zmag:
#    print '%s fnt; %s bri; %s vFnt; %s vBri'\
#        %( len(inBin(mag,zFnt)),len(inBin(mag,zBri)),len(inBin(mag,zVFnt)),len(inBin(mag,zVBri)) )
    # as fn of distance:

    ###oka,okb,dis = spm.match_radec(mgal['RA'],mgal['DEC'],fInfo['cra'],fInfo['cdec'],1.0,nearest=False)

    dra = (gal['RA'] - fInfo['grpra'])/np.cos(np.deg2rad(gal['DEC']))
    ddec = gal['DEC'] - fInfo['grpdec']
    dis = np.sqrt(dra**2 + ddec**2)
    
    disMpc = dis*3600.0 / (1.0*1000.0*asperkpc(fInfo['grpz'])).value
    inn = np.where( disMpc <= 0.5)[0]
#    print dis
#    print disMpc
#    print '%s within 0.5 Mpc'%len(inn)
#    print

    # update inMask flag with binary to indicate which number(s) mask in
#    gal.inMask[ok1] = gal.inMask[ok1] + 2**(masknum-1)


def bitMask(x):
    # turn N array of floats (up to 2**(M-1))into
    # into N*M bitmask array 
    strBits = [binary_repr(y,3) for y in x.astype('int')]
    



def verifyDes(maskname): # maskname should really be, e.g. bestSpARCS_0035in1.txt, to check the best design of each
    id,xp,yp,ra,dec,wt,slen,swid,pri = np.loadtxt(maskname,unpack=True)
    plt.figure()
    plt.plot(xp,yp,'k,')
    sz=wt*100.+0.1
    plt.scatter(xp,yp,s=sz,color='r',alpha=0.3)

    outname=maskname.replace('in','out')
#    outname=maskname.replace('des','out')
    sra,sdec,sdec,sid = np.loadtxt(outname,unpack=True)

#    ok1,ok2=match(id,sid)
#    ok1 = [x for x in id if x in sid]
    ok1 = [i for i,x in enumerate(id) if x in sid]
    
    plt.scatter(xp[ok1],yp[ok1],s=sz[ok1],color='b',alpha=0.3)
    plt.show()



def updateinMask(gal,maskfile):
    mra,mdec,mpri,mID = np.loadtxt(maskfile, unpack=True)
#    ok1,ok2=match(gal['ID'],mID)
    ok1 = [i for i,x in enumerate(gal['ID']) if x in mID]
    
    gal.inMask[ok1]=1.0





#def simpCheckColl(x0,xWidth,y0,yWidth,wt,inMask,ss,xobj,yobj,xpad=xpadPix,ypad=ypadPix):
    




###def addFillers(name,nMasks=5,det='Ham',fInfo=None,isilent=True):
def addFillers(name,aMaskNums,det='Ham',fInfo=None,isilent=True):
    #if(1):
    
    
    global cfg
    
    global redCoeffs,redSeqCoeffs,redLimCoeffs
    
    #*** reload()?
    if det=='Ham':
        import MDconfigHam as cfg
    elif det=='E2v':
        import MDconfigE2v as cfg
    
    #    reload(cfg)

    
    
    #    fInfo = fits.getdata('%sinfo.fits'%name) ; fInfo=fInfo[0] # convert to scalars
#    photfile = eval(cfg.photfileExpr)
    #photfile = '%s/Catalogues/%s/%s_phot.fits'%(os.getenv('GoGREEN_data'),name,name)
    photfile = getPhotFilename(name)
    gal = fits.getdata(photfile)
    gals = np.where(gal['AQSTAR_HANDCHECK']==0)[0]
    isStar = np.where(gal['AQSTAR_HANDCHECK']==1)[0]
    stars = np.copy(gal[isStar])
    gal=gal[gals]

    #stars = fits.getdata('%ssetupCands.fits'%name)

    # CAREFUL: this must only be reset at the start of 
    inMask = np.zeros(len(gal)) # flag whether gal has been assigned to a mask or not:
    prob = np.zeros(len(gal)) # probability of being included in mask
    
    
    # calculate distances:
    disMpc = calcdisMpc(fInfo,gal)
    
    # add all these other arrays to gal array, to make sure everything stays in sync:
    gal = recf.append_fields(gal,('inMask','prob','disMpc'),(inMask,prob,disMpc),asrecarray=True,usemask=False)
    # trim to field:
    inFoV = np.where( (gal['XP']>cfg.xminField) & (gal['XP']<=cfg.xmaxField) &  (gal['YP']>cfg.yminField) & (gal['YP']<=cfg.ymaxField))[0]
    gal = gal[inFoV]
    
    print '%s gals in full catalogue within imaging field'%(len(gal))
    print

    redSeqCoeffs = np.array((fInfo['RSslope'][0],fInfo['RSintcpt'][0]))
    redCoeffs = redSeqCoeffs+np.array((0,cfg.dRed))
    redLimCoeffs = redSeqCoeffs+np.array((0,cfg.dRedLim))


    print
    print '---------- Field info -----------'
    print 'Mask centre     ra,dec = %.6f, %.6f; rotang = %.2f'%(fInfo['cra'],fInfo['cdec'],fInfo['rotang'])
    if (fInfo['grpra']<0.0):
        print '*** using field centre as clus centre ***'
        fInfo['grpra'] = fInfo['cra'] ; fInfo['grpdec'] = fInfo['cdec']
    print 'Cluster centre: ra,dec = %.6f, %.6f; redshift = %.4f'%(fInfo['grpra'],fInfo['grpdec'],fInfo['grpz'])
    print '---------------------------------'
    print


    genWeights(name,gal,fInfo,scheme='filler')
    
    
#    inAllMasks = np.zeros(len(gal))
    # read in all masks to exclude already allocated objects:

    #**
    #nMasks=1
    #**
###    for jj in range(nMasks):
    for jj in aMaskNums:
        inmaskfile = 'best%sout%s.txt'%(name,jj+1)
        # get best (ith) mask     
        mra,mdec,mpri,mID = np.loadtxt(inmaskfile,unpack=True)
        
#        oka,okb = match(gal['ID'],mID)
        oka = [i for i,x in enumerate(gal['ID']) if x in mID]
        
        gal.prob[oka] = 0.0 # already in a mask
        # but need to make sure setup stars stay in!
#        inAllMasks[oka]=1.0
    
    # Now, add fillers to each mask:
###    for jj in range(nMasks):
    for jj in aMaskNums:

        slen = np.repeat(cfg.nomsLen,len(gal))

        if(cfg.allocateSchemes[jj][0:4]=='band'):
            NSmode='band'
##            slen=slen/2.0
            #** divided by 2 in defineGMOSspec later??
            yHt=cfg.ymaxField-cfg.yminField
            yF0=cfg.yminField+0.33*yHt
            ymaxField=cfg.yminField+0.67*yHt
            yminField = yF0
        else:
            ymaxField = cfg.ymaxField
            yminField = cfg.yminField
            NSmode='micro'

        inmaskfile = 'best%sout%s.txt'%(name,jj+1)
        print inmaskfile
        
        mra,mdec,mpri,mID = np.loadtxt(inmaskfile,unpack=True)

        oka = [i for i,x in enumerate(gal['ID']) if x in mID ]

#        oka,okb = match(gal['ID'],mID)
        gal.prob[oka] = 1.0
        print '%s must-haves'%len(oka)
        gal.inMask[oka]=1.0
        #print '**',np.sum(gal.inMask)
        # ***stars??
#        sok1,sok2 = match(stars.ID,mID)
        sok1 = [i for i,x in enumerate(stars['ID']) if x in mID ]
        stars = stars[sok1]
#        okst = np.where(mpri==9.0)[0]
#        gal.prob[okb[okst]]=9.0
#        print '%s stars'%len(okst)
        print '%s stars'%len(sok1)

        """ #???
        # check that stars aren't being repeated in galaxy cat!:
        sokc = [i for i,x in enumerate(gal['ID']) if x in stars['ID'] ]
        if(len(sokc)>0):
            gal.prob[sokc]=0.0
#        stop()
#        print '**',np.sum(gal.inMask)
        """

#        inname = 'best'+name+'infill%s.txt'%(jj+1)
#        writeInput(inname,gal,fInfo,stars=stars)
#        MD2.desmask( fInfo['cra'],fInfo['cdec'],fInfo['rotang'],inname,niter=nSingleIter ,\
#                     iplot=plotFlag, sortScheme=sortScheme, NSmode=NSmode )


#        infile = inmaskfile.replace('out','in')
        
        #---
        #id,xobj,yobj,ra,dec,wt,slen,swid,stilt = np.loadtxt(inname,unpack=True)
        #slen = np.repeat(cfg.nomsLen,len(gal))
        xspe,yspe,xToLeft,xToRight,yAbove,yBelow, keepFlag = MD.defineGMOSspec(gal['XP'],gal['YP'],slen,gal.prob,NSmode=NSmode, fillMode=True)
        # set weight of keepflag==0 objects to 0.0
#        rej=np.where(keepFlag==0.0)[0]
#        if (len(rej)>0): wt[rej]=0.0
        
        x0 = xspe-xToLeft ; xWidth = xToLeft + xToRight
        y0 = yspe-yBelow ; yWidth = yAbove+yBelow
        
        #inMask=np.ones(len(id))
        #zmag=np.repeat(23.,len(id)) #***
        zmag = gal['ZMAG']
        mag = gal['IRAC1']
        #plt.plot([0,1],[0,1])
        
        xpad=cfg.xpadPix
        #**
        ypad=cfg.ypadPix+1.0 # extra padding for additional slits
        #ypad=0.0#***
        
        tx0=x0-xpad/2.
        tx1=x0+xWidth+xpad/2. # [ careful switching between lower left and width and lower left and upper right notation ]
        ty0=y0-ypad/2.
        ty1=y0+yWidth+ypad/2.
        
        
        
##        bad= np.where( (tx0<cfg.xminSField) | (tx1>cfg.xmaxSField) )[0]
        bad= np.where( (tx0<cfg.xminSField) | (tx1>cfg.xmaxSField) | (ty0<yminField) | (ty1>ymaxField) )[0]
#        inMask[bad]=0.0  
        gal.prob[bad]=0.0
 
        print '**',np.sum(gal.inMask)       
#        bad=np.where(gal.prob==0.0)[0]
#        inMask[bad]=0.0
        
        
        #---
        

        # --
        print 'tot %s'%np.sum(inMask)
        # --- simplified version of resolveCollisions!
        added = np.zeros(len(gal))
        # --- MLB modification so that loop begins with highest priority objects.
        #for i in range(len(gal)):
        yfitLim = np.polyval(redLimCoeffs,mag)
        bluefitLim = np.polyval(cfg.blueCoeffs,mag)
        for i in np.argsort(gal.prob)[::-1]:
            if(gal.prob[i]==0.0):
                continue
        
            if (gal.inMask[i]==1.0):
                continue # already in mask
            okMask = np.where(gal.inMask>=1.0)[0]
            # if either top or bottom edge of this slit lies within range of an allocated slit, reject:
            olap0 = np.where((ty0[i]>ty0[okMask]) & (ty0[i]<ty1[okMask]) )[0]
            olap1 = np.where((ty1[i]>ty0[okMask]) & (ty1[i]<ty1[okMask]) )[0]
            if ( (len(olap0)>0 ) | (len(olap1)>0) ):
                # reject this slit
                #print 'rejected'
                #print i,gal.prob[i],zmag[i],zmag[i]-mag[i],yfitLim[i],bluefitLim[i]
                pass
            
            else: 
                print 'added slit!!'
                #print i,gal.prob[i],zmag[i],zmag[i]-mag[i],yfitLim[i],bluefitLim[i]
                
                gal.inMask[i]=1.0 # add to mask!
                added[i]=1.0
          
        okMask = np.where(gal.inMask>=1.0)[0]
        
        print len(okMask)

        # --
        aok=np.where(added==1.0)[0]
#        print np.transpose((gal['ID'][aok],gal['IRAC1'][aok],gal['ZMAG'][aok]-gal['IRAC1'][aok],gal.prob[aok]))
        print np.transpose((gal['IRAC1'][aok],gal['ZMAG'][aok]-gal['IRAC1'][aok]))
        
        fillname = 'best'+name+'outfill%s.txt'%(jj+1)
        np.savetxt( fillname,np.transpose( (gal['RA'][okMask],gal['DEC'][okMask],gal.prob[okMask],gal['ID'][okMask]) ),fmt=('%.8f %.8f %.4f %.0f') )
        xtrname = 'best'+name+'extras%s.txt'%(jj+1)
        np.savetxt( xtrname,np.transpose( (gal['RA'][aok],gal['DEC'][aok],gal.prob[aok],gal['ID'][aok]) ),fmt=('%.8f %.8f %.4f %.0f' ))
        
        
        # reset must-haves back to zero for next mask!:
        gal.prob[oka] = 0.0
        gal.inMask[oka] = 0.0
        
if __name__ == '__main__':
    name = 'SPT-0546'
    nMasks=5
    det='Ham'
    addFillers(name,aMasknums=np.arange(nMasks),det=det,isilent=True)
    
    
        
    
    
    
    
