#!/Users/clidman/Science/Programs/anaconda3/envs/iraf27/bin/python
# Python script to process WIRCam data

import WIRCam_library as WIRCam

def reduce(options):

    if options.config!=None:
        params=WIRCam.buildDictionary(options.config)
    else:
        params={}
        
    if options.setup:
        # Copy accross files that are used by SExtactor, Swarp and SCAMP
        WIRCam.setup(params,options)

    if options.list != None:
        # Create the lists that are used by the various tasks below
        WIRCam.createLists(params,options)
        
    if options.preprocess != None:
        # Copy accross the data, uncompress them, average along the 3rd dimentionm produce weight maps and flag maps
        WIRCam.preprocess(params,options)

    if options.skySub != None:
        # Sky subtract
        WIRCam.skySub(params,options)

    if options.pack != None:
        # Repack the data
        WIRCam.pack(params,options)
        
    if options.maskRemnants != None:
        # Mask remnants from bright stars
        WIRCam.maskRemnants(params,options)
        
    if options.removeBiasOffsets != None:
#        WIRCam.removeBiasOffsets(params,options,'IRAF')
        WIRCam.removeBiasOffsets(params,options,'')
        
    if options.SExtractor != None:
        # Run SExtractor in preparation for scamp and swarp
        WIRCam.SExtractor(params,options)
        
    if options.swarp != None:
        # Run Swarp
        WIRCam.swarp(params,options)
        
    if options.digitise != None:
        # Modify the weight maps produced by Swarp, update the BPM keyword
        WIRCam.digitise(params,options)
        
    if options.normalise != None:
        # Normalise the exposures with respect to one another
        WIRCam.normalise(params,options)
        
    if options.quality != None:
        # Determine seeing and noise
        WIRCam.quality(params,options)
        
    if options.combine != None and options.type=="ChipByChip":
        # Combine exposures chip by chip
        WIRCam.combineChipByChip(params,options)

    if options.normaliseChips != None:
        # Normalise the chips with respect to one another
        WIRCam.normaliseChips(params,options)

    if options.normaliseRuns != None:
        # Normalise the run with respect to another run
        WIRCam.normaliseRuns(params,options)

    if options.combine != None and options.type in ['All','FWHM','noise','FWHM_and_noise']:
        # Combine all exposures
        WIRCam.combineAll(params,options)

    if options.combine != None and options.type in ['coadd']:
        # Combine caodds
        WIRCam.combineCoAdds(params,options)

    if options.merge != None:
        # Merge the combined image ana weight maps into one file
        WIRCam.merge(params,options)

    if options.zeroPoint != None:
        # Determine the ZP using 2MASS
        WIRCam.zeroPoint(params,options)

    if options.qsoflags:
        # Remove temporary files
        WIRCam.qsoflags(params,options)

    if options.cleanup:
        # Remove temporary files
        WIRCam.cleanup(params,options)

    if options.deepcleanup:
        # Remove temporary files
        WIRCam.cleanup(params,options)
        WIRCam.deepcleanup(params,options)
        
    return


from optparse import OptionParser

if __name__ == '__main__':

    parser=OptionParser()

    parser.add_option('-c','--config',dest='config',
                      default=None,help='Configuration file')

    parser.add_option('-l','--list',dest='list',
                      default=None,help='Create a number of useful lists')

    parser.add_option('-s','--setup',dest='setup',
                      default=False, action="store_true",
                      help='Run setup')

    parser.add_option('-S','--SExtractor',dest='SExtractor',
                      default=None,
                      help='Run SExtractor')
    
    parser.add_option('-w','--swarp',dest='swarp',
                      default=None,
                      help='Run swarp')
    
    parser.add_option('-B','--pixelScale',dest='pixelScale',
                      default=None,
                      help='Putput pixel scale')
    
    parser.add_option('-p','--preprocess',dest='preprocess',
                      default=None,
                      help='Preprocess data (detrended | pre-sky)')

    parser.add_option('-u','--skySub',dest='skySub',
                      default=None,
                      help='Sky subtraction')

    parser.add_option('-b','--pack',dest='pack',
                      default=None,
                      help='Pack the sky subtracted frames')

    parser.add_option('-k','--cleanup',dest='cleanup',
                      default=False, action="store_true",
                      help='Clean up directory')

    parser.add_option('-K','--deepcleanup',dest='deepcleanup',
                      default=False, action="store_true",
                      help='Deep Clean')

    parser.add_option('-d','--digitise',dest='digitise',
                      default=None,
                      help='Digitise the bad pixel maps')

    parser.add_option('-n','--normalise',dest='normalise',
                      default=None,
                      help='Normalise the relative throughputs')

    parser.add_option('-q','--quality',dest='quality',
                      default=None,
                      help='Measure the noise and image quality')

    parser.add_option('-Q','--QC',dest='qc',
                      default=None,
                      help='file containing qc metrics on each image')

    parser.add_option('-f','--qsoflags',dest='qsoflags',
                      default=False, action="store_true",
                      help='Collate qso flags')

    parser.add_option('-N','--normaliseChips',dest='normaliseChips',
                      default=None,
                      help='Normalise chips 2, 3 and 4 wrt to chip1')

    parser.add_option('-F','--factors',dest='factors',
                      default=None,
                      help='Scale factors to apply to chips 2, 3 and 4 wrt to chip1')
    
    parser.add_option('-R','--normaliseRuns',dest='normaliseRuns',
                      default=None,
                      help='Normalise one run with respect to another')
    
    parser.add_option('-C','--combine',dest='combine',
                      default=None,
                      help='Combine data')

    parser.add_option('-m','--maskRemnants',dest='maskRemnants',
                      default=None,
                      help='Mask remnants')

    parser.add_option('-M','--merge',dest='merge',
                      default=None,
                      help='Merge the image and weight map')

    parser.add_option('-o','--output',dest='output',
                      default=None,
                      help='Output name')

    parser.add_option('-r','--removeBiasOffsets',dest='removeBiasOffsets',
                      default=None,
                      help='Remove Bias Offsets')

    parser.add_option('-t','--type',dest='type',
                      default=None,
                      help='Type of combining')

    parser.add_option('-z','--zeroPoint',dest='zeroPoint',
                      default=None,
                      help='Determine zero point')

    parser.add_option('-P','--passband',dest='passband',
                      default=None,
                      help='Passband')

    
    (options,args)=parser.parse_args()

    reduce(options)
    
