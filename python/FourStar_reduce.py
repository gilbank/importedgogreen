#!/Users/clidman/Science/Programs/anaconda3/envs/iraf27/bin/python
# Python script to process FourStar data

import FourStar_library as fourStar

# ----------------  STEPS ------------
# i) Preprocess the data
#
# ii) Create a flat from images of twilight
#
# iii) Flat field the data
#
# iv) Sky subtract
#
# v) Combine
#
# vi) Refine astrometry, if required
#
# vii) Determine ZPs
#
# ---------------- Changes -----------

def reduce(options):

    params=fourStar.buildDictionary(options.config)
    
    if options.filelist:
        fourStar.filelist(params,options)

    if options.preProcess:
        fourStar.preProcess(params,options)

    if options.twilightFlat!=None:
        fourStar.createTwilightFlat(params,options)

    if options.sciFlat!=None:
        fourStar.createSciFlat(params,options)

    if options.badPixel:
        fourStar.createBadPixelMasks(params,options)

    if options.detrend!=None:
        fourStar.detrend(params,options)

    if options.skySubtract!=None:
        fourStar.skySubtract(params,options)

    if options.normaliseGains!=None:
        # This could be done at the flat fielding stage
        fourStar.normaliseGains(params,options)

    if options.weights:
        fourStar.createWeightMaps(params,options)

    if options.Merge!=None:
        fourStar.Merge(params,options)

    if options.findObjects!=None:
        fourStar.findObjects(params,options)

    if options.addFrames!=None:
        fourStar.addFrames(params,options)

    if options.mergeFrames!=None:
        fourStar.mergeFrames(params,options)

    if options.zeroPoint!=None:
        fourStar.zeroPoint(params,options)

    if options.examineOffsets!=None:
        fourStar.examineOffsets(params,options)

    return


from optparse import OptionParser

if __name__ == '__main__':

    parser=OptionParser()

    parser.add_option('-c','--config',dest='config',
                      default='Fourstar.config',help='Configuration file')

    parser.add_option('-l','--filelist',dest='filelist',
                      action='store_true',
                      default=False,help='list info about each file')

    parser.add_option('-p','--preProcess',dest='preProcess',
                      action='store_true',
                      default=False,help='Preprocess the data')

    parser.add_option('-t','--twilightFlat',dest='twilightFlat',
                      default=None,help='List of Twilight flats')

    parser.add_option('-u','--sciFlat',dest='sciFlat',
                      default=None,help='List of Science flats')

    parser.add_option('-o','--output',dest='output',
                      default=None,help='Output')

    parser.add_option('-b','--badPixel',dest='badPixel',
                      action='store_true',
                      default=False,help='Create the bad pixel masks from the flat fields')

    parser.add_option('-d','--detrend',dest='detrend',
                      default=None,help='Dark Subtract and Flatfield the data')

    parser.add_option('-F','--flat',dest='flat',
                      default=None,help='Use the specified flat')

    parser.add_option('-N','--noFlat',dest='noFlat',default=False,
                      action='store_true',
                      help='Do not apply the flat field')

    parser.add_option('-s','--skySubtract',dest='skySubtract',
                      default=None,help='Sky subtract')

    parser.add_option('-S','--skipFirstPass',dest='skipFirstPass',
                      default=False,action='store_true',help='Skip the first pass sky subtraction')

    parser.add_option('-n','--normaliseGains',dest='normaliseGains',
                      default=None,help='Normalise the gains')

    parser.add_option('-C','--combined',dest='combined',
                      action='store_true',
                      default=False,help='Apply the normalisation on the images combined by XDIMSUM')

    
    parser.add_option('-M','--Merge',dest='Merge',
                      default=None,help='Merge the four extensions into one ')

    parser.add_option('-w','--weights',dest='weights',
                      default=None,help='Create Weight Maps')

    parser.add_option('-r','--regions',dest='regions',
                      default=None,help='Bad pixel regions')

    parser.add_option('-f','--findObjects',dest='findObjects',
                      default=None,help='Add a WCS and run SExtractor')

    parser.add_option('-a','--addFrames',dest='addFrames',
                      default=None,help='Scamp, Swarp and imcombine')
    
    parser.add_option('-m','--mergeFrames',dest='mergeFrames',
                      default=None,help='Merge the image and weight maps')

    parser.add_option('-z','--zeroPoint',dest='zeroPoint',
                      default=None,help='Compute the ZP')

    parser.add_option('-P','--passband',dest='passband',
                      default='Ks',help='Passband')

    parser.add_option('-e','--examineOffsets',dest='examineOffsets',
                      default=None,help='Examine the offsets')

    
    (options,args)=parser.parse_args()

    reduce(options)
    
