from optparse import OptionParser
import Constants
import GoGREEN_Library as GoGREEN
from astropy.io import fits,ascii
from astropy import wcs
import GoGREEN_spectra as ggsp
import numpy as np
import os
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import sys
import getopt
import re
import matplotlib.ticker as plticker
from astropy.convolution import convolve, Box1DKernel
'''Written by Michael Balogh 31 Jan 2017

Explore GoGREEN spectral extractions for a specific mask.  Presents a figure showing the direct image, slit position, 
1D and 2D spectra, trace and S/N vs magnitude.  It is set up to only present priority=1 galaxies, ordered from brightest to faintest.


Top row:  z-band image with the slit position (red rectangle) and object centre (blue cross indicated).  Plot on the right
shows S/N (at 850 +/- 50 nm) as a function of total z magnitude.
Second row: An cutout of one raw, input 2D frame.  The slit of interest is in the middle; cutout is large enough to 
show surrounding slits.  Observations are dithered in the spectral direction so this is not aligned horizontally with
the following plots
Third row:  Extracted 1D spectrum.  Thick black line shows the final spectrum, smoothed (5 pixel boxcar).  The red and blue
lines show extraction of the lower and upper traces of the nod-and-shuffle pair, respectively.  
Fourth row: Sky subtracted, wavelength calibrated 2D spectrum, aligned with the 1D spectrum above it.  The trace of the two
spectra in the n/s pair are indicated with red lines
Last row: The trace fit to the 2D spectra, used for the extraction.

CALLING SEQUENCE
python GoGREEN_inspect.py -c ggextract.config cluster maskname

cluster - the name of the cluster, e.g. SpARCS0035
maskname - the name of the spectroscopic mask, e.g. GS2016BLP001-07
ggextract.config - a configuration file that sets default parameters for the extraction.  May be omitted.

REQUIRED PYTHON MODULES
astropy
os
Constants
GoGREEN_Library
GoGREEN_spectra
matplotlib
sys
getopt
re

NOTES
This expects data to be in the GoGREEN directory structure, with the GoGREEN_data environment variable set.
For each mask, the following files are needed:
	GOGREEN.fits master table in the Software config directory
	rfcs_maskname or cs_maskname - the MEF FITS file produced by the ggspecred.cl pipeline
	preimage as given in the GOGREEN.fits table.  Should exist in the Imaging/ subdirectory for the cluster
	maskname.lis file to identify filenames of raw images
	mgs*.fits file for the first entry in maskname.lis.  This is an intermediate product of the data reduction pipeline.  Optional.
	maskname.onesn file produced by ggspecred.cl pipeline, giving S/N and magnitudes for the extracted spectra.
'''
parser=OptionParser()
parser.add_option("-c", "--configFile", dest="configFile",
				  default=None, help="Configuration file")
(options, args) = parser.parse_args()

try:
	cname=str(args[0])
except:
	cname = raw_input('Cluster name? e.g. SXDF64')
try:
	maskname=str(args[1])
except:
	maskname = raw_input('Mask name? e.g. GS2014BLP001-08')
try:
	apnum=int(args[2])
except:
	apnum=0
traceorder=0
# Set maglim to only inspect spectra fainter than this limit.
#maglim=23.5
maglim=0

constants=Constants.GoGREENPath()
dir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"
params=GoGREEN.buildDictionary(options.configFile)
flagfile=dir+maskname+'.flags'
f = open(flagfile, 'wb')
try:
	with open(options.configFile) as file:
		pass
except IOError as e:
	print "Configuration file not specified or does not exist; using defaults"


showplots=True
if 'weight_method' not in params.keys(): params['weight_method']='no_weight'
weight_method = params['weight_method']
# 2.  Data-specific parameters
if 'width_arcsec' not in params.keys(): params['width_arcsec']=.7 # Default sigma width in arcseconds of spatial profile
width_arcsec = np.float(params['width_arcsec'])
if 'slittrim' not in params.keys(): params['slittrim']=2.      # number of pixels at slit edge to ignore
slittrim = np.int(params['slittrim'])
# 3. Fitting parameters
if 'f0' not in params.keys(): params['f0']=0.5 # f0-f1 is the range of pixels (as fraction of total) to use for a single best spatial profile
f0p = np.float(params['f0'])
if 'f1' not in params.keys(): params['f1']=0.85
f1p = np.float(params['f1'])
if 'dxmax' not in params.keys(): params['dxmax'] = 2.      # max shift in predicted spatial position, in pixels
dxmax = np.float(params['dxmax'])
if 'dxstep' not in params.keys(): params['dxstep']=0.1   # step to search for spatial shift
dxstep = np.float(params['dxstep'])
if 'dwmax' not in params.keys(): params['dwmax']=0.2    # max shift in spatial profile (sigma), in arcseconds
dwmax = np.float(params['dwmax'])
if 'dwstep' not in params.keys(): params['dwstep']=0.025 # step to search for sigma shift
dwstep = np.float(params['dwstep'])
if 'trace1' not in params.keys(): params['trace1']=0.2    # trace1 and trace2 are limits (in fraction)within which to fit the trace
trace1 = np.float(params['trace1'])
if 'trace2' not in params.keys(): params['trace2']=.85
trace2 = np.float(params['trace2'])
if 'trace_step' not in params.keys(): params['trace_step']=0.05 # step (in fraction) for fitting trace.
trace_step = np.float(params['trace_step'])

trace1=0.1
trace2=0.95
trace_step=0.05
onedprefix='oned_'
imname="rfcs_"+maskname
onedfile=dir+onedprefix+imname+".fits"
if not os.path.isfile(onedfile):
	onedfile=onedfile+'.gz'
if not os.path.isfile(onedfile):
	imname="cs_"+maskname
	onedfile=dir+onedprefix+imname+".fits"
if not os.path.isfile(onedfile):
	print onedfile
try:
	reducedfile=dir+imname+".fits.gz"
	if not os.path.isfile(reducedfile):
		reducedfile=reducedfile.replace('.gz','')
	mdf,mdfhd = fits.getdata(reducedfile,header=True,ext=('mdf',1))
	hdulinput=fits.open(reducedfile)
except:
	print 'No fits file ',reducedfile
	sys.exit()
header_primary=hdulinput[0].header

nsciext=header_primary['NSCIEXT']
header_primary['NEXTEND']=1+3*nsciext
#if ((header_primary['NODAYOFF']<0) | (header_primary['INSTRUME']=='GMOS-N')):
if ((header_primary['NODAYOFF']<0) | (header_primary['DETECTOR']=='GMOS + e2v DD CCD42-90')):
	reverse_NODAB=True
	#reverse_NODAB=False
else:
	reverse_NODAB=False

try:
	masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
	mdat = fits.getdata(masterfile)
except:
	print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
	print "Stopping"
	stop()
#preimage_root=mdat['Preim_name'][np.where(mdat['cluster']==cname)]
preimage_root=mdat['z_image'][np.where(mdat['cluster']==cname)]
imdir=os.getenv('GoGREEN_data')+'/Data/reduced/'+cname+'/Imaging/GMOS/Z/'
preimage=imdir+preimage_root[0]+'.fits.gz'
if not os.path.isfile(preimage):
	preimage=preimage.replace('.gz','')
try:
	preim,preim_header=fits.getdata(preimage,header=True)
	imagewcs=wcs.WCS(preim_header)
except:
	print 'No preimage ',preimage
	sys.exit()
#preim_xscale=np.max([np.abs(preim_header['CD1_1']),np.abs(preim_header['CD1_2'])])*3600
#preim_yscale=np.max([np.abs(preim_header['CD2_1']),np.abs(preim_header['CD2_2'])])*3600
preim_xscale=(np.abs(preim_header['CD1_1'])+np.abs(preim_header['CD1_2']))*3600
preim_yscale=(np.abs(preim_header['CD2_1'])+np.abs(preim_header['CD2_2']))*3600
#print preim_xscale,preim_yscale
preim=preim-np.median(preim)

lisfile=dir+maskname+'.lis'
ref_sciimage=ascii.read(lisfile,data_start=0)[0][0]
ref_flatimage=ascii.read(lisfile,data_start=0)[0][1]
try:
	scimosaic_fn=dir+'mgs'+ref_sciimage+'.fits.gz'
	flatmosaic_fn=dir+'rg'+ref_flatimage+'_comb.fits.gz'
	if not os.path.isfile(scimosaic_fn):
		scimosaic_fn=scimosaic_fn.replace('.gz','')
	if not os.path.isfile(flatmosaic_fn):
		flatmosaic_fn=flatmosaic_fn.replace('.gz','')
	scimosaic=fits.getdata(scimosaic_fn,ext=('sci',1))
	flatmosaic,flathd=fits.getdata(flatmosaic_fn,header=True,ext=('MDF',1))
	scimosaic=np.transpose(scimosaic)
	mosaic_exists='yes'
except:
	print 'No mosaic image',scimosaic_fn,' exists.  Ignoring.'
	mosaic_exists='no'
try:
	snr_fn=dir+maskname+'.onedsn'
	sndat=ascii.read(snr_fn,data_start=2)
	snr_exists=True
except:
	snr_exists=False
pixscale=header_primary['PIXSCALE']
nodpix=header_primary['NODAYOFF']*2/pixscale
nodpix=np.abs(nodpix)
width_pix=width_arcsec/pixscale
dx = np.arange(-dxmax,dxmax,dxstep)
dw = np.arange(-dwmax,dwmax,dwstep)/pixscale # in pixels  
if apnum > 0: 
	irange=range(apnum,nsciext+1)
else:
	irange=range(1,nsciext+1)
	magsort=True
	p1galaxies=np.where((mdf['priority']=='1') & (mdf['MAG']>=maglim))
	if magsort: irange=mdf['EXTVER'][p1galaxies][np.argsort(mdf['MAG'][p1galaxies])]
usercontinue=True
for i in irange:
	if usercontinue:
		subim=hdulinput['sci',i].data
		hd=hdulinput['sci',i].header
		subvar=hdulinput['var',i].data
		varhd=hdulinput['var',i].header
		subdq=hdulinput['dq',i].data
		dqhd=hdulinput['dq',i].header
		pix=np.arange(1,hd['naxis1']+1)
		ll=hd['crval1']+(pix-hd['crpix1'])*hd['cd1_1']
		# --- Try to find best offset of profile pair within slit:####
		npix=subim.shape[0]
		# Centre of slit.  Convention is that index starts at 0 and references the bottom of the pixel
		#slit_centre=(npix/2.)
		# Hmm... why do I say that?  imshow treats index as representing centre of pixel.  Why doesn't that make sense?
		slit_centre=((npix-1)/2.)
		# Offset is expected position of the spectrum at the bottom of the slit
		offset=slit_centre-nodpix/2.0
		xsec_avg = ggsp.getmedprof(subim,subdq,f0p,f1p)
		vsec_avg = ggsp.getmedprof(subvar,subdq,f0p,f1p)
		(pk,pw,useProf)=ggsp.getprof(xsec_avg,vsec_avg,offset,width_pix,dx,dw,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)
		dofit=True
		dxarray=[]
		dwarray=[]
		pixarray=[]
		dwfixed=dw*0+dw[pw]
		profarray=[]
		for specfrac in np.arange(trace1,trace2,trace_step):
			xsec = ggsp.getmedprof(subim,subdq,f0=specfrac,f1=specfrac+trace_step)
			vsec = ggsp.getmedprof(subvar,subdq,f0=specfrac,f1=specfrac+trace_step)
			(pktry,pwtry,Proftry)=ggsp.getprof(xsec,vsec,offset,width_pix,dx,dwfixed,width_pix,nodpix,slittrim,reverse_NODAB=reverse_NODAB)
			dxarray=np.append(dxarray,offset+dx[pktry])
			dwarray=np.append(dwarray,dw[pwtry])
			pixval=(specfrac+trace_step/2.)*subim.shape[1]
			pixarray=np.append(pixarray,pixval)
			profarray=np.append(profarray,Proftry)
		profarray=np.reshape(profarray,[-1,Proftry.size])
		x_model=[]
		y_model=[]
		coeff2=np.array([])
		try:
			outim,hdx = fits.getdata(onedfile,header=True,ext=('sci',i))
			if hdx['TCOEF2']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF2']))
			if hdx['TCOEF1']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF1']))
			if hdx['TCOEF0']!=0: coeff2 = np.append(coeff2,float(hdx['TCOEF0']))
			order=np.size(coeff2)-1
			print "Using existing fit of order ",order," and coefficients ",coeff2
			bestfit2 = np.polyval(coeff2,pixarray) 
		except:
			print'WARNING: No existing trace coefficients in',onedfile
			print 'Using order=0 to fit'
			traceorder=0
			order=0
			(bestfit2,coeff2,x_model,y_model)=ggsp.getFit(pixarray,dxarray,order)
		postrace=np.polyval(coeff2,np.arange(subim[0,:].size))
		negtrace=postrace+nodpix
		(exdata,exvar,exdq,expos,exneg)=ggsp.SpExtract(subim,subvar,subdq,weight_method,postrace,width_pix+dw[pw],nodpix,slittrim,reverse_NODAB=reverse_NODAB)
		badreg=np.where(np.abs(expos-exneg) > 5.*np.sqrt(exvar))
		target=np.where(mdf['extver']==i)
		RA=mdf['RA'][target][0]*15.
		DEC=mdf['DEC'][target][0]
		radec=np.array([[RA,DEC]])
		pixels=imagewcs.wcs_world2pix(radec,0)
		#pixels=imagewcs.wcs_world2pix(radec,1)
		# I don't know why x and y are reversed here!  Works for GMOS-N Hamamatsu data
		yccd=pixels[0][0]
		xccd=pixels[0][1]
		#yccd=mdf['x_ccd'][target][0]
		#xccd=mdf['y_ccd'][target][0]
		defstampsize=5/preim_xscale*np.array([1,1])
		ximage=[int(np.max([xccd-defstampsize[0]/2+.5,0])),int(np.min([xccd+defstampsize[0]/2+.5,np.shape(preim)[0]]))]
		yimage=[int(np.max([yccd-defstampsize[1]/2+.5,0])),int(np.min([yccd+defstampsize[1]/2+.5,np.shape(preim)[1]]))]
		#ximage=[np.max([xccd-defstampsize[0]/2,0]),np.min([xccd+defstampsize[0]/2,np.shape(preim)[0]])]
		#yimage=[np.max([yccd-defstampsize[1]/2,0]),np.min([yccd+defstampsize[1]/2,np.shape(preim)[1]])]
		centralstampsize=1/preim_xscale*np.array([1,1])
		xcent=[int(np.max([xccd-centralstampsize[0]/2+.5,0])),int(np.min([xccd+centralstampsize[0]/2+.5,np.shape(preim)[0]]))]
		ycent=[int(np.max([yccd-centralstampsize[1]/2+.5,0])),int(np.min([yccd+centralstampsize[1]/2+.5,np.shape(preim)[1]]))]
		#xcent=[int(np.max([xccd-centralstampsize[0]/2,0])),int(np.min([xccd+centralstampsize[0]/2,np.shape(preim)[0]]))]
		#ycent=[int(np.max([yccd-centralstampsize[1]/2,0])),int(np.min([yccd+centralstampsize[1]/2,np.shape(preim)[1]]))]
		xstamp=xccd-ximage[0]
		ystamp=yccd-yimage[0]
		#xstamp=xccd-ximage[0]
		#ystamp=yccd-yimage[0]


		fig,axarr=plt.subplots(5,gridspec_kw={'height_ratios':[2,1,1,1,1]})
		# Top figure:  postage stamp of target from preimage  
		preim_sub=preim[ximage[0]:ximage[1],yimage[0]:yimage[1]]
		minval=np.nanmin(preim_sub)
		maxval=np.nanmax(preim[xcent[0]:xcent[1],ycent[0]:ycent[1]])
		vmin=minval
		vmax=0.99*maxval
		#print minval,maxval
		#print vmin,vmax
		#minval=30500
		#maxval=32000
		axarr[0].axis('off')
		ax=fig.add_subplot(4,2,1)
		ax.axis('off')
		ax.imshow(preim_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
		ax.add_patch(patches.Rectangle(
			(xstamp-1/2./preim_xscale,ystamp-3./4/preim_yscale),
			1/preim_xscale,
			3/preim_yscale,
			fill=False,
			edgecolor='red')
		)
		ax.add_patch(patches.Rectangle(
			(xstamp-1/2./preim_xscale,ystamp-9./4/preim_yscale),
			1/preim_xscale,
			3/preim_yscale,
			fill=False,
			edgecolor='yellow')
		)

		ax.scatter(xstamp,ystamp,marker='x')
		ax.set_xlim(0,int(defstampsize[0]))
		ax.set_ylim(0,int(defstampsize[1]))
		# Figure: 2D dispersed image 
		if mosaic_exists=='yes':
			secy1=flatmosaic['SECY1'][target][0]
			secy2=flatmosaic['SECY2'][target][0]
			secx1=flatmosaic['SECX1'][target][0]
			secx2=flatmosaic['SECX2'][target][0]
			specx=int(0.5*(secx1+secx2))
			#specy=int(0.5*(secy1+secy2))
			# This references the centre of the n&s pair
			specy=secy1
			defmossize=np.array([1500,160])
			# Something is screwy when doing with preimages.  x and y swapped or something?
			relpixscale=1.
			#if re.compile('SXDF').match(cname) or re.compile('COS').match(cname): relpixscale=2.

			xmosimage=[int(np.max([specx/relpixscale-defmossize[0]/2,0])),int(np.min([specx/relpixscale+defmossize[0]/2,np.shape(scimosaic)[0]]))]
			ymosimage=[int(np.max([specy/relpixscale-defmossize[1]/2,0])),int(np.min([specy/relpixscale+defmossize[1]/2,np.shape(scimosaic)[1]]))]
			mosaic_sub=scimosaic[xmosimage[0]:xmosimage[1],ymosimage[0]:ymosimage[1]]
			vmin=-1000
			vmax=1000				
			#mosaic_sub=np.transpose(mosaic_sub[:][::-1])[::-1]
			# Following is right for SXDF64
			mosaic_sub=np.transpose(mosaic_sub[:][::-1])

			#fig3,axarr3=plt.subplots(1)
			#axarr3.imshow(mosaic_sub,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'))
			#axarr[1].imshow(mosaic_sub[:,::-1],aspect=0.88,vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'),origin='fuck')
			axarr[1].imshow(mosaic_sub,aspect='auto',vmax=vmax,vmin=vmin,cmap=plt.get_cmap('gray'),origin='lower')
			#axarr[4].set_xlim()
			#thismanager=plt.get_current_fig_manager()
			#thismanager.resize(2400,160)
			#plt.show(block=False)
		# Figure: Extracted spectrum
		axarr[2].plot(ll,expos,c='r',linewidth=0.5)
		axarr[2].plot(ll,exneg,c='b',linewidth=0.5)
		axarr[2].plot(ll,convolve(exdata,Box1DKernel(5)),c='k')
		axarr[2].scatter(ll[badreg],exdata[badreg],c='y',s=20)
		axarr[2].set_xlim(ll[0],ll[-1])
		#axarr[2].set_ylim(np.nanmin(exdata[:-200]),np.nanmax(exdata[:-200]))
		axarr[2].set_ylim(np.nanmean(exdata[:-200])-2.*np.nanstd(exdata[:-200]),np.nanmean(exdata[:-200])+2.*np.nanstd(exdata[:-200]))
			
		# Figure: spatial profile
		ypix=np.arange(np.size(xsec_avg[slittrim:-slittrim]))
		#axarr[3].plot(ypix+slittrim,xsec_avg[slittrim:-slittrim])
		#axarr[3].plot(ypix+slittrim,useProf[slittrim:-slittrim])
		extent=[ll[0],ll[-1],1,20]
		# Figure: 2D sky-subtracted slit with trace
		maxval=0.99*np.nanmax(np.abs(xsec_avg[2:18]))
		axarr[3].imshow(subim[:,:],extent=extent,origin='lower',aspect=10,vmax=maxval,vmin=-maxval,cmap=plt.get_cmap('gray'))
		axarr[3].plot(ll,postrace,'r')
		axarr[3].plot(ll,negtrace,'r--')
		axarr[3].set_xlim(ll[0],ll[-1])
		# Figure: trace position of positive flux
		axarr[4].plot(pixarray,dxarray, 'ro')
		try:
			axarr[4].plot(x_model,y_model,'bo')
		except:
			pass
		axarr[4].plot(pixarray,bestfit2,'b')
		axarr[4].set_xlim(0,ll.size)
		axarr[4].set_ylim(offset-dxmax,offset+dxmax)
		thismanager=plt.get_current_fig_manager()
		thismanager.resize(1200, 1000)
		#thismanager.window.SetPosition((500,0))
		if snr_exists:
			ax=fig.add_subplot(4,2,2)
			thisgal=np.where(sndat['ID']==mdf['ID'][target])
			ax.scatter(sndat['Mag'],sndat['S/N'],facecolor='none',edgecolor='k',s=50)
			ax.scatter(sndat['Mag'][thisgal],sndat['S/N'][thisgal],c='r',s=50)
			ax.set_yscale('log')
			ax.get_yaxis().set_major_formatter(plticker.ScalarFormatter())
			ax.set_ylim(0.5,500)
			plt.ylabel ("S/N@850nm")
			plt.xlabel ("ZMAG")
		plt.show(block=False)
		#fig2=plt.figure()
		#fig2.show()
		print "Slit ",i,":"
		print "ID: ",mdf['id'][target][0],"ZMAG=",mdf['mag'][target][0],"priority=",mdf['priority'][target][0]
		#try:
	   # 	userinput=raw_input('Continue? (n to quit): ').split()[0]
	   # 	if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': usercontinue=False
	   # except:
	   # 	pass
		flagerror=True
		while flagerror:
			userinput=raw_input('Flag? (integer or n/N to quit): ').split()[0]
			if userinput=='n' or userinput=='N' or userinput=='no' or userinput=='NO' or userinput == 'No': 
				usercontinue=False
				flagerror=False
			else:
				try:
					flag=int(userinput)
					flagerror=False
				except:
					flagerror=True
				if not flagerror:
					try:
						comment=raw_input('Comment?')
					except:
						comment=''
				f.write(str(i)+' '+str(flag)+' '+comment+'\n')
		plt.close('all')
	else:
		plt.close('all')
		f.close()