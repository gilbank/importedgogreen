#!/Users/clidman/Science/Programs/Ureka/variants/common/bin/python
# Python scripts for processing LIRIS data on WHT

# ----------------  STEPS ------------
# i) Preprocess the data

# ii) Create dome flats

# iii) Process science data

# iv) Determine ZPs

# v) Meger and add a version number

# vi) Adjust ZPs if required

# ---------------- Changes -----------

# 26/04/2015
# If available, creates the dark frame

import GoGREEN_libraryLIRIS as LIRIS
from optparse import OptionParser

def reduce(options):
    params=LIRIS.buildDictionary(options.config)

    if options.adjustZP > 0:
        LIRIS.adjustZP(params,options)
        return

    if options.astrometricRef != None and not options.scamp:
        LIRIS.createAstrometricReference(params,options)
        return

    if options.combine!=None:
        LIRIS.combine(params,options)
        return


    # Set up the relevant directories and work out the observing dates
    dates=LIRIS.setUp(params)

    # Reduce each date in turn
    for date in dates:
    
        # We work with file lists. In future, we should derive this from
        # the headers or the log
        # There is one list for the dome flats with the lamp on
        # Another list for the dome flats off
        # And one list for each object

        if options.preProcess:
            LIRIS.preProcess(params,date)

        if options.createFlat:
            # Dome flat and bad pixel mask
            LIRIS.createFlat(params,date)

        if options.reduceObjects:
            LIRIS.reduceObjects(params,date,options.target)

        if options.scamp:
            LIRIS.scamp(params,date,options)

        if options.determineZP:
            LIRIS.determineZP(params,date,options)

        if options.merge:
            LIRIS.merge(params,date,options.target,options.merge)

    return

if __name__ == '__main__':

    parser=OptionParser()

    parser.add_option('-c','--config',dest='config',
                      default='LIRIS.config',help='Configuration file')


    parser.add_option('-t','--target',dest='target',
                      default=None,help='target')

    parser.add_option('-p','--preProcess',dest='preProcess',
                      action='store_true',
                      default=False,help='Preprocess the data')

    parser.add_option('-u','--update',dest='update',
                      action='store_false',
                      default=True,help='Update the WCS if requested')

    parser.add_option('-a','--adjustZP',dest='adjustZP',
                      default=-99.9,help='Adjust the ZP if required')

    parser.add_option('-A','--astrometricRef',dest='astrometricRef',
                      default=None,help='Create the external astrometric reference catalogue')

    parser.add_option('-s','--scamp',dest='scamp',
                      action='store_true',
                      default=False,help='SCAMP only')

    parser.add_option('-C','--combine',dest='combine',
                      default=None,help='Combine after Swarp')

    parser.add_option('-d','--date',dest='date',
                      default=-99.9,help='Adjust the ZPs for this date')

    parser.add_option('-f','--createFlat',dest='createFlat',
                      action='store_true',
                      default=False,help='Creatge the flat fields')

    parser.add_option('-r','--reduceObjects',dest='reduceObjects',
                      action='store_true',
                      default=False,help='Reduce the science data')

    parser.add_option('-z','--determineZP',dest='determineZP',
                      action='store_true',
                      default=False,help='Determine the ZP')

    parser.add_option('-m','--merge',dest='merge',
                      default=None,help='Merge exposure map with image and add a version number')

    (options,args)=parser.parse_args()

    reduce(options)
    
