import os,sys
from astropy.io import fits
from astropy.io.fits import Column
from numpy.lib import recfunctions as recf
import numpy as np
from time import gmtime
# simple manipulation of Master table, GoGREEN.fits




"""
MLB:
It would certainly be helpful to have a simple code to make quick
modifications.  I imagine two basic functions, either to add a column
(e.g. codename cluster colname addcol=True) or to edit a value (e.g.
codename cluster colname value)
"""

def modTable(mdat,cluster,colname,option):
    if(option=='add'):
        # cluster name is not needed to add a column
        try:
            blankdata = np.repeat(np.nan,len(mdat))
            odat = recf.append_fields(mdat, names=colname, data=blankdata, asrecarray=True, usemask=False)
            return odat
        except:
            print 'could not add column %s\n'%colname
            sys.exit(1)

    elif(option=='print'):
        ok=np.where(mdat['cluster']==cluster)[0]
        try:
            print mdat[colname][ok]
            if(len(ok)==0):
                print '\ncluster %s not found\n'%cluster
                print 'clusters:\n'
                print mdat['cluster']
            return mdat
        except:
            try:
                print mdat[colname]
            except:
                print '\ncolname %s not found\ncolumns present:\n'%colname
                print mdat.columns

    else:
        value=option
        ok=np.where(mdat['cluster']==cluster)[0]
        try:
#            print value
            mdat[colname][ok]=value
            return mdat
        except:
            print 'could not set colname %s = %s\n'%(colname,value)


def printUsage():
    print '\nusage:\n python GoGREEN_modMasterTable.py cluster colname option\n\n'+\
          'where option is:\n add -- add new column of name colname\n print -- print value of colname for cluster\n'+\
          ' value -- set colname value to value for cluster\n'
if __name__ == "__main__":
    #    name='SPT-0546' ; nMasks=5 ; det='Ham' ; isilent=True

    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mdat = fits.getdata(masterfile)



    try:
        clusname = sys.argv[1]
        colname = sys.argv[2]
        option = sys.argv[3]
    except:
        printUsage()
        sys.exit(1)


    odat = modTable(mdat,clusname,colname,option)
    if (option=='print'):
        sys.exit(0)
    else:
        # need to save new output
#        fits.writeto('fbar.fits',odat,clobber=True)
        # Add modification date:
        fits.writeto(masterfile,odat,clobber=True) # update in-place. Overwrite!
        # e.g. 2015-02-13T13:13:53 UT
        t=gmtime()
        dateHDU = '%04d-%02d-%02dT%02d:%02d:%02d UT'%(t.tm_year,t.tm_mon,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec)
        fits.setval(masterfile,keyword='DATE-HDU',value=dateHDU,ext=1)