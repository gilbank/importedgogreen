from astropy.io import fits,ascii
from astropy import wcs

import os,sys
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle

from GoGREEN_utils import getPhotFilename
from GoGREEN_mkcatCat import radec2xyGmskcreate, crudeSky2xy

try:
    from astropy.coordinates import SkyCoord
except:
    print 'We seem to have an older version of astropy (probably from Ureka).\nTrying to work around'
    from astropy.coordinates import ICRS as SkyCoord

import astropy.units as u
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

from GoGREEN_checkPhotCat import getZphWts


try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)


ilogin=False
ilogintmp=False
if not os.path.isfile('login.cl'):
    try:
        irafhome = os.getenv('HOME')+'/iraf'
#        irafhome = os.getenv('UR_DIR')#+'/iraf'
        if(os.path.isfile(irafhome+'/login.cl')):
            ilogin=True
            ilogintmp=True # need to delete at home
            os.system('cp '+irafhome+'/login.cl .')
        else:
            pass # no login found. need to die
            print
            print "no login.cl found. copy to current directory to run this task"
            print
            exit(1)
    except:
        print
        print "no login.cl found. copy to current directory to run this task"
        print

else:
    pass
    # got login.cl here. okay


try:
    from pyraf import iraf
    iraf.gemini()
    iraf.gmos()
    iraf.mostools()
    iraf.stsdas()
    iraf.tables()
    iraf.ttools()
    ipyraf = True
except:
    ipyraf=False
    print
    print "The required gemini IRAF/pyraf tasks are not available"
    print
    exit(1)


def cats2OT(clusname,masknum):

# Do same as cats2OT.cl
#real slitx {1.0,prompt="Slit width in arcseconds"}
#real slity {3.0,prompt="Slit length in arcseconds"}

    slitx = 1.0 #arcsec
    slity = 3.0

    #masknum=1
    #clusname='SXDF60'

    filename='best'+clusname+'out%s.txt'%masknum
    outOTname = 'best'+clusname+'_m%s_OT.fits'%masknum
    ra,dec,pwt,idnum = np.loadtxt(filename,unpack=True)#,comment='#')

    # Need to assign gmmps priority flags.
    # SETUP =0
    # science =1
    # filler = 2
    pri = np.ones(len(pwt))
    stars = np.where(pwt>8.0)[0]
    pri[stars] = 0

    #print pri

    fillername='best'+clusname+'extras%s.txt'%masknum
    fillra,filldec,fillpwt,fillidnum = np.loadtxt(fillername,unpack=True)#,comment='#')
    ra = np.append(ra,fillra)
    dec = np.append(dec,filldec)
    fillpri = fillpwt*0+3
    pri = np.append(pri,fillpri)
    idnum = np.append(idnum,fillidnum)

    #print ra
    #print idnum
    #print pri
    np.savetxt('_tt',np.transpose((ra,dec,pri,idnum)),fmt='%f %f %i %i',header='tra tdec priority tid')

    # SXDF60_GMI.fits
    # will already have been created in current dir.
    # Might not be true for pre-imaging clusters, though. Do this explicitly later ***
    if not os.path.isfile("%s_GMI.fits"%clusname):
        print 'Creating _GMI file. May take some time...'
        # need to create:
        # modified from mkcatCat:
        photfile = getPhotFilename(clusname)
        obj = fits.getdata(photfile) # NOTE: this now includes all objects, including setup stars****
        np.savetxt('_tmp.txt',np.transpose( (obj['ID'],obj['RA'],obj['DEC'],obj['ZMAG']) ),fmt='%i %.7f %.7f %.3f')
        xp,yp = radec2xyGmskcreate(clusname,'_tmp.txt',obj)
        assert len(xp) == len(obj)
        os.system('rm -f _tmp.txt')
    else:
        print 'found '+clusname+'_GMI.fits'


    # Is there really no smarter way to do this in IRAF than?:
    os.system('rm -f testit.*')
    ss = iraf.tmatch(clusname+"_GMI.fits",\
                     "_tt", "testit.fits", "ID", "c4", 0.1, incol1=" ", incol2="c3", factor=" ",\
                     diagfile=" ", nmcol1=" ", nmcol2=" ", sphere="no", Stdout=1)
    if(1):
        for i in np.arange(len(ra)):
            # might be fewer lines in match. repetition?
        #    print i
            try:
                sss=iraf.tabpar('testit.fits',column="c3",row=i+1,Stderr=1)
            #    print sss
        #        print iraf.tabpar.value
                iraf.partab(iraf.tabpar.value,'testit.fits',column="priority",row=i+1)

                iraf.partab(slitx,'testit.fits',column="slitsize_x",row=i+1)
                iraf.partab(slity,'testit.fits',column="slitsize_y",row=i+1)
            except:
                pass
        # hopefully having this leftover column isn't a problem for GMMPS
        iraf.tchcol("testit.fits","c3","junk","","", verbose="no")
        os.system('mv -f testit.fits '+outOTname)


    else:
        # MLB recommends stsdas2objt
        # this might actually be easier anyway, as we can use priority from c4:

        no = "no"
        yes = "yes"
        #**** This seems to require the pre-image too.?? SKIP THIS FOR NOW
        sss = iraf.stsdas2objt ("testit",\
                           image="default", fl_wcs=no, outtable="stestit.tab", instrument="gmos",\
                           id_col="id", mag_col="mag", x_col="x_ccd", y_col="y_ccd", ra_col="RA",\
                           dec_col="DEC", pri_col="c4", other_col="", priority="2", logfile="",\
                           verbose=no, status=0, scanfile="", Stdout=1)
        print sss

#    iraf.tchcol("testit.tab","c3","junk","","")
 #   os.system('mv -f stestit.tab '+outOTname)
    print 'made '+outOTname
    os.system('rm -f _tt testit.fits')


if __name__ == "__main__":

    name = sys.argv[1]
#    imagename = sys.argv[2]
#    incat = sys.argv[3]

#    cats2OT(name,2)
#    stop()
    for i in range(6):
        try:
            cats2OT(name,i)
        except:
            pass

