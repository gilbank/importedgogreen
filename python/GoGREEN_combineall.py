import os
from astropy.io import fits
from subprocess import check_call
try:
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    mdat = fits.getdata(masterfile)
except:
    print "Cannot access ",os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
    print "Stopping"
    stop()
for cluster in mdat['cluster']:
	print cluster
	try:
		check_call(["python","GoGREEN_scombine.py",cluster,"-o"+cluster+"_oned.fits"])
	except:
		print "Failed oned"
	try:
		check_call(["python","GoGREEN_scombine.py",cluster,"-p"+"twod_rfcs_","-o"+cluster+"_twod.fits"])
	except:
		print "Failed twod"