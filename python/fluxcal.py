import Constants
from optparse import OptionParser
import GoGREEN_Library as GoGREEN
from astropy.io import fits,ascii
import numpy as np
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import sys
import getopt
import pyfits
import ds9
from scipy.interpolate import interp1d,interp2d

def getlambda(hd,pix):
    wmin=hd['crval1']
    disp=hd['cd1_1']
    refpix=hd['crpix1']
    ll=wmin+(pix-refpix)*disp
    return ll

# Unbinned pixels
#chip_width=2048
#chip_gap=61
#chip_bin=2

parser=OptionParser()
parser.add_option("-c", "--configFile", dest="configFile",
                  default=None, help="Configuration file")
(options,args)=parser.parse_args()
#Read in the configuration file
params=GoGREEN.buildDictionary(options.configFile)
chip_bin=float(params['bin'])
chip_gap=float(params['chip_gap'])
chip_width=float(params['chip_width'])
try:
    cname=str(sys.argv[1])
except:
    cname = raw_input('Cluster name? e.g. COSMOS-221')
try:
    maskname=str(sys.argv[2])
except:
    maskname = raw_input('Mask name? e.g. GS2014BLP001-05')
try:
    mosname=str(sys.argv[3])
except:
    mosname = raw_input('MOS filename name? e.g. tgsngsS20150223S0005')

constants=Constants.GoGREENPath()
dir=constants.reduced+cname+"/Spectroscopy/"+maskname+"/"

# 1 Read sensitivity data
c1,h1 = fits.getdata(params['chip1']+".fits",header=True)
p1=np.arange(1,h1['naxis1']+1)
l1=getlambda(h1,p1)
y1=interp1d(l1,c1,bounds_error=False)
c2,h2 = fits.getdata(params['chip2']+".fits",header=True)
p2=np.arange(1,h2['naxis1']+1)
l2=getlambda(h2,p2)
y2=interp1d(l2,c2,bounds_error=False)
c3,h3 = fits.getdata(params['chip3']+".fits",header=True)
p3=np.arange(1,h3['naxis1']+1)
l3=getlambda(h3,p3)
y3=interp1d(l3,c3,bounds_error=False)   

# 3 Read MOS data
hdulist=fits.open(dir+mosname+".fits")
header_primary=hdulist[0].header
hdulist.close()
nsciext=header_primary['NSCIEXT'] 
# Initialize output MEF
hdul=fits.HDUList()
hdul.append(fits.PrimaryHDU(header=header_primary))
mdf,mdfhd = fits.getdata(dir+mosname+".fits",header=True,ext=('mdf',1))
hdul.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))
for i in range(1,nsciext+1):
    im,hd = fits.getdata(dir+mosname+".fits",header=True,ext=('sci',i))
    ipix=np.arange(1,hd['naxis1']+1)
    reflambda=getlambda(hd,ipix)
    detsec=hd['DETSEC']
    x=detsec.partition(',')[0].strip('[')
    x2=int(x.rpartition(':')[2].rstrip(']'))
    x1=int(x.partition(':')[0].strip('['))
    chip1_x=np.arange(1,(x2-(2*chip_gap+2*chip_width))/chip_bin)
    chip2_x=np.arange((x2-(chip_gap+2*chip_width))/chip_bin,(x2-(chip_gap+chip_width))/chip_bin)
    chip3_x=np.arange((x2-chip_width)/chip_bin,(x2-x1)/chip_bin)
    chip1_l=getlambda(hd,chip1_x)
    chip2_l=getlambda(hd,chip2_x)
    chip3_l=getlambda(hd,chip3_x)
    yfinal=interp1d(np.hstack((chip1_x,chip2_x,chip3_x)),np.hstack((y1(chip1_l),y2(chip2_l),y3(chip3_l))),bounds_error=False)
    hdul.append(fits.ImageHDU(data=yfinal(ipix),header=hd,name='SCI'))
    #plt.figure()
    #plt.plot(reflambda,yfinal(ipix))
    #plt.show()
    #print y1(chip1_l)
hdul.writeto(params['prefix']+mosname+".fits",clobber=True)

