import os
import pyfits
import numpy
from pyraf import iraf
import subprocess as sp
import time
import sys

extinction={'Ks':0.08}

def buildDictionary(file):
    file=open(file,'r')
    lines=file.readlines()
    file.close()
    parameters=[]

    for line in lines:
        if line[0]!='#':
            parameters.append(line.split())

    return dict(parameters)

def makeDir(directory):
    # Create a directory. Do nothing if the directory exists
    try:
        os.makedirs(directory)
    except:
        pass
    return

def unlearn(tasks):
    for task in tasks:
        iraf.unlearn(task)
    return

def setUp(params):
    # Create the reduced directory, if it dos not exist
    makeDir(params['reducedDir'])
    # Create one directory for each date
    dates=[]
    for date in os.listdir(params['rawDir']):
        dates.append(date)
        makeDir(params['reducedDir']+'/'+date)

    return dates

def decimalRA(RA):
    if ':' in RA:
        h,m,s=RA.split(':')
        return (float(h)+float(m)/60.+float(s)/3600.)*15.0
    else:
        return float(RA)

def decimalDEC(DEC):
    if ':' in DEC:
        d,m,s=DEC.split(':')
        if '-' not in  d:
            return (float(d)+float(m)/60.+float(s)/3600.)
        else:
            return (float(d)-float(m)/60.-float(s)/3600.)
    else:
        return float(DEC)

def removeShade(data):
    # Take the median of each row after excluding pixels that are 
    # more that 2.0 times NMAD away from the median
    # Subtract this value from the data
    # We do tend to oversubtract in regions where there are objects
    # Alternatively, one could mask the objects
    vector=numpy.zeros(data.shape[0])
    for i in range(data.shape[0]):
        y=numpy.sort(data[i,:])
        median=numpy.median(y[200:len(y)-200])
        nmad=1.48*numpy.median(numpy.abs(y-median))
        use=(numpy.abs(y-median) < 2.0 * nmad)
        vector[i]=numpy.median(y[use])

    return (data.T-vector).T

def parseCDC(input,output,filter):
    lines=input.split('\n')
    file=open(output,'w')
    ra=[]
    dec=[]
    mag=[]
    magerr=[]
    flag=[]
    magIndex={'J':4,'H':4,'K':5}
    flagIndex={'J':0,'H':1,'K':2}
    file.write('ID\tRA\tDEC\tmag\tmag_err\tQfl\n')
    for line in lines:
        if len(line) > 0:
            if line[0]!='#':
                entries=line.split('|')
                TwoMASSflag=entries[6].split()[0][flagIndex[filter]:flagIndex[filter]+1]
                if TwoMASSflag in ['A','B']:
                    ra.append(float(entries[0].split()[0]))
                    dec.append(float(entries[0].split()[1]))
                    mag.append(float(entries[magIndex[filter]].split()[0]))
                    if entries[5].split()[1]=='---':
                        magerr.append(-9.99)
                    else:
                        magerr.append(float(entries[magIndex[filter]].split()[1]))
                    flag.append(TwoMASSflag)
#                file.write('%s\t%9.5f\t%9.5f\t%s\t%s\t%s\n' % (entries[2],ra[-1],dec[-1],mag[-1],magerr[-1],flag[-1]))

    file.close()
    stars=numpy.zeros(len(ra),dtype=[('id','int'),('ra','float'),('dec','float'),('mag','float'),('magerr','float'),('flag','a3')])
    stars['ra']=ra
    stars['dec']=dec
    stars['mag']=mag
    stars['magerr']=magerr
    stars['flag']=flag
    stars['id']=numpy.arange(1,len(ra)+1)

    return stars

def adjustZP(params,options):
    reducedDir=params['reducedDir']+'/'+options.date+'/'

    # Find the list files, open them and preprocess the data listed in them
    # Preprocessing involves copying the files to one Primary
    # extension and moving the pixels to the correct locaitons
    for filename in os.listdir(reducedDir):
        if 'LIRIS' in filename:
            data=pyfits.open(reducedDir+filename,mode='update')
            if data[0].header['ZP'] < 0 or data[0].header['ERR_ZP'] > 0.05 or data[0].header['SCATT_ZP'] > 0.05:
                print "Resetting ZP for", filename
                print "Old ZP",  data[0].header['ZP']
                data[0].header['ZP']=float(options.adjustZP)-data[0].header['AIRMASS']*extinction['Ks']+2.5*numpy.log10(data[0].header['EXPTIME'])
                print "New ZP",  data[0].header['ZP']
                data[0].header['RUNAVEZP']=True
            else:
                data[0].header['RUNAVEZP']=False
            data.close()

    return

def preProcess(params,date):
    # Set up the raw and reduced directories
    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir=params['reducedDir']+'/'+date+'/'

    # Find the list files, open them and preprocess the data listed in them
    # Preprocessing involves copying the files to one Primary
    # extension and moving the pixels to the correct locaitons
    for filename in os.listdir(rawDir):
        if 'list' in filename:
            file=open(rawDir+filename)
            filelist=file.readlines()
            file.close()
            for fitsfile in filelist:
                file=pyfits.open(rawDir+fitsfile.strip())
                output=reducedDir+fitsfile.strip().replace('.fit','.fits')
                # Some of the FITS keywords are non-standard. Ignore for now
                data,hdr=fix(file)
                pyfits.writeto(output,data,hdr,clobber=True,output_verify='ignore')
                file.close()

    return
        
def fix(file):
    # LIRIS data
    # Following the IRAF script lcpixmap.cl
    data2=numpy.array(file[1].data)
    hdr=file[0].header
    if 'PIXMAPCR' not in hdr.keys():
        data2[512:1023,512:514]=file[1].data[513:1024,0:2]
        data2[0:511,512:514]=file[1].data[1:512,0:2]
        data2[:,0:2]=file[1].data[:,512:514]
        hdr['PIXMAPCR']=True
    return data2,hdr

def createFlat(params,date):
    # Creates the dome flat from the on and off exposures
    # If available, also creates the darsk
    # Set up the raw and reduced directories
    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir=params['reducedDir']+'/'+date+'/'

    unlearn(['imcom','imar'])

    # Find the list of dark frames
    if 'dark.list' in os.listdir(rawDir):
        file=open(rawDir+'dark.list')
        listing=file.readlines()
        file.close()

        file=open(reducedDir+'dark.list','w')
        for line in listing:
            file.write('%s%s\n' % (reducedDir,line.strip().replace('.fit','.fits')))
        file.close()
        iraf.imcom(input='@'+reducedDir+'dark.list',output=reducedDir+'dark',combine='average',reject='minmax',scale='none',zero='median',nlow='3',nhigh='3')
    else:
        print 'No list of dark frames available'


    # Find the list of domeflats
    if 'domeOff.list' in os.listdir(rawDir):
        file=open(rawDir+'domeOff.list')
        listing=file.readlines()
        file.close()
        
        file=open(reducedDir+'domeOff.list','w')
        for line in listing:
            file.write('%s%s\n' % (reducedDir,line.strip().replace('.fit','.fits')))
        file.close()
        iraf.imcom(input='@'+reducedDir+'domeOff.list',output=reducedDir+'domeOff',combine='average',reject='minmax',scale='median',nlow='3',nhigh='3')
    else:
        print 'No list of dome flats with the lamp off'
        return
    
    if 'domeOn.list' in os.listdir(rawDir):
        file=open(rawDir+'domeOn.list')
        listing=file.readlines()
        file.close()
        
        file=open(reducedDir+'domeOn.list','w')
        for line in listing:
            file.write('%s%s\n' % (reducedDir,line.strip().replace('.fit','.fits')))
        file.close()
        iraf.imcom(input='@'+reducedDir+'domeOn.list',output=reducedDir+'domeOn',combine='average',reject='minmax',scale='median',nlow='3',nhigh='3')
    else:
        print 'No list of dome flats with the lamp on'
        return
    
    # Subtract domeOff from domeOn and scale by the median
    iraf.imar(operand1=reducedDir+'domeOn',op='-',operand2=reducedDir+'domeOff',result=reducedDir+'domeFlat')
    median=numpy.median(pyfits.getdata(reducedDir+'domeFlat.fits'))
    iraf.imar(operand1=reducedDir+'domeFlat',op='/',operand2=median,result=reducedDir+'domeFlat')

    # Create the bad pixel mask from the flat
    unlearn(['imexpr'])
    iraf.imexpr(expr='(a>2 || a <0.5) ? 0 : 1',output=reducedDir+'badpix.pl',a=reducedDir+'domeFlat.fits')

    return

def reduceObjects(params,date,cluster):
    # Set up the raw and reduced directories
    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir='reduced/'+date+'/'

    # IRAF tasks
    unlearn(['imar','hedit'])
    iraf.xdimsum()
    unlearn(['xfirstpass','xmaskpass','hedit','imexpr','imcom','imtran'])

    lists=[]
    # Find the object lists
    print rawDir
    for filename in os.listdir(rawDir):
        if 'list' in filename and cluster in filename:
            lists.append(filename)

    # We do not change to the directory where the files exist
    # because of a bug in xdimsum

    # One object at a time
    for list in lists:
        
        file=open(rawDir+list)
        filelist=file.readlines()
        file.close()

        # Divide by the flat field
        for image in filelist:
            imageName=reducedDir+image.strip().replace('.fit','.fits')
            try:
                pyfits.getval(imageName,'FLAT')
                print '%s is aleady flat fielded' % imageName
            except:
                iraf.imar(operand1=imageName,op='/',operand2=reducedDir+'domeFlat.fits',result=imageName)
                iraf.hedit(images=imageName,fields='FLAT',value=reducedDir+'domeFlat.fits',add='yes',verify='no')

        # Sky subtraction
        firstfile=reducedDir+filelist[0].strip().replace('.fit','')
        rootname=reducedDir+list.replace('.list','')

        shiftfile=rootname+'.offsets'
        file=open(rootname+'.xfirstpass','w')
        for line in filelist:
            file.write(reducedDir+line.replace('.fit',''))
        file.close()


        nmean=11
        nskymin=4
        statSection=params['statSection']

        iraf.xfirstpass(inlist='@'+rootname+'.xfirstpass',referen=firstfile,output=firstfile+'_fp',expmap='.exp',xslm='yes',maskfix='yes',xzap='yes',statsec=statSection,nmean=nmean,nskymin=nskymin,nreject='2',bpmask=reducedDir+'badpix.pl',badpixu='yes',mkshift='yes',chkshif='no',xnregis='yes',shiftli=shiftfile)

        iraf.xmaskpass(input=firstfile+'_fp',inexpmap=firstfile+'_fp.exp',seccorn=firstfile+'_fp.corners',output=firstfile+'_mp',outexpma='.exp',statsec=statSection,chkmask='no',kpchking='no',nmean=nmean,nskymin=nskymin,nreject='1',bpmask=reducedDir+'badpix.pl',badpixu='no',shiftli=shiftfile,mag='1',xnregis='no')

    # Combine the bad pixel masks
    # .crm - contains cosmic rays

        for file in filelist:
            # Create the bad pixel map
            root=reducedDir+file.strip().replace('.fit','')
            bpm=root+'.sub.bpm.pl'
            iraf.imdelete(images=bpm,verify='no')
            iraf.imexpr(expr='(a==0 || b==1) ? 0 : 1',output=bpm,a=reducedDir+'badpix.pl',b=root+'.sub.crm.pl')
            iraf.hedit(images=root+'.sub.fits',fields='BPM',value=bpm,add='yes',verify='no')

            # Add a WCS to each image
            wcs={'CRPIX1':527,
                 'CRPIX2':617,
                 'CRVAL1':decimalRA(pyfits.getval(root+'.sub.fits','RA')),
                 'CRVAL2':decimalDEC(pyfits.getval(root+'.sub.fits','DEC')),
                 'CD1_1':-0.000069722,
                 'CD1_2':0.0,
                 'CD2_2':0.000069722,
                 'CD2_1':0.0,
                 'CTYPE1':'RA---TAN',
                 'CTYPE2':'DEC--TAN'}
             
            for key in wcs.keys():
                iraf.hedit(images=root+'.sub.fits',fields=key,value=wcs[key],add='yes',verify='no',show='no')

            # Remove residual shading
            try:
                pyfits.getval(root+'.sub.fits','SHADECOR')
                print 'Shade corrections already applied'
            except:
                data,hdr=pyfits.getdata(root+'.sub.fits',header=True)
                # Need the clip, otherwise objects drag the flux down
                corrected=removeShade(data)
                hdr['SHADECOR']=True
                pyfits.writeto(root+'.sub.fits',corrected,hdr,output_verify='ignore',clobber=True)

        # Create the files for imcombine
        file=open(shiftfile)
        images=file.readlines()
        file.close()
        list4imcom=open(rootname+'.list4imcom','w')
        offsets4imcom=open(rootname+'.offsets4imcom','w')
        for image in images:
            entries=image.split()
            list4imcom.write(entries[0]+'.fits\n')
            offsets4imcom.write('%8.2f %8.2f\n' %(-1.0*float(entries[1]),-1.0*float(entries[2])))
        list4imcom.close()
        offsets4imcom.close()

        iraf.imdelete(images=rootname,verify='no')
        iraf.imdelete(images=rootname+'_exp.pl',verify='no')

        iraf.imcom(input='@'+rootname+'.list4imcom',output=rootname,expmask=rootname+'_exp',combine='average',reject='minmax',offsets=rootname+'.offsets4imcom',masktyp='badvalue',maskval='0',scale='none',zero='median',statsec=statSection,nlow='1',nhigh='1')

    return


def determineZP(params,date,options):
    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir='reduced/'+date+'/'

    # IRAF tasks
    ## unlearn(['imar','hedit'])


    lists=[]
    # Find the object lists
    for filename in os.listdir(rawDir):
        if 'list' in filename and options.target in filename:
            lists.append(filename)

    # We do not change to the directory where the files exist
    # because of a bug in xdimsum

    # One object at a time
    for list in lists:
        image=reducedDir+list.replace('.list','.fits')
        # Use 2MASS stars to determine the ZP
        try:
            filter=pyfits.getval(image,'LIRF2NAM',0)
        except:
            filter='ks'
        CRVAL=numpy.matrix([[pyfits.getval(image,'CRVAL1',0)],[pyfits.getval(image,'CRVAL2')]])
        CRPIX=numpy.matrix([[pyfits.getval(image,'CRPIX1',0)],[pyfits.getval(image,'CRPIX2')]])
        CD=numpy.matrix([[pyfits.getval(image,'CD1_1',0),0],[0,pyfits.getval(image,'CD2_2',0)]])

        twoMASSfilter={'ks':'K'}
        # Find stars in the 2MASS catalogue
        cmd='find2mass -r 2.6 -c %9.5f %9.5f' % (CRVAL[0,0],CRVAL[1,0])
        p1=sp.Popen(cmd, shell=True, stdout=sp.PIPE)
        stars=parseCDC(p1.communicate(0)[0],image.replace('.fits','.2MASS'),twoMASSfilter[filter])
        print stars

        # Determine the x,y position of the 2MASS stars
        # The coordinate transformation in the header is
        # c-c_0 = M * (x-x_0)

        tvmarkFile=image.replace('.fits','.txt')
        file=open(tvmarkFile,'w')
        xcenterRef=[]
        ycenterRef=[]
        for star in stars:
            coord=numpy.matrix([[star['ra']],[star['dec']]])
            proj=numpy.matrix([[numpy.cos(coord[1,0]*numpy.pi/180.),0.0],[0.0,1.0]])
            x=numpy.linalg.inv(CD)*proj*(coord-CRVAL)+CRPIX
            file.write('%7.2f %7.2f %s\n' % (x[0,0],x[1,0],star['id']))
            xcenterRef.append(x[0,0])
            ycenterRef.append(x[1,0])


        file.close()
        unlearn(['display','tvmark'])
        iraf.display(image=image,frame=1)
        iraf.tvmark(frame='1',coords=tvmarkFile,label='yes',color=205,mark='circle',nxoffse=40,radii=20,pointsize=2,txsize=3)
    
        # Ask the user to select stars in the image
        iraf.noao()
        iraf.digi()
        iraf.apphot()
        unlearn(['phot','txdump'])
        iraf.datapars.scale=0.251
        iraf.photpars.apertur='4'
        iraf.photpars.zmag='0'
        iraf.fitskypars.salgori='mode'
        iraf.fitskypars.annulus='6'
        iraf.fitskypars.dannulus='4'
        iraf.centerpars.calgori='centroid'
        print "Select the stars one at a time in numerical order"
        iraf.phot(image=image.replace('.fits',''))
        # Phot strips any directories from the filename
        mags=os.path.split(image.replace('.fits','.mag.1'))[1]
        print mags
        xcenterIn=iraf.txdump(textfile=mags,fields='XCENTER',expr='yes',Stdout=1)
        ycenterIn=iraf.txdump(textfile=mags,fields='YCENTER',expr='yes',Stdout=1)
        mag=iraf.txdump(textfile=mags,fields='MAG',expr='yes',Stdout=1)
        # The magnitude error is not very precise
        magerr=iraf.txdump(textfile=mags,fields='MERR',expr='yes',Stdout=1)
    
        iraf.delete(files=mags,verify='no')

        # 1st order shift. We adjust the WCS directly, if requested
        xoff=0
        yoff=0
        for i in range(len(xcenterIn)):
            if mag[i]!='INDEF':
                xoff+=float(xcenterIn[i])-xcenterRef[i]
                yoff+=float(ycenterIn[i])-ycenterRef[i]

        if options.update:
            iraf.hedit(images=image,fields='CRPIX1',value=CRPIX[0,0]+xoff/len(xcenterIn),verify='no',show='no')
            iraf.hedit(images=image,fields='CRPIX2',value=CRPIX[1,0]+yoff/len(ycenterIn),verify='no',show='no')

        # ZP
        ZP=[]
        ZP_err=[]
        sum1=0
        sum2=0
        phot=open(image.replace('.fits','.phot'),'w')
        phot.write('ID\tRA\tDEC\tmag_INST\terr_mag_INST\tmag_2MASS\terr_mag_2MASS\tQfl\tZP\terr_ZP\n')
        # Only A class stars are considered
        use=[]
        for i in range(len(xcenterIn)):
            if stars[i]['flag'] in ['A','B'] and stars[i]['magerr'] > 0 :
                reply=raw_input('Using star # %d?' % (stars[i]['id']))
                if reply=='y':
                    use.append(i)

        for i in use:
            ZP.append(stars[i]['mag']-float(mag[i]))
            ZP_err.append(numpy.sqrt(stars[i]['magerr']**2.+float(magerr[i])**2.))
            print 'Using star #',stars[i]['id']
            phot.write('%s\t%9.5f\t%9.5f\t%6.3f\t%6.3f\t%6.3f\t%6.3f\t%s\t%6.3f\t%6.3f\n' %(stars[i]['id'],stars[i]['ra'],stars[i]['dec'],float(mag[i]),float(magerr[i]),stars[i]['mag'],stars[i]['magerr'],stars[i]['flag'],ZP[-1],ZP_err[-1]))
            sum1+=ZP[-1]/ZP_err[-1]**2.
            sum2+=1.0/ZP_err[-1]**2.

        phot.close()

        # The error bars are probably understimated
        if len(ZP) > 0:
            iraf.hedit(images=image,fields='ZP',value=sum1/sum2,add='yes',verify='no')
            iraf.hedit(images=image,fields='err_ZP',value=numpy.sqrt(1.0/sum2),add='yes',verify='no')
            iraf.hedit(images=image,fields='scatt_ZP',value=numpy.std(ZP)/numpy.sqrt(len(ZP)),add='yes',verify='no')
            iraf.hedit(images=image,fields='n_ZP',value=len(ZP),add='yes',verify='no')
        else:
            iraf.hedit(images=image,fields='ZP',value=-9.99,add='yes',verify='no')
            iraf.hedit(images=image,fields='err_ZP',value=-9.99,add='yes',verify='no')
            iraf.hedit(images=image,fields='scatt_ZP',value=-9.99,add='yes',verify='no')

        # Do some postprocessing
            
        # Remove the residuals from the correction of the shade pattern
        #
        try:
            pyfits.getval(image,'RESIDCOR')
            print 'Residual correction already applied'
        except:
            data,hdr=pyfits.getdata(image,header=True)
            corrected=removeShade(data)
            hdr['RESIDCOR']=True
            pyfits.writeto(image,corrected,hdr,output_verify='ignore',clobber=True)

        # Merge the exposure map with the image - to do


    return

def merge(params,date,target,version):
    # Merge the exposure map with the image, add a version number and
    # update a number of keywords.

    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir='reduced/'+date+'/'

    # IRAF tasks
    unlearn(['imcopy'])
    instrument='LIRIS'
    filter='Ks'


    lists=[]
    # Find the object lists
    for filename in os.listdir(rawDir):
        if 'list' in filename and target in filename:
            lists.append(filename)

    # One object at a time
    for list in lists:
        image=reducedDir+list.replace('.list','.fits')
        output=reducedDir+list.replace('.list','_'+instrument+'_'+filter+'_v'+version+'.fits')

        data,hdr=pyfits.getdata(image,header=True)
        hdr['VERSION']=version
        hdr['REDUCED']='Chris Lidman'
        hdr['REDUCON']= time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())
        pyfits.writeto(output,data,hdr,clobber=True,output_verify='ignore')

        # Rename and ppend the exposure map
        expmap=image.replace('.fits','_exp.fits')
        iraf.imdel(images=image.replace('.fits','_exp.fits'),verify='no')
        iraf.imcopy(input=image.replace('.fits','_exp.pl'),output=expmap)
        exp,hdr=pyfits.getdata(expmap,header=True)
        pyfits.append(output,exp,hdr)
    
    return
    
def createCat(image,exp,config,flags):
    params=''
    for key in flags.keys():
        params=params+' -'+key+' '+flags[key]
    
    cat=os.path.split(image)[1].replace('.fits','.cat')
    if exp==None:
        cmd='sex -c '+config+params+' -CATALOG_NAME '+cat+' '+image
    else:
        cmd='sex -c '+config+params+' -CATALOG_NAME '+cat+' -WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE '+exp+' '+image

    print cmd
    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "SExtractor was terminated by signal", -retcode
        else:
            print >>sys.stderr, "SExtractor returned", retcode
    except OSError, e:
        print >>sys.stderr, "SExtractor failed:", e

    return cat

def setUpSExtractor(setupDir):

    for config in os.listdir(setupDir):
        cmd='cp '+ setupDir+config+' .'
        sp.call(cmd,shell=True)
        
    return

def createAstrometricReference(params,options):
    # Copy accross the setup files
    setUpSExtractor(params['setupDir'])
    flags={}
    createCat(options.astrometricRef,None,params['astromConfig'],flags)

    return

def scamp(params,date,options):
    rawDir=params['rawDir']+'/'+date+'/'
    reducedDir='reduced/'+date+'/'

    # IRAF tasks
    unlearn(['imcopy'])
#    instrument='LIRIS'
    filter='Ks'


    lists=[]
    # Find the object lists
    for filename in os.listdir(rawDir):
        if 'list' in filename and options.target in filename:
            lists.append(filename)
            
    
    # Run SExtractor on each list
    # Copy accross the SExtractor paraemeter files
    setUpSExtractor(params['setupDir'])
    flags={}
    catList=''
    for fileList in lists:
        file=open(rawDir+fileList)
        listing=file.readlines()
        file.close()
        for exposure in listing:
            exposure=exposure.strip()
            image=exposure.replace('.fit','.sub.fits')
            # Addjust the WCS - Hack to adjust WCS
            wcs={'CD1_2':0.0,
                 'CD2_1':0.0,
                 'FILTER':filter,
                 'CRPIX1':pyfits.getval(image,'CRPIX1'),
                 'CRPIX2':pyfits.getval(image,'CRPIX2')}
            for key in wcs.keys():
                iraf.hedit(images=image,fields=key,value=wcs[key],add='yes',verify='no',show='no')

            expmap_pl=exposure.replace('.fit','.sub.bpm.pl')
            expmap_fits=exposure.replace('.fit','.sub.bpm.fits')
            
            if not os.path.isfile(expmap_fits):
                iraf.imcopy(input=expmap_pl,output=expmap_fits)
            catList=catList+' '+createCat(image,expmap_fits,params['sexConfig'],flags)
           
    # Run SCAMP
            
    cmd='scamp -c ' +params['scampConfig'] + ' -ASTREF_CATALOG FILE -ASTREFCAT_NAME '+ options.astrometricRef +' '+catList
    try:
        retcode=sp.call(cmd,shell=True)
        if retcode < 0:
            print >>sys.stderr, "Scamp was terminated by signal", -retcode
        else:
            print >>sys.stderr, "Scamp returned", retcode
    except OSError, e:
        print >>sys.stderr, "Scamp failed:", e

    return

def combine(params, options):
    unlearn(['imcopy','hedit','imexpr'])
    file=open(options.combine)
    fitsList=file.readlines()
    file.close()

    statSection=params['statSection']

    file=open('temp1','w')
    for fits in fitsList:
        fits=fits.strip()
        image=fits.replace('.fits','.resamp.fits')
        weight=fits.replace('.fits','.resamp.weight.fits')
        bpm=fits.replace('.fits','.resamp.bpm.fits')
        # Convert the weight map to an exposure map
#        iraf.imexpr(expr="(a>0) ? 1 :0",output=bpm,a=weight)
#        iraf.hedit(images=image,fields='BPM',value=bpm,add='yes',verify='no')
        file.write(image+'\n')
    file.close()

    output=options.combine.replace('_swarp.list','.fits')
    expmask=options.combine.replace('_swarp.list','.exp.pl')
    iraf.imcom(input='@temp1',output=output,expmask=expmask,reject='minmax',offsets='wcs',masktype='goodvalue',maskval='1',zero='median',statsec=statSection,lthresh='-1000',nlow='3',nhigh='3')
    return
