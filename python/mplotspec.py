import numpy as np
from astropy.io import fits
import matplotlib.pyplot as plt
import pickle
from astropy.convolution import convolve,Gaussian1DKernel

###import ezgal

#from zscale import zscale
#import astropy.io.fits as pyfits
def zscale(image, contrast=1.0):
   """Implementation of the IRAF zscale algorithm to find vmin and vmax parameters for the dynamic range of a display. It finds the image values near the median image value without the time consuming process of computing a full image histogram."""

   from scipy import optimize
   #import matplotlib.pyplot as plt

   # Get ordered list of points
   I=np.sort(image.flatten())

   # Get number of points
   npoints=len(I)

   # Find the midpoint (median)
   midpoint=(npoints-1)/2

   # Fit a linear function
   # I(i) = intercept + slope * (i - midpoint)

   fitfunc = lambda p, x: p[0]*x+p[1]
   errfunc = lambda p, x, y: fitfunc(p, x) - y

   # Initial guess for the parameters
   p0 = [(I[-1]-I[0])/npoints,I[midpoint]] 

   # Fit
   i=np.arange(len(I))
   p1, success = optimize.leastsq(errfunc, p0[:], args=(i, I))

#    plt.plot(i,I,'r+')
#    plt.plot(i,fitfunc(p1,i))
#    plt.show()

   if success in [1,2,3,4]:
       slope=p1[0]
       z1=I[midpoint]+(slope/contrast)*(1-midpoint)
       z2=I[midpoint]+(slope/contrast)*(npoints-midpoint)
   else:
       z1=np.min(image)
       z2=np.max(image)

   return z1, z2


#clus = 'SPT0546' ; z=1.07
clus = 'SpARCS1634' ; z=1.2
#clus='COSMOS-221' ; z = 1.1
file1d = '%s_oned.pkl'%clus
file2d = '%s_twod.pkl'%clus

f = np.load(file1d)

with open(file1d,'r') as f:
    specarr = pickle.load(f)
    specid = pickle.load(f)
    lam = pickle.load(f)
    mdf = pickle.load(f)
with open(file2d,'r') as f:
    specarr2 = pickle.load(f)
    specid2 = pickle.load(f)
    lam2 = pickle.load(f)
    mdf2 = pickle.load(f)




# if 1d and 2d files are out of sync, we're in trouble!
assert np.max(np.array(specid)-np.array(specid2))==0



#pri1 = np.where(mdf['priority']==1)[0]
#ook = (mdf['priority']==1)
newmdf = mdf[mdf['priority']==1]
mdf = newmdf
#mdf2 = mdf2[ook]

#mdf = mdf[pri1]
ss = np.argsort(mdf['MAG'])
#ss=ss[::-1]

plt.figure(figsize=(20,10))
#nx,ny = 4,4
nx,ny = 3,3
plt.subplot2grid((nx,ny),(0,0))
k = 0
ii=0

oii = 3727.*(1.+z)
CaK = 3933.*(1.+z)
CaH = 3969.*(1.+z)
gBand = 4303.*(1.+z)
MgII = 2796.*(1.+z)
MgIIa = 2804.*(1.+z)




def modelTempl(z):
    model = ezgal.model( 'bc03_ssp_z_0.02_chab.model' )
    sed = model.get_sed( 3, age_units='gyrs', units='Fl' )
    plt.plot(model.ls*(1.+z),sed*1.e8,color='b',alpha=0.4) # don't worry about normalisation

def plotTempl(z):
#    abslines = np.array([3933.,3969.,4303.,2796.,2804.,2852.,4104.,4341])*(1.+z) 
    abslines = np.array([3933.,3969.,4303.,2796.,2804.,2852.])*(1.+z) 
    plt.axvline(oii,color='b',alpha=0.3)
#    plt.axvline(CaH,color='g',alpha=0.3)
#    plt.axvline(CaK,color='g',alpha=0.3)
#    plt.axvline(gBand,color='g',alpha=0.3)
#    plt.axvline(gBand,color='g',alpha=0.3)
#    plt.axvline(gBand,color='g',alpha=0.3)
    for i in range(len(abslines)):
        plt.axvline(abslines[i],color='g',alpha=0.3)
        

ok = np.where(specid ==mdf['ID'][ss[0]])[0]
dat = specarr[ok][0,:]
gauss = Gaussian1DKernel(stddev=2)
refspec = convolve(dat,gauss)

from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import MultipleLocator, MaxNLocator

#while (ii<len(specarr)-1):
while (ii<19):
    for i in range(nx):
        for j in range(ny):
            k = k + 1
            plt.subplot2grid((nx,ny),(i,j))
            ok = np.where(specid ==mdf['ID'][ss[ii]])[0] 
            dat = specarr[ok][0,:]
            gauss = Gaussian1DKernel(stddev=2)
            smdat = convolve(dat,gauss)
            #        plt.plot(lam,dat)
            plt.plot(lam,smdat,'k-')
            ax = plt.gca()
            ax.axis('tight')
            plt.plot(lam,specarr[ok][1,:]/1.,'r-',alpha=0.3)
            plt.plot(lam,refspec/2.,'m-',alpha=0.3)


            plt.title('ID: %s, pri=%.0f, mag=%.2f  [%s]'\
                      %(specid[ok],mdf['priority'][ss[ii]],mdf['MAG'][ss[ii]],specid2[ok]))
            print specid[ok],mdf['priority'][ss[ii]],mdf['ID'][ss[ii]]
            plotTempl(z)
            ###modelTempl(z)
            plt.axvspan(5000,6500,color='k',alpha=0.3) # bad WLC

#            y0,y1 = zscale(dat)
            y0,y1 = zscale(dat[0:1250]) # ignore junk at red end
            plt.ylim([y0,y1])

            divider1 = make_axes_locatable(ax)
            cax1 = divider1.append_axes("top", size="50%", pad=0.1)
            z0,z1 = zscale(specarr2[ok][0])
            cmap='grey_r'
            cmap=None
            plt.imshow(specarr2[ok][0],origin='lower',extent=[lam.min(),lam.max(),0,20],aspect=20,cmap=cmap)
#            plt.imshow(specarr2[ok][0],origin='lower',extent=[lam.min(),lam.max(),0,20],aspect=20,cmap=cmap,vmin=z0,vmax=z1)
#            plt.imshow(specarr2[ok][0],origin='lower',extent=[lam.min(),lam.max(),0,20],aspect=20,cmap=cmap,vmin=z0,vmax=z1)
            frame = plt.gca()
            frame.axes.get_xaxis().set_ticks([])


            plt.show()
            if i*j == (nx-1)*(ny-1):
                k=0
                plt.show()
#                stop()
                #plt.figure()
                plt.figure(figsize=(20,10))
            ii = ii + 1


"""
ns = len(mdf)
aspec = np.zeros((ns,len(dat)))
for i in range(ns):
    ok = np.where(specid ==mdf['ID'][ss[i]])[0] 
    aspec[i,:] = specarr[ok][0,:]
"""
