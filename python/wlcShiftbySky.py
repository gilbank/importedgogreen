import Constants
from optparse import OptionParser
import GoGREEN_Library as GoGREEN
from astropy.io import fits,ascii
import numpy as np
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import sys
import getopt
import pyfits
from scipy.interpolate import interp1d,interp2d
#def xcor2(inspec,indq,refspec,refdq,maxshift,dx=.1,quiet='false'):#       tshift=xcor(tflux,tay0,tay2,maxshift)
def xcor2(inspec,refspec,maxshift,dx=.1,quiet='false'):#       tshift=xcor(tflux,tay0,tay2,maxshift)
    # do crude discrete cross-correlation on each slit (between y0,y1) to find best offset:
    if (int(dx)<dx):
        ivec=np.arange(-maxshift*10,maxshift*10,int(dx*10.)) 
        ipix=np.arange(0,len(refspec))
        ipix10=np.arange(0,len(refspec)*10.)/10.
        invec=interp1d(ipix,inspec,bounds_error=False,fill_value=0.)(ipix10)
        refvec=interp1d(ipix,refspec,bounds_error=False,fill_value=0.)(ipix10)
    else:
        ivec=np.arange(-maxshift,maxshift,dx) 
        invec=inspec
        refvec=refspec
        #indqvec=indq
        #refdqvec=refdq
        
    xcorval=np.zeros(len(ivec))
    for ii in range(len(ivec)):
        shifrefvec=np.roll(refvec,int(ivec[ii]))
        #shifrefdq=np.roll(refdqvec,int(ivec[ii]))
        
#        print np.shape(shifrefvec)
#        print np.shape(inspec)
#        w=np.where(np.logical_and(shifrefdq == 0,indqvec == 0))
#        tmp=shifrefvec[w]*invec[w]
        xcorval[ii]=np.sum(shifrefvec * invec)
#        xcorval[ii]=np.sum(tmp)/len(tmp)
#        print ii,ivec[ii],xcorval[ii]
    #pl.figure()
    #pl.plot(ivec,xcorval)
    pk=np.argmax(xcorval)
#    stop()
    if(quiet=='false'):
        if ((pk==0) | (pk==(len(xcorval)-1))): 
            print
            print 'WARNING: xcor2 peak at edge of window!'
            print 'CHECK RESULTS'
            print
    if (int(dx)<dx):
        return ivec[pk]/10.,xcorval[pk]
    else:
        return ivec[pk],xcorval[pk]

#def zeroshift(inspec,indq,reflam,refspec,refdq,maxshift=50.,idebug=False,globshift=0.,smooth=0.):
def zeroshift(inspec,reflam,refspec,maxshift=50.,idebug=False,globshift=0.,smooth=0.):
    xpix=np.arange(len(reflam))
    coeff=np.polyfit(xpix,reflam,1)
    yf=np.polyval(coeff,xpix)
    
###    inspec,hdr=pyfits.getdata(filename,header='true')
###    dx=-float(hdr['xp']) + 1700. # shift to match refarc.txt pixels
###    if (idebug==1): print dx
##    dx=0.
    dx=globshift # global shift on input (to all RSS to work)
#    if(idebug==1):
#        plt.figure()
#        plt.title(filename+'  '+str(dx))
###        plt.plot(reflam+dx,inspec/10.,'b--')
#        rline=plt.plot(reflam,refspec,'k-')    
    #pl.show()
    
    if (smooth>1.):
        #nn=5
        nn=smooth
        kern=np.ones(nn)/float(nn)
        srefspec=np.convolve(refspec,kern,mode='same')
        refspec=srefspec
###
#    refspec=srefspec
###
                        
###    foo=np.roll(np.reshape(inspec,-1),int(dx))/10.
#    nn=10
#    kern=np.ones(nn)/float(nn)
#    foo=np.convolve(inspec,kern,mode='same')
    
##    foo=inspec
    foo=np.roll(np.reshape(inspec,-1),int(dx))#/10.
    #foodq=np.roll(np.reshape(indq,-1),int(dx))#/10.
    ###
#    if (idebug==1): 
#        print np.shape(inspec)
###        print np.shape(dx)
        ###plt.plot(inspec/1.,'g:')
    
    #pixshift,corrval=xcor2(foo,foodq,refspec,refdq,maxshift=maxshift,quiet='true')
    pixshift,corrval=xcor2(foo,refspec,maxshift=maxshift,quiet='true')
    #if (idebug==1): print pixshift
    foo2=np.roll(np.reshape(foo,-1),int(-pixshift))
    #if (idebug==1): plt.plot(foo2/1.,'r-')
    # works in pixels, now need to shift in A accordingly
    totdx=dx-pixshift
    #**** this isn't quite right yet:
    #midx=int(round(float(len(reflam)/2.)))
    #totdl=totdx*(reflam[midx]-reflam[midx-1])
    #speclam=reflam-totdl
    speclam=np.polyval(coeff,(xpix+totdx)) # shift cubic fit for wl soln.
    if (idebug):
#        plt.figure()
        nomline=plt.plot(xpix,foo,'b--')
        rline=plt.plot(xpix,refspec,'k-')  
        fline=plt.plot(xpix+totdx,inspec,'r--')
#        plt.legend((rline,nomline,fline),('ref','nominal','fitted'))
        plt.legend(('nominal','ref','fitted'))
    #    plt.plot(speclam,inspec,'r-')
#        plt.show()
    
    
    
    # -- write spectrum and wavelength solution to file:
    #outfile=re.sub('.ms.fits','_wlc0.txt',filename)
    #np.savetxt(outfile,np.transpose((speclam,inspec)))
    #return (speclam,inspec)
    return (pixshift)
#    stop()
def fill_dq(A,dq):
    jnds=np.arange(A.shape[0])
    inds=np.arange(A.shape[1])
    B=np.copy(A)
    for j in jnds:
        good=np.where(reduce(np.logical_and,[dq[j] == 0,np.isfinite(A[j])]))
        if np.size(good)<2:
            #MLB: This is a poor way of dealing with rows that are all dq=0
            try:
                B[j]=0.5*(B[j-1]+B[j+1])
            except:
                try:
                    B[j]=B[j-1]
                except:
                    B[j]=B[j+1]
        else:
            f=interp1d(inds[good],A[j][good],bounds_error=False)
            B[j]=np.where(reduce(np.logical_and,[dq[j] == 0,np.isfinite(A[j])]),A[j],f(inds))
    return B

def diagplot(x,range):
        fig=plt.figure()
        for i in np.arange(len(range)):
            ax=fig.add_subplot(len(range),1,i+1)
            minval=1e5
            maxval=0
            for v in x:
                plt.plot(v[0],v[1],label=v[2])
                maxval=1.05*np.max((v[1][np.where(reduce(np.logical_and,[v[0]>range[i][0],v[0]<range[i][1]]))]))
                minval=1.05*np.min((v[1][np.where(reduce(np.logical_and,[v[0]>range[i][0],v[0]<range[i][1]]))]))
            plt.ylim((minval,maxval))
            plt.xlim(range[i])
            if (i==0): plt.legend(loc='best')
        plt.show(block=False)


parser=OptionParser()
parser.add_option("-s", "--slit", dest="refslit",
                  default=1, help="Reference slit number")
parser.add_option("-o", "--outpref", dest="outpref",
                  default="h", help="Prefix to add to filename of shifted spectrum")
parser.add_option("-i", "--inter", dest="inter",
                  default="True", help="Evaluate interactively?")
parser.add_option("-r", "--shiftref", dest="shiftref",
                  default="True", help="Shift reference spectrum?")
(options, args) = parser.parse_args()
try:
    reffile=str(sys.argv[1])
except:
    reffile = raw_input('Reference sky frame')
try:
    skyfile=str(sys.argv[2])
except:
    skyfile = raw_input('Target sky frame')
try:
    scifile=str(sys.argv[3])
except:
    scifile = raw_input('Target science frame')
minshift=-0.1
refslit=int(options.refslit)
outpref=options.outpref
inter=options.inter
shiftref=int(options.shiftref)
hdulist = fits.open(reffile)
header_primary=hdulist[0].header
hdulist.close()
nsciext=header_primary['NSCIEXT']
refspec0,hd = fits.getdata(reffile,header=True,ext=('sci',refslit))
refdq = fits.getdata(reffile,header=False,ext=('dq',refslit))
# Interpolate over bad dq:
refspec=fill_dq(refspec0,refdq)
wmin=hd['crval1']
disp=hd['cd1_1']
ipix=np.arange(1,hd['naxis1']+1)
refpix=hd['crpix1']
reflambda=wmin+(ipix-refpix)*disp
refspec=np.nansum(refspec,axis=0)
refdq=np.nansum(refdq,axis=0)
#refspec=refspec[10,:]
#refdq=refdq[10,:]

#refspec=refspec[10,:]
#refdq=refdq[10,:]
#skyfile=dir+"tgsskygsS20141117S0045.fits"
refshift=0
cnt=0
if (shiftref==1):
    while(refshift!=0 or cnt == 0):
        reflambda=reflambda-refshift*disp
        x=((reflambda,refspec,"Reference spectrum"),)
        # Lines 6300, 6863, 7913, 8827
        diagplot(x,((6250,6350),(6813,6913),(7863,7963),(8777,8877)))
        refshift = float(raw_input("Shift in pixels (0 to proceed): "))
        plt.close()
        cnt=cnt+1
writenew=True
if writenew:
    # Create HDUL for Science and Sky files.  Copy primary header and MDF from original.
    outsci=outpref+scifile
    hdulist = fits.open(scifile)
    sci_primary=hdulist[0].header
    hdulist.close()
    hdulsci = fits.HDUList()
    hdulsci.append(fits.PrimaryHDU(header=sci_primary))
    mdf,mdfhd = fits.getdata(scifile,header=True,ext=('mdf',1))
    hdulsci.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))

    outsky=outpref+skyfile
    hdulist = fits.open(skyfile)
    sky_primary=hdulist[0].header
    hdulist.close()
    hdulsky = fits.HDUList()
    hdulsky.append(fits.PrimaryHDU(header=sky_primary))
    hdulsky.append(fits.TableHDU(data=mdf,header=mdfhd,name='MDF'))
shifts=np.array([])
for i in range(1,nsciext+1):
    skyspec0,hdsky = fits.getdata(skyfile,header=True,ext=('sci',i))
    skydq = fits.getdata(skyfile,header=False,ext=('dq',i))
    skyspec=fill_dq(skyspec0,skydq)
    pix=np.arange(1,hdsky['naxis1']+1)
    ll=hdsky['crval1']+(pix-hdsky['crpix1'])*hdsky['cd1_1']
    skyspec_ref = interp1d(ll,skyspec,bounds_error=False,fill_value=0.)(reflambda)
    pixshift=zeroshift(np.nansum(skyspec_ref,axis=0),reflambda,refspec,maxshift=5.)
    if (np.abs(pixshift)>minshift):
        print "Shift: ",pixshift,"pixels for slit ",i
        if (inter=="True"):
            answer=pixshift
            cnt=0
            while (answer!="" or cnt==0):
                pixshift=float(answer)
                diagplot(
                    ((ll,np.nansum(skyspec,axis=0),"Input sky"),
                     (reflambda,refspec,"Reference"),
                     (ll-pixshift*hdsky['cd1_1'],np.nansum(skyspec,axis=0),"Shifted sky")),
                    ((6250,6350),(6813,6913),(7863,7963),(8777,8877))
                    )
                answer = raw_input("Keep (Enter) or change (value)? ")
                plt.close()
                cnt=cnt+1
            plt.close()
    else:
        pixshift=0.
    shifts=np.append(shifts,pixshift)
medshift=np.median(shifts)
print ("Median shift is ",medshift)
if (inter=="True"):
    answer = raw_input("Use median (1) or slit-specific (0) shifts?")
    if answer is 1:
        shifts=shifts*0+medshift
else:
    shifts=shifts*0+medshift
if writenew:
    for i in range(1,nsciext+1):
        pixshift=shifts[i-1]
        print "Writing slit ",i," with shift ",pixshift
        skyspec,hdsky = fits.getdata(skyfile,header=True,ext=('sci',i))
        skydq,hddq = fits.getdata(skyfile,header=True,ext=('dq',i))
        skyvar,hdvar = fits.getdata(skyfile,header=True,ext=('var',i))
        pix=np.arange(1,hdsky['naxis1']+1)
        wlc_from_header=hdsky['CRVAL1']+(pix-hdsky['CRVAL1'])*hdsky['cd1_1']
        wlc_actual=wlc_from_header-pixshift*hdsky['cd1_1']
        skyspec_interp=interp1d(wlc_actual,skyspec,bounds_error=False,fill_value=0.)(wlc_from_header)
        skyvar_interp=interp1d(wlc_actual,skyvar,bounds_error=False,fill_value=0.)(wlc_from_header)
        skydq_interp=(interp1d(wlc_actual,skydq,bounds_error=False,fill_value=0.)(wlc_from_header)+0.5).astype(int).astype(float)
        #hdsky['CRVAL1']=hdsky['crval1']-pixshift*hdsky['cd1_1']
        #hdvar['CRVAL1']=hdvar['crval1']-pixshift*hdvar['cd1_1']
        #hddq['CRVAL1']=hddq['crval1']-pixshift*hddq['cd1_1']
        hdulsky.append(fits.ImageHDU(data=skyspec_interp,header=hdsky,name='SCI'))
        hdulsky.append(fits.ImageHDU(data=skyvar_interp,header=hdvar,name='VAR'))
        hdulsky.append(fits.ImageHDU(data=skydq_interp,header=hddq,name='DQ'))

        scispec,hdsci = fits.getdata(scifile,header=True,ext=('sci',i))
        scidq,hddqsci = fits.getdata(scifile,header=True,ext=('dq',i))
        scivar,hdvarsci = fits.getdata(scifile,header=True,ext=('var',i))
        wlc_from_header=hdsci['CRVAL1']+(pix-hdsci['CRVAL1'])*hdsci['cd1_1']
        wlc_actual=wlc_from_header-pixshift*hdsci['cd1_1']
        scispec_interp=interp1d(wlc_actual,scispec,bounds_error=False,fill_value=0.)(wlc_from_header)
        scivar_interp=interp1d(wlc_actual,scivar,bounds_error=False,fill_value=0.)(wlc_from_header)
        scidq_interp=(interp1d(wlc_actual,scidq,bounds_error=False,fill_value=0.)(wlc_from_header)+0.5).astype(int).astype(float)
        #hdsci['CRVAL1']=hdsci['crval1']-pixshift*hdsci['cd1_1']
        #hdvarsci['CRVAL1']=hdvarsci['crval1']-pixshift*hdvarsci['cd1_1']
        #hddqsci['CRVAL1']=hddqsci['crval1']-pixshift*hddqsci['cd1_1']
        hdulsci.append(fits.ImageHDU(data=scispec_interp,header=hdsci,name='SCI'))
        hdulsci.append(fits.ImageHDU(data=scivar_interp,header=hdvarsci,name='VAR'))
        hdulsci.append(fits.ImageHDU(data=scidq_interp,header=hddqsci,name='DQ'))
if writenew:
    hdulsci.writeto(outsci,clobber=True)
    hdulsky.writeto(outsky,clobber=True)
    
                         
                         
