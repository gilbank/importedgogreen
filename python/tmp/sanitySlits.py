from astropy.io import fits,ascii
from astropy import wcs

import os,sys,string
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle

try:
    from astropy.coordinates import SkyCoord
except:
    print 'We seem to have an older version of astropy (probably from Ureka).\nTrying to work around'
    from astropy.coordinates import ICRS as SkyCoord

import astropy.units as u
from astropy.cosmology import WMAP9
asperkpc = WMAP9.arcsec_per_kpc_proper

#from GoGREEN_checkPhotCat import getZphWts
from GoGREEN_utils import getPhotFilename, getMaskDir

try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)


def readOutFile(name,maskNum):
    # read file and return separately IDs of bright and faint science targets. Ignore fillers for now.
    maskdir = getMaskDir(name)
#    maskfile = maskdir+'best'+name+'out%s.txt'%maskNum
    maskfile = 'best'+name+'out%s.txt'%maskNum # current dir
    dat = ascii.read(maskfile,names=('ra','dec','pwt','idnum'))
    keep = np.where(dat['pwt']<8.)[0] # reject setup stars
    dat = dat[keep]
    id = dat['idnum'].astype('int')

    # need to match to phot cat
    photfile = getPhotFilename(name)
    gal = fits.getdata(photfile)

    ok = [i for i,val in enumerate(gal['ID']) if val in id]
#    print id
#    print ok
    gal = gal[ok] # full phot cat now matched line-for-line with maskfile
#    print gal['ID']
#    print gal['ZMAG']

    # should only be PRI=1 objects in here:
    bri = np.where(gal['ZMAG']<23.5)[0]
    fnt = np.where(gal['ZMAG']>=23.5)[0]

    # careful: band-shuffled masks will have no fnts:
    if len(fnt)>0:
        return id[bri], id[fnt]
    else:
        return id[bri], None


def readOTfile(name,maskNum):
    maskfile =  'best'+name+'_%s_OT.fits'%i




# Perform sanity checks on masks generated.
# Make sure:
#  must-have faint objects are correctly propagated thru subsequent masks
#  bright objects are not repeated.
# (need to separate out fillers)

# (Later maybe do fancier things with noRpt flags and updated later masks)

def sanitySlits(name,nMasks=5):


    #name = 'SXDF49'
    #nMasks = 3

    # simple checks:
    # length of all BRIs and length of its unique entries must be same
    # length of fnts on each mask should be same

    aIDbri=[]
    #aIDfnt=[]
    for i in np.arange(1,nMasks+1):
        print 'mask %s'%i
        IDbri, IDfnt = readOutFile(name,i)
    #    print '\n\nbri%s\n'%i,IDbri, '\nfnt%s\n'%i, IDfnt
        aIDbri=np.append(aIDbri,IDbri)
        print '%s bri'%len(IDbri)
        print '%s fnt'%len(IDfnt)

        print IDfnt

        exec ('IDfnt%s = np.array(IDfnt)'%i)

    print
    print 'Total # of BRIs = %s, # of unique BRIs = %s\n'%(len(aIDbri),len(np.unique(aIDbri)))



    """
    # This doesn't seem to work as I imagine!***
    # Now check FNTs in detail:
    assert(IDfnt1.all()==IDfnt2.all())
    assert(IDfnt1.all()==IDfnt3.all())
    if (nMasks>3):
        assert(IDfnt1.all()==IDfnt4.all())
    # Assume mask5 is always band shuffled. Should have been dealt with in bri tests

    print 'all FNTs match'
    """

if __name__ == '__main__':
    name = sys.argv[1]
#    name = 'SXDF87'
#    nMasks=3
    nMasks = int(sys.argv[2])
    sanitySlits(name,nMasks)
