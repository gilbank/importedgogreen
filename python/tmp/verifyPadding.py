from astropy.io import fits,ascii
from astropy import wcs

import os,sys, string
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle
from GoGREEN_utils import getPhotFilename,readMaster


# read in clusname.stars
# and set AQSTAR_HANDCHECK=1 flag in phot FITS table

try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)


ilogin=False
ilogintmp=False
if not os.path.isfile('login.cl'):
    try:
#        irafhome = os.getenv('HOME')+'/iraf'
        irafhome = os.getenv('UR_DIR')#+'/iraf'
        if(os.path.isfile(irafhome+'/login.cl')):
            ilogin=True
            ilogintmp=True # need to delete at home
            os.system('cp '+irafhome+'/login.cl .')
        else:
            pass # no login found. need to die
            print
            print "no login.cl found. copy to current directory to run this task"
            print
            exit(1)
    except:
        print
        print "no login.cl found. copy to current directory to run this task"
        print

else:
    pass
    # got login.cl here. okay


try:
    from pyraf import iraf
    iraf.gemini()
    iraf.gmos()
    iraf.mostools()
    iraf.stsdas()
    iraf.tables()
    iraf.ttools()
    ipyraf = True
except:
    ipyraf=False
    print
    print "The required gemini IRAF/pyraf tasks are not available"
    print
    exit(1)




# Check spacing between slits in a mask:
name='SPT0205'
masknum=3

if(0):
    masterfile = os.getenv('GoGREEN_scripts')+'/config/GOGREEN.fits'
#    masterfile = eval(cfg.masterfileExpr) # needs to be set before cfg is declared
    mdat = fits.getdata(masterfile)
    try:
        ok=np.where(mdat['cluster']==name)[0]
    except:
        print '%s not found in %s'%(name,masterfile)

    mdat = mdat[ok] # only keep line we want!

    maskname = mdat['mask1cut']

tmaskdir = getPhotFilename(name)
maskdir = string.join(tmaskdir.split('/')[0:-1],'/')+'/'

#maskname = maskdir + '2014B/SPT-0205_mask1_ver1.03_GMI_OT.fits'
maskname = maskdir + '2014B/SPT-0205_mask%s_ver1ODF1.fits'%masknum

odat,ohdr = fits.getdata(maskname,header=True)
pxscale = ohdr['PIXSCALE']

ss = np.argsort(odat['y_ccd'])
odat=odat[ss]

yp = odat['y_ccd']
pri = odat['priority']
ID = odat['ID']
mag = odat['MAG'] # assumes z-mag here is correct. Otherwise need ot match back to original phot cat
fntLim = 23.5 #*** hard-wired here
ok=np.where(mag < fntLim)[0]
pri[ok]='2' # set bright targets to 2 for reference. pri=2 targets can be removed if they collide; pri=1 need more thought!

slitlen = odat['slitsize_y']
#fid = np.where(slitlen>3.0)[0]
#slitlen[fid]=3.00

dy = yp[1::] - yp[0:-1]
dyArcsec = dy*pxscale
minSepArcsec = 6.145 #*** CHECK
for t in range(len(dy)):
    i = np.argsort(dy)[t]  # sort by spacing
    gap = dyArcsec - (slitlen[i]+slitlen[i+1])
    if dyArcsec[i] > minSepArcsec: continue
    print 'spacing = %.1f pix (%.2f"), objects: %.0f (P=%s)--%.0f (P=%s) // gap = %6.2f // (%.2f & %.2f)'\
          %(dy[i],dyArcsec[i],ID[i],pri[i],ID[i+1],pri[i+1],gap[i],slitlen[i],slitlen[i+1])















"""
os.system('rm -f _ttt _tttt')
#ss = iraf.tdump ("bestSXDF60_m2_OT.fits",\
#                 cdfile="_ttt", pfile="_tttt", datafile="STDOUT", columns="", rows="-", pwidth=-1, Stdout=1)
ss = iraf.tdump (maskname,\
                 cdfile="STDOUT", pfile="STDOUT", datafile="STDOUT", columns="", rows="-", pwidth=-1, Stdout=1)

print ss
stop()

dat = ascii.read(ss,names=('ID','rah','dec','xp','yp','mag','pri','ssize_x','ssize_y','slittilt','slitpos_y','junk'))
#print dat

# only y pos matters:
ss = np.argsort(dat['yp'])
dat=dat[ss]

print dat
yp = dat['yp']
dy =  yp[1::] - yp[0:-1]

print np.transpose((dat['ID'][0:-1], dat['ID'][1::],dy))
# transpose((dat['ID'][0:-1], dat['ID'][1::],dy,dat['pri'][0:-1],dat['pri'][1::]))

# get detector info to find out what min. separation in pixels is supposed to be:

"""