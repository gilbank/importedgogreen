from astropy.io import fits,ascii
from astropy import wcs

import os,sys
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle
from GoGREEN_utils import getPhotFilename


# read in clusname.stars
# and set AQSTAR_HANDCHECK=1 flag in phot FITS table

try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)


def starApply(clusname):
    #clusname = 'SXDF60'


    starID = np.loadtxt(clusname+'.stars')#, dtype='string')


    # should sort out version number really ****:
    #photfile='/home/gilbank/Proj/goGreen/New/Data/reduced/%s/Catalogues/Masks/%s_phot_v2.0.fits'%(clusname,clusname)
    photfile = getPhotFilename(clusname)
    #imfile='/home/gilbank/Proj/goGreen/New/Data/reduced/%s/Imaging/%sXGG_z.fits.gz'%(clusname,clusname)

    dat = fits.getdata(photfile)
    # get AQSTAR_CAND info:
    okst = np.where(dat['AQSTAR_CAND']==1)[0]

    goodStarIndx = [i for i,x in enumerate(dat['ID']) if x in starID]
    handcheckFlag = np.zeros(len(dat)).astype(int)
    handcheckFlag[goodStarIndx] = 1

    out = recf.append_fields(dat, 'AQSTAR_HANDCHECK', handcheckFlag, usemask=False, asrecarray=True)


        # Need to fix table-writing problem as MLB did for mkcatCat:
    #    ra = gal['RA']
    #    dec = gal['DEC']
    #    irac1 =
    gal = out
    if(1):
        # MLB: Attempt to make table readable by IRAF, FV, others.
        # Following http://astropy.readthedocs.org/en/v1.0.3/io/fits/
        prihdu=fits.PrimaryHDU(header=fits.Header())
        col1=fits.Column(name='ID',format='1J',disp='I11',array=gal['ID'].astype(int))
        col2=fits.Column(name='RA',format='E',array=gal['RA'])
        col3=fits.Column(name='DEC',format='E',array=gal['DEC'])
        col4=fits.Column(name='IRAC1',format='E',array=gal['IRAC1'])
        col5=fits.Column(name='EIRAC1',format='E',array=gal['EIRAC1'])
        col6=fits.Column(name='ZMAG',format='E',array=gal['ZMAG'])
        col7=fits.Column(name='EZMAG',format='E',array=gal['EZMAG'])
        col8=fits.Column(name='ZPH',format='E',array=gal['ZPH'])
        col9=fits.Column(name='ZPHLO',format='E',array=gal['ZPHLO'])
        col10=fits.Column(name='ZPHHI',format='E',array=gal['ZPHHI'])
        col11=fits.Column(name='IZPH',format='E',array=gal['IZPH'])
        col12=fits.Column(name='ZS',format='E',array=gal['ZS'])
        col13=fits.Column(name='AQSTAR_CAND',format='1J',disp='I11',array=gal['AQSTAR_CAND'])
        col14=fits.Column(name='XP',format='E',array=gal['XP'])
        col15=fits.Column(name='YP',format='E',array=gal['YP'])
        col16=fits.Column(name='AQSTAR_HANDCHECK',format='E',array=gal['AQSTAR_HANDCHECK'])
        cols=fits.ColDefs([col1,col2,col3,col4,col5,col6,col7,col8,col9,col10,col11,col12,col13,col14,col15,col16])
        #cols=fits.ColDefs(obj)
        tbhdu=fits.BinTableHDU.from_columns(cols)
        thdulist=fits.HDUList([prihdu,tbhdu])
        thdulist.writeto('fooWStars.fits',clobber=True)


    #fits.writeto('fooWStars.fits',out,clobber=True)
    print('\nRemember to overwrrite %s with fooWStars.fits\n'%photfile)


if __name__ == '__main__':
    name = sys.argv[1]
    starApply(name)


