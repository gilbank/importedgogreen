from astropy.io import fits,ascii
from astropy import wcs

import os,sys
import numpy as np
import matplotlib.pyplot as plt

from numpy.lib import recfunctions as recf
from matplotlib.patches import Rectangle

from GoGREEN_utils import getPhotFilename

try:
    foo = 'blah'+os.getenv('GoGREEN_scripts')
except:
    print
    print "GoGREEN_scripts is not defined"
    print
    exit(1)

clusname = 'SXDF60'

# should sort out version number really ****:
#photfile='/home/gilbank/Proj/goGreen/New/Data/reduced/%s/Catalogues/Masks/%s_phot_v1.0_USE.fits'%(clusname,clusname)
photfile = getPhotFilename(clusname)
imfile='/home/gilbank/Proj/goGreen/New/Data/reduced/%s/Imaging/%sXGG_z.fits.gz'%(clusname,clusname)

dat = fits.getdata(photfile)
# get AQSTAR_CAND info:
okst = np.where(dat['AQSTAR_CAND']==1)[0]
print dat['ID'][okst],dat['RA'][okst],dat['DEC'][okst]

# Make into region file and make ascii list of IDs to be manually edited
regfile = clusname+'_stars.reg'
xxx = open(regfile,'w')
for i in range(len(okst)):
    xxx.write('fk5; circle (%.7f,%.7f,5") #text={%s}\n'%(dat['RA'][okst[i]],dat['DEC'][okst[i]],dat['ID'][okst[i]]))
xxx.close()

np.savetxt(clusname+'.stars',dat['ID'][okst],fmt='%s')

print ('ds9 %s -region %s'%(imfile,regfile))
print
#os.system('ds9 %s -region %s'%(imfile,regfile))




