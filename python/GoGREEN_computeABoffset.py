# Python program to compute AB offsets

import argparse
import numpy
from scipy.interpolate import interp1d

parser = argparse.ArgumentParser(description='Plot kilonova host')

parser.add_argument('--filter', default=None,
                        help='Filter curve')

parser.add_argument('--vega', default=None,
                        help='Vega Spectrum')

parser.add_argument('--nm', default=False, action='store_true',
                        help='Filter wavelength in nm')

args = parser.parse_args()

class spectrum:
    """A spectrum"""
    def __init__(self):
        self.wave=numpy.array([],'float')
        self.flux=numpy.array([],'float')
        return

    def read(self,file):
        data=numpy.genfromtxt(file,dtype=[('wavelength',float),('flux',float)])
        self.wave=data['wavelength']
        self.flux=data['flux']
        return


class filterCurve:
    """A filter"""
    def __init__(self):
        self.wave=numpy.array([],'float')
        self.trans=numpy.array([],'float')
        self.effectiveWavelength=0.
        self.magAB=0.
        self.magVega=0.
        self.ABoffset=0.

        return

    def read(self,file,nm):
        data=numpy.genfromtxt(file,comments='#',dtype=[('wavelength',float),('transmission',float)])
        if nm:
            factor=10.
        else:
            factor=1
        self.wave=data['wavelength'] * factor
        self.trans=data['transmission']
        return

    def computeEffectiveWavelength(self,template):
        flux1=interp1d(template.wave,template.flux)(self.wave)*self.trans*self.wave
        flux2=interp1d(template.wave,template.flux)(self.wave)*self.trans
        # There is not need to code it in this way
        self.effectiveWavelength=flux1.sum() / flux2.sum()
        
        return self.effectiveWavelength

    def computeABmag(self,template):
        # To convert this into f_lambda, divide by c/lambda^2
        c=2.992792e18 # Angstrom/s
        flux=interp1d(template.wave,template.flux)(self.wave)*self.trans*self.wave**2./2.992792e18

        # An object with an AB mag of zero has a constant flux density of 3631 Jy
        # 3631 Jy # 1 Jy = 1e-26 W / m^2 / Hz = 3.631 e-20 erg / s /cm^2 / Hz
        # NB 48.60=2.5*log10(3.631e-20)

        const=self.trans
        self.magAB=-2.5*numpy.log10(flux.sum()/const.sum())-48.60

        return self.magAB

# Create a filter instance

filt=filterCurve()
filt.read(args.filter,args.nm)

# Create a spectrum instance

spec=spectrum()
spec.read(args.vega)

# Compute the AB offset
print "AB offset = %4.3f" % (filt.computeABmag(spec))
