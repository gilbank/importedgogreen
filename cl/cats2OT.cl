procedure cats2OT(cluster,catalogue)
# Feb 7, 2015 MLB
# Simple IRAF script to take output from Dave's mask design code and create OT table.  Adapted for new formats.
# Input catalogue is COSMOS-125_GMI.fits.  Output from gmskcreate
# Assumes that the Masks  subdirectory is populated the OT subdirectory.
string cluster {prompt="Name of GOGREEN cluster"}
string catalogue {prompt="Catalogue with handpicked AQ stars identified"}
int    Nmasks {prompt="Number of masks designed"}
int    first {prompt="First mask number"}
string rootdir="/home/mbalogh/projects/GOGREEN/data/" {prompt="Main project directory"}
string catversion="1.2" {prompt="version number should match input catalogue"}
string maskversion="1.0" {prompt="Version number of mask design"}
int    status {0.,prompt="Exit status (0=good)"}
begin
   string mdir,OTdir
   string targets,filler,primaryOT,fillerOT,stars,galaxies,starsOT
   int N,nfill,NR,i,nstars
   cache tinfo
   #if (gemini.verno != 'v1.12'){
   #    goto crash
   #}
   mdir=rootdir//"Data/reduced/"//cluster//"/Catalogues/Masks/"
# 
# Now identify entries from Dave's mask design code.
# best(Cluster)outN.txt are priority 1 targets and acquisition stars in mask N
# best(Cluster)extrasN.txt are fillers in mask N and should be priority 3.  Might be empty.
  for (N=first;N<=Nmasks;N+=1){
    targets=mdir//"best"//cluster//"out"//N//".txt"
    filler=mdir//"best"//cluster//"extras"//N//".txt"
    stars=mdir//"best"//cluster//"stars"//N//".txt"
    galaxies=mdir//"best"//cluster//"galaxies"//N//".txt"
    primaryOT=mdir//cluster//"_p1_mask"//N//"_GMI_OT.fits"
    fillerOT=mdir//cluster//"_p3_mask"//N//"_GMI_OT.fits" 
    starsOT=mdir//cluster//"_p0_mask"//N//"_GMI_OT.fits"
    tdelete(primaryOT,>&"dev$null")
    tdelete(fillerOT,>&"dev$null")
    tdelete(starsOT,>&"dev$null")

    awk ("\'$3>=9\'",targets,>stars)
    awk ("\'$3<9\'",targets,>galaxies)
    tmatch(mdir//catalogue,galaxies,primaryOT,"ID","c4",0.1,incol2="c10")
    tmatch(mdir//catalogue,stars,starsOT,"ID","c4",0.1,incol2="c10")
    tinfo(stars,ttout-)
    nstars=tinfo.nrows
    for (i=1;i<=tinfo.nrows;i+=1){
	 partab("0",starsOT,column="priority",row=i)
    }
    tinfo(filler,ttout-)
    nfill=tinfo.nrows
    if (nfill>0){
          tmatch(mdir//catalogue,filler,fillerOT,"ID","c4",0.1,incol2="c10")
	  tinfo(fillerOT,ttout-)
	  NR=tinfo.nrows
	  for (i=1;i<=NR;i+=1){
	    partab("3",fillerOT,column="priority",row=i)
          }
    }
    OTdir=mdir//"OT/"
    tdelete(OTdir//cluster//"_mask"//N//"_ver"//maskver//"_GMI_OT.fits",>&"dev$null")
    tmerge (mdir//cluster//"_p?_mask"//N//"_GMI_OT.fits",OTdir//cluster//"_mask"//N//"_ver"//maskver//"_GMI_OT.fits","append")
    }

crash:
status=1
end
