procedure preim2OT(cluster,objcat)
# Oct 29, 2014 MLB
# Simple IRAF script to take output from Dave's mask design code and create OT table.
# Must use v113 version of IRAF.  
# Assumes that the masks/ subdirectory is populated with Cluster/ and Cluster/OT subdirectories.
# Dave provides gmmps-like tables, but only for primary targets, and magnitudes are wrong.
# Can match list of IDs with input catalogue.  Remember to:
# 1) Divide RA by 15
# 2) Do not multiply pixels by 2!!  If I use Dave's tables I must divide by 2.
# Have to append filler targets separately so I can adjust their priority

string cluster {prompt="Name of GOGREEN cluster"}
string objcat {prompt="Catalogue from preimaging.  "}
string preimage {prompt="Name of corresponding preimage"}
real slitx {1.0,prompt="Slit width in arcseconds"}
real slity {3.0,prompt="Slit length in arcseconds"}
int    Nmasks {prompt="Number of masks designed"}
int    first {prompt="First mask number"}
string rootdir="/home/mbalogh/projects/GOGREEN/data/Data/reduced/" {prompt="Main project directory"}
string catdir="Catalogues/" {prompt="catalogue subdirectory"}
string maskdir="Masks/" {prompt="mask subdirectory"}
string imdir="Imaging/" {prompt="image subdirectory"}
string catversion="1.2" {prompt="version number should match input catalogue"}
string maskversion="1.0" {prompt="Version number of mask design"}
int    status {0.,prompt="Exit status (0=good)"}
begin
   string infits,mdir,OTdir
   string colfile,targets,filler,temp,p1GMI,p0GMI,p3GMI
   int N,nfill
   cache tinfo
# First create a fits table with relevant columns, from Adam's .cat files
# Make sure to convert RA to hours
# Pixels should be binned according to the preimage
   colfile=mktemp("catcols")
   temp=mktemp("tmp")
   if (gemini.verno != 'v1.14'){
       goto crash
   }
   mdir=rootdir//cluster//"/"//catdir//maskdir
   infits=mdir//cluster//"_v"//catversion//".fits"
   tdelete(infits,>&"dev$null")
   #awk("\'NR>3{print $1,$2/15,$3,$4,$5,$10,"//slitx//","//slity//",0}\'",rootdir//catdir//muzzinfits,>infits)
   tproject(objcat,infits,"ID,RA,DEC,XP,YP,ZMAG")
   tchcol(infits,"XP","x_ccd","%15.7g","pixels",verbose-)
   tchcol(infits,"YP","y_ccd","%15.7g","pixels",verbose-)
   tchcol(infits,"ZMAG","MAG","%15.7g","mag",verbose-)
   tcalc (infits,"RA","RA/15.")
   tchcol(infits,"RA","RA","%15.7g","H",verbose-)
   tchcol(infits,"ID","ID","%-20s","CH*20",verbose-)
   tcalc(infits,"slitsize_x",slitx,colunits="arcsec",colfmt="%15.7g")
   tcalc(infits,"slitsize_y",slity,colunits="arcsec",colfmt="%15.7g")
   tcalc(infits,"slittilt",0,colunits="degrees",colfmt="%15.7g")
   #print ("ID   CH*20   %-20s   \"\"",>colfile)
   #print ("RA    R      %15.7g  H",>>colfile)
   #print ("DEC              R          %15.7g  deg",>>colfile)
   #print ("x_ccd            R          %15.7g  pixels",>>colfile)
   #print ("y_ccd            R          %15.7g  pixels",>>colfile)
   #print ("MAG              R          %15.7g  mag",>>colfile)
   #print ("slitsize_x       R          %15.7g  arcsec",>>colfile)
   #print ("slitsize_y       R          %15.7g  arcsec",>>colfile)
   #print ("slittilt         R          %15.7g  degrees",>>colfile)
   #tcreate(infits,colfile,infits)
   #delete(colfile)
# 
# Now identify entries from Dave's mask design code.
# best(Cluster)outN.txt are priority 1 targets and acquisition stars in mask N
# best(Cluster)extrasN.txt are fillers in mask N and should be priority 3.  Might be empty.
  for (N=first;N<=Nmasks;N+=1){
    targets=mdir//"best"//cluster//"out"//N//".txt"
    filler=mdir//"best"//cluster//"extras"//N//".txt"
    tmatch(infits,targets,temp,"ID","c4",0.1,incol2="c3")
    p1GMI=mdir//cluster//"_p1_mask"//N//"_GMI"
    p0GMI=mdir//cluster//"_p0_mask"//N//"_GMI"
    p3GMI=mdir//cluster//"_p3_mask"//N//"_GMI"    
    tinfo(filler,ttout-)
    nfill=tinfo.nrows
    if (nfill>0){tmatch(infits,filler,p3GMI,"ID","c4",0.1,incol2="c300")}
    tdelete(p1GMI//".fits",>&"dev$null")
    tdelete(p0GMI//".fits",>&"dev$null")
    tdelete(p3GMI//".fits",>&"dev$null")
    tdelete(p1GMI//"_OT.fits",>&"dev$null")
    tdelete(p0GMI//"_OT.fits",>&"dev$null")
    tdelete(p3GMI//"_OT.fits",>&"dev$null")
    tselect(temp,p1GMI//".fits","c3<9")
    tselect(temp,p0GMI//".fits","c3==9")
    tdelete(temp)
    stsdas2objt(p1GMI,image=rootdir//cluster//"/"//imdir//preimage,instrument="gmos",pri_col="",priority=1,other_col="slitsize_x,slitsize_y,slittilt")
    # Had to edit stsdas2objt to allow this!
    stsdas2objt(p0GMI,image=rootdir//cluster//"/"//imdir//preimage,instrument="gmos",pri_col="",priority=0,other_col="slitsize_x,slitsize_y,slittilt")
    if (nfill>0){stsdas2objt(p3GMI,image=rootdir//cluster//"/"//imdir//preimage,instrument="gmos",pri_col="",priority=3,other_col="slitsize_x,slitsize_y,slittilt")}
    OTdir=mdir//"OT/"
    tdelete(OTdir//cluster//"_mask"//N//"_ver"//maskver//"_GMI_OT.fits",>&"dev$null")
    tmerge (mdir//cluster//"_p?_mask"//N//"_GMI_OT.fits",OTdir//cluster//"_mask"//N//"_ver"//maskver//"_GMI_OT.fits","append")
    }
goto clean

crash:
  print ("Wrong version of gemini package")
  status=1
  goto theend
clean:
  status=0

theend:
  if (status==0){print "Done"}
  else {print "Error"}
end
