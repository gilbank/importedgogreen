procedure ggpreim(cluster)
# v2.0 MLB Apr 13, 2015
# Version number must be udpated in internal variable swversion
# Preimaging script for GOGREEN deep z-band data
# Uses Gemini IRAF packages gemini_v113
# 
# Requires a file named cluster.lis (where cluster is the required first argument).  
#  This file contains the file names of each science image and must be located in the datadir
#  Optional files named cluster_bias.lis/cluster_flat.lis similarly list the file names of biases and flats.  
#  These are used if usebias/useflat are null.  
#  Otherwise usebias/useflat give the name of the processed calibration file to use.
#
string cluster {prompt="Name of cluster or other useful tag"}
string datadir {"gogreen$Data/",prompt="Root directory"}
string biasdir {"raw/GMOS-S/bias/"}
string rawdir {"raw/GMOS-S/15A/"}
string reddir {"reduced/",prompt="Location of reduced data.  Subdirectory of root."}
string usebias {"gS20140930S0148_bias.fits", prompt="Name of bias or null to generate"}
string useflat {"", prompt="Name of flat or null to generate"}
string bpm {"gmos$data/gmos-s_bpm_HAM_22_12amp_v1.fits", prompt="Name of bad pixel mask"}
bool doreduce {yes, prompt="Yes to reduce the science data"}
bool dofringe {yes, prompt="Yes to subtract fringe frame "}
string fringe {"", prompt="Fringe file name to subtract.  If null and dofringe is yes, fringe frame is generated."}
bool domosaic {yes, prompt="Make mosaics of reduced images"}
bool dostack  {yes, prompt="Stack science images"}
bool clean {no, prompt="Remove intermediate files when done"}
struct *flist
begin
  string infile,outdir,fnames,image,logfile
  string flat,bias,gmstring,fringename
  string swversion 
  swversion="2.0" #HEADER variable to identify which version of this script was used
  outdir=datadir//reddir//cluster//"/Imaging/"
  infile=outdir//cluster//".lis"
  logfile=outdir//cluster//".log"
  delete(logfile,>&"dev$null")
  print ("Reduced with version "//swversion//" of ggpreim", >>logfile)
  if (usebias ==""){
      # Read names of bias images from file and generate a master bias
      # Not implemented yet
      bnames=datadir//cluster//"_bias.lis"
  } else {
      # Set master bias to the value of usebias 
      bias=datadir//biasdir//usebias
  }
  if (useflat == ""){
     # Read names of flats from directory and generate master flat
     fnames=outdir//cluster//"_flats.lis"
     cd(outdir)
     imdelete(cluster//"_flat.fits",>&"dev$null")
     imdelete("rg_"//cluster//"*.fits",>&"dev$null")
     imdelete("g_"//cluster//"*.fits",>&"dev$null")
     print ("Generating "//cluster//"_flat.fits", >>logfile)
     # gprepare, reduce and combine all flats for this cluster
     giflat("@"//fnames,
	    cluster//"_flat.fits",
	    normsec="default",
	    fl_scale+,
	    sctype="mean",
	    statsec="default",
	    key_gain="GAIN",
	    fl_stamp-,
	    sci_ext="SCI",
	    var_ext="VAR",
	    dq_ext="DQ",
	    fl_vardq+,
	    sat="default",
	    verbose+,
	    logfile=logfile,
	    status=0,
	    combine="average",
	    reject="avsigclip",
	    lthreshold=INDEF,
	    hthreshold=INDEF,
	    nlow=0,
	    nhigh=1,
	    nkeep=1,
	    mclip+,
	    lsigma=3.,
	    hsigma=3.,
	    sigscale=0.1,
	    grow=0.,
	    gp_outpref="g_"//cluster//"_",
	    rawpath=datadir//rawdir,
	    key_ron="RDNOISE",
	    key_datasec="DATASEC",
	    gaindb="default",
	    bpm=bpm,
	    gi_outpref="r",
	    bias=bias,
	    fl_over+,
	    fl_trim+,
	    fl_bias+,
	    fl_qecorr+,
	    fl_inter-,
	    nbiascontam="default",
	    biasrows="default",
	    key_biassec="BIASSEC",
	    median-,
	    function="chebyshev",
	    order=3,
	    low_reject=3.,
	    high_reject=3.,
	    niterate=2,
	    qe_data="gmosQEfactors.dat",
	    qe_datadir="gmos$data/")
     flat=outdir//cluster//"_flat.fits"
     hedit(flat//"[0]","GGPREIM_VERSION",swversion,add+,show-,verify-,update+)
  } else {
     # Use the flat specified by useflat
     flat=outdir//useflat
     print ("Using "//flat, >>logfile)
  }
  if (doreduce){
      # Attach cluster name to image during gprepare to allow later globbing of filenames
      # gprepare, overscan, bias and flat field correct
      imdelete("rg_"//cluster//"_*",>&"dev$null")
      imdelete("g_"//cluster//"_*",>&"dev$null")
      print ("Gprepare, overscan, bias and flat field correcting data", >>logfile)
      gireduce("@"//infile,
	       outpref="r",
	       outimages="", 
	       fl_over+, 
	       fl_trim+,
	       fl_bias=yes, 
	       fl_dark=no,
	       fl_qecorr+,
	       fl_flat+, 
	       fl_vardq+, 
	       fl_addmdf=no, 
	       bias=bias,
	       dark="", 
	       flat1=flat, 
	       flat2="", 
	       flat3="", 
	       flat4="",
	       qe_data="gmosQEfactors.dat",
	       qe_datadir="gmos$data/",
	       key_exptime="EXPTIME", 
	       key_biassec="BIASSEC", 
	       key_datasec="DATASEC", 
	       rawpath=datadir//rawdir,
	       gp_outpref="g_"//cluster//"_", 
	       sci_ext="SCI", 
	       var_ext="VAR", 
	       dq_ext="DQ",
	       key_mdf="MASKNAME", 
	       mdffile="", 
	       mdfdir="gmos$data/", 
	       bpm=bpm,
	       gaindb="default", 
	       sat="default", 
	       key_nodcount="NODCOUNT",
	       key_nodpix="NODPIX",
	       key_filter="FILTER2",
	       key_ron="RDNOISE", 
	       key_gain="GAIN", 
	       ron=3.5, 
	       gain=2.2, 
	       fl_mult+,
	       fl_inter-, 
	       median=no,
	       function="chebyshev", 
	       nbiascontam="default",
	       biasrows="default",
	       order="default", 
	       low_reject=3., 
	       high_reject=3., 
	       niterate=2,
	       logfile=logfile, 
	       verbose=yes,
	       status=0, 
	       scanfile="")
  }
  if (dofringe) {
      # Subtract fringe frame from each data frame.
      if (fringe == ""){
         # No fringe frame is specified, so we generate one:
         fringename=cluster//"_fringe.fits"
         imdelete (fringename,>&"dev$null")
         print ("Generating Fringe frame "//fringename, >>logfile)
         # BPM doesn't work here
         gifringe ("rg_"//cluster//"_*.fits",
		   fringename,
		   typezero="mean",
		   skysec="default",
		   skyfile="",
		   key_zero="OFFINT",
		   msigma=4.,
		   bpm="", 
		   fl_mask-,
		   combine="median",
		   reject="avsigclip",
		   scale="none",
		   weight="none",
		   statsec="[*,*]",
		   expname="EXPTIME",
		   nlow=1,
		   nhigh=1,
		   nkeep=0,
		   mclip+,
		   lsigma=3.,
		   hsigma=3.,
		   sigscale=0.1,
		   sci_ext="SCI",
		   var_ext="VAR",
		   dq_ext="DQ",
		   fl_vardq+,
		   logfile=logfile,
#		   glogpars="",
		   verbose+)      
        hedit(cluster//"_fringe.fits[0]","GGPREIM_VERSION",swversion,add+,show-,verify-,update+)
      } else {
        # Use specified fringe name
        fringename=fringe
      }
      imdelete ("frg_"//cluster//"_*.fits",>&"dev$null")
      print ("Removing fringes using file "//fringename, >>logfile)
      girmfringe ("rg_"//cluster//"*.fits",
		  fringename,
		  outimages="",
		  outpref="f", 
		  fl_statscale=no,
		  statsec="default", 
		  scale=0., 
		  fl_propfvard+,
		  logfile=logfile, 
		  verbose=yes, 
		  status=0,
		  scanfile="")
      # gmstring tells which files to mosaic.  If doing fringe correction, this is:
      gmstring="frg_"//cluster
  } else {
      # No fringe correction so we will mosaic rg_cluster*files
      gmstring="rg_"//cluster
  }
  if (domosaic){
    print ("Making mosaics", >>logfile)
    imdelete ("m*"//cluster//"*.fits",>&"dev$null")
    delete ("m*"//cluster//"*",>&"dev$null")
    # Mosaic the detectors for each reduced image
    #
    # Something very weird is happening here.  File globbing is not working within the script for this task.
    # Bug may be in above - last run seemed to skip girmfringe, but executed the line above it.  
    # This should (and used to!) work:
    #
    gmosaic (gmstring//"*.fits",
    #
    # But for now I have to loop over files:			   
    #flist=infile
    #while (fscan(flist,image) != EOF){
    #gmosaic (gmstring//"_"//image//".fits",
    #
	     outimages="", 
	     outpref="m", 
	     fl_paste=no, 
	     fl_vardq+, 
	     fl_fixpix=no,
	     fl_clean=no, 
	     fl_fulldq-,
	     dqthresh=0.1,
	     bitflags="all",
	     geointer="linear", 
	     gap="default", 
	     bpmfile="gmos$data/chipgaps.dat",
	     statsec="default", 
	     obsmode="IMAGE", 
	     sci_ext="SCI", 
	     var_ext="VAR", 
	     dq_ext="DQ",
	     mdf_ext="MDF", 
	     key_detsec="DETSEC", 
	     key_ccdsec="CCDSEC",
	     key_datsec="DATASEC", 
	     key_ccdsum="CCDSUM",
	     key_obsmode="OBSMODE", 
	     logfile=logfile, 
	     fl_real=no, 
	     verbose=yes, 
	     status=0,
	     scanfile="")
    # Closing bracket that can be removed if I get the globbing to work:
    #}
    #
   }
   if (dostack){
   # Finally coadd all the mosaicked images.  Output file cluster//"_coadd.fits".
     imdelete (cluster//"_coadd.fits",>&"dev$null")
     print ("Coadding mosaicked images to create "//cluster//"_coadd.fits",>>logfile)
     imcoadd ("m*"//cluster//"*.fits",
	     outimage=cluster//"_coadd.fits",
	     sci_ext="SCI", 
	     var_ext="VAR", 
	     dq_ext="DQ",
	     immasks="DQ", 
	     database="imcoadd.dat", 
	     threshold=10., 
	     fwhm=7., 
	     box=15.,
	     alignmethod="wcs", 
	     asection="default", 
	     xwindow=181, 
	     key_inst="INSTRUME",
	     key_camera="CAMERA", 
	     key_inport="INPORT", 
	     key_xoff="default",
	     key_yoff="default", 
	     instscale=1., 
	     xsign="default", 
	     ysign="default",
	     key_pixscale="PIXSCALE", 
	     pixscale=1., 
	     dispmag=1., 
	     rotate=no, 
	     scale=no,
	     geofitgeom="rscale", 
	     order=3, 
	     sigfit=2.5, 
	     niter=5, 
	     coolimit=0.3,
	     geointer="linear", 
	     geonxblock=2048, 
	     geonyblock=2048, 
	     key_ron="RDNOISE",
	     key_gain="GAIN", 
	     ron=1., 
	     gain=1., 
	     datamin=-1000., 
	     key_sat="SATURATI",
	     datamax=50000., 
	     aperture=30., 
	     limit=15., 
	     key_limit=yes, 
	     lowsigma=7.,
	     lowlimit=500., 
	     scalenoise=0., 
	     growthrad=1, 
	     statsec="default",
	     badpixfile="default", 
	     fl_inter=no, 
	     fl_refmark=no, 
	     fl_mark=no, 
	     fl_fixpix=no,
	     fl_find=yes, 
	     fl_map=yes, 
	     fl_trn=yes, 
	     fl_med=yes, 
	     fl_add=yes, 
	     fl_avg=no,
	     fl_scale=yes, 
	     fl_overwrite=no, 
	     logfile=logfile, 
	     verbose=yes, 
	     status=0,
	     inimag="", 
	     inmask="", 
	     input="")
   hedit(cluster//"_coadd.fits[0]","GGPREIM_VERSION",swversion,add+,show-,verify-,update+)
   }
   if (clean){
     print ("Cleaning up",>>logfile)
     cd(outdir)
     imdelete("rg_"//cluster//".fits",>&"dev$null")
     imdelete("g_"//cluster//".fits",>&"dev$null")
     imdelete("rg_"//cluster//"_"//image,>&"dev$null")
     imdelete ("frg_"//cluster//"_*.fits",>&"dev$null")
     imdelete ("m*"//cluster//"_*.fits",>&"dev$null")
     delete ("m*"//cluster//"*",>&"dev$null")
     delete ("tmp*",>&"dev$null")
   }
end

