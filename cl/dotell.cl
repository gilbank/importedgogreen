procedure dotell (filename,outname,tellname)

# Do telluric corrections.  

string filename 
string outname
string tellname
string caldir
real shift=0.0
real scale=1.0

begin
    int nsci,i1,i,imax,npix
    real ymean,ymax,mean,stddev,min,max,air
    string tmpfile,junk,tmptell
    imgets(filename//"[0]", "NSCIEXT", >& "dev$null") 
    nsci=int(imgets.value)
    imgets(filename//"[0]", "AIRMASS", >& "dev$null") 
    air=real(imgets.value)
    print ("Doing telluric correction on ",nsci,"extensions with airmass ",air)
    imdelete(outname,>&"dev$null")
    fxcopy(filename,outname//".fits","0-1",verbose-)
    ymax=0  
    for (i=1; i<= nsci; i +=1){
      for (j=1; j<=3; j +=1){ # Loop over SCI, VAR, DQ extensions
	   tmpfile=mktemp("tmpsplit")  
	   tmptell=mktemp("tmptell")
	   i1=(i-1)*3+j+1  
	   fxcopy(filename,tmpfile//".fits",i1,verbose-)
	   if (j==1){
	      # Do all at once:
              telluric(tmpfile//".fits",tmpfile//"_corr.fits",tellname,air,"no",tweak+,xcorr+,shift=shift,scale=scale,sample="6840:7054,7560:7700,8115:8360,8935:9840",interactive+,lag=50,>&"dev$null")
              fxinsert(tmpfile//"_corr.fits",outname//".fits["//i1//"]",0,verbose-)
	      # Or one at a time:
              #telluric(tmpfile//".fits",tmpfile//"_Acorr.fits",caldir//"Aband_cont.fits",air,"no",tweak+,xcorr+,shift=shift,scale=scale,>&"dev$null")
              #telluric(tmpfile//"_Acorr.fits",tmpfile//"_ABcorr.fits",caldir//"Bband_cont.fits",air,"no",tweak+,xcorr+,shift=shift,scale=scale,>&"dev$null")
              #telluric(tmpfile//"_ABcorr.fits",tmpfile//"_WABcorr.fits",caldir//"Waterband_cont.fits",air,"no",tweak+,xcorr+,shift=shift,scale=scale,>&"dev$null")
              #telluric(tmpfile//"_WABcorr.fits",tmpfile//"_W2ABcorr.fits",caldir//"Waterband2_cont.fits",air,"no",tweak+,xcorr+,shift=shift,scale=scale,>&"dev$null")
              #fxinsert(tmpfile//"_W2ABcorr.fits",outname//".fits["//i1//"]",0,verbose-)
	      hedit(outname//".fits["//i1//"]","EXTNAME","SCI",addonly+,verify-,show-,update+)
	      hedit(outname//".fits["//i1//"]","EXTVER",i,addonly+,verify-,show-,update+)
            } else {        
	      if (j==2){
	         sarith(outname//".fits[SCI,"//i//"]","/",filename//".fits[SCI,"//i//"]",tmptell)
		 sarith(tmptell,"sqrt","",tmptell,clobber+)
		 sarith(tmpfile//".fits","*",tmptell,tmpfile,clobber+)
		 fxinsert(tmpfile,outname//".fits["//i1//"]",0,verbose-)
	         hedit(outname//".fits["//i1//"]","EXTNAME","VAR",addonly+,verify-,show-,update+)
		 hedit(outname//".fits["//i1//"]","EXTVER",i,addonly+,verify-,show-,update+)
	      }
	      if (j==3){
	         fxinsert(tmpfile,outname//".fits["//i1//"]",0,verbose-)
	         hedit(outname//".fits["//i1//"]","EXTNAME","DQ",addonly+,verify-,show-,update+)
		 hedit(outname//".fits["//i1//"]","EXTVER",i,addonly+,verify-,show-,update+)
	      }
            }
            imdelete(tmpfile)
            imdelete(tmptell)
            imdelete(tmpfile//"_corr.fits")
            #imdelete(tmpfile//"_Acorr.fits")
            #imdelete(tmpfile//"_ABcorr.fits")
            #imdelete(tmpfile//"_WABcorr.fits")
       }
    }
end
