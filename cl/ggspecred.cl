procedure ggspecred (cluster,maskname)
#
# Adapted from GEEC2 IRAF scripts.  Uses gmosexamples NS_MOS as a starting point, but
# reduces each science image, including flux calibration completely before combining.  
# This ensures that DTA offsets and especially
# spectral dithers (shifts in central wavelength) are correctly accounted for.
#
# Must use v113 version of IRAF.  
#
# Changelog
# v3.2 May 29, 2015.   Minor change to use gradimage as refimage, to be sure slit positions are taken from MDF
# v3.1 May 26, 2015.   Fixed major bug.  gradimage cannot be the same for all flats since the slits aren't all in the same position.
#                      This leads to a potential problem for slits that don't have the same size. Will not be able to coadd.  So altered
#                      gscut in gemini/v113, to force the slit length to be as given in the MDF.  Also changed parameters of gemcombine to scale input images by 1./median in some region.
# v2.4 April 28, 2015. Improved wlcShiftbySky routine. Checks absolute wlc of reference and allows interactive slit-by-slit correction.
# v2.3 April 3, 2015.  Added option for dark correction.
# v2.2 April 2, 2015.  Added option for applying telluric correction.
# v2.1 March 16, 2015. Major change due to new gemini IRAF release v113.  This includes QE correction, gemrspec and gemscombine tasks.
#                      Note: had to edit gmos/gscut.cl line 784-788: need to avoid gapl[0] and gaph[0]
# v2.0  March 8, 2015. Implemented flux calibration.
# v1.20 Feb 23, 2015.  Changed S/N to calculate from 1D if available, and automatically write output file.
# v1.19 Feb 12, 2015.  Adapted to new directory structure and naming convention.
# v1.18 Dec 9, 2014.   Implement translational shift based on sky lines.  Needs wlcShiftbySky.py.
#                      Implement 1D extraction.  Needs ggextract.py
# v1.17 Nov 28, 2014.  Modified to allow user to specify key gswavelength parameters.  Can do wlc only with arconly+, for testing.
# v1.16 Nov 27, 2014.  Added ability to generate sky spectra to check wlc.  Needs mlb_nsskysub.cl.
# v1.15 Nov 17, 2014.  Fixed bug in reading number of science extensions.  Only affected S/N output which was limited to the first ~20 slits.  Disabled nod-folded spectra, which is broken for some reason.
# v1.14 Nov 17, 2014.  Further changes to gswavelength.  ntarget=15, fwidth=10., gsigma=2.5, cradius=15., minsep=10.,match=-8.
#                      Fixed error in stacking pos&neg spectra in the case stray tgsngs* files are lying around.
# v1.13 Nov 15, 2014.  Further changes to gswavelength parameters.  nsum=5 and order=3.
#                      Added checks to make sure files in .lis are correct OBSTYPE and that arc central wavelength matches science.
# v1.12 Nov 15, 2014.  Changed parameters in gswavelength: fwidth=6 and step=2.  
# v1.11 Nov 14, 2014.  Added a dowavtran option to allow you to skip all steps that require an arc.  This is useful to see the data as it's taken, if the daytime arc is not available.
# v1.1  Nov 14, 2014.  Calcsn now outputs the magnitude of the target alongside the spectrum S/N.
#
#
# Usage: 
#
# For quick checks of data as it comes off the telescope:
#    ggspecred cluster maskname reduce+ rereduce- dowavtran- dostack+ calcsn+ shiftbySky- extract-
# This will reduce and stack 2D spectra, with only approx wavelength solution.  Good enough for estimating S/N.
#
#   Input: 
#      There are several requirements for this to run properly.  
#      1.  Main user parameters are the name of the cluster (e.g. "SPT0205") and the corresponding mask (e.g. "GS2014BLP001-06").
#      2.  Requires a specific directory structure.  User can specify datadir, rawdir, reddir and sensdir.  Reduced data will be found in:
#            datadir/reddir/cluster/Spectroscopy/maskname/
#          Sensitivity curves are found in datadir/reddir/sensdir
#      3.  A file with name maskname//".lis" must already exist in the datadir/reddir/cluster/maskname/ directory.
#          This file contains, on each line, the name of an image and its corresponding flat and arc.  These
#          raw data files are located in the datadir/rawdir directory. 
#      4.  Need to have a reduced bias frame in the datadir/ and specify this as a parameter.  Bias must be overscan corrected.
#      5.  If a bpm file is available, may specify this too.
#      6.  Finally the MDF file must exist and be located in the datadir/reddir/cluster/maskname/ directory
#      7.  1D extraction expects a ggextract.config file in the datadir/reddir directory
#
# Outputs and Checks:
# DOREDUCE:
#    "gs"//flat//".reg" : For each flat, a region file showing the location of the identified slit.  
# It is useful to display one of the flats with this region file overlaid, to check they have all been identified correctly.
#    "ngs"//science//".fits": A sky-subtracted 2d image of each science exposure.  Again it is useful to overplot the relevant flat .reg file to check that it has been done correctly.
#    "tgsngs//science//".fits: fully reduced, sky-subtracted cutouts of each slit in each science image.  Should have a positive flux and negative flux spectrum.
# DOSTACK:
# cs_maskname.fits: reduced, stacked 2D image.  With positive and negative flux spectra
# 
# DOEXTRACT
# oned_cs_maskname.fits:  1D extracted spectrum
# stacked_cs_maskname.fits: 2D spectra stacked vertically and aligned.
#
# CALCSN:
# Calculates the S/N at a specified wavelength, integrated over a specified interval.
# Outputs a table with the mean counts and S/N for each slit.
#
string cluster {prompt="Name of GOGREEN cluster"}
string maskname {prompt="Name of GMOS mask"}
string bias {prompt="Name of bias frame to use"}
bool custbias {no,prompt="Use custom bias frame for each science image?"}
string dark {prompt="Name of dark frame to use (optional)"}
string bpm {"",prompt="Name of bad pixel mask"}
string combslit {"cs_",prompt="Prefix added to cluster name for median combined slit image"}
bool nsdata {yes,prompt="Nod and shuffle data?"}
bool doreduce {yes,prompt="Reduce the images?"}
bool doflat {no,prompt="Flat field the spectra?"}
bool dodark {no,prompt="Apply dark correction?"}
bool dowavtran {yes,prompt="Wavelength calibrate?  Set to no if no arc available"}
bool arconly {no,prompt="Only reduce/calibrate the arc frame, for testing and establishing parameters"}
bool flatonly {no,prompt="Only reduce/calibrate the flat frames, for checking slit positions"}
bool shiftbySky {no,prompt="Apply small wavelength shift to each frame based on sky lines"}
int  refslit {0,prompt="Reference slit for shiftbySky.  Default 0 is middle slit"}
bool dostack  {yes,prompt="Stack the reduced images?"}
bool rereduce {yes,prompt="Re-reduce if final reduced image already exists?"}
bool calcsn {yes,prompt="Calculate S/N of final spectrum?"}
bool extract {yes,prompt="Extract 1D spectrum"}
bool doqecorr {yes,prompt="Do QE correction"}
bool docrreject {yes,prompt="Use van Dokkum lacos_spec?"}
bool dofluxcal {no,prompt="Flux calibrate?"}
bool dotell {yes,prompt="Do telluric correction?"}
real snlambda {8500,prompt="Wavelength at which to calculate S/N (A)"}
real sndlambda {20,prompt="Wavelength interval over which to compute S/N (A)"}
string datadir {"gogreen$Data",prompt="Root directory"}
string biasdir {"raw/GMOS-S/bias/"}
string darkdir {"raw/GMOS-S/dark/"}
string bpmdir {"gmos$data/"}
string rawdir {"raw/GMOS-S/14B/"}
string reddir {"reduced/",prompt="Location of reduced data.  Subdirectory of root."}
string ssname {"CD32",prompt="Name of standard star."}
string tellname {"CD32_15A_Telluric.fits",prompt="Name of telluric correction file."}
string observatory {"Gemini-South",prompt="Name of observatory, for ggcalibrate"}
string config {"ggextract.config",prompt="Name of ggextract config file.  In reddir."}
int yoffset {INDEF,prompt="Offset in y direction between predicted and actual spectrum location"}
bool usegrad {yes,prompt="Use gradient method to locate slits?"}
int nsum {5,prompt="gswavelength parameter"}
real fwidth {13.,prompt="gswavelength parameter"}
real gsigma {2.5,prompt="gswavelength parameter"}
real cradius {13.,prompt="gswavelength parameter"}
real threshold {0.1,prompt="gswavelength parameter"}
real minsep {5.,prompt="gswavelength parameter"}
real match {-10.,prompt="gswavelength parameter"}
#real order {4,prompt="gswavelength parameter"}
#changed by GHR on 11.Feb.2016 to order=3
real order {3,prompt="gswavelength parameter"}
bool refit {yes,prompt="gswavelength parameter"}
int step {2,prompt="gswavelength parameter"}
int ntarget {10,prompt="gswavelength parameter"}
bool gswtrace {yes,prompt="gswavelength parameter"}
bool gswinter {yes,prompt="gswavelength parameter"}
bool verbose {yes,prompt="Verbose output in all tasks?"}
bool clean {no,prompt="Remove temporary files"}
int status {0,prompt="Exit status (0=good)"}
string *list

begin
   string science,flat,arc,filename,wdir,gradimage,stackstring,imsec,image,rdir,bdir,ddir
   string refflat,sciimage,prefix,skyref,snfn,fluxconfig,sciprefix,skyprefix,pri,keep
   int redint,ext,cnt,i,nsciext,id   
   real pixscale,statmean,crval1,crpix1,cd1_1,cenpix,statvar,stddev,mag
   real arccentwave,scicentwave,pixshift,newcrval
   int pix1,pix2,statnpix,imwidth,mdfrow,isci
   bool snfrom1d,retry,fl_doqecorr,fl_dofluxcal
   int shref,sbsskydone,sbsscidone
   real lambda1,lambda2,dx
   string version,biasfile
# Code version.  Must update here!  Will only appear in combined image header.
   version ="3.2"
   delete ("tmp*",>&"dev$null")
# MLB: Need a better way to do this.  At least should be parameters.  
   lambda1=5500
   lambda2=10500
# GMOS-S Hamamatsu, 2x2 bin:
 dx=3.906255
# GMOS-N EEV, 2x2 bin:
#   dx=3.56
   stackstring=""
# Do some checks to make sure things are set up correctly.
# Must use a Hamamatsu-compatible version of the gemini packages:
   if (gemini.verno != 'v1.14'){
       print ("ERROR: Wrong version of gemini package")
       goto crash
   }
   wdir=datadir//reddir//cluster//"/Spectroscopy/"//maskname//"/"
   rdir=datadir//rawdir
   bdir=datadir//biasdir
   ddir=datadir//darkdir
   biasfile=bdir//bias
# Directory must exist and there must be a correctly named .lis file in it:
# 
   filename=maskname//".lis"
   if (!access(wdir//filename)){ 
      print ("ERROR: ",wdir//filename," does not exist")
      goto crash
   }
   cd (wdir)
if (doreduce){
   gimverify(bdir//bias)
   if (gimverify.status==1){ 
#      goto crash
   }
   gimverify(maskname//".fits")
   if (gimverify.status<4){ 
      print ("WARNING: MDF file ",maskname//".fits"," does not exist")
#      goto crash
   }
   }
   if (arconly){
# Only the arcs should have this prefix.  If arconly is set then we presume the user
# Wants at least to redo the wavelength calibration.
      imdelete("tgsS*.fits",>&"dev$null")
   }
   list=filename
# Reset status of gsreduce and gswave in case program last ended with an error.
   gsreduce.status=0
   gswavelength.status=0
   tmatch.incol1=""
   tmatch.incol2=""
#
# Now process each line in filename.  
#
   fl_doqecorr=doqecorr
   fl_dofluxcal=dofluxcal
   cnt=1
   if (doreduce || rereduce || dostack){
     while (fscan(list, science, flat, arc) != EOF){
   		if (custbias){
   			biasfile=wdir//science//"_bias"
   			print ("Using bias ",biasfile)
   		}
# 
# Check that science, flat and arc are what they are supposed to be.
# Check that arc and science have same central wavelength.
      if (doreduce){
         imgets(rdir//science//"[0]","OBSTYPE")
         if (imgets.value != "OBJECT"){
            print ("Image ",science," is not a science exposure.  Exiting.")
            goto crash
         }
         imgets(rdir//flat//"[0]","OBSTYPE")
         if (imgets.value != "FLAT"){
            print ("Image ",flat," is not a gcal flat.  Exiting.")
            goto crash
         }
         if (dowavtran){
            imgets(rdir//arc//"[0]","OBSTYPE")
            if (imgets.value != "ARC"){
               print ("Image ",arc," is not an arc.  Exiting.")
               goto crash
            }
            imgets(rdir//arc//"[0]","CENTWAVE")
            arccentwave=real(imgets.value)
            imgets(rdir//science//"[0]","CENTWAVE")
            scicentwave=real(imgets.value)
            if (arccentwave != scicentwave){
              print ("Arc central wavelength ",arccentwave," does not match science: ",scicentwave)
              goto crash
            }
         } else {
           fl_doqecorr=no
	   fl_dofluxcal=no
	   print "QE and flux correction not possible without wlc"
         }
      }
# This is no good because slits aren't necessarily in the same position (if DTAX !=0).
#      if (cnt==1){
#        refflat=flat
#        if (usegrad){
#            gradimage="rg"//refflat//"_comb" 
#        } else {
#            gradimage=""
#        }
#      } 
#  Reduce the image, flat and arc.
#  Decide on each step depending on parameters:
#    doreduce (no reduction if this is no)
#    rereduce (no reduction if this is yes and output exists)
#    arconly  (only steps necessary to reduce and calibrate arc)
#    wavtran  (no arc or wlc steps if this is no)
# 1. Reduce flat, if arconly=no, doreduce=yes      
      if (usegrad){
         gradimage="rg"//flat//"_comb"  
      } else {
         gradimage=""
      }
      gimverify("rg"//flat//".fits")
      if (doreduce && arconly==no && (rereduce || gimverify.status==1)){
        print (flat//" -> "//"rg"//flat//".fits")
        imdelete ("rg"//flat//"*.fits",>&"dev$null")
        imdelete ("g*"//flat//".fits",>&"dev$null")
        imdelete ("mg*"//flat//".fits",>&"dev$null")
# 1.1 Bias subtract.  Needs to be done separately now to accommodate gqecorr and/or gemcrspec
        gsreduce(flat,
		 outimages="",
		 outpref="gs",
		 fl_over+,
		 fl_trim+,
		 fl_bias+,
		 fl_gscrrej-,
		 fl_dark-,
		 fl_flat-,
		 fl_gmosaic-,
		 fl_fixpix-,
		 fl_gsappwave-,
		 fl_cut-,
		 fl_title+,
		 fl_oversize-,
		 fl_vardq+,
		 fl_fulldq+,
		 dqthresh=0.1,
		 bias=biasfile,dark="",flatim="",
		 geointer="linear",
		 gradimage="",
		 refimage="",
		 key_exptime="EXPTIME",
		 key_biassec="BIASSEC",
		 key_datasec="DATASEC",
		 fl_inter-,
		 rawpath=rdir,
		 sci_ext="SCI",var_ext="VAR",dq_ext="DQ",key_mdf="MASKNAME",
		 mdffile="",mdfdir="gmos$data",bpm=bpmdir//bpm,
		 gaindb="default",
		 gratingdb = "gmos$data/GMOSgratings.dat",
		 filterdb = "gmos$data/GMOSfilters.dat",
		 xoffset=INDEF, yoffset=yoffset, yadd=0,
		 wave_limit=INDEF,
		 bpmfile = "gmos$data/chipgaps.dat",
		 key_ron="RDNOISE", key_gain="GAIN",
		 ron=3.5, gain=2.2,
		 sat="default",
		 key_nodcount="NODCOUNT", key_nodpix="NODPIX",
		 ovs_flinter-,ovs_med-,ovs_func="chebyshev",ovs_order="default",ovs_highr=3.,ovs_highr=3.,ovs_niter=2,
		 nbiascontam="default",
		 biasrows="default",
		 logfile="",
		 verbose=verbose)	
           if (gsreduce.status>0){
                    print "Error in gsreduce"
                    goto crash}
# 1.2 The gsflat call below requires input data to be mosaicked when already bias
# subtracted, so do that here.
             gmosaic ("gs"//flat,
		    outimages="",
		    outpref="m",
		    fl_paste-,
		    fl_vardq+,
		    fl_fixpix+,
		    fl_clean+,
		    fl_fulldq+,
		    dqthresh=0.1,
		    bitflags="all",
		    geointer="linear",
		    gap="default",
		    bpmfile="gmos$data/chipgaps.dat",
		    statsec="default",
		    obsmode="MOS",
		    sci_ext="SCI",
		    var_ext="VAR",
		    dq_ext="DQ",
		    mdf_ext="MDF",
		    key_detsec="DETSEC",
		    key_ccdsec="CCDSEC",
		    key_datsec="DATASEC",
		    key_ccdsum="CCDSUM",
		    key_obsmode="OBSMODE",
		    logfile="",
		    fl_real-,
		    verbose=verbose)		      
# 1.3 Make the initial flat field (as a reference for tracing/cutting)
# Then, since we will later let GSCUT find the slit edges automatically, 
# we need to keep the 'combflat' image.
	     gsflat("mgs"//flat, 
	       "rg"//flat//".fits", 
	       fl_slitcorr-,slitfunc="",fl_keep+,combflat="rg"//flat//"_comb.fits",
	       fl_over-,fl_trim-,fl_bias-,fl_dark-,
	       fl_fixpix+,
	       fl_oversize-,
	       fl_vardq+,fl_fulldq+,dqthresh=0.1,
	       bias=biasfile, dark="",
	       key_exptime="EXPTIME",
	       key_biassec="BIASSEC",
	       key_datasec="DATASEC",
	       rawpath=rdir,
	       sci_ext="SCI",
	       var_ext="VAR",
	       dq_ext="DQ",
	       key_mdf="MASKNAME",
	       mdffile="",
	       mdfdir="gmos$data",
	       bpm="",
	       gaindb="default",
	       gratingdb = "gmos$data/GMOSgratings.dat",
	       filterdb = "gmos$data/GMOSfilters.dat",
	       bpmfile = "gmos$data/chipgaps.dat",
	       refimage="",
	       sat="default",
	       xoffset=INDEF,
	       yoffset=INDEF,
	       yadd=0,
	       wave_limit=INDEF,
	       fl_usegrad=no,
	       fl_emis-,
	       nbiascontam="default",
	       biasrows="default",
	       minval=INDEF,
	       fl_inter-,
	       fl_answer+,
	       fl_detec-,
	       fl_seprows+,
	       function="spline3",
	       order="29",
	       low_reject=3,
	       high_reject=3,
	       niterate=2,
	       combine="average",
	       reject="avsigclip",
	       masktype="goodvalue",
	       maskvalue=0.,
	       scale="mean",
	       zero="none",
	       weight="none",
	       statsec="",
	       lthreshold=INDEF,
	       hthreshold=INDEF,
	       nlow=1,nhigh=1,nkeep=0,
	       mclip+,lsigma=3.,hsigma=3.,
	       key_ron="RDNOISE",
	       key_gain="GAIN",
	       ron=3.5,
	       gain=2.2,
	       snoise="0.0",
	       sigscale=0.1,
	       pclip=-0.5,
	       grow=0.,
	       ovs_flinter-,
	       ovs_med-,
	       ovs_func="chebyshev",ovs_order=1.,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
	       fl_double-,
	       nshuffle=0,
	       logfile="",
	       verbose=verbose)
# 1.4 Update the _comb frame created by GSFLAT above with the location of
# the slit edges.
           delete ("gs"//flat//".sec",>&"dev$null")
           delete ("gs"//flat//"_repl.sec",>&"dev$null")
           delete ("gs"//flat//".reg",>&"dev$null")
           gscut  ("rg"//flat//"_comb.fits",
		 	outimage="",
		 	secfile="gs"//flat//".sec",
		 	fl_update+,
		 	fl_vardq+,
		 	fl_oversize-,
		 	gratingdb = "gmos$data/GMOSgratings.dat",
		 	filterdb = "gmos$data/GMOSfilters.dat",
		 	bpmfile="gmos$data/chipgaps.dat",
		 	gradimage=gradimage,
		 	refimage="",
		 	xoffset=INDEF,yoffset=yoffset,
		 	yadd=0,
		 	w2=INDEF,
		 	sci_ext="SCI",
		 	var_ext="VAR",
		 	dq_ext="DQ",
		 	key_gain="GAIN",
		 	key_ron="RDNOISE",
		 	ron=3.5,
		 	gain=2.2,
		 	logfile="",
		 	verbose=verbose)
#
# Create region file showing identified slit locations
#
          #translit("gs"//flat//".sec",":[],", " ",>"gs"//flat//"_repl.sec")
          #awk ("\'{print \"image; box \",0.5*($1+$2),0.5*($3+$4),$2-$1+1,$4-$3+1}\'","gs"//flat//"_repl.sec",>"gs"//flat//".reg")
# Better: this allows slit positions to be read from the gradimage MDF.  Thus if manual change are made they will be reflected.
        } # END if (doreduce && arconly==no && (rereduce || gimverify.status==1))
        gimverify("rg"//flat//"_comb.fits")
        if (gimverify.status==0){
        tdump("rg"//flat//"_comb.fits",col="SECX1,SECX2,SECY1,SECY2",cdfile="dev$null",pfile="dev$null",>"gs"//flat//"_repl.sec")
        awk ("\'{print \"image; box \",0.5*($1+$2),0.5*($3+$4),$2-$1+1,$4-$3+1}\'","gs"//flat//"_repl.sec",>"gs"//flat//".reg")
        }
# 
# 2. Reduce and transform arc if available and not done.
        gimverify("gs"//arc)
        if (gimverify.status==0&&rereduce){
           print "NOTE: Using existing reduction of "//arc
        }
        if (doreduce && dowavtran && flatonly==no && gimverify.status==1){
           print (arc//" -> "//"gs"//arc//".fits")
# Reduce the arc, find the wavelength solution and transform
           imdelete ("gs"//arc//".fits",>&"dev$null")
 	   gsreduce(arc,
		    outimages="",
		    outpref="gs",
		    fl_over+, fl_trim+,fl_bias+,
		    fl_gscrrej-,fl_dark-,
		    fl_flat-, fl_gmosaic+, fl_fixpix+,
		    fl_gsappwave+, fl_cut+,fl_title-, 
		    fl_oversize-,
		    fl_vardq+,fl_fulldq+,dqthresh=0.1,
		    bias=biasfile, dark="",flatim="",
		    geointer="linear",
		    gradimage="",
		    refimage=gradimage,
		    key_exptime="EXPTIME",
		    key_biassec="BIASSEC",
		    key_datasec="DATASEC",
		    fl_inter-,
		    rawpath=rdir,
		    sci_ext="SCI",
		    var_ext="VAR",
		    dq_ext="DQ",
		    key_mdf="MASKNAME",
		    mdffile="",
		    mdfdir="gmos$data/",
		    bpm="",
		    gaindb="default",
		    gratingdb="gmos$data/GMOSgratings.dat",
		    filterdb="gmos$data/GMOSfilters.dat",
		    xoffset=INDEF,
		    yoffset=INDEF,
		    yadd=0,
		    wave_limit=INDEF,
		    bpmfile="gmos$data/chipgaps.dat",
		    key_ron="RDNOISE",
		    key_gain="GAIN",
		    ron=3.5,
		    gain=2.2,
		    sat="default",
		    key_nodcount="NODCOUNT",
		    key_nodpix="NODPIX",
		    ovs_flinter-,ovs_med-,
		    ovs_func="chebyshev",ovs_order=1,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
		    nbiascontam="default",
		    biasrows="default",
		    logfile="",
		    verbose=verbose)	  
           }
           if (gsreduce.status>0){
                    print "Error in gsreduce"
                    goto crash}
           gimverify("tgs"//arc)
           if (doreduce && dowavtran &&flatonly==no && gimverify.status==1){
# Uncomment to avoid redoing the arc solution again when you just need to redo the transformation.
#             if (1==0){
             print ("Wavelength calibrating "//"gs"//arc//".fits")
	     gswavelength("gs"//arc//".fits", 
			crval="CRVAL1",
			cdelt="CD1_1",
			crpix="CRPIX1",
			key_dispaxis="DISPAXIS",
			dispaxis=1,
			database="database",
#			coordlist="gmos$data/CuAr_GMOS.dat",
#changed by GHR 11.Feb.2016
			coordlist="gmos$data/CuAr_GMOS.R150.dat",
			gratingdb="gmos$data/GMOSgratings.dat",
			filterdb="gmos$data/GMOSfilters.dat",
			fl_inter=gswinter,
			section="default",
			nsum=nsum, 
			ftype="emission",
			fwidth=fwidth,
			gsigma=gsigma,
			cradius=cradius,
			threshold=threshold,
			minsep=minsep,
			match=match,
			function="chebyshev",
			order=order, 
#			sample="*",niterate=10,low_reject=2.,high_reject=2.,
			#changed by GHR 11.Feb.2016 
			sample="*",niterate=6,low_reject=2.,high_reject=2.,
			grow=0.,refit=refit,
			step=step, 
			trace=gswtrace,
			nlost=15,maxfeatures=150,ntarget=ntarget,npattern=3,
			fl_addfeat+,
			aiddebug="",
			fl_dbwrite="YES",
			fl_overwrite+,
			fl_gsappwave-,
			fitcfunc="chebyshev",fitcxord=4,fitcyord=4,
			logfile="",
			verbose=verbose)	     
              if (gswavelength.status>0){
                 print "Error in gswavelength"
                 print "Try again, interactively:"
                 gswavelength("gs"//arc//".fits", 
			crval="CRVAL1",
			cdelt="CD1_1",
			crpix="CRPIX1",
			key_dispaxis="DISPAXIS",
			dispaxis=1,
			database="database",
#			coordlist="gmos$data/CuAr_GMOS.dat",
#changed by GHR 11.Feb.2016
			coordlist="gmos$data/CuAr_GMOS.R150.dat",
			gratingdb="gmos$data/GMOSgratings.dat",
			filterdb="gmos$data/GMOSfilters.dat",
			fl_inter=yes,
			section="default",
			nsum=nsum, 
			ftype="emission",
			fwidth=fwidth,
			gsigma=gsigma,
			cradius=cradius,
			threshold=threshold,
			minsep=minsep,
			match=match,
			function="chebyshev",
			order=order, 
#			sample="*",niterate=10,low_reject=2.,high_reject=2.,
			#changed by GHR 11.Feb.2016 
			sample="*",niterate=6,low_reject=2.,high_reject=2.,
			grow=0.,refit=refit,
			step=step, 
			trace=gswtrace,
			nlost=15,maxfeatures=150,ntarget=ntarget,npattern=3,
			fl_addfeat+,
			aiddebug="",
			fl_dbwrite="YES",
			fl_overwrite+,
			fl_gsappwave-,
			fitcfunc="chebyshev",fitcxord=4,fitcyord=4,
			logfile="",
			verbose=verbose)	     
               }
              if (gswavelength.status>0){goto crash}
# Matching end-bracked for if (1==0){ condition:
#              }
              imdelete ("tgs"//arc//".fits",>&"dev$null")
              print ("gs"//arc//" -> "//"tgs"//arc//".fits")
	      gstransform("gs"//arc, 
		       outimages="",
		       outprefix="t",
		       fl_stran-,
		       sdistname="",
		       fl_wavtran+,
		       wavtraname="gs"//arc,
		       database="database",
		       fl_vardq-,
		       interptype="linear",
		       lambda1=lambda1,
		       lambda2=lambda2,
		       dx=dx,
#		       lambda1=INDEF,
#		       lambda2=INDEF,
#		       dx=INDEF,
		       nx=INDEF,
		       lambdalog-,ylog-,
		       fl_flux+,
		       gratingdb = "gmos$data/GMOSgratings.dat",
		       filterdb = "gmos$data/GMOSfilters.dat",
		       key_dispaxis="DISPAXIS",
		       dispaxis=1,
		       sci_ext="SCI",
		       var_ext="VAR",
		       dq_ext="DQ",
		       logfile="",
		       verbose=verbose)
           if (gstransform.status>0){
                    print "Error in gstransform"
                    goto crash}
        } # END reduction of arc

# 3. Apply a correction to the flat exposures for differences in QE varation with wavelength.  Regenerate final flat field.
#
        gimverify("qgs"//flat//".fits")
        if (doreduce && arconly==no && flatonly==no && fl_doqecorr && (rereduce || gimverify.status==1)){
	imdelete("qgs"//flat//".fits",>&"dev$null")
	imdelete("mqgs"//flat//".fits",>&"dev$null")
        gqecorr("gs"//flat,
		outimages="",
		outpref="q",
		refimages="gs"//arc,
		fl_correct+,
		fl_keep+,
		corrimages="",
		corrimpref="qecorr",
		ifu_preced=1,
		fl_vardq+,
		sci_ext="SCI",
		var_ext="VAR",
		dq_ext="DQ",
		mdf_ext="MDF",
		key_detsec="DETSEC",
		key_ccdsec="CCDSEC",
		key_datasec="DATASEC",
		key_biassec="BIASSEC",
		key_ccdsum="CCDSUM",
		qecorr_data="gmos$data/gmosQEfactors.dat",
		database="database/",
		logfile="",
		verbose=verbose)
             if (gqecorr.status>0){
                    print "Error in gqecorr"
                    goto crash}
             gmosaic ("qgs"//flat,
		    outimages="",
		    outpref="m",
		    fl_paste-,
		    fl_vardq+,
		    fl_fixpix+,
		    fl_clean+,
		    fl_fulldq+,
		    dqthresh=0.1,
		    bitflags="all",
		    geointer="linear",
		    gap="default",
		    bpmfile="gmos$data/chipgaps.dat",
		    statsec="default",
		    obsmode="MOS",
		    sci_ext="SCI",
		    var_ext="VAR",
		    dq_ext="DQ",
		    mdf_ext="MDF",
		    key_detsec="DETSEC",
		    key_ccdsec="CCDSEC",
		    key_datsec="DATASEC",
		    key_ccdsum="CCDSUM",
		    key_obsmode="OBSMODE",
		    logfile="",
		    fl_real-,
		    verbose=verbose)	 
             if (gmosaic.status>0){
                    print "Error in gmosaic"
                    goto crash}    
	     imdelete ("rg"//flat//".fits",>&"dev$null")
	     gsflat("mqgs"//flat, 
	       "rg"//flat//".fits", 
	       fl_slitcorr-,slitfunc="",fl_keep-,combflat="",
	       fl_over-,fl_trim-,fl_bias-,fl_dark-,
	       fl_fixpix+,
	       fl_oversize-,
	       fl_vardq+,fl_fulldq+,dqthresh=0.1,
	       bias=biasfile, dark="",
	       key_exptime="EXPTIME",
	       key_biassec="BIASSEC",
	       key_datasec="DATASEC",
	       rawpath=rdir,
	       sci_ext="SCI",
	       var_ext="VAR",
	       dq_ext="DQ",
	       key_mdf="MASKNAME",
	       mdffile="",
	       mdfdir="gmos$data",
	       bpm="",
	       gaindb="default",
	       gratingdb = "gmos$data/GMOSgratings.dat",
	       filterdb = "gmos$data/GMOSfilters.dat",
	       bpmfile = "gmos$data/chipgaps.dat",
	       refimage="",
	       sat="default",
	       xoffset=INDEF,
	       yoffset=yoffset,
	       yadd=0,
	       wave_limit=INDEF,
	       fl_usegrad=usegrad,
	       fl_emis-,
	       nbiascontam="default",
	       biasrows="default",
	       minval=INDEF,
	       fl_inter-,
	       fl_answer+,
	       fl_detec-,
	       fl_seprows+,
	       function="spline3",
	       order="29",
	       low_reject=3,
	       high_reject=3,
	       niterate=2,
	       combine="average",
	       reject="avsigclip",
	       masktype="goodvalue",
	       maskvalue=0.,
	       scale="mean",
	       zero="none",
	       weight="none",
	       statsec="",
	       lthreshold=INDEF,
	       hthreshold=INDEF,
	       nlow=1,nhigh=1,nkeep=0,
	       mclip+,lsigma=3.,hsigma=3.,
	       key_ron="RDNOISE",
	       key_gain="GAIN",
	       ron=3.5,
	       gain=2.2,
	       snoise="0.0",
	       sigscale=0.1,
	       pclip=-0.5,
	       grow=0.,
	       ovs_flinter-,
	       ovs_med-,
	       ovs_func="chebyshev",ovs_order=1.,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
	       fl_double-,
	       nshuffle=0,
	       logfile="",
	       verbose=verbose)
             if (gsflat.status>0){
                    print "Error in gsflat"
                    goto crash}
	} #END QE section: if (doreduce && arconly==no && fl_doqecorr && (rereduce || gimverify.status==1))
#
# 4. Reduce Science data.  
#    Since we have spectral dithers, we need to fully reduce each image and wavelength calibrate, before combining.
# 4.1 Have to first run gsreduce to embed necessary header information and subtract bias.
#    Do if arconly=no, doreduce=yes, flatonly=no
        sciprefix="gs"
        gimverify(sciprefix//science//".fits")      
        if (doreduce && arconly==no && flatonly==no && (rereduce || gimverify.status==1)){
          print (science//" -> "//sciprefix//science//".fits")
          imdelete (sciprefix//science//".fits",>&"dev$null")
          gsreduce(science, 
		 outimages="",
		 outpref=sciprefix,
		 fl_over+, fl_trim+,fl_bias+,
		 fl_gscrrej-,fl_dark=dodark,
		 fl_flat-, fl_gmosaic-, fl_fixpix-,
		 fl_gsappwave-, fl_cut-,fl_title-,fl_oversize-,
		 fl_vardq+,fl_fulldq+,dqthresh=0.1,
		 bias=biasfile, dark=ddir//dark,flatim="",
		 geointer="linear",
		 gradimage="",
		 refimage=gradimage,
		 key_exptime="EXPTIME",
		 key_biassec="BIASSEC",
		 key_datasec="DATASEC",
		 fl_inter-,
		 rawpath=rdir,
		 sci_ext="SCI",
		 var_ext="VAR",
		 dq_ext="DQ",
		 key_mdf="MASKNAME",
		 mdffile="",
		 mdfdir="gmos$data/",
		 bpm=bpmdir//bpm,
		 gaindb="default",
		 gratingdb="gmos$data/GMOSgratings.dat",
		 filterdb="gmos$data/GMOSfilters.dat",
		 xoffset=INDEF,
		 yoffset=INDEF,
		 yadd=0,
		 wave_limit=INDEF,
		 bpmfile="gmos$data/chipgaps.dat",
		 key_ron="RDNOISE",
		 key_gain="GAIN",
		 ron=3.5,
		 gain=2.2,
		 sat="default",
		 key_nodcount="NODCOUNT",
		 key_nodpix="NODPIX",
		 ovs_flinter-,ovs_med-,
		 ovs_func="chebyshev",ovs_order=1,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
		 nbiascontam="default",
		 biasrows="default",
		 logfile="",
		 verbose=verbose)
             if (gsreduce.status>0){
                    print "Error in gsreduce"
                    goto crash}
           }
# 
# 4.2 Do cosmic ray rejection
        gimverify("x"//sciprefix//science//".fits")
        if (doreduce && docrreject && arconly==no && flatonly==no && (rereduce || gimverify.status==1)){
            imdelete ("x"//sciprefix//science,>&"dev$null")
            gemcrspec(sciprefix//science,
		  "x"//sciprefix//science,
		  xorder=9,
		  yorder=-1,
		  #sigclip=4.5,
		  #sigfrac=0.5,
		  #objlim=1.,
      sigclip=5.5,
      sigfrac=4.0,
      objlim=3.,
		  niter=4,
		  fl_vardq+,
		  sci_ext="SCI",
		  var_ext="VAR",
		  dq_ext="DQ",
		  key_ron="RDNOISE",
		  key_gain="GAIN",
		  ron=3.5,
		  gain=2.2,
		  logfile="",
		  verbose-)
             if (gemcrspec.status>0){
                    print "Error in gemcrspec"
                    goto crash}
        }
        if (docrreject){sciprefix="x"//sciprefix}
# 4.3 If data are n&s we then subtract the sky
        gimverify("n"//sciprefix//science//".fits")
        if (doreduce && arconly==no && flatonly==no && nsdata && (rereduce || gimverify.status==1)){
          print (sciprefix//science//" -> "//"n"//sciprefix//science//".fits")
          imdelete ("n"//sciprefix//science//".fits",>&"dev$null")
          mlb_nsskysub(sciprefix//science,
	       outimages="",
	       outpref="n",
	       op="-",
	       fl_fixnc+,
	       sci_ext="SCI",
	       var_ext="VAR",
	       dq_ext="DQ",
	       mdf_ext="MDF",
	       fl_vardq+,
	       logfile="",
	       verbose=verbose)
        }
        gimverify("sky"//sciprefix//science//".fits")
        if (doreduce && arconly==no && flatonly==no && nsdata && (rereduce || gimverify.status==1)){
           print (sciprefix//science//" -> "//"sky"//sciprefix//science//".fits")
           imdelete ("sky"//sciprefix//science//".fits",>&"dev$null")
# Now add, rather than subtract, to create a sky image for diagnostics
           mlb_nsskysub(sciprefix//science,
	       outimages="",
	       outpref="sky",
	       op="+",
	       fl_fixnc+,
	       sci_ext="SCI",
	       var_ext="VAR",
	       dq_ext="DQ",
	       mdf_ext="MDF",
	       fl_vardq+,
	       logfile="",
	       verbose=verbose)
         }
	 skyprefix="sky"//sciprefix
         if (nsdata){
	   sciprefix="n"//sciprefix
         }
# 4.4 QE correction.  This must be done after n&s sky subtraction and cosmic ray rejection
        gimverify("q"//sciprefix//science//".fits")      
        if (doreduce && arconly==no &&flatonly==no && fl_doqecorr && (rereduce || gimverify.status==1)){	
          imdelete ("q"//sciprefix//science//".fits",>&"dev$null")	
          imdelete ("q"//skyprefix//science//".fits",>&"dev$null")
          gqecorr(sciprefix//science,
		outimages="",
		outpref="q",
		refimages="gs"//arc,
		fl_correct+,
		fl_keep+,
		corrimages="qecorrgs"//arc,
		corrimpref="qecorr",
		ifu_preced=1,
		fl_vardq+,
		sci_ext="SCI",
		var_ext="VAR",
		dq_ext="DQ",
		mdf_ext="MDF",
		key_detsec="DETSEC",
		key_ccdsec="CCDSEC",
		key_datasec="DATASEC",
		key_biassec="BIASSEC",
		key_ccdsum="CCDSUM",
		qecorr_data="gmos$data/gmosQEfactors.dat",
		database="database/",
		logfile="",
		verbose=verbose)
             if (gqecorr.status>0){
                    print "Error in gqecorr"
                    goto crash}	
          if (nsdata){
           gqecorr(skyprefix//science,
		outimages="",
		outpref="q",
		refimages="gs"//arc,
		fl_correct+,
		fl_keep+,
		corrimages="qecorrgs"//arc,
		corrimpref="qecorr",
		ifu_preced=1,
		fl_vardq+,
		sci_ext="SCI",
		var_ext="VAR",
		dq_ext="DQ",
		mdf_ext="MDF",
		key_detsec="DETSEC",
		key_ccdsec="CCDSEC",
		key_datasec="DATASEC",
		key_biassec="BIASSEC",
		key_ccdsum="CCDSUM",
		qecorr_data="gmos$data/gmosQEfactors.dat",
		database="database/",
		logfile="",
		verbose=verbose)
             if (gqecorr.status>0){
                    print "Error in gqecorr"
                    goto crash}
           }
	}
	if (fl_doqecorr){
         sciprefix="q"//sciprefix
         skyprefix="q"//skyprefix
        }
#
# 4.5 Now do the flat fielding and dark correction (if desired), initial wavelength calibration and cut into slits:
#
# This step could be skipped in a preliminary run to check slit cutouts (with wlc turned off).
        gimverify("gs"//sciprefix//science//".fits")
        if (doreduce && arconly==no && flatonly==no && (rereduce || gimverify.status==1)){
           print (sciprefix//science//" -> "//"gs"//sciprefix//science//".fits")
           imdelete ("gs"//sciprefix//science//".fits",>&"dev$null")
	   gsreduce(sciprefix//science, 
		 outimages="",
		 outpref="gs",
		 fl_over-, fl_trim-,fl_bias-,
		 fl_gscrrej-,fl_dark-,fl_oversize-,
		 fl_flat=doflat, fl_gmosaic-, fl_fixpix-,
		 fl_gsappwave+, fl_cut+,fl_title-,
		 fl_vardq+,fl_fulldq+,dqthresh=0.1,
		 bias=biasfile, dark=ddir//dark,flatim="rg"//flat//".fits",
		 geointer="nearest",
		 gradimage="",
		 refimage=gradimage,
		 key_exptime="EXPTIME",
		 key_biassec="BIASSEC",
		 key_datasec="DATASEC",
		 fl_inter-,
		 rawpath=rdir,
		 sci_ext="SCI",
		 var_ext="VAR",
		 dq_ext="DQ",
		 key_mdf="MASKNAME",
		 mdffile="",
		 mdfdir="gmos$data/",
		 bpm=bpmdir//bpm,
		 gaindb="default",
		 gratingdb="gmos$data/GMOSgratings.dat",
		 filterdb="gmos$data/GMOSfilters.dat",
		 xoffset=INDEF,
		 yoffset=INDEF,
		 yadd=0,
		 wave_limit=INDEF,
		 bpmfile="gmos$data/chipgaps.dat",
		 key_ron="RDNOISE",
		 key_gain="GAIN",
		 ron=3.5,
		 gain=2.2,
		 sat="default",
		 key_nodcount="NODCOUNT",
		 key_nodpix="NODPIX",
		 ovs_flinter-,ovs_med-,
		 ovs_func="chebyshev",ovs_order=1,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
		 nbiascontam="default",
		 biasrows="default",
		 logfile="",
		 verbose=verbose)
             if (gsreduce.status>0){
                    print "Error in gsreduce"
                    goto crash}
        }
        gimverify("gs"//skyprefix//science//".fits")
        if (doreduce && arconly==no && flatonly==no && nsdata && (rereduce || gimverify.status==1)){
           print (skyprefix//science//" -> "//"gs"//skyprefix//science//".fits")
           imdelete ("gs"//skyprefix//science//".fits",>&"dev$null")
	   gsreduce(skyprefix//science, 
		 outimages="",
		 outpref="gs",
		 fl_over-, fl_trim-,fl_bias-,
		 fl_gscrrej-,fl_dark-,fl_oversize-,
		 fl_flat=doflat, fl_gmosaic-, fl_fixpix-,
		 fl_gsappwave+, fl_cut+,fl_title-,
		 fl_vardq+,fl_fulldq+,dqthresh=0.1,
		 bias=biasfile, dark="",flatim="rg"//flat//".fits",
		 geointer="nearest",
		 gradimage="",
		 refimage=gradimage,
		 key_exptime="EXPTIME",
		 key_biassec="BIASSEC",
		 key_datasec="DATASEC",
		 fl_inter-,
		 rawpath=rdir,
		 sci_ext="SCI",
		 var_ext="VAR",
		 dq_ext="DQ",
		 key_mdf="MASKNAME",
		 mdffile="",
		 mdfdir="gmos$data/",
		 bpm=bpmdir//bpm,
		 gaindb="default",
		 gratingdb="gmos$data/GMOSgratings.dat",
		 filterdb="gmos$data/GMOSfilters.dat",
		 xoffset=INDEF,
		 yoffset=INDEF,
		 yadd=0,
		 wave_limit=INDEF,
		 bpmfile="gmos$data/chipgaps.dat",
		 key_ron="RDNOISE",
		 key_gain="GAIN",
		 ron=3.5,
		 gain=2.2,
		 sat="default",
		 key_nodcount="NODCOUNT",
		 key_nodpix="NODPIX",
		 ovs_flinter-,ovs_med-,
		 ovs_func="chebyshev",ovs_order=1,ovs_lowr=3.,ovs_highr=3.,ovs_niter=2,
		 nbiascontam="default",
		 biasrows="default",
		 logfile="",
		 verbose=verbose)
             if (gsreduce.status>0){
                    print "Error in gsreduce"
                    goto crash}
        }
	skyprefix="gs"//skyprefix
	sciprefix="gs"//sciprefix
        
#
# Wavelength calibrate the science frame based on arc solution
#
        gimverify("t"//sciprefix//science)
        if (doreduce && arconly==no && flatonly==no && dowavtran && (rereduce || gimverify.status==1)){
           print (sciprefix//science//" -> "//"t"//sciprefix//science//".fits")
           imdelete ("t"//sciprefix//science//".fits",>&"dev$null")
	   gstransform(sciprefix//science, 
		    outimages="",
		    outprefix="t",
		    fl_stran-,
		    sdistname="",
		    fl_wavtran+,
		    wavtraname="gs"//arc,
		    database="database",
		    fl_vardq+,
		    interptype="linear",
		    lambda1=lambda1,
		    lambda2=lambda2,
		    dx=dx,
#		    lambda1=INDEF,
#		    lambda2=INDEF,
#		    dx=INDEF,
		    nx=INDEF,
		    lambdalog-,ylog-,
		    fl_flux+,
		    gratingdb = "gmos$data/GMOSgratings.dat",
		    filterdb = "gmos$data/GMOSfilters.dat",
		    key_dispaxis="DISPAXIS",
		    dispaxis=1,
		    sci_ext="SCI",
		    var_ext="VAR",
		    dq_ext="DQ",
		    logfile="",
		    verbose=verbose)
      }
        if (dowavtran){sciprefix="t"//sciprefix}
        gimverify("t"//skyprefix//science)
        if (doreduce && arconly==no && flatonly==no && dowavtran && nsdata && (rereduce || gimverify.status==1)){
           print (skyprefix//science//" -> "//"t"//skyprefix//science//".fits")
           imdelete ("t"//skyprefix//science//".fits",>&"dev$null")
	   gstransform(skyprefix//science, 
		    outimages="",
		    outprefix="t",
		    fl_stran-,
		    sdistname="",
		    fl_wavtran+,
		    wavtraname="gs"//arc,
		    database="database",
		    fl_vardq+,
		    interptype="linear",
		    lambda1=lambda1,
		    lambda2=lambda2,
		    dx=dx,
#		    lambda1=INDEF,
#		    lambda2=INDEF,
#		    dx=INDEF,		    
		    nx=INDEF,
		    lambdalog-,ylog-,
		    fl_flux+,
		    gratingdb = "gmos$data/GMOSgratings.dat",
		    filterdb = "gmos$data/GMOSfilters.dat",
		    key_dispaxis="DISPAXIS",
		    dispaxis=1,
		    sci_ext="SCI",
		    var_ext="VAR",
		    dq_ext="DQ",
		    logfile="",
		    verbose=verbose)
      }
      if (dowavtran){skyprefix="t"//skyprefix}
# To deal with non-n&s data (like standard stars).  Subtract sky.
        gimverify("s"//sciprefix//science//".fits")
        if (!nsdata){
          if (doreduce && flatonly==no && arconly==no && (rereduce || gimverify.status==1)){
            print (sciprefix//science//" -> "//"s"//sciprefix//science//".fits")
            imdelete ("s"//sciprefix//science//".fits",>&"dev$null")
            gsskysub(sciprefix//science,no,
	       output="",
	       outpref="s",
	       sci_ext="SCI",
	       var_ext="VAR",
	       dq_ext="DQ",
	       fl_vardq+,
	       fl_oversize-,
	       long_sample= "*",
	       mos_sample=0.8,
	       mosobjsize = 1,
	       naverage = 1,
	       function = "chebyshev",
	       order = 2,
	       low_reject=2.5,
	       high_reject=2.5,
	       niterate=2,
	       grow=0.,
	       fl_inter-,
	       logfile="",
	       verbose=verbose)
          }
          sciprefix="s"//sciprefix
        }
# Here we run wlcShiftbySky.py to calculate median offset from sky frames.
# Then update header of all tgsngs and tgssky frames, SCI, VAR and DQ extensions, if necessary.
      gimverify ("h"//sciprefix//science//".fits")
      sbsscidone=gimverify.status
      gimverify ("h"//skyprefix//science//".fits")
      sbsskydone=gimverify.status
      if (doreduce && shiftbySky==yes && nsdata && flatonly==no && arconly==no){
          if (cnt==1){
             shref=1
             skyref=skyprefix//science//".fits" 
             imgets(skyref//"[0]","NSCIEXT")
             if (refslit==0){refslit=int(imgets.value)/2}
          } 
          if (cnt==2) {
# For all subsequent files, use the shifted sky reference spectrum as the new reference
            skyref="h"//skyref
            shref=0
          }
          if ((rereduce || sbsskydone==1 || sbsscidone==1)){
            gimverify(skyprefix//science)
            if (cnt==1){
# Use first processed sky spectrum encountered as the reference.  
# If for some reason it doesn't exist, shiftbysky will not be done.
               if (gimverify.status>0){
                  print "Reference sky spectrum "//skyprefix//science//" does not exist"
                  print "Not shifting by sky"
                  shiftbySky=no
               }
             }
             print ("Running wlcShiftbySky with reference spectrum ",skyref," slit ",refslit)
             imdelete("h"//sciprefix//".fits",>&"dev$null")
             imdelete("h"//skyprefix//".fits",>&"dev$null")
             if (gimverify.status>0){
                print "Target sky spectrum "//skyprefix//science//" does not exist"
                print "Not shifting by sky"
                copy(skyprefix//science,"h"//skyprefix//science)
                copy(sciprefix//science,"h"//sciprefix//science)
             } else {
                print ("Shifting ",sciprefix//science," -> ","h"//sciprefix//science)
                wlcShiftbySky(skyref,skyprefix//science//".fits",sciprefix//science//".fits","--outpref=\"h\"","--inter=\"False\"","--slit="//refslit,"--shiftref="//shref) 
             }
           }
      } # END shiftbysky
      if (shiftbySky){
          sciprefix="h"//sciprefix
          skyprefix="h"//skyprefix
      }
      gimverify("e"//sciprefix//science//".fits")
      if (!nsdata){
          if (doreduce && arconly==no && flatonly==no && (rereduce || gimverify.status==1)){
          imdelete ("e"//sciprefix//science,>&"dev$null")
	  gsextract(sciprefix//science,
		 outimages="",
		 outprefix="e",
		 refimages="",
		 apwidth=1.,
		 fl_inter+,
		 database="database",
		 find+,
		 recenter+,
		 trace+,
		 tfunction = "chebyshev",
		 torder=5,
		 tnsum=20,
		 tstep=50,
		 weights="variance",
		 clean-,
		 lsigma=3.,
		 usigma=3.,
		 background="none",
		 bfunction="chebyshev",
		 border=1,
		 long_bsample="*",
		 mos_bsample=0.9,
		 bnaverage=1,
		 bniterate=2,
		 blow_reject=2.5,
		 bhigh_reject=2.5,
		 bgrow=0.,
		 fl_vardq+,
		 sci_ext="SCI",
		 var_ext="VAR",
		 dq_ext="DQ",
		 key_ron="RDNOISE",
		 key_gain="GAIN",
		 ron=3.5,
		 gain=2.2,
		 verbose=verbose)
	  }
	    sciprefix="e"//sciprefix
	  }
     gimverify("rf"//sciprefix//science)
     if (gimverify.status==0){sciprefix="rf"//sciprefix}
     if (cnt==1){
        stackstring=sciprefix//science
     } else {
        stackstring=stackstring//","//sciprefix//science
     }
     cnt=cnt+1
   }    # END loop over science frames
   } # END if doreduce || reduce || dostack
#
#combine the skysubtracted images
#
   if (dostack && !nsdata && arconly==no &&flatonly==no){ 
      imdelete (combslit//maskname,>"dev$null")      
      gemcombine(stackstring,
	      combslit//maskname,
	      title="",
	      combine="median",
	      reject="minmax",
	      offsets="wcs",
	      masktype="goodvalue",
	      maskvalue=0,
	      scale="none",
	      zero="none",
	      weight="none",
	      statsec="",
	      expname="EXPTIME",
	      lthreshold=INDEF,
	      hthreshold=INDEF,
	      nlow=1,nhigh=1,nkeep=1,
	      mclip+,lsigma=3.,hsigma=3.,
	      key_ron="RDNOISE",
	      key_gain="GAIN",
	      ron=0.,
	      gain=1.,
	      snoise="0.0",
	      sigscale=0.1,
	      pclip=-0.5,
	      grow=0.,
	      bpmfile="",
	      nrejfile="",
	      sci_ext="SCI",
	      var_ext="VAR",
	      dq_ext="DQ",
	      fl_vardq+,
	      logfile="gemcombine.log",
	      fl_dqprop+,
	      verbose=verbose) 
#      gemscombine(stackstring,
#		  combslit//maskname,
#		  combine="average",
#		  reject="none",
#		  scale="none",
#		  zero="none",
#		  weight="none",
#		  sample="",
#		  section="default",
#		  lthreshold=INDEF,
#		  hthreshold=INDEF,
#		  nlow=0,nhigh=1,lsigma=3.,hsigma=3.,radius=0.,
#		  project-,
#		  fl_vardq+,
#		  logfile="",
#		  verbose=verbose)
   }
   if (dostack && nsdata && arconly==no&&flatonly==no){
      imdelete (combslit//maskname,>"dev$null")
      gemcombine(stackstring,
	      combslit//maskname,
	      title="",
	      combine="median",
	      reject="avsigclip",
	      offsets="wcs",
	      masktype="goodvalue",
	      maskvalue=0,
	      scale="median",
	      zero="none",
	      weight="none",
	      statsec="[400:1000,3:8]",
	      expname="EXPTIME",
	      lthreshold=INDEF,
	      hthreshold=INDEF,
	      nlow=1,nhigh=1,nkeep=1,
	      mclip+,lsigma=3.,hsigma=3.,
	      key_ron="RDNOISE",
	      key_gain="GAIN",
	      ron=0.,
	      gain=1.,
	      snoise="0.0",
	      sigscale=0.1,
	      pclip=-0.5,
	      grow=0.,
	      bpmfile="",
	      nrejfile="",
	      sci_ext="SCI",
	      var_ext="VAR",
	      dq_ext="DQ",
	      fl_vardq+,
	      logfile="gemcombine.log",
	      fl_dqprop-,
	      verbose+) 
       nhedit(combslit//maskname//".fits[0]","SDRVER",version,"ggspecred version",add+,verify-,show-)
        }
   if (extract && nsdata && arconly==no &&flatonly==no){
   	   print (datadir//reddir//config)
       ggextract("-c "//datadir//reddir//config,cluster,maskname,"recall") 
   }
   if (fl_dofluxcal && arconly==no && flatonly==no ){
       imdelete ("cal_oned_"//combslit//maskname,>&"dev$null")
       gscalibrate("oned_"//combslit//maskname,
		   output="cal_oned_"//combslit//maskname,
		   outpref="",
		   sfunction=datadir//reddir//ssname//"/Spectroscopy/"//ssname//"/sens_"//ssname,
		   sci_ext="SCI",
		   var_ext="VAR",
		   dq_ext="DQ",
		   key_airmass="AIRMASS",
		   key_exptime="EXPTIME",
		   fl_vardq+,
		   fl_ext+,
		   fl_flux+,
		   fl_scale+,
		   fluxscale=1000000000000000.,
		   ignoreaps+,
		   fl_fnu-,
		   extinction="onedstds$ctioextinct.dat",
		   observatory=observatory,
		   logfile="",		   
		   verbose=verbose)
   } 
   if (dotell && arconly==no && flatonly==no){
       imdelete ("tcal_oned_"//combslit//maskname,>&"dev$null")
       dotell("cal_oned_"//combslit//maskname,
	      "tcal_oned_"//combslit//maskname,
	      caldir=datadir//reddir//ssname//"/Spectroscopy/"//ssname//"/",
	      tellname=datadir//reddir//"Telluric/"//tellname,
	      shift=0.0,
	      scale=1.0)
   }
   if (calcsn && arconly==no && flatonly==no){      
        gimverify("oned_"//combslit//maskname)
        if (gimverify.status==1){
	  image=combslit//maskname
          print "Calculating S/N from 2D spectrum (not optimal)"
          snfrom1d=no
          snfn=maskname//".sn"
        } else {
	  image="oned_"//combslit//maskname
          print "Calculating S/N from 1D spectrum"
          snfrom1d=yes
          snfn=maskname//".onedsn"
        }
        delete (snfn,>&"dev$null")
        printf ("slit ID Mag Priority Mean S/N\n")
        printf ("=============================\n")
        printf ("slit ID Mag Priority Mean S/N Keep\n",>>snfn)
        printf ("==================================\n",>>snfn)
	
        imgets(image//".fits[0]","NSCIEXT")
        nsciext=int(imgets.value)
        for (i=1;i<=nsciext;i+=1){ 
	     sciimage=image//"[SCI,"//i//"]"
	     imgets(sciimage,"MDFROW")
	     mdfrow=int(imgets.value)
	     tprint (image//".fits[MDF]",prparam-,prdata+,rows=mdfrow,columns="ID,MAG,priority",showrow-,showhdr-) |scan id,mag,pri
             #mag=-2.5*log10(mag)+25
	     imgets(sciimage,"CRVAL1")
	     crval1=real(imgets.value)
	     imgets(sciimage,"CRPIX1")
	     crpix1=real(imgets.value)
	     imgets(sciimage,"CD1_1")
	     cd1_1=real(imgets.value)
	     cenpix=(snlambda-crval1)/cd1_1+crpix1
	     pix1=int(cenpix-sndlambda/2./cd1_1)
	     pix2=int(cenpix+sndlambda/2./cd1_1)
	     imgets(sciimage,"i_naxis2")
	     imwidth=int(imgets.value)
             imsec="["//pix1//":"//pix2//"]"
             if (snfrom1d){
	        imstat(sciimage//imsec,fields="mean,npix,stddev",format-)|scan(statmean,statnpix,stddev)
		keep=0
		if (mag>=23.5 && pri=="1") {keep=1}
                printf ("%d %d %5.2f %s %4.2f %4.2f\n",i,id,mag,pri,statmean,statmean/stddev*sqrt(statnpix))
                printf ("%d %d %5.2f %s %4.2f %4.2f %s\n",i,id,mag,pri,statmean,statmean/stddev*sqrt(statnpix),keep,>>snfn)
             } else {
	        imstat(sciimage//imsec,fields="mean,npix",format-)|scan(statmean,statnpix)
	        imstat(image//"[VAR,"//i//"]"//imsec,fields="mean",format-)|scan(statvar)
                printf ("%d %d %5.2f %s %4.2f %4.2f\n",i,id,mag,pri,statmean,sqrt(2.)*statmean/sqrt(statvar+.001)*sqrt(statnpix))
                printf ("%d %d %5.2f %s %4.2f %4.2f\n",i,id,mag,pri,statmean,sqrt(2.)*statmean/sqrt(statvar+.001)*sqrt(statnpix),>>snfn)
             }
        }

   }
   if (clean){
     print ("Removing intermediate files")
     delete ("tmp*",>&"dev$null")
     delete ("g*.fits",>&"dev$null")
     delete ("r*.fits",>&"dev$null")
     delete ("n*.fits",>&"dev$null")
     #delete ("t*.fits",>&"dev$null")
     delete ("*.sec",>&"dev$null")
   }
goto cleanexit

crash:
  status=1
  refslit=0
  goto theend
cleanexit:
  refslit=0
  status=0

theend:
  if (status==0){print "Done"}
  else {print "Error"}
  refslit=0
end

