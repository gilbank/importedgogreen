procedure ggstdred (starname)
# v1.1
# MLB March 17, 2015
# Adapted from ggspecred v1.18 for standard star.
#
# Must use v113 version of IRAF.  
#
# Changelog
#
# March 17: Updated to use v113 and use QE correction.

string starname {prompt="Name of Standard star"}
string bias {prompt="Name of bias frame to use"}
string bpm {"",prompt="Name of bad pixel mask"}
string combslit {"cs_",prompt="Prefix added to star name for median combined slit image"}
bool doreduce {yes,prompt="Reduce the images?"}
bool dostack  {yes,prompt="Stack the reduced images?"}
bool rereduce {yes,prompt="Re-reduce if final reduced image already exists?"}
bool doqecorr {yes,prompt="Do QE correction?"}
bool extract {yes,prompt="Extract 1D spectrum"}
bool calcsens {yes,prompt="Calculate sensitivity function"}
string datadir {"/home/mbalogh/projects/GOGREEN/data/MOS-S/",prompt="Root directory"}
string rawdir {"raw/GMOS-S/15A/"}
string biasdir {"raw/GMOS-S/bias/"}
string reddir {"reduced/standard/",prompt="Location of reduced data.  Subdirectory of root."}
string caldir {"onedstds$ctiocal/",prompt="Location of standard star data."}
int nsum {5,prompt="gswavelength parameter"}
real fwidth {10.,prompt="gswavelength parameter"}
real gsigma {2.5,prompt="gswavelength parameter"}
real cradius {15.,prompt="gswavelength parameter"}
real threshold {0.1,prompt="gswavelength parameter"}
real minsep {10.,prompt="gswavelength parameter"}
real match {-8.,prompt="gswavelength parameter"}
real order {3,prompt="gswavelength parameter"}
bool refit {yes,prompt="gswavelength parameter"}
int step {2,prompt="gswavelength parameter"}
int ntarget {10,prompt="gswavelength parameter"}
bool gswtrace {yes,prompt="gswavelength parameter"}
bool gswinter {no,prompt="gswavelength parameter"}
int dx=2048
int gap=61
int bin=2
bool verbose {yes,prompt="Verbose output in all tasks?"}
bool clean {no,prompt="Remove temporary files"}
int status {0,prompt="Exit status (0=good)"}
string *list

begin
   string science,flat,arc,filename,wdir,gradimage,stackstring,imsec,image
   string refflat,sciimage,prefix,rdir,bdir,reg1,reg2,reg3
   int redint,ext,cnt,x1,x2,y1,y2,c1_x1,c1_x2,c2_x1,c2_x2,c3_x1,c3_x2,sciprefix
   real arccentwave,scicentwave
   delete ("tmp*",>&"dev$null")
   stackstring=""
# Do some checks to make sure things are set up correctly.
# Must use a Hamamatsu-compatible version of the gemini packages:
   if (gemini.verno != 'v1.13'){
       print ("ERROR: Wrong version of gemini package")
       goto crash
   }
  ggspecred(starname,starname,
	    bias=bias,
	    bpm="",
	    combslit="cs_",
	    nsdata-,
	    doreduce+,dowavtran+,arconly-,shiftbySky-,dostack+,
	    rereduce=rereduce,
	    calcsn-,extract-,doqecorr+,docrreject-,dofluxcal-,
	    datadir=datadir,
	    biasdir=biasdir,
	    rawdir=rawdir,
	    reddir=reddir,
	    yoffset=INDEF,
	    usegrad+,
	    nsum=nsum, fwidth=fwidth, gsigma=gsigma, cradius=cradius, threshold=threshold, minsep=minsep, match=match,
	    order=order,refit=refit,step=step,ntarget=ntarget,gswtrace=gswtrace,
	    gswinter=gswinter,
	    verbose=verbose,
	    clean=clean)
	 
   wdir=datadir//reddir//starname//"/"
   rdir=datadir//rawdir
   bdir=datadir//"reduced/bias/"
   
   if (calcsens){
      imdelete("sens_"//starname,>&"dev$null")
      delete ("std_"//starname,>&"dev$null")
      gsstandard("cs_"//starname,
		  sfile="std_"//starname,		  
		  sfunction="sens_"//starname,		  
		  sci_ext="SCI",		  
		  var_ext="VAR",
                  dq_ext="DQ",
		  key_air="AIRMASS",
		  key_exp="EXPTIME",
		  fl_inter+,
		  starname=starname,
		  samestar+,
		  apertures="",
		  beamswitch-,
		  bandwidth=INDEF,
		  bandsep=INDEF,
		  fnuzero=3.68e-20,
		  caldir=caldir,
		  observa="Gemini-South",
		  mag="",
		  magband="",
		  teff="",
		  ignoreaps+,
		  extinction="onedstds$ctioextinct.dat",
		  out_extinction="extinct.dat",
		  function="spline3",
		  order=6,
		  verbose=verbose)
      }
   if (clean){
     print ("Removing intermediate files")
     delete ("tmp*",>&"dev$null")
     delete ("g*.fits",>&"dev$null")
     delete ("s*.fits",>&"dev$null")
     delete ("t*.fits",>&"dev$null")
   }
goto cleanexit

crash:
  status=1
  goto theend
cleanexit:
  status=0

theend:
  if (status==0){print "Done"}
  else {print "Error"}
end

