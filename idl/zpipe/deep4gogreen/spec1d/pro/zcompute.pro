;+
; NAME:
;   zcompute
;
; PURPOSE:
;   Compute relative redshift of object(s) vs. eigen-templates.
;
; CALLING SEQUENCE:
;   zans = zcompute(objflux, objivar, starflux, [starmask, nfind=, $
;    poffset=, pspace=, pmin=, pmax=, mindof=, width=, minsep=, $
;    plottitle=, /doplot, /debug ]
;
; INPUTS:
;   objflux    - Object fluxes [NPIXOBJ,NOBJ]
;   objivar    - Object inverse variances [NPIXOBJ,NOBJ]
;   starflux   - Eigen-template fluxes [NPIXSTAR,NTEMPLATE]
;
; OPTIONAL INPUTS:
;   starmask   - Eigen-template mask; 0=bad, 1=good [NPIXSTAR]
;   nfind      - Number of solutions to find per object; default to 1.
;   poffset    - Offset between all objects and templates, in pixels.
;                A value of 10 indicates that STARFLUX begins ten pixels
;                after OBJFLUX, i.e. OBJFLUX[i+10] = STARFLUX[i] for the
;                case when the relative redshift should be zero.  If the
;                wavelength coverage of the templates is larger, then the
;                value of ZOFFSET will always be negative.
;                [Scalar or vector of size NOBJ]
;   pspace     - The spacing in redshifts to consider; default to 1 [pixels];
;                [Scalar or vector of size NOBJ]
;   pmin       - The smallest redshift to consider [pixels].
;                [Scalar or vector of size NOBJ]
;   pmax       - The largest redshift to consider [pixels].
;                [Scalar or vector of size NOBJ]
;   mindof     - Minimum number of degrees of freedom for a valid fit;
;                default to 10.
;   width      - Parameter for FIND_NMINIMA(); default to 3 * PSPACE.
;   minsep     - Parameter for FIND_NMINIMA(); default to the same as WIDTH.
;   plottitle  - ???
;   doplot     - ???
;   debug      - ???
;
; OUTPUTS:
;   zans       - Output structure [NOBJECT,NFIND] with the following elements:
;                z : The relative redshift.
;                z_err : Error in the redshift, based upon the local quadratic
;                        fit to the chi^2 minimum. 
;                chi2 : Fit value for the best (minimum) chi^2
;                dof : Number of degrees of freedom, equal to the number of
;                      pixels in common between the object and templates
;                      minus the number of templates.
;                theta : Mixing angles [NTEMPLATE].  These are computed at the
;                        nearest integral redshift, e.g. at ROUND(ZOFFSET).
;
; COMMENTS:
;   Fits are done to chi^2/DOF, not to chi^2.
;
; EXAMPLES:
;
; BUGS:
;
; DATA FILES:
;   $IDLSPEC2D_DIR/etc/TEMPLATEFILES
;
; PROCEDURES CALLED:
;   find_nminima()
;   splog
;
; INTERNAL SUPPORT ROUTINES:
;   create_zans()
;
; REVISION HISTORY:
;   10-Jul-2000  Written by D. Schlegel, Princeton
;   23-Sep-2002  adapted by MD for use by DEIMOS, DEEP2 survey
;------------------------------------------------------------------------------
; Create output structure
function create_zans, nstar, nfind

   zans1 = create_struct( $
    name = 'ZANS'+strtrim(string(nstar),1), $
    'z'     , 0.0, $
    'z_err' , 0.0, $
    'chi2'  , 0.0, $
    'dof'   ,  0L, $
    'theta' , fltarr(nstar), $
    'R_xcor' , 0.0 )

   return, replicate(zans1, nfind)
end

;------------------------------------------------------------------------------
function zcompute, objflux, objivar, starflux, starmask, nfind=nfind, $
                   poffset=poffset, pspace=pspace, pmin=pmin, pmax=pmax, $
                   mindof=mindof, width=width, minsep=minsep, pbreak=pbreak,$
                   throughput=throughput,$
                   plottitle=plottitle, doplot=doplot1, debug=debug

   if (NOT keyword_set(nfind)) then nfind = 1
   if (NOT keyword_set(pspace)) then pspace = 1
   if (NOT keyword_set(width)) then width = 3 * pspace
   if (NOT keyword_set(plottitle)) then plottitle = ''

;   on_error, 0 ;stop at error

   ; Plot if either /DOPLOT or /DEBUG is set.
   if (keyword_set(doplot1)) then doplot = doplot1
   if (keyword_set(debug)) then doplot = 1

   ;---------------------------------------------------------------------------
   ; Check dimensions of object vectors

   ndim = size(objflux, /n_dimen)
   dims = size(objflux, /dimens)
   npixobj = dims[0]
   if (ndim EQ 1) then nobj = 1 $
    else if (ndim EQ 2) then nobj = dims[1] $
    else message, 'OBJFLUX is neither 1-D or 2-D'

   if total(abs(size(objflux, /dimens)-size(objivar, /dimens))) NE 0 $
    OR size(objflux, /n_dimen) NE size(objivar, /n_dimen) THEN  $
    message, 'Dimensions of OBJFLUX and OBJIVAR do not match'

   ;---------------------------------------------------------------------------
   ; If multiple object vectors exist, then call this routine recursively.

   if (nobj GT 1) then begin
      t0 = systime(1)
      for iobj=0L, nobj-1 do begin
         if (n_elements(poffset) EQ 1) then poffset1 = poffset $
          else poffset1 = poffset[iobj]
         if (n_elements(pspace) EQ 1) then pspace1 = pspace $
          else pspace1 = pspace[iobj]
         if (n_elements(pmin) EQ 1) then pmin1 = pmin $
          else pmin1 = pmin[iobj]
         if (n_elements(pmax) EQ 1) then pmax1 = pmax $
          else pmax1 = pmax[iobj]

         zans1 = zcompute(objflux[*,iobj], objivar[*,iobj], $
          starflux, starmask, nfind=nfind, $
          poffset=poffset1, pspace=pspace1, pmin=pmin1, pmax=pmax1, $
          mindof=mindof, width=width, minsep=minsep, $
          plottitle=plottitle+ ' -- Object #'+strtrim(string(iobj+1),2), $
          doplot=doplot, debug=debug)
         if (iobj EQ 0) then zans = zans1 $
          else zans = [[zans], [zans1]]
         splog, 'Object #', iobj, '  Elap time=', systime(1)-t0, $
          ' (sec)  z=', zans1[0].z, ' (pix)'
      endfor
      return, zans
   endif

;---------------------------------------------------------------------------

   if (NOT keyword_set(mindof)) then mindof = 10
   if (NOT keyword_set(width)) then width = 3 * pspace
   if (NOT keyword_set(minsep)) then minsep = width

   ndim = size(starflux, /n_dimen)
   dims = size(starflux, /dimens)
   npixstar = dims[0]
   if (ndim EQ 1) then nstar = 1 $
    else if (ndim EQ 2) then nstar = dims[1] $
    else message, 'STARFLUX is neither 1-D or 2-D'

   if (NOT keyword_set(starmask)) then begin
      starmask = bytarr(npixstar) + 1
   endif else begin
      if (n_elements(starmask) NE npixstar) then $
       message, 'Dimensions of STARFLUX and STARMASK do not match'
   endelse

   if (NOT keyword_set(throughput)) then begin
      throughput = fltarr(npixobj) + 1
   endif else begin
      if (n_elements(throughput) NE npixobj) then $
       message, 'Dimensions of OBJFLUX and THROUGHPUT do not match'
   endelse

   if (n_elements(poffset) EQ 0) then poffset = 0
   if (n_elements(poffset) GT 1) then $
    message, 'ZOFFSET must be a scalar'
   pixoffset = round(poffset)

   if (n_elements(pmin) EQ 0) then $
    pmin = -2 * ((npixobj < npixstar) + 1) + pixoffset
   if (n_elements(pmax) EQ 0) then $
    pmax = pmin + 2 * ((npixobj < npixstar) - 1)
   
   if (n_elements(pmin) GT 1) then $
    message, 'PMIN must be a scalar'
   if (n_elements(pmax) GT 1) then $
    message, 'PMAX must be a scalar'
   if (pmin GT pmax) then $
    message, 'PMAX must be greater than or equal to PMIN'

   nlag = ((pmax - pmin + 1) / pspace) > 1
   lags = - lindgen(nlag) * pspace + pixoffset - long(pmin) ; must be integers

   if (n_elements(pbreak) EQ 0) then lagbreak = max(lags[0]) $
   else lagbreak = pixoffset-long(pbreak)

   chi2arr = fltarr(nlag)
   dofarr = fltarr(nlag)
   ccorarr = fltarr(nlag)
   normarr = fltarr(nlag)
   thetaarr = fltarr(nstar,nlag)
   physicalarr = intarr(nlag)
   zans = create_zans(nstar, nfind)

   ;---------------------------------------------------------------------------

   sqivar = sqrt(objivar > 0) ;force this to be positive
   objmask = objivar NE 0

   for i=0,nstar-1 do begin
      if median(starflux[*,i]) eq 0.0 then begin
         if n_elements(emi_column) eq 0 then emi_column=i else emi_column=[emi_column,i]
      endif else begin 
         if n_elements(abs_column) eq 0 then abs_column=i else abs_column=[abs_column,i]
      endelse
   endfor
   n_emi = n_elements(emi_column)
   n_abs = n_elements(abs_column)

   for ilag=0L, nlag-1 do begin
      j1 = lags[ilag] < (npixstar-1L)
;;; FOR THE NEGATIVE LAGS, JUST SHIFT THE OBJECT SPECTRUM.
      if (j1 LT 0) then i1 = -j1 $
;;; OTHERWISE SHIFT THE TEMPLATE SPECTRUM (SET i1=0). 
       else i1 = 0L
;;; IF j1 IS NEGATIVE, THEN SET j1=0 (SHIFT OBJECT SPECTRUM
;;; INSTEAD).
      j1 = j1 > 0L
;;; SET j2 SO THAT WE DON'T EXCEED THE LENGTH OF THE TEMPLATE.
      j2 = npixstar-1 < (npixobj+j1-i1-1L)
;;; SIMILARLY 
      i2 = i1 + j2 - j1

;;; When the spectra cover only redward of Hgamma, we do not use
;;; the young stellar template in fitting. 
;;; When the spectra cover only [OII], i.e., blueward of Hgamma, we use 
;;; only one emission template instead of three.
      if lags[ilag] gt lagbreak and nstar eq 5 then $
        usecolumn = [0,1,3,4] $
      else if lags[ilag] lt lagbreak-npixobj and nstar eq 5 then $ 
               usecolumn = [0,1,2] $
           else usecolumn = indgen(nstar)
      mstarflux = starflux[*,usecolumn] 

      n_mstar=n_elements(mstarflux[0,*])

IF abs(j2-j1) LT 100 THEN BEGIN
    chi2arr[ilag] = 1E50
    dofarr[ilag] = 1
    thetaarr[*,ilag] = 1
ENDIF ELSE BEGIN
      chi2arr[ilag] = computechi2( objflux[i1:i2], $ 
                       sqivar[i1:i2] * starmask[j1:j2], $
                       mstarflux[j1:j2,*]*(throughput[i1:i2]#(fltarr(n_mstar)+1)), $
                            acoeff=acoeff, dof=dof)
      dofarr[ilag] = dof
      thetaarr[usecolumn,ilag]=acoeff
; Reform acoeff to have nstar elements. Now it corresponds to starflux.
      acoeff = thetaarr[*,ilag]
;
; is this a physical solution or not??- either both positive, or one +
;                                       and the other modest in strength
;Find the major contributor
;When the emission line templates dominate --- see if the strongest line is positive.
;When the absorption template dominate, compare the sign of 
;the largest feature with a good case. If the sign is the same, 
;then physical. Otherwise, unphysical.

      if n_emi gt 0 then $
         emi_in = starflux[j1:j2,emi_column]#acoeff[emi_column] * throughput[i1:i2] * (objivar[i1:i2] gt 0.0)

      if n_abs gt 0 then begin
         abs_in = starflux[j1:j2,abs_column]#acoeff[abs_column] * throughput[i1:i2] * (objivar[i1:i2] gt 0.0)
         abs_good= starflux[j1:j2,abs_column]#(fltarr(n_elements(abs_column))+1)
         junk = max(abs(abs_in),max_ind)
      endif

      if n_emi gt 0 and n_abs eq 0 then begin ; Only have emission templates
         if total(emi_in) gt 0 then physicalarr[ilag]=1 else physicalarr[ilag]=0
      endif
      if n_emi eq 0 and n_abs gt 0 then begin ; Only have absorption templates
         if abs_in[max_ind]*abs_good[max_ind] gt 0 then $
              physicalarr[ilag]=1 else physicalarr[ilag]=0
      endif
      if n_emi gt 0 and n_abs gt 0 then begin ; Have both
         if max(abs(emi_in)) gt max(abs(abs_in)) then begin
            if total(emi_in) gt 0 then physicalarr[ilag]=1 else physicalarr[ilag]=0
         endif else begin
            if abs_in[max_ind]*abs_good[max_ind] gt 0 then $
                 physicalarr[ilag]=1 else physicalarr[ilag]=0
         endelse
      endif
ENDELSE

      ; Burles counter of lag number...
      if(ilag mod 100 eq 50) then $ 
        print, format='("Lag ",i5," of ",i5,a1,$)', $
         ilag, nlag, string(13b)

   endfor
   if nlag gt 1000 then begin ;remove long range trends
      medchi2 = median(chi2arr)
      diffchi2arr =  djs_median(chi2arr,  width= 201,  boundary='reflect')
; By making the width an odd number in median smoothing, any sharp jumps in chi2arr will not leave a spike in the median-smoothed array. 
      chi2arr = chi2arr-diffchi2arr + medchi2 ;redefine chisqr array
   endif

   ;-----
   ; Fit this chi2/DOF minimum
   indx = where(dofarr GT mindof, ct)
   if (ct GT width) then begin
;      xpeak1 = find_npeaks(-chi2arr[indx]/dofarr[indx], lags[indx], $
;       nfind=nfind, minsep=minsep, width=width, $
;       ypeak=ypeak1, xerr=xerr1, npeak=npeak)
;      zans[0:npeak-1].chi2 = -ypeak1
      xpeak1 = find_nminima(chi2arr[indx], lags[indx], $
       dofarr=dofarr[indx], nfind=nfind, minsep=minsep, width=width, $
       ypeak=ypeak1, xerr=xerr1, npeak=npeak, errcode=errcode, $;/flatnoise, $
       physicalarr=physicalarr[indx],  plottitle=plottitle, doplot=doplot)
      zans[0:npeak-1].z = poffset - xpeak1
      ; Set Z_ERR equal to the error-code if it is non-zero
      zans[0:npeak-1].z_err = xerr1 * (errcode EQ 0) + errcode
      zans[0:npeak-1].chi2 = ypeak1
      for ipeak=0L, npeak-1 do begin
         junk = min(abs(lags-xpeak1[ipeak]), ilag)
         zans[ipeak].dof = dofarr[ilag]
         zans[ipeak].theta = thetaarr[*,ilag]
      endfor
      zans[0:npeak-1].chi2 = zans[0:npeak-1].chi2 * zans[0:npeak-1].dof

      ;Do the x-corr R value calculation
      fullmodel = starflux#zans.theta
      models = fltarr(n_elements(objflux),npeak)
      for ipeak=0, npeak-1 do begin  
         junk = min(abs(lags-xpeak1[ipeak]), ilag) 
         j1 = lags[ilag] < (npixstar-1L)
         if (j1 LT 0) then i1 = -j1 $
          else i1 = 0L
         j1 = j1 > 0L
         j2 = npixstar-1 < (npixobj+j1-i1-1L)
         i2 = i1 + j2 - j1
         models[i1:i2,ipeak] = fullmodel[j1:j2,ipeak]
      endfor
      zans.R_xcor = Rcalc(objflux,objivar,models)
  
      ; Wait for a keystroke...
      if (keyword_set(debug)) then begin
         print, 'Press any key...'
         cc = strupcase(get_kbrd(1))
      endif

   endif else if (ct GE 1) then begin
      zans[0].chi2 = -min(-chi2arr[indx]/dofarr[indx], ilag)
      zans[0].z = poffset - lags[indx[ilag]]
      zans[0].z_err = 0
      zans[0].dof = dofarr[indx[ilag]]
      zans[0].chi2 = zans[0].chi2 * zans[0].dof
      zans[0].theta = thetaarr[*,indx[ilag]]
   endif

   return, zans
end
;------------------------------------------------------------------------------















