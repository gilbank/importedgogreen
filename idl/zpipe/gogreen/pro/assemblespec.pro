pro assemblespec,filename,dir=dir,minrow=minrow,maxrow=maxrow


   ; dir = '/data4/gilbank/IMACS/finalBrian/'
   if NOT keyword_set(dir) then dir = './'
   obj0=mrdfits(dir+filename,0,header)
   nspec= 100;sxpar(header,'N_SLITS')
   ncol = sxpar(header,'NAXIS1')
   gap = fltarr(ncol,10);+10000
;   tot = obj0[*,*,0]
   for i=minrow,maxrow-1 do begin
     obj = mrdfits(dir+filename,i,hdr)

     if sxpar(hdr,'NAXIS1') ne ncol then message,'Unmatching number of columns!'
     nrow = sxpar(hdr,'NAXIS2')
     row = 3+findgen(nrow-6) ; exclude two rows on either edge.
;     row = findgen(nrow)
     if i eq minrow then begin 
          tot = obj[*,row,0] 
	  err = obj[*,row,1] 
     endif else begin
          tot = [[tot],[gap],[obj[*,row,0]]] 
	  err = [[err],[gap],[obj[*,row,1]]]
     endelse
   endfor
   stop
  
  crval1 = fxpar(header, 'crval1')
  cdelt1 = fxpar(header, 'cdelt1')

  mkhdr, hdr, tot, /extend
  sxaddpar, hdr, 'crval1', crval1
  sxaddpar, hdr, 'cdelt1', cdelt1
  sxaddpar, hdr, 'CD1_1',  cdelt1
  sxaddpar, hdr, 'CD2_2',  1.
  sxaddpar, hdr, 'CRPIX1', 1.
  sxaddpar, hdr, 'CRPIX2', 1.
  sxaddpar, hdr, 'CRVAL2', 1.
  sxaddpar, hdr, 'CTYPE1', 'Wavelength'
  sxaddpar, hdr, 'CUNIT1', 'Angstrom'

  mwrfits,tot,'totspec.fits',hdr,/create
  mwrfits,err,'totspec.fits',hdr
end
