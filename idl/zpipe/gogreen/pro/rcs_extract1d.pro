function rcs_extract1d, file, extno, flux, error, ivar, pos, fwhm, nsigma=nsigma,d2dir=d2dir
 
    nrows = n_elements(flux[0,*])
    ncols = n_elements(flux[*,0])

    if NOT keyword_set(d2dir) then d2dir = getenv('RCS_D2DIR')
    header = headfits(d2dir+'/'+file)
    lambda0 = sxpar(header, 'CRVAL1')
    dlam   = sxpar(header, 'CDELT1')
    lambda = lambda0 + dlam*findgen(ncols)

    hdr = headfits(d2dir+'/'+file,ext=extno)
    slitnum = sxpar(hdr,'SLITNUM')
    object  = sxpar(hdr,'OBJECT')

    zero1d = {twod_file: file, twod_extno: fix(extno), slitnum: slitnum, object: object, r1: 0, r2: 0, s2n_peak:0.0, spec:fltarr(ncols), lambda:lambda, ivar:fltarr(ncols), bitmask:intarr(ncols), nbadpix:intarr(ncols)};, objpos:float(pos),fwhm:float(fwhm)}

; check that the pos argument is valid. if not valid, then return a
; spectrum of all zeros and print an error message.
    if pos lt 0 or pos gt nrows-1 or finite(pos) eq 0 then begin
     print, '(extract1d.pro) ERROR: Invalid position ('+ $
           strcompress(string(pos),/rem) + ') for slit ' + $
	   strcompress(string(slitnum),/rem) + '!!'
     return, zero1d
    endif

; check that the fwhm argument is valid. if not valid, then return a spectrum
; of all zeros and print an error message.
    if fwhm le 0 or finite(fwhm) eq 0 then begin
     print, '(extract1d.pro) ERROR: Invalid fwhm for slit ',slitnum, ' in extension ', extno
     print, 'Using a FWHM of 5.'
     fwhm = 5.
;     return,zero1d
    endif

; locate bad pixels in the flux array and interpolate over them.
; interpolate across CTE problems, bad spots in pixmap, in spatial
; direction. do not interpolate across vignetted regions.
  interpolate = error le 0.0

  flux2d = djs_maskinterp(flux, interpolate, iaxis=1, /const)
  ivar2d = djs_maskinterp(ivar, interpolate, iaxis=1, /const)
  skyivar = total(ivar2d,2) ; Temporary. need to fix!!!!!
  interpix = where(interpolate, inter_cnt)
  if inter_cnt gt 0 then ivar2d[interpix] = 0.

   horne = 1
   optimal = 0

; ----------------------------
; if keyword optimal or horne is passed then extract the 1-d spectrum
; according to one of the two variations of the optimal extraction
; algorithm. for more see Horne, K. 1986, /pasp, 98, 609.
  if optimal or horne then begin
; define range in the spatial direction [r1:r2] over which to do
; extraction. exclude the first and last few pixels due to slit edge
; effects.
      r1 = 3; 4
      r2 = nrows-4; -5
; estimate the spatial profile map (P) by modeling the spatial profile
; as a gaussian. let's define the width (sigma) of the gaussian
; according to the fwhm of the spatial profile as measure by the
; find_object routine. and take the center of the gaussian (x0) to be
; at the object position pos (as determined using find_object + peakinfo).
      sigma = fwhm / 2.35482
      x0 = pos
; now do a linear least-squares fit to solve for the continuum and
; amplitude of the gaussian y = a0 + a1*e^(-(x-x0)^2/(2*sigma)).
;      x = (findgen(nrows))[r1:r2] 
;      xdata = exp( -(x - x0)^2 / (2.*sigma^2) )
;      sprof = find_object(slitfile) ;, /CR, /BPM, /NOSUBTRACT, /USETILT)
;    mcc_polyfit, xdata, sprof[r1:r2], [0, 1], a=a
; finally, construct the spatial profile map (P) according to the
; gaussian profile as inferred from the spatial profile given by
; find_object. 
      nlams = n_elements(flux[*,0])
      x = findgen(nrows)
      xdata = exp( -(x - x0)^2 / (2.*sigma^2) )
      P = (xdata) ## (fltarr(nlams)+1.) 
; and make sure that P is >= 0.
;      P = P > 0
      P = abs(P)
; then normalize P.
      Ptot = total(P, 2)
      P = P / (Ptot # (fltarr(nrows)+1.) )
;      for i=0,nlams-1 do P[i,*] = P[i,*] / Ptot[i]



; construct a bad pixel mask (a zero value in the slit.badmask
; corresponds to a good pixel).
;    M = ABS( (slit.mask - 1) < 0 )

; WE REALLY SHOULDN'T HAVE BEEN DISABLING THE BAD PIXEL MASK FOR THE
; OPTIMAL.  _REALLY_.
;      M = slit.mask*0. + 1.
      M = error gt 0.0 ;(slit.mask EQ 0b)
; now define some arrays of the proper length which we will fill with
; the extracted 1-d spectrum, ivar, and bad pixel masks.
      nbits = n_elements(flux[*,0])
      spec_num = fltarr(nbits)
      spec_denom = fltarr(nbits)
      var_num = fltarr(nbits)
      sky_num=var_num
      var_denom = fltarr(nbits)
      crmask = intarr(nbits)
      bitmask = intarr(nbits)+1
      ormask = intarr(nbits)
      infomask = intarr(nbits)
      nbadpix = intarr(nbits)
      var2d = ivar*0. 
      ivar = fltarr(nbits) ; ivar is erased!!!!! Use ivar2d for the original value.
      spec = ivar
; extract the 1-d spectrum. iterate row by row and account for the
; tilt. define the range (in spatial direction) over which to do
; extraction. take only those pixel columns for which we are within
; nsigma*sigma from the object's position (pos).
; 1.5sigma ---> 0.12952 Bevington page 251.
; 2.0sigma ---> 0.05400
      if keyword_set(nsigma) then nsigma=nsigma[0] else nsigma=1.5
      Pgauss = 1.0 / sqrt(2.0 * !pi) / sigma * exp( (-1.0/2.0) * nsigma^2)
      incs = where(P[0,*] ge Pgauss, incnt)
      if incnt gt 4 then begin
          r1 = min(incs)
          r2 = max(incs)
      endif else begin
;          print, 'ERROR: not enough rows selected in optimal extraction!!'
;	  print, 'FWHM: ', fwhm, '  Number of rows: ',incnt
;          return, zero1d
          tmp = max(P[0,*],ind)
	  r1 = ind-2
	  r2 = ind+2
      endelse

; begin iteration...
;      for i=r1,r2 do begin
; fill the var2d array with ivar at the good points....exclude the
; points where ivar=0 since this will blow up upon inversion. the
; variance values at the bad pixels will be set at zero, but this
; isn't a concern since these variance values are not used anywhere.
      good = where(ivar2d gt 0, goodcnt)
      if goodcnt gt 0 then var2d[good] = 1./ivar2d[good]
; now perform the optimal extraction (truly a modified optimal
; extraction) or the horne extraction (which is a true optimal
; extraction). recall the these vectors (spec2d_num, spec2d_denom,
; etc.) are initially filled with zeros. 
      row = r1+findgen(r2-r1+1)
      if optimal then begin
          spec_num = total(M[*,row] * P[*,row] * flux2d[*,row], 2)
          spec_denom = total(M[*,row] * (P^2)[*,row], 2)
	  var_num = total(M[*,row] * P[*,row]^2 * var2d[*,row], 2)
          var_denom = total(M[*,row] * (P[*,row])^2, 2) 
	  sky_num = total(M[*,row] * P[*,row], 2)
      endif
      if horne then begin
	  spec_num = total(M[*,row] * P[*,row] * flux2d[*,row] * ivar2d[*,row], 2)
	  spec_denom = total(M[*,row] * (P^2)[*,row] * ivar2d[*,row], 2)
	  var_num = total(M[*,row] * P[*,row]^2 * ivar2d[*,row], 2)
	  var_denom = total(M[*,row] * (P[*,row])^2 * ivar2d[*,row], 2)
	  sky_num = total(M[*,row] * P[*,row] * ivar2d[*,row], 2)
      endif
; determine the number of pixels at each wavelength which have a CR
; hit in them.
;        crmask = crmask + shift(slit.crmask[*,i] gt 0, cshift)
      for i=r1, r2 do begin
; take the minimum bitmask value (along the row) at each wavelength.
         bitmask = bitmask and M[*,i] 
;      bitmask = bitmask and shift(slit.mask[*,i], cshift)
; also take the OR of the bitmask.
         ormask = ormask or M[*,i]
;        ormask = ormask or shift(slit.mask[*,i], cshift)
      endfor
; determine the number of badpixels at each wavelength.
      nbadpix = fix(r2-r1+1-total(M[*,row],2))
;        nbadpix = nbadpix + shift(slit.mask[*,i] ne 0b, cshift)
; take the OR of the infomask.
;        infomask = infomask or shift(slit.infomask[*,i], cshift)
;    endfor
; now calculate the final spectrum. only do this where things wont
; blow up, that is, when the denominator is non-zero. at places where
; we have a denominator equal to zero, the spectrum will be zero.
    gpix = where(spec_denom gt 0 and nbadpix lt (r2-r1+1)/2., goodcnt)
    if goodcnt gt 0 then $
      spec[gpix] = spec_num[gpix] / spec_denom[gpix] $ ;otherwise 0.
    else print, '(extract1d.pro) ERROR: No good pts in denominator ' + $
      'of spectrum for slit ' + slitno + '!!!'
; and calculate the final ivar array. only do this where things wont
; blow up (just like for spec).
    gpix = where(var_denom ne 0, goodcnt)
    if goodcnt gt 0 then begin
; I _think_ I got the propagation of errors right...
        if optimal then $
          ivar[gpix]= 1/ (var_num[gpix]/(var_denom[gpix])^2 + $
               sky_num[gpix]^2/var_denom[gpix]^2/skyivar[gpix])
        if horne then $
	  ivar[gpix]= var_denom[gpix]
;          ivar[gpix]= 1 / (var_num[gpix]/var_denom[gpix]^2 + $
;               sky_num[gpix]^2/var_denom[gpix]^2/skyivar[gpix])
    endif else print, '(extract1d.pro) ERROR: No good pts in numerator ' + $
      'of variance or denominator of spectrum for slit ' + slitno + '!!!'
; construct the output structure and return it.
    ss1d = zero1d
    ss1d.spec = spec
    ss1d.ivar = ivar
    ss1d.bitmask = bitmask
    ss1d.nbadpix = nbadpix
    ss1d.r1 = r1
    ss1d.r2 = r2
;    ss1d = {twod_file:file, twod_extno:extno, slitnum: slitnum, OBJECT: object, spec:spec, lambda:lambda, ivar:ivar,bitmask:bitmask,nbadpix:nbadpix}
;    ss1d =  {spec:spec, lambda:cwave, ivar:ivar, crmask:crmask, $
;             bitmask:bitmask, ormask:ormask, nbadpix:nbadpix, $
;             infomask:fix(infomask), $
;             objpos:float(pos), fwhm:float(fwhm), $
;             nsigma:nsigma, r1:r1, r2:r2, skyspec: skyspec, $
;             ivarfudge:ivarfudge(spec,ivar)}
    return, ss1d
endif    

; if the keyword nsigma is set, then multiply the fwhm by nsigma. this
; will determine the extraction width spatially.
if boxcar then begin
    if keyword_set(nsigma) then nsigma = nsigma[0] $
    else nsigma = 1.5
    width = fwhm * nsigma

    ext = (ceil(width/2.)) < pos < (nrows-1-pos)
    r1 = (round(pos-ext)) > 3
    r2 = (round(pos+ext)) < (nrows-4)
    
    spec = total(flux[*,r1:r2],2)
    var  = total(1/ivar[*,r1:r2],2)

    bitmask = intarr(ncols)
    nbadpix = intarr(ncols)

    ss1d = zero1d
    ss1d.spec = spec
    ss1d.ivar = 1./var
    ss1d.bitmask = bitmask
    ss1d.nbadpix = nbadpix
    ss1d.r1 = r1
    ss1d.r2 = r2
;    ss1d = {slitnum: slitnum, OBJECT: object, spec:spec, lambda:lambda, ivar:1./var,bitmask:bitmask,nbadpix:nbadpix}
    return,ss1d
endif

end
