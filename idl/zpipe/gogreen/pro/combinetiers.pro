pro combinetiers,maskname,username

  firstier ='zspec.'+username+'.'+maskname+'.firstier.sav'
  secondtier ='zspec.'+username+'.'+maskname+'.secondtier.sav'

  restore,firstier,/ver
  first=results

  cnt = where(results.zquality EQ -1, cn1)
  cnt = where(results.zquality EQ 0, c0)
  cnt = where(results.zquality EQ 1, c1)
  cnt = where(results.zquality EQ 2, c2)
  cnt = where(results.zquality EQ 3, c3)
  cnt = where(results.zquality EQ 4, c4)

  print,'First Tier: ',n_elements(results), ' objects'
  IF(cn1 GT 0) THEN print, cn1, ' objects with Q = -1'
  IF(c0 GT 0)  THEN print, c0, ' objects with Q = 0'
  IF(c1 GT 0)  THEN print, c1, ' objects with Q = 1'
  IF(c2 GT 0)  THEN print, c2, ' objects with Q = 2'
  IF(c3 GT 0)  THEN print, c3, ' objects with Q = 3'
  IF(c4 GT 0)  THEN print, c4, ' objects with Q = 4'

  good = c3+c4
  all = n_elements(results)
  IF(all GT 0) THEN BEGIN
      perc = string(format='(F5.1)', float(good)/all * 100.0)
      print, '-------------------------'
      print, '  Completeness of galaxy redshifts checked: ', perc, '%'
  ENDIF
   
  restore,secondtier,/ver
  second=results

  print,'Second Tier: ',n_elements(results), ' objects'
  cnt = where(results.zquality EQ -1, cn1)
  cnt = where(results.zquality EQ 0, c0)
  cnt = where(results.zquality EQ 1, c1)
  cnt = where(results.zquality EQ 2, c2)
  cnt = where(results.zquality EQ 3, c3)
  cnt = where(results.zquality EQ 4, c4)

  IF(cn1 GT 0) THEN print, cn1, ' objects with Q = -1'
  IF(c0 GT 0)  THEN print, c0, ' objects with Q = 0'
  IF(c1 GT 0)  THEN print, c1, ' objects with Q = 1'
  IF(c2 GT 0)  THEN print, c2, ' objects with Q = 2'
  IF(c3 GT 0)  THEN print, c3, ' objects with Q = 3'
  IF(c4 GT 0)  THEN print, c4, ' objects with Q = 4'

  good = c3+c4
  all = n_elements(results)
  IF(all GT 0) THEN BEGIN
      perc = string(format='(F5.1)', float(good)/all * 100.0)
      print, '-------------------------'
      print, '  Completeness of galaxy redshifts checked: ', perc, '%'
  ENDIF

  results=[first,second] 
  filename='zspec.'+username+'.'+maskname+'.sav'
  save,results,filename=filename
end
