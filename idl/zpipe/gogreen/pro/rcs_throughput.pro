function thirdord, x, p
   return, p[0]+p[1]*x+p[2]*x*x+p[3]*x*x*x
end
function fourthord, x, p
   return, p[0]+p[1]*x+p[2]*x*x+p[3]*x*x*x+p[4]*x^4
end

pro rcs_throughput


    D1_DIR = getenv('RCS_D1DIR')
    readcol,'fhd49798.dat',lam,flux,jansky

; For Z1
    sp1d = mrdfits(D1_DIR+'/sp1d.HD49798z1.fits',1)
    npix = n_elements(sp1d.lambda)
    model = interpol(flux,lam,sp1d.lambda)
    ratio = sp1d.spec/sp1d.lambda/model
    throughput = ratio/max(ratio)

    range = [4550,4675,4697,4775,4782,4848,4885,5388,5434,5600,5632,5750,6020,6215,6325,6357,6420,6500,6580,6655,6706,6852]
    num = n_elements(range)/2
    blueside = range[findgen(num)*2]
    redside = range[findgen(num)*2+1]

    mask = bytarr(n_elements(sp1d.lambda))
    for i=0,num-1 do begin 
      ind=where(sp1d.lambda gt blueside[i] and sp1d.lambda lt redside[i])
      mask[ind] = 1
    endfor
    fitrange=where(mask)

    sm_thrupt= smoothivar(throughput,mask+1.e-10,21)
    int_thrupt = interpol(sm_thrupt[fitrange],sp1d.lambda[fitrange],sp1d.lambda) > 0.0
;    parms=mpfitfun('thirdord',sp1d.lambda,int_thrupt,fltarr(1327)+0.1,fltarr(4),yfit=yfit)
    th_z1 = {lambda:sp1d.lambda,throughput:int_thrupt}

; For Z2
    sp1d = mrdfits(D1_DIR+'/sp1d.HD49798z2.fits',1)
    npix = n_elements(sp1d.lambda)
    model = interpol(flux,lam,sp1d.lambda)
    ratio = sp1d.spec/sp1d.lambda/model
    throughput = ratio/max(ratio)

    range = [5154,5388,5434,5847,5904,6012,6325,6357,6420,6500,6580,6655,6705,6850,6967,7050,7348,7570,7710,7780]
    num = n_elements(range)/2
    blueside = range[findgen(num)*2]
    redside = range[findgen(num)*2+1]

    mask = bytarr(n_elements(sp1d.lambda))
    for i=0,num-1 do begin 
      ind=where(sp1d.lambda gt blueside[i] and sp1d.lambda lt redside[i])
      mask[ind] = 1
    endfor
    fitrange=where(mask)

    sm_thrupt= smoothivar(throughput,mask+1.e-10,21)
    int_thrupt = interpol(sm_thrupt[fitrange],sp1d.lambda[fitrange],sp1d.lambda) > 0.0
;    parms=mpfitfun('thirdord',sp1d.lambda,int_thrupt,fltarr(1327)+0.1,fltarr(4),yfit=yfit)
    th_z2 = {lambda:sp1d.lambda,throughput:int_thrupt}

; For Z3
    sp1d = mrdfits(D1_DIR+'/sp1d.HD49798z3.fits',1)
    npix = n_elements(sp1d.lambda)
    model = interpol(flux,lam,sp1d.lambda)
    ratio = sp1d.spec/sp1d.lambda/model
    throughput = ratio/max(ratio)

    range = [6027,6105,6239,6266,6325,6363,6420,6500,6580,6655,6705,6850,6967,7050,7080,7155,7348,7570,7710,8210,8260,8668]
    num = n_elements(range)/2
    blueside = range[findgen(num)*2]
    redside = range[findgen(num)*2+1]

    mask = bytarr(n_elements(sp1d.lambda))
    for i=0,num-1 do begin 
      ind=where(sp1d.lambda gt blueside[i] and sp1d.lambda lt redside[i])
      mask[ind] = 1
    endfor
    fitrange=where(mask)

    sm_thrupt= smoothivar(throughput,mask+1.e-10,21)
    int_thrupt = interpol(sm_thrupt[fitrange],sp1d.lambda[fitrange],sp1d.lambda) > 0.0
;    parms=mpfitfun('thirdord',sp1d.lambda,int_thrupt,fltarr(1327)+0.1,fltarr(4),yfit=yfit)
    th_z3 = {lambda:sp1d.lambda,throughput:int_thrupt}
    th_all = [th_z1,th_z2,th_z3]
   
stop
    mwrfits,th_all,'rcs_throughput.fits',/create
end
