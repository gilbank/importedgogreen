pro rcs_fluxcal, spec1d, fluxcal

   minlam=min(spec1d.lambda)
   maxlam=max(spec1d.lambda)

   if maxlam lt 7000 then filter=1 else $
       if minlam gt 6000 then filter =3 else $
           if minlam gt 5000 then filter =2 else filter=4

   throughput=interpol(fluxcal[filter-1].throughput,fluxcal[filter-1].lambda,spec1d.lambda) > 0.05
   spec1d.spec = spec1d.spec/throughput
   spec1d.ivar = spec1d.ivar*throughput*throughput
end

