pro template_qso_rcs, fileout

  deep_eigendir=getenv('IDLSPEC1D_DIR')+'/templates/'
  deep_eigenfile = 'spEigenQSOdeep.fits'

  qso=mrdfits(deep_eigendir+deep_eigenfile,0,hdr)

  loglam0=sxpar(hdr,'COEFF0')
  dloglam=sxpar(hdr,'COEFF1')
  npix=sxpar(hdr,'NAXIS1')
  loglam=loglam0+findgen(npix)*dloglam
  slam = 10^loglam

; Make a sigma=200km/s velocity kernel to convolve with the templates.
  cc = 299792.5 ; km/s
  vv = 200.
  width = ceil(alog10(1+vv/cc)/dloglam)
  kernel = fltarr(6*width+1)
  xx = findgen(6*width+1)-3*width
  kernel = exp(-xx*xx/(2*width*width))

  template = convol(qso,kernel,total(kernel))

  newdloglam = 1.e-4
  nspec = npix *dloglam/newdloglam -1
  logwave = loglam0+findgen(nspec)*newdloglam

  qsotempl = interpol(template,loglam,logwave)

  hdr_rcs=hdr
  sxaddpar,hdr_rcs,'COEFF0',loglam0
  sxaddpar,hdr_rcs,'COEFF1',newdloglam
  rcs_eigendir=getenv('RCS_TEMPLATES_DIR') 
  mwrfits,qsotempl,rcs_eigendir+'/spEigenQSOrcs.fits',hdr_rcs,/create 
end
