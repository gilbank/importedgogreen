pro collectsp1d,objlist,filename

   d1dir=getenv('RCS_D1DIR')
 
   maskname=strtrim(objlist.maskname,2)

   ss = sort(maskname)
   obj = objlist[ss]
   maskname=maskname[ss]

   previousmask = '0000'
   for i=0,n_elements(obj)-1 do begin 
	sp1dfile=d1dir+'sp1d.'+maskname[i]+'.fits'
        if i eq 0 or maskname[i] ne previousmask then sp1dall=mrdfits(sp1dfile,1,hdr) 
        slit = where(sp1dall.slitnum eq long(obj[i].slitname), ct) 
        if ct eq 1 then sp1d=sp1dall[slit] else message,'Error: slit not found!'
        if n_elements(totsp1d) eq 0 then totsp1d=sp1d else begin 
           if size_struct(sp1d) eq size_struct(totsp1d) then totsp1d=[totsp1d,sp1d] $
 	   else begin 
		if n_elements(mismatch) eq 0 then mismatch=obj[i] $
		else mismatch=[mismatch,obj[i]]
           endelse
        endelse
        previousmask=maskname[i]
   endfor

   fxbhmake,infohdr,50
   sxaddpar,infohdr,'DATE','0000-00-00'
   sxaddpar,infohdr,'AIRMASS',1.1
   sxaddpar,infohdr,'MASKNAME','0000'
   tmphdr = infohdr
   sxaddpar,tmphdr,'NAXIS2',n_elements(totsp1d)
   mwrfits,totsp1d,'sp1d.'+filename+'.fits',tmphdr,/create

   if n_elements(mismatch) ne 0 then collectsp1d,mismatch,filename+'1'

end
