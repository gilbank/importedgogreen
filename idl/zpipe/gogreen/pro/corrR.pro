pro corrR, maskname, slitnum,outfile

;    zres_file = getenv('RCS_ZRESDIR')+'zresult.'+maskname+'.fits'
    zres_file = '/home/yan/rcs/rcs_result/zresult/zresult.'+maskname+'.fits'
;    sp1d_file = getenv('RCS_D1DIR')+'sp1d.'+maskname+'.fits'
    sp1d_file = '/home/yan/rcs/rcs_result/sp1d.'+maskname+'.fits'

    zresult=mrdfits(zres_file,2)
    slitname = string(slitnum,format='(i0.0)')
    n=where(strmatch(strtrim(zresult.slitname,2), slitname) and zresult.class eq 'GALAXY', ct)
    
    if ct eq 5 then zres5 = zresult[n] else message,'Error: Found too many or too few redshifts.'
    eigenvalue = zres5.theta[0:2,*]

    sp1dall = mrdfits(sp1d_file,1)
    n=where(sp1dall.slitnum eq slitnum, ct)
    if ct eq 1 then spec1d=sp1dall[n] else message,'Error: No spectrum found.'

    templ_file = getenv('GOGREEN_TEMPLATES_DIR')+'spRCS.fits' 
    templates = mrdfits(templ_file,0,hdr)
    coeff0 = sxpar(hdr,'COEFF0')
    coeff1 = sxpar(hdr,'COEFF1')
    npix = sxpar(hdr,'NAXIS1')
 
    logwave0 = coeff0+coeff1*dindgen(npix)

    lambda = spec1d.lambda
    airtovac, lambda
    spec1d.lambda = float(lambda)

    loglam = linear2log(spec1d, binsize=coeff1, flux=objflux, ivar=objivar)
    objcont = djs_median(objflux,width=200,boundary='reflect')
    objflux = objflux - objcont
    negind = where(objivar lt 0., tmp)
    if tmp gt 0 then objivar[negind] = 0.0
;    error = 1.0/sqrt(objivar+1.d-32)

if 0 then begin
  skyspec = mrdfits(getenv('GOGREEN_TEMPLATES_DIR')+'brisky.fits',0,skyhdr)
  skyloglam0 = sxpar(skyhdr,'COEFF0')
  skydloglam = sxpar(skyhdr,'COEFF1')
  skyloglam = skyloglam0+skydloglam*dindgen(n_elements(skyspec))
  
  sky = interpol(skyspec,skyloglam,loglam)
  mask = sky gt 0.1
  objivar[where(mask)] = 0.0
endif

    model = templates#eigenvalue

    models = fltarr(n_elements(loglam),5)
    for i=0,4 do begin
      logwave = logwave0+alog10(1+zres5[i].z)
      models[*,i] = interpol(model[*,i],logwave,loglam)
      cont = djs_median(models[*,i],width=200,boundary='reflect')
      models[*,i] = models[*,i]-cont
    endfor

    save,zres5,loglam,objflux,objivar,models,filename=outfile
; loglam is the log10(lambda)
; objflux is the flux 
; error is the error array
; models[*,i] contains the combined template for the ith redshift
    stop
end
