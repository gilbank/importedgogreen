pro changename,oldname,newname

  d2dir = getenv('RCS_D2DIR')+'/'
  spawn,'cp '+d2dir+oldname+'_2spec.fits '+d2dir+newname+'_2spec.fits'

  d1dir = getenv('RCS_D1DIR')
  sp1d = mrdfits(d1dir+'/sp1d.'+oldname+'.fits',1,hdr)
  sxaddpar,hdr,'MASKNAME',newname
  sp1d.maskname = newname
  mwrfits,sp1d,d1dir+'/sp1d.'+newname+'.fits',hdr

  zresdir = getenv('RCS_ZRESDIR')
  zres = mrdfits(zresdir+'/zresult.'+oldname+'.fits',1,hdr)
  zres2 = mrdfits(zresdir+'/zresult.'+oldname+'.fits',2,hdr)
  zres.maskname = newname
  zres2.maskname = newname
  mwrfits,zres,zresdir+'/zresult.'+newname+'.fits',/create
  mwrfits,zres2,zresdir+'/zresult.'+newname+'.fits'
end

  
  

   
