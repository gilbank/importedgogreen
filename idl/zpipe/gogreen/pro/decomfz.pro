pro decomfz


  list=findfile('*.fz')
  pos=strpos(list,'.fz')
  name = strmid(list,0,pos[0])
  outputname = '../'+name+'.fits'
  openw,lun1,'decompress.csh',/get_lun
  printf,lun1,'#!/bin/tcsh'
  for i=0,n_elements(name)-1 do $ 
   printf,lun1,'imcopy '+list[i]+' '+outputname[i]
  close,lun1


end
