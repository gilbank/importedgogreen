pro combinebatches, maskname, username

   dir = getenv('RCS_D2DIR')+'zspec/'
   files=file_search(dir,'zspec.'+username+'*.'+maskname+'.secondtier.'+'?.sav', count=filect)
   if filect eq 0 then return

   namerank = ['elling','yan']

   batches = intarr(filect)
   reviewer = strarr(filect)
   rank = intarr(filect)

   pos1 = strpos(files,'zspec.')+strlen(username)+7
   pos2 = strpos(files,maskname)-1
   pos3 = strpos(files,'.sav')
   for i=0,filect-1 do begin 
     reviewer[i] = strmid(files[i],pos1[i],pos2[i]-pos1[i])
     rank[i] = where(namerank eq reviewer[i])
     batchnum = strmid(files[i],pos3[i]-1,1)
     batches[i] = fix(batchnum)
;     print,reviewer[i],batches[i],rank[i]
   endfor
   for j=1,max(batches) do begin
     ind=where(batches eq j,ct)
     if ct eq 0 then begin 
        message,'WARNING: Missing batch '+string(j,format='(i0.0)')+' for mask '+maskname,/continue
        continue
     endif
     if ct ge 1 then begin 
        tmp=max(rank[ind],tt)
        ind = ind[tt]
     endif
     if n_elements(totind) eq 0 then totind=reform(ind) else totind=[totind,reform(ind)]
   endfor

   print,'Files used in the final results:'
   for k=0,n_elements(totind)-1 do begin
     print,'  '+files[totind[k]]
     restore,files[totind[k]]
     if k eq 0 then totres=results else totres=[totres,results]
   endfor
   
   results=totres
   savename=dir+'zspec.'+username+'.'+maskname+'.secondtier.sav'
   save,results,filename=savename
   second=results

;--------------------Now for the first tier files.

   firstfiles = file_search(dir,'zspec.*.'+maskname+'.firstier.sav', count=filect)
   if filect ge 1 then begin
     rank = intarr(filect)
     finaluser=strarr(filect)
     pos1 = strpos(firstfiles,'zspec')+6
     pos2 = strpos(firstfiles,maskname)-1
     for i=0,filect-1 do begin 
       userstr = strmid(firstfiles[i],pos1[i],pos2[i]-pos1[i])
       users = strsplit(userstr,'.',/extract,count=userct) 
       finaluser[i] = users[userct-1]
       rank[i] = where(namerank eq finaluser)
     endfor
     tmp = max(rank,tt)
     firstier=firstfiles[tt]
     print,'  '+firstier
     restore,firstier
     first = results

     results=[first,second] 
   endif else results=second
   filename=dir+'zspec.'+username+'.'+maskname+'.sav'
   save,results,filename=filename

end
