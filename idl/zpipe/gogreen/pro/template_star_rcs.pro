pro template_star_rcs, fileout

  deep_eigendir=getenv('IDLSPEC1D_DIR')+'/templates/'
  sdss_eigenfile = 'spEigenStar.fits'
  deep_eigenfile = 'spEigenStarDeep2.fits'

  stars=mrdfits(deep_eigendir+sdss_eigenfile,0,hdr_sdss)
  nsdss = n_elements(stars[0,*])

  loglam0=sxpar(hdr_sdss,'COEFF0')
  dloglam=sxpar(hdr_sdss,'COEFF1')
  npix=sxpar(hdr_sdss,'NAXIS1')
  slam = 10^(loglam0+findgen(npix)*dloglam)

; Make a sigma=200km/s velocity kernel to convolve with the templates.
  cc = 299792.5 ; km/s
  vv = 200.
  width = ceil(alog10(1+vv/cc)/dloglam)
  kernel = fltarr(6*width+1)
  xx = findgen(6*width+1)-3*width
  kernel = exp(-xx*xx/(2*width*width))

;   template0= convol(deep_eigen[*,0],kernel,total(kernel))
;   template2= convol(deep_eigen[*,2],kernel,total(kernel))

  deepstars=mrdfits(deep_eigendir+deep_eigenfile,0,hdr_deep)
  nstars=n_elements(deepstars[0,*])

  sdssstar=strarr(nsdss)
  for i=0,nsdss-1 do begin
    namei = 'NAME'+string(i,format='(i0)')
    sdssstar[i] = sxpar(hdr_sdss,namei)
  endfor

  for i=0,nstars-1 do begin
    namei = 'NAME'+string(i,format='(i0)')
    starnamei = sxpar(hdr_deep,namei)
    ind = where(strtrim(sdssstar) eq strtrim(starnamei), ct)
    if ct ne 1 then message,'None or multiple matches found for '+namei
    print,ind,sdssstar[ind]
    templstar=stars[*,ind]
    bad=where(templstar eq 0.0)
    good=where(templstar gt 0.0)
    templstar[bad]=interpol(templstar[good], slam[good], slam[bad])
    newstar = convol(templstar,kernel,total(kernel))
    newstar = newstar*slam/mean(slam)
    if i eq 0 then totstar=newstar else totstar=[[totstar],[newstar]]
  endfor
  hdr_rcs=hdr_deep
  sxaddpar,hdr_rcs,'COEFF0',loglam0
  sxaddpar,hdr_rcs,'COEFF1',dloglam
  rcs_eigendir=getenv('RCS_TEMPLATES_DIR') 
  mwrfits,totstar,rcs_eigendir+'/spEigenStarRCS.fits',hdr_rcs,/create 
end
