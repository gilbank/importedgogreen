
function get_blank_spec1d_struct, npix
  ssbl = {object:'none', slitnum:0L, ra:0D, dec:0D, $
          twod_file:'none', twod_extno:0L, $
          r1:1L, r2:2L, s2n_peak:0.0, $ 
          lambda:fltarr(npix), spec:fltarr(npix), $
          ivar:fltarr(npix), bitmask:lonarr(npix), $
          nbadpix:lonarr(npix)}
  return, ssbl
end

pro make_spec1dfile_gogreen, file1d, file2d, outfile

  fits_info, file1d, n_ext=nspec, /silent
  if (nspec - 1L) mod 3L ne 0 then message, 'Wrong # of extensions in file!'
  nspec = (nspec - 1L) / 3L
;  ssinfo = mrdfits(file1d, 'MDF', hdrinfo, /silent)
  ssinfo = mrdfits(file1d, 1, hdrinfo, /silent)
  dex = where(ssinfo.extver gt 0, nslit)
  if nslit ne nspec then message, 'ERROR with # of slits!'

; check that 2-d file exists..
  jnkfile = findfile(file2d, count=n2d)
  if n2d ne 1 then message, '2-d file not found!'

; construct new base table with info about 2-d spectra...
  nobj = n_elements(ssinfo.extver)
  tmp = {id:0L, ra:0D, dec:0D, extver:0L, slitid:0L, $
         twod_file:'none', twod_extno:0L, $
         refpix1:0L, secx1:0L, secx2:0L, secy1:0L, secy2:0L, speccen:0D}
  ssnew = replicate(tmp, nobj)
  struct_assign, ssinfo, ssnew, /nozero
  ssnew.twod_file = file2d
  ssnew.twod_extno = ssinfo.extver ;(3L * ssinfo.extver) - 2L 

  fxhclean, hdrinfo
  mwrfits, ssnew, outfile, hdrinfo, /silent, /create


  for ii=0L,nobj-1 do begin
     extn = ssinfo[ii].extver

     if extn gt 0 then begin
        specii = mrdfits(file1d, 3L*extn-1L, hdrii, /silent)
        varii = mrdfits(file1d, 3L*extn, /silent)
        dqii = mrdfits(file1d, 3L*extn+1L, /silent)
; grab the iith spectra and pack it as a DEEP2-esque structure...
;        spec1d = get_blank_spec1d_struct(n_elements(specii))
        npix = 1550
        spec1d = get_blank_spec1d_struct(npix)
        spec1d.slitnum = ssinfo[ii].slitid
        spec1d.ra = ssinfo[ii].ra
        spec1d.dec = ssinfo[ii].dec
        spec1d.object = strcompress(ssinfo[ii].id, /rem)
        spec1d.lambda = (findgen(npix) * sxpar(hdrii, 'CD1_1')) + $
                        sxpar(hdrii, 'CRVAL1')
        spec1d.spec = specii
        spec1d.ivar = 1.0 / varii
        spec1d.nbadpix = dqii

        mwrfits, spec1d, outfile, /silent
     endif
  endfor

  print, 'File written: ' + outfile
  print, 'Compressing...'
  spawn, 'gzip -1vf ' + outfile
  print, 'Done.'



end
