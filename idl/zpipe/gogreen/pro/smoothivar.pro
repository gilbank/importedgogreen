function  smoothivar, curve, ivar,scale
   ivarcopy = ivar
   k = where(finite(ivar) eq 0,ct)
   if ct gt 0 then ivarcopy[k] = 0.0
   numerator = smooth(curve*ivarcopy,scale,/edge_truncate)
   denominator = smooth(ivarcopy,scale,/edge_truncate)
   ind = where(denominator eq 0.0,ct)
   if ct gt 0 then denominator[ind] = 1.e-16
   out = numerator/denominator
;smooth(curve*ivar,scale,/edge_truncate)/smooth(ivar,scale,/edge_truncate)
   return,out
end

