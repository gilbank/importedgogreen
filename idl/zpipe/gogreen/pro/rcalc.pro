function rcalc, objflux, objivar, models
;restore,'example315.sav',/ver

; assumes that spectrum and model are aligned to match features
npix=n_elements(objflux)   ; number of pixels in spectrum
ntemplate=n_elements(models)/npix ; number of models
coroffset = floor(npix*0.3)  ; number of pixels around best redshift for R calculation
;this shouldn;t be a very large fraction of the size of the spectrum because we need enough pixel overlap between model and spectrum
cor=fltarr(2*coroffset+1,ntemplate)  ; correlation function
prod=fltarr(npix)   ; product of spectrum and model; will be integrated to get cor
asym=cor
symm=asym
r=fltarr(ntemplate)

;modelsshift=shift(models,-1,0)  ; check that features are not shifted

for ntemp=0,ntemplate-1 do begin  ; loop over templates
  for j=-coroffset,coroffset do begin  ; calculate correlatin function over chsen window
    prod=prod*0.0
; shift models by j
    prod = objflux*objivar*shift(models[*,ntemp],j)
; Because the 'shift' function is doing a circular shift, we shall not sum up the whole prod array. Find the right range and sum that part up.
    start = 0 > j
    finish = j+npix-1 < npix-1
    cor[j+coroffset,ntemp] = total(prod[start:finish])/float(npix-abs(j))
;correlation function is "shift, multiply and add"
  endfor
  asym[*,ntemp] = cor[*,ntemp] - reverse(cor[*,ntemp])
  symm[*,ntemp] = cor[*,ntemp] + reverse(cor[*,ntemp])
; The REVERSE function is useful here.

;  plot,cor(*,ntemp)
  ssdev=(stddev(asym[*,ntemp]))/2.
  r[ntemp]=cor[0+coroffset,ntemp]/ssdev
;  print,ntemp,r(ntemp)

endfor
  return, r 
end

