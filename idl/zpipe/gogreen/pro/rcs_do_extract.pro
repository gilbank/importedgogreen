pro rcs_do_extract,maskname,dir=dir,slitrange=slitrange,list=list,outfile=outfile,nsigma=nsigma
 
   D2_dir = getenv('RCS_D2DIR')
   if NOT keyword_set(dir) then dir=D2_dir+'/'

goto, skip_rcs_specific
;Collect observation infor, and construct spec1dfilename     
   fxbhmake,infohdr,50
   allobs = mrdfits(D2_dir+'/allobs.fits',1)
   n = where(strmatch(allobs.maskname, maskname+'*',/fold_case), ct)
   if ct gt 1 then begin 
;    print,'Multiple Observation of this mask. Choose one!'
;    stop
;    When there are multiple, take the latest observations which should be best.
     ss=sort(allobs[n].mjd)
     n = n[ss[ct-1]]
   endif
   if ct eq 0 then begin
     print,'This mask is not found in allobs.fits. Please manually specify the utdate,airmass.'
     stop
   endif else begin
     utdate = allobs[n].utdate
     airmass = allobs[n].airmass
   endelse
   sxaddpar,infohdr,'DATE',utdate
   sxaddpar,infohdr,'AIRMASS',airmass
   sxaddpar,infohdr,'MASKNAME',maskname
skip_rcs_specific:
   fxbhmake,infohdr,50

   D1_dir = getenv('RCS_D1DIR')
   if NOT keyword_set(outfile) then outfile='sp1d.'+maskname+'.fits'
   spec1dfilename = concat_dir(D1_dir, outfile)

; define the window and nbuf values passed to the peakinfo routine.
   window = 5.
   nbuf = 2.
   thresh = 10.0
   
   filename = maskname + '_2spec.fits'
   fullname = dir + filename

;   readcol,'objlist2',mag,objnm,format='f,a'
;   phm = mrdfits('phm'+maskname+'.fits',1)

   temp = mrdfits(fullname,0,header)
   nslits = sxpar(header, 'N_SLITS')
   lambda0 = sxpar(header, 'CRVAL1')
   dlam   = sxpar(header, 'CDELT1')
;   tmpcat = {object:' ',magR:0.0,magZ:0.0,s2nwin:0.0,s2nfwhm:0.0}

;   totcat = replicate(tmpcat,nslits) 

   if NOT keyword_set(slitrange) then slitrange=[0,nslits-1]
   minslit = slitrange[0]
   maxslit = slitrange[1]

;   psplot,'sprofile.ps',/square,/color
;   !p.multi=[0,4,4]
   for i=slitrange[0], slitrange[1]  do begin
     spec2d = mrdfits(fullname,i,hdr)
     slitnm =strcompress(sxpar(hdr,'OBJECT'),/remove_all)
     if keyword_set(list) then $ 
     	if total(strtrim(list.objname,2) eq strtrim(slitnm,2)) eq 0 then continue
;check slit length is not zero
     if sxpar(hdr,'SLITLEN') lt 20 then continue

     centrow=sxpar(hdr,'CNTRLINE')

     flux = reform(spec2d[*,*,0])
     err  = reform(spec2d[*,*,1])

;;change the error flag from 0.0 to 1.e10
;     tmp = where(err eq 0.0 or err eq -9999., have)
;     if have gt 0 then begin 
;       err[tmp] = 1.e10
;;Extend the catastrophic region to cover the affected area
;       paderr  = smooth(err,[7,1],/edge_truncate)
;       badpart = where(paderr gt 1.e9)
;       err[badpart] = paderr[badpart]
;     endif
     mask = err gt 0.0
     npix = total(mask, 1)

     ivar = 1./(err*err)
     wh = WHERE(err LE 0.0, whct)
     IF whct GT 0 THEN ivar[wh] = 0.0
;     profile = total(flux*mask,1)/total(ivar,1)
;     pro_ivar = total(ivar,1)
;     plot,profile
     sflux = median(flux, 5, dimension=1)
     sprof = total((sflux*mask), 1) /float(npix)
     profivar = FLOAT(npix)^2/(TOTAL(mask/(ivar > 1.E-10), 1) )


;;; MAKE SURE THAT THE SPATIAL PROFILE IS FINITE IN
;;; ALL PIXEL COLUMNS.
     wh = WHERE(FINITE(sprof) EQ 0, whct)
     IF whct GT 0 THEN sprof(wh) = 0.

;Find the peak in the slit profile and estimate its significance.
     peakinfo, sprof, pk_pixel, fwhm, pk_quad=pkq, pk_cent=pkc, $
               profivar=profivar, npix=npix, s2n_window=s2n_win, $
               nbuf=nbuf, window=window, s2n_fwhm=s2n_fwhm, signif=thresh

     pkcol = pk_pixel[0]
     if pk_pixel[0] ne -1.0 then begin
       s2nwinperpix = s2n_win[0]/sqrt(npix[pkcol])
       s2nfwperpix = s2n_fwhm[0]/sqrt(npix[pkcol])
     endif else begin
       s2nwinperpix = -1.0
       s2nfwperpix  = -1.0
     endelse 

;     ind = where(strcmp(strcompress(phm.object,/rem), slitnm), have)
;     if have ne 1 then message,'Object not found in list or more than one are found' else begin
;       tmpcat = {object:slitnm,magR:phm[ind].magR,magZ:phm[ind].magZ,s2nwin:s2nwinperpix,s2nfwhm:s2nfwperpix}
;       totcat[i]=tmpcat
;     endelse
 
;Check the position of the peaks.
;     if pk_pixel[0] gt 0 then begin
;       plot, sprof,ytick_get=ylabel
;       print,pk_pixel
;       print,fwhm
;       print,s2n_win
;       plots,[pk_pixel[0],pk_pixel[0]],[ylabel[0],ylabel[n_elements(ylabel)-1]]
;       plots,[pk_pixel[0]-fwhm[0]/2.,pk_pixel[0]+fwhm[0]/2.],[sprof[pk_pixel[0]]/2.,sprof[pk_pixel[0]]/2.]
;       plots,[centrow,centrow],[ylabel[0],ylabel[n_elements(ylabel)-1]],linest=2
;     endif


; if the peak position determined via centroiding is good, then use
; it. otherwise use the peak pixel position. recall that the peakinfo
; routine returns the value -1 for pk_cent if the centroid fails.
     pkcol = float(pk_pixel)
     gdex = where(pkc ge 0., ngood)
     if ngood gt 0 then pkcol[gdex] = pkc[gdex]

; now compare the locations of the peaks found in the spatial profile
; to the object position given by the design specifications.
     mindiff = min(abs(pkcol-centrow),minpos)

; And look for serendips. to do. RY

     extno = i
     spec1d=rcs_extract1d(filename,extno,flux,err,ivar,pkcol[minpos],fwhm[minpos],nsigma=nsigma)
     spec1d.s2n_peak=s2n_fwhm[minpos]

     if n_elements(tot) eq 0 then tot = spec1d else tot=[tot,spec1d]     
     if i mod 50 eq 0 then begin 
       tmphdr = infohdr
       sxaddpar,tmphdr,'NAXIS2',n_elements(tot)
       mwrfits,tot,spec1dfilename,tmphdr,/create
     endif
   endfor
;   psclose
;   !p.multi=0

   tmphdr = infohdr
   sxaddpar,tmphdr,'NAXIS2',n_elements(tot)
;   mwrfits,totcat,'s2n_est.fits',/create
   mwrfits,tot,spec1dfilename,tmphdr, /create  
end
