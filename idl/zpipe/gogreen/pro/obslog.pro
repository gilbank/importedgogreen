pro obslog,fz=fz

  if NOT keyword_set(fz) then filelist=findfile('ccd????c?.fits*') $
			 else filelist=findfile('ccd????c?.fz*')
  frameno = strmid(filelist,3,4)
  ss = sort(frameno)
  ind = uniq(frameno[ss])
  filelist = filelist[ss[ind]]
;  filelist=filelist[sort(filelist)]

  openw,lun1,'Listing',/get_lun
  printf,lun1,'#FileNo', 'description', 'exptime', 'filter', 'disperser', 'airmass', 'slitmask', 'UTdate','UTtime', 'localtime',format='(a7,a25,a8,a12,a13,a7,a12,a11,a9,a9)'
  for i=0,n_elements(filelist)-1 do begin
   fileno = strmid(filelist[i],0,7) 
   if keyword_set(fz) then hdr=headfits(filelist[i],exten=1) $
  		      else hdr=headfits(filelist[i])
   description = sxpar(hdr,'OBJECT')
   exptime = sxpar(hdr,'EXPTIME')
   filter  = sxpar(hdr,'filter')
   disperser = sxpar(hdr,'DISPERSR')
   airmass = sxpar(hdr,'AIRMASS')
   slitmask = sxpar(hdr,'SLITMASK')
   UTdate = sxpar(hdr,'DATE-OBS')
   UTtime = sxpar(hdr,'UT-TIME')
   localtime = sxpar(hdr,'LC-TIME')
   cal_lamp = sxpar(hdr,'CAL-LAMP')
    
   printf,lun1,fileno, description, exptime, filter, disperser, airmass, slitmask, UTdate,UTtime, localtime,format='(a7,a25,f8.1,a12,a13,f7.3,a12,a11,a9,a9)'
  endfor
  free_lun,lun1

end
