pro obsinfo, frames

  n=n_elements(frames)
  filenames=frames[0]+'c1.fits*'
  file=file_search(filenames,count=ct)
  if ct eq 0 then begin 
     message,'No files found matching '+files
     stop
  endif
  obj=mrdfits(file[0],0,hdr)
  date = sxpar(hdr,'NIGHT')
  maskname = sxpar(hdr,'SLITMASK')

  airmass_arr=fltarr(n)
  airmass_arr[0] = sxpar(hdr,'AIRMASS')
  for i=1,n-1 do begin
     filenames=frames[i]+'c1.fits*'
     file=file_search(filenames,count=ct)
     if ct ge 1 then begin
       obj=mrdfits(file[0],0,hdr)
       airmass_arr[i] = sxpar(hdr,'AIRMASS') 
       if NOT strcmp(sxpar(hdr,'SLITMASK'),maskname,/fold_case) $
    	  then begin 
            message,'Wrong file specified. Masks do not match.'
	    stop
       endif
     endif else begin 
        message,'No files found matching '+files
        stop
     endelse
  endfor

  if n eq 1 then mean_am = airmass_arr[0]
  if n eq 2 then mean_am = airmass_arr[n/2]
  if n ge 3 then mean_am = median(airmass_arr)

  info_str={maskname:' ', date:' ', airmass:0.0}
  info_str.maskname=maskname
  info_str.date=date
  info_str.airmass=mean_am
  infofile=maskname+'_info.fits'
  mwrfits,info_str,infofile,/create
end
