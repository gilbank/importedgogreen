function obslogstr,dir,status=status

 filelist=findfile(dir+'ccd????c?.fits*')
 pos = strpos(filelist[0],'ccd')
 frameno = strmid(filelist,pos,7)
 ss = sort(frameno)
 ind = uniq(frameno[ss])
 filelist = filelist[ss[ind]]
 ct = n_elements(ind)
 if ct ge 1 then begin
;  filelist=filelist[sort(filelist)]

  info = {DIR:' ',FRAMENO:' ', UTDATE:' ',UTTIME:' ',NIGHT:' ', OBJECT:' ',EXPTYPE:' ',EXPTIME:0.0,FILTER:' ',DISPERSER:' ',AIRMASS:0.0,SLITMASK:' ', BINNING:' ',COMMENT:' ',RA:0.0,Dec:0.0,EQUINOX:' ',DEWARORI:' '}
  info = replicate(info,ct)
  for i=0,n_elements(filelist)-1 do begin
   info[i].dir = dir
   pos = strpos(filelist[i],'.fits')
   info[i].frameno = strmid(filelist[i],pos-9,7) 
   hdr=headfits(filelist[i])
   info[i].UTdate = sxpar(hdr,'DATE-OBS')
   info[i].UTtime = sxpar(hdr,'UT-TIME')
   info[i].night= sxpar(hdr,'NIGHT')
   info[i].object = sxpar(hdr,'OBJECT')
   info[i].exptype = sxpar(hdr,'EXPTYPE')
   info[i].exptime = sxpar(hdr,'EXPTIME')
   info[i].filter  = sxpar(hdr,'filter')
   info[i].disperser = sxpar(hdr,'DISPERSR')
   info[i].airmass = sxpar(hdr,'AIRMASS')
   info[i].slitmask = sxpar(hdr,'SLITMASK')
   info[i].binning = sxpar(hdr,'BINNING')
   info[i].ra = sxpar(hdr,'RA-D')
   info[i].dec = sxpar(hdr,'DEC-D')
   info[i].equinox = sxpar(hdr,'EQUINOX')
   info[i].dewarori = sxpar(hdr,'DEWARORI')
  endfor
  status = 1
 endif else status = 0
 if status eq 1 then return,info $
    	 	else return, 0
end
