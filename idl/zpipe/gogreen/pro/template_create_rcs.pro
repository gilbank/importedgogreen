pro template_create_rcs,fileout

   cc = 299792.5

   eigendir =getenv('IDLSPEC1D_DIR')+'/templates/'
   deep_eigen = mrdfits(eigendir+'spDEEP.fits',0,hdr)

   lam0 = sxpar(hdr,'COEFF0')
   dlam = sxpar(hdr,'COEFF1')
   ncol = sxpar(hdr,'NAXIS1')
   logwave0 = lam0+findgen(ncol)*dlam
   wave0 = 10^(lam0+findgen(ncol)*dlam)


   vv = 200.

;   template0= convol(deep_eigen[*,0],kernel,total(kernel))
; Do not convolve the early-type template. The coadd spectrum from Eisenstein
; already has a ~315 km/s velocity dispersion (galaxy & instrument combined).
; The number is very approximate.
   template0 = deep_eigen[*,0]

   dloglam = 1.0e-4
   nspec = 5800
   logwave1 = lam0+findgen(nspec)*dloglam
   
   newtempl0 = interpol(template0,logwave0,logwave1)

;Below make a young population template.
   readcol,'/home/yan/models/templates/cst1_01g_07g.spec',wave,youngpop
   airtovac,wave
   logwave2 = alog10(wave)
   spec2 = interpol(youngpop,logwave2,logwave1)
   v_bc03 = 90.0
   vdiff = sqrt(vv*vv-v_bc03*v_bc03)

   width = ceil(alog10(1+vdiff/cc)/dloglam)
   kernel = fltarr(6*width+1)
   xx = findgen(6*width+1)-3*width
   kernel = exp(-xx*xx/(2*width*width))
   newtempl2 = convol(spec2,kernel,total(kernel))
   newtempl2 = newtempl2*10^logwave1/mean(10^logwave1) ; Convert the units to photon-counts/AA.

stop 
; Add in a black body spectrum   
   hcokt = 6.626e-34*3.e8/1.38e-23/3.e4  ; hc/kT for B0 star whose surface T is 30000K.
   bb = 1.e19/(10^(4*logwave1))/(exp(hcokt*1.e10/10^logwave1)-1) ; In units of photon-count/AA.
   newtempl2 = newtempl2+bb/max(bb)*max(newtempl2)*1.5

;   newtempl2 = interpol(template2,logwave0,logwave1)

   inputfile = GETENV('RCS_TEMPLATES_DIR')+ '/RCSlinelist1.dat'
   input2 = GETENV('RCS_TEMPLATES_DIR')+ '/RCSlinelist2.dat'
   input3 = GETENV('RCS_TEMPLATES_DIR')+ '/RCSlinelist3.dat'
   

;   lam0 = lam0-2.5*dloglam ;adjust c0 for expansion

   lambda0 = 10.^lam0 ;initial wavelength, including expansion of scale
; and presumption that SDSS spectra are pointing at central wavelengths
   lambda1 = 10.^(logwave1[nspec-1]+dloglam)  ;final wavelength
; create emission line template
   maketemplate,  inputfile, 'junk.fits', minl=lambda0, maxl=lambda1, $
         logres=dloglam, vdisp=200., template=emitemplate, header=headf

   maketemplate,  input2, 'junk.fits', minl=lambda0, maxl=lambda1, $
         logres=dloglam, vdisp=200., template=emitemplate2, header=headf
   maketemplate,  input3, 'junk.fits', minl=lambda0, maxl=lambda1, $
         logres=dloglam, vdisp=200., template=emitemplate3, header=headf
   
   maketemplate,  inputfile, 'junk.fits', minl=lambda0, maxl=lambda1, $
         logres=dloglam, vdisp=0., template=vdisptemplate, header=headf

   newtempl0 = newtempl0/mean(newtempl0) ;normalize
   newtempl2 = newtempl2/mean(newtempl2)
stop
   emitemplate = emitemplate*10^logwave1/mean(10^logwave1) ; Convert the units to photon-counts/AA. 
   emitemplate2 = emitemplate2*10^logwave1/mean(10^logwave1) ; Convert the units to photon-counts/AA. 
   emitemplate2 = emitemplate3*10^logwave1/mean(10^logwave1) ; Convert the units to photon-counts/AA. 

   result = [[newtempl0], [emitemplate], [newtempl2],[emitemplate2/9.0],[emitemplate3/10.0]]
   sxaddpar,headf,'UNITS','photon-counts',after='COEFF1'
   
   writefits, fileout, result, headf

   writefits,'Vdisp.fits',vdisptemplate,headf
    
   stop
end
