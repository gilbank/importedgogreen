pro brisky_template

   cc = 299792.5

   dir=getenv('GOGREEN_TEMPLATES_DIR')
   templates = mrdfits(dir+'spRCS.fits',0,hdr)
   lam0 = sxpar(hdr,'COEFF0')
   dloglam = sxpar(hdr,'COEFF1')
   ncol = sxpar(hdr,'NAXIS1')
   logwave0 = lam0+findgen(ncol)*dloglam
   
   inputfile = concat_dir(GETENV('GOGREEN_SPEC1D'), 'etc')+ '/brisky.dat'

   lambda0 = 10.^lam0 ;initial wavelength, including expansion of scale
   lambda1 = 10.^logwave0[ncol-1]+dloglam  ;final wavelength
; create bright skyline template
   maketemplate,  inputfile, 'brisky.fits', minl=lambda0, maxl=lambda1, $
         logres=dloglam, vdisp=0., template=skytemplate, header=headf
   
;   sxaddpar,headf,'UNITS','photon-counts',after='COEFF1'
;   writefits, fileout, result, headf

;   writefits,'Vdisp.fits',vdisptemplate,headf
    
   stop
end
