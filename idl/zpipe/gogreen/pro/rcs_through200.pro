pro rcs_through200

   grismdir=getenv('GOGREEN_SPEC1D')+'/etc/'
   readcol,grismdir+'gris200.qe',lam200,th200
   readcol,grismdir+'gris150.qe',lam150,th150

   throughput=mrdfits(getenv('GOGREEN_TEMPLATES_DIR')+'/rcs_throughput.fits',1)
   plot,throughput[1].lambda,throughput[1].throughput
   newlam=4750+findgen(1327)*(7850-4750.)/1327.
   thruptz2 = interpol(throughput[1].throughput,throughput[1].lambda,newlam)

   grism150 = interpol(th150,lam150,newlam)
   grism200 = interpol(th200,lam200,newlam)

   newput = thruptz2/grism150*grism200
   oplot,newlam,newput
   tmp={lambda:newlam,throughput:newput}
   throughput=[throughput,tmp]
   mwrfits,throughput,getenv('GOGREEN_TEMPLATES_DIR')+'/rcs_throughput.fits',/create
   stop
end


