;+
; KEYWORDS: 
;   nocal -- If there is no flux calibration available, specify this keyword.
;            The default is to use the calibration file named 
;            'rcs_throughput.fits' in the directory GOGREEN_TEMPLATES_DIR. 
;   start -- the starting point in the object list. By default, it starts
;            from the zeroth slit.
;-


pro reduce1d_gogreen, cluster, maskname, file1d, $
                      doplot=doplot,debug=debug,start=start,nocal=nocal,nonrcs=nonrcs
  common plotcolors, black, red, green, blue, cyan, magenta, yellow, white

  d1dir = getenv('GOGREEN_D1DIR')
  file = findfile(file1d, count=nfiles) 
  if nfiles ne 1 then message, 'File not found!' else file = file[0]

  ss1d = mrdfits(file, 1, sp1d_hdr)
  date = sxpar(sp1d_hdr, 'DATE')
  airmass = sxpar(sp1d_hdr, 'AIRMASS')

;This part prepares for adjusting the velocity resolution of the templates.
;I decided not to use it because the object size combined with seeing disk 
;could be smaller than the slit width used in RCS spectroscopy.
;IF NOT keyword_set(nonrcs) THEN BEGIN ; RCS specific codes
;   D2_dir = getenv('RCS_D2DIR')
;   if NOT keyword_set(dir) then dir=D2_dir+'/'
;   allobs = mrdfits(D2_dir+'/allobs.fits',1)
;   n = where(strmatch(allobs.maskname, strmid(maskname,0,8)+'*',/fold_case), ct)
;   if ct gt 1 then begin 
;;    print,'Multiple Observation of this mask. Choose one!'
;;    stop
;;    When there are multiple, take the latest observations which should be best.
;     ss=sort(allobs[n].mjd)
;     n = n[ss[ct-1]]
;   endif
;   slitwidth = allobs[n].slitwidth
;   if strmatch(allobs[n].disperser,'*200*') then dispersion=2.037 $  ;in Angstrom/pixel
;   else if strmatch(allobs[n].disperser,'*150*') then dispersion=2.630
;
;   fwhm = slitwidth/0.2*dispersion
;   centralwave = (min(ss1d[0].lambda)+max(ss1d[0].lambda))/2.0
;   vdisp = fwhm/centralwave/2.35*3.e5
;ENDIF


  fits_info, file, n_ext=next, /silent
  nspec = next - 1L
  ssinfo = mrdfits(file, 1, hdrinfo, /silent)
  
  logfile = getenv('GOGREEN_ZRESDIR') + '/log/reduce.' + $
            strcompress(cluster, /rem) + '.' + strcompress(maskname, /rem) + '.log'
  if not(keyword_set(debug)) then begin 
     splog, filename=logfile
     splog, 'Log file ' + logfile + ' opened ' + SYSTIME()
  endif
  
; write the IDL version and operating system to the log.
   splog, 'IDL version: ' + string(!version, format='(99(a," "))')
   spawn, 'uname -a', uname
   splog, 'UNAME: ' + uname[0]
; write the version numbers for the idlutils and spec1d packages to
; the log file.
;  splog, 'idlspec2d version ' + idlspec2d_version()
   splog, 'idlutils version ' + idlutils_version()

   if NOT keyword_set(start) then start =0

   eigenfile_gal = 'spRCS.fits'
   eigenfile_star = 'spEigenStarRCS.fits'
   eigenfile_qso = 'spEigenQSOrcs.fits'
   eigendir = getenv('GOGREEN_TEMPLATES_DIR')
   thrupt_file = 'rcs_throughput.fits'

   splog, 'Using galaxy eigenfile: ',eigenfile_gal
   splog, '      star eigenfile: ',eigenfile_star
   splog, '      QSO eigenfile: ',eigenfile_qso
   splog, 'and throughput correction in file',thrupt_file
   splog, 'all from directory:', eigendir
   splog, 'Mean airmass for this mask: ', airmass 

; select the stars eigen-file here to determine how many templates are in it.
   allfiles = findfile(djs_filepath(eigenfile_star, root_dir=eigendir), $
                       count=ct)
   if (ct eq 0) then $
     message, 'Unable to find EIGENFILE matching ' + eigenfile_star
   eigenfile_star = fileandpath(allfiles[ (reverse(sort(allfiles)))[0] ])
   shdr = headfits(djs_filepath(eigenfile_star, root_dir=eigendir))
   nstar = sxpar(shdr, 'NAXIS2') > 1
   subclass = strarr(nstar)      ;types of stars
   for istar=0, nstar-1 do $
     subclass[istar] = $
     strtrim( sxpar(shdr, 'NAME'+strtrim(string(istar),2)), 2)

  if NOT keyword_set(nocal) then begin
     if file_test(eigendir+thrupt_file) then fluxcal = mrdfits(eigendir+thrupt_file,1) $
     else begin 
        print,'Warning: Calibration file not found. No flux calibration will be applied!!!'
        nocal = 1
     endelse
  endif

; loop through the spectra...
  for kk=start,nspec-1 do begin
    splog,'Number: ',kk

    spec1d = mrdfits(file, kk+2L, hdrkk, /silent)

    if max(spec1d.lambda) eq 0.0 or max(spec1d.spec) eq 0.0 then continue
    badcol = where(abs(spec1d.spec) gt 5e3 and abs(spec1d.spec*spec1d.ivar) gt 1.0,nbadcol)
    if nbadcol gt 0 and nbadcol lt 100 then spec1d.ivar[badcol] = 0.0
    ; Reject bogus pixels with very small variance, i.e., huge ivar.
    ivarmed = median(spec1d.ivar,5)
    ivardiff = spec1d.ivar-ivarmed
    nonzero = where(ivardiff ne 0.0) 
    badivarcol = where(abs(ivardiff[nonzero]) gt 30*djsig(ivardiff[nonzero],maxiter=3),nbadivarcol)
    if nbadivarcol gt 0 then spec1d.ivar[nonzero[badivarcol]] = 0.0

    ;Reject pixels with NaN or Inf
    nanspec = where(finite(spec1d.spec) eq 0 or finite(spec1d.ivar) eq 0,sp_nan)
    if sp_nan gt 0 then begin 
	spec1d.spec[nanspec]=0.0
        spec1d.ivar[nanspec]=0.0
    endif

    remove_telluric,spec1d,airmass

    If NOT keyword_set(nocal) then begin 
      if max(spec1d.lambda) lt 7000 then filter=1 else $
          if spec1d.lambda[0] gt 6000 then filter =3 else filter =2
      throughput=interpol(fluxcal[filter-1].throughput,fluxcal[filter-1].lambda,spec1d.lambda) 
    ENDIF

    lambda = spec1d.lambda
    airtovac, lambda
    spec1d.lambda = float(lambda)

    skyspec = mrdfits(getenv('GOGREEN_TEMPLATES_DIR')+'/brisky.fits',0,skyhdr)
    skyloglam0 = sxpar(skyhdr,'COEFF0')
    skydloglam = sxpar(skyhdr,'COEFF1')
    skyloglam = skyloglam0+skydloglam*dindgen(n_elements(skyspec))
  
    sky = interpol(skyspec,skyloglam,alog10(spec1d.lambda))
    mask = sky gt 0.1
    if max(mask) eq 1B then spec1d.ivar[where(mask)] = 0.0

    zmin = 0.02
    zmax = max(spec1d.lambda)/3727. -1.  ;allows for variable end
    pspace = 2            ; was 1
    width = 5*pspace
    nfind = 5
    npoly = 0             ; 3
    columns = [0, 1, 2, 3, 4]     ;include absorption and emission templates in all cases

    subtractwin = 200.

; write info to log file.
    splog, 'Compute GALAXY redshifts: ', $
      ' ZMIN=', zmin, ' ZMAX=', zmax, ' PSPACE=', pspace
    splog, 'Smoothing window size for continuum subtraction: ',subtractwin

    ts0 = systime(1)
; determine the redshift of object by comparing to the galaxy
; templates.
    result = zfind(spec1d, /linear_lambda, eigenfile=eigenfile_gal, $
                   eigendir=eigendir, npoly=npoly, subtractwin=subtractwin,$
                   zmin=zmin, zmax=zmax, nfind=nfind, pspace=pspace, $
                   width=width, plottitle=plottitle, doplot=doplot, $
                   debug=debug, objflux=objflux, objivar=objivar, $
                   loglam=loglam,columns=columns, throughput=throughput, $
                   vdisp=vdisp ) 
    
    result.objname = spec1d.object
    result.slitname = string(spec1d.slitnum, format='(i3.0)')
    result.maskname = strcompress(cluster, /rem) + '.' + strcompress(maskname, /rem)
    result.class = ' '
    result.subclass = ' '
    result.comment = ' '
    result.date = date

    pspace = 1
    res_gal = zrefind(spec1d, objflux,objivar,hdr=hdr,pwidth=15, $
    		      pspace=pspace, width=3.*pspace, zold=result, $
		      loglam=loglam, plottitle=plottitle, $
		      doplot=doplot, debug=debug, columns=columns,$
                      throughput=throughput, vdisp=vdisp)
    
    result = res_gal
    delta_chisqr = (result[1].rchi2-result[0].rchi2)*.5* $
        (result[1].dof + result[0].dof)
    result[0].rchi2diff = result[1].rchi2-result[0].rchi2
    result.class = 'GALAXY'
    result.subclass = ' '

    splog, 'CPU time to compute Galaxy redshifts = ', systime(1)-ts0

; -------------------
; Find STAR redshifts - do all stellar templates with one call.

    npoly = 0             ; With only 1 eigen-template, fit 3 poly terms as well.
    zmin = -0.004         ; -1200 km/sec
    zmax = 0.004          ; +1200 km/sec
    pspace = 1
    nfind = 1
; check the system time.
    ts0 = systime(1)
            
;            splog, 'Compute STAR (' + subclass + ') redshifts:', $
;              ' ZMIN=', zmin, ' ZMAX=', zmax, ' PSPACE=', pspace
    res_star = zfind_star(spec1d, /linear_lambda, eigendir=eigendir, $
                          eigenfile=eigenfile_star, npoly=npoly, $
                          zmin=zmin, zmax=zmax, pspace=1, $
                          nfind=nfind, width=5*pspace, $
                          subclass=subclass, subtractwin=subtractwin, $
  			  doplot=0, debug=debug, throughput=throughput,$
			  vdisp=vdisp)
    res_star.objname = spec1d.object
    res_star.slitname = strcompress(spec1d.slitnum, /rem)
    res_star.maskname = strcompress(cluster, /rem) + '.' + strcompress(maskname, /rem)
    tmp=where(res_star.subclass eq '', count)
    if count ne 0 then res_star[tmp].subclass = ' '
    res_star.comment = ' '
    res_star.date = date

    res_star = res_star[sort(res_star.rchi2)] ;sort by rchi2 
          
    result = [result, res_star[0:2]] ; Append result of top 2 star

    splog, 'CPU time to compute STAR redshifts = ', systime(1)-ts0
 
   ;----------
   ; Find QSO redshifts


    npoly = 0
    zmin = 0.0033         ; +1000 km/sec
;   zmax = max(ss1d.lambda)/1215. -1  ; Max range we can expect to see
    zmax = 5.
    pspace = 3 
    nfind = 2             ;find best QSO candidate z
    plottitle = 'QSO Redshift'

    splog, 'Compute QSO redshifts:', $
           ' ZMIN=', zmin, ' ZMAX=', zmax, ' PSPACE=', pspace
    t0 = systime(1)
    res_qso = zfind_qso(spec1d, /linear_lambda, eigendir=eigendir, $
                        eigenfile = eigenfile_qso, npoly = npoly, zmin = zmin, $
                        zmax = zmax, pspace = pspace, loglam = loglam, $
                        nfind = nfind, width = 5*pspace, objflux = objflux, $
                        objivar=objivar, plottitle = plottitle, $
                        subtractwin=subtractwin, doplot = doplot, debug = debug, $
                        throughput=throughput, vdisp=vdisp)

    splog, 'CPU time to compute QSO redshifts = ', systime(1)-t0
    res_qso.class = 'AGN'
    res_qso.subclass = ' '
    res_qso.objname = spec1d.object
    res_qso.slitname = strcompress(spec1d.slitnum, /rem)
    res_qso.maskname = maskname
    res_qso.comment = ' '
    res_qso.date = date

;skip refitting QSO', as not necessary
;
;          splog, 'Locally re-fitting QSO redshifts'
;         t0 = systime(1)
;          res_qso = zrefind(ss1d, objflux, objivar, hdr=hdr, pwidth=91, $
;                            pspace=1, width=3.*pspace, zold=res_qso, $
;                            loglam=loglam, plottitle=plottitle, $
;                            doplot=doplot, debug=debug  )
;
;
;          splog, 'CPU time to re-fit QSO redshifts = ', systime(1)-t0


   ;----------
   ; Sort results for each object by ascending order in chi^2/DOF,
   ; but putting any results with zero degrees-of-freedom at the end.
   
    minvdiff = 1000.0     ; km/s
    cspeed = 2.99792458e5 
    rchi2 = result.rchi2
          
    isort = sort(rchi2 + (result.dof EQ 0)*max(rchi2))
    result = result[isort]

; append the QSO results here! -- so that they aren't sorted by chi2!
    result = [result, res_qso]

    result.s2n_peak = spec1d.s2n_peak

    nper = (size(result,/dimens))[0]
; Find the difference in reduced chi^2 between each result and the next
    rchi2 = result.rchi2
    for ia=0,nper-2 do begin
        inext = (where(abs(result[ia+1:nper-1].z - result[ia].z) GT $
                  minvdiff/cspeed AND result[ia+1:nper-1].dof GT 0))[0]
        if (inext ne -1) then $
          result[ia].rchi2diff = rchi2[ia+1+inext] - rchi2[ia]
    endfor

;    stop
    if n_elements(tot0) eq 0 then tot0=result[0] else tot0=[tot0,result[0]]
    if n_elements(tot) eq 0 then tot=result else tot=[tot,result]
   endfor

  mwrfits, tot0, getenv('GOGREEN_ZRESDIR')+'/zresult.'+ strcompress(cluster, /rem) + '.' + $
           strcompress(maskname, /rem) + '.fits',/create
  mwrfits, tot, getenv('GOGREEN_ZRESDIR')+'/zresult.' + strcompress(cluster, /rem) + '.' + $
           strcompress(maskname, /rem) + '.fits'

  splog, 'Successful completion of REDUCE_GOGREEN at ', systime()
  if (keyword_set(logfile)) then splog, /close


end
