; string version of tenv -- read in string sexigesimal ra, dec and
;                           return as decimals (vector)

function stenv, sra, sdec, ra, dec

len=n_elements(sra)
ra=dblarr(len) & dec=ra

for ii=0,len-1 do begin
    get_coords,coords,instring=sra[ii]+' '+sdec[ii]
    tra=coords[0] & tdec=coords[1]
    ra[ii]=tra & dec[ii]=tdec
endfor
    

return,transpose([[ra],[dec]])

end
